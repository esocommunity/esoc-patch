// RM script of Sweden
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Picks the map size
   int playerTiles = 11000;
   if (cNumberNonGaiaPlayers > 4)
      playerTiles = 10000;
   if (cNumberNonGaiaPlayers > 6)
      playerTiles = 8500;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses the side where teams start in team games
   int teamSide = rmRandInt(1,2);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("Great Lakes");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.10, 4, 0.4, 7.0);
   rmSetBaseTerrainMix("new_england_forest");
   rmTerrainInitialize("new_england\ground3_ne", 1.0);
   rmSetMapType("grass");
   rmSetMapType("water");
   rmSetMapType("greatPlains");
   

   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classIsland=rmDefineClass("island");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("nuggets");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 20.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 60.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 55.0);
   int avoidCow=rmCreateTypeDistanceConstraint("cows avoids cows", "cow", 55.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 6.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 60.0);

   // Area avoidance
   int islandConstraint=rmCreateClassDistanceConstraint("avoid island", classIsland, 20.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);

   // Trade route avoidance
   int avoidTradeRoute=rmCreateTradeRouteDistanceConstraint("trade route", 5.0);
   int avoidTradeSockets=rmCreateTypeDistanceConstraint("avoid trade sockets", "SocketTradeRoute", 5.0);

   // Water objects avoidance	
   int fishVsFish=rmCreateTypeDistanceConstraint("fish v fish", "FishSalmon", 20.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish land", "land", true, 5.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag vs land", "land", true, 15.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid same", "HomeCityWaterSpawnFlag", 25);


   // Text
   rmSetStatusText("",0.10);


   // Define and place V�nern to middle

   int lakeID=rmCreateArea("Lake");
   rmSetAreaWaterType(lakeID, "great lakes");
   rmSetAreaSize(lakeID, 0.12, 0.12);
   rmSetAreaCoherence(lakeID, 0.8);
   rmSetAreaLocation(lakeID, 0.5, 0.5);
   rmSetAreaBaseHeight(lakeID, 0.0);
   rmSetAreaMinBlobs(lakeID, 10);
   rmSetAreaMaxBlobs(lakeID, 20);
   rmSetAreaMinBlobDistance(lakeID, 10);
   rmSetAreaMaxBlobDistance(lakeID, 20);
   rmSetAreaSmoothDistance(lakeID, 10);
   rmBuildArea(lakeID);


   // Define and place island to lake IF it's KOTH or FFA game!
   if (rmGetIsFFA() || rmGetIsKOTH()) 
   {
      int islandID=rmCreateArea("Island");
      rmSetAreaTerrainType(islandID, "nwterritory\ground_grass3_nwt");
      rmSetAreaMix(islandID, "nwt_grass1");
      if (cNumberNonGaiaPlayers <= 3)
            rmSetAreaSize(islandID, 0.007, 0.007);
      if (cNumberNonGaiaPlayers >= 4)
            rmSetAreaSize(islandID, 0.006, 0.006);
      if (cNumberNonGaiaPlayers >= 6)
            rmSetAreaSize(islandID, 0.005, 0.005);
      rmSetAreaCoherence(islandID, 0.6);
      rmSetAreaLocation(islandID, 0.5, 0.5);
      rmSetAreaBaseHeight(islandID, 1.0);
      rmSetAreaMinBlobs(islandID, 4);
      rmSetAreaMaxBlobs(islandID, 5);
      rmSetAreaMinBlobDistance(islandID, 4);
      rmSetAreaMaxBlobDistance(islandID, 5);
      rmSetAreaSmoothDistance(islandID, 5);
      rmAddAreaToClass(islandID, classIsland);
      rmBuildArea(islandID);
   }


   // Text
   rmSetStatusText("",0.40);


   // Place players - in team and FFA games

   // Player placement if FFA (Free For All) - place players in circle
   if(cNumberTeams > 2)
   {
      rmSetTeamSpacingModifier(0.7);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);
   }

   // Player placement if teams - place teams in circle to apart from each other
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      if (teamSide == 1)
            rmSetPlacementSection(0.00, 0.25);
      else if (teamSide == 2)
            rmSetPlacementSection(0.50, 0.75);
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);

      rmSetPlacementTeam(1);
      if (teamSide == 1)
            rmSetPlacementSection(0.50, 0.75);
      else if (teamSide == 2)
            rmSetPlacementSection(0.00, 0.25);
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.50);
		

   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "TreeGreatLakes", 6, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 12);
   rmSetObjectDefMaxDistance(startAreaTreeID, 16);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startMooseID=rmCreateObjectDef("starting moose");
   rmAddObjectDefItem(startMooseID, "moose", 5, 4.0);
   rmSetObjectDefCreateHerd(startMooseID, true);
   rmSetObjectDefMinDistance(startMooseID, 14);
   rmSetObjectDefMaxDistance(startMooseID, 18);
   rmAddObjectDefConstraint(startMooseID, avoidStartResource);
   rmAddObjectDefConstraint(startMooseID, avoidImpassableLand);
   rmAddObjectDefConstraint(startMooseID, shortAvoidStartingUnits);

   int startSilverID=rmCreateObjectDef("player silver");
   rmAddObjectDefItem(startSilverID, "Mine", 1, 5.0);
   rmSetObjectDefMinDistance(startSilverID, 14);
   rmSetObjectDefMaxDistance(startSilverID, 18);
   rmAddObjectDefConstraint(startSilverID, avoidStartResource);
   rmAddObjectDefConstraint(startSilverID, avoidImpassableLand);
   rmAddObjectDefConstraint(startSilverID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startMooseID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startSilverID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
      rmAddClosestPointConstraint(flagVsFlag);
      rmAddClosestPointConstraint(flagLand);		
      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Text
   rmSetStatusText("",0.70);


   // Define and place trade routes

   // Create the trade routes
   int tradeRouteID = rmCreateTradeRoute();
   int socketID=rmCreateObjectDef("sockets to dock trade posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

   int tradeRoute2ID = rmCreateTradeRoute();
   int socket2ID=rmCreateObjectDef("sockets to dock trade posts 2");
   rmSetObjectDefTradeRouteID(socket2ID, tradeRoute2ID);

   // Define the sockets
   rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socketID, true);
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 12.0);

   rmAddObjectDefItem(socket2ID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socket2ID, true);
   rmSetObjectDefMinDistance(socket2ID, 0.0);
   rmSetObjectDefMaxDistance(socket2ID, 12.0);

   // Set the trade route waypoints
   rmAddTradeRouteWaypoint(tradeRouteID, 1.0, 0.3);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.7, 0.25, 8, 8);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.65, 0.0, 8, 8);
   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route");

   rmAddTradeRouteWaypoint(tradeRoute2ID, 0.3, 1.0);
   rmAddRandomTradeRouteWaypoints(tradeRoute2ID, 0.25, 0.7, 8, 8);
   rmAddRandomTradeRouteWaypoints(tradeRoute2ID, 0.0, 0.65, 8, 8);
   placedTradeRoute = rmBuildTradeRoute(tradeRoute2ID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route 2"); 
  
   // Place sockets to trade routes
   vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.1);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.9);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

   socketLoc = rmGetTradeRouteWayPoint(tradeRoute2ID, 0.1);
   rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRoute2ID, 0.9);
   rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc);


   // Define and place forests
	
   int numTries=500*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(650), rmAreaTilesToFraction(700));
      rmSetAreaForestType(forest, "great lakes forest");
      rmSetAreaForestDensity(forest, 1.0);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 1.0);
      rmSetAreaForestUnderbrush(forest, 0.3);
      rmSetAreaBaseHeight(forest, 1.0);
      rmSetAreaMinBlobs(forest, 1);
      rmSetAreaMaxBlobs(forest, 3);
      rmSetAreaMinBlobDistance(forest, 10);
      rmSetAreaMaxBlobDistance(forest, 15);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, islandConstraint);
      rmAddAreaConstraint(forest, avoidTradeRoute);
      rmAddAreaConstraint(forest, avoidTradeSockets);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 10 times in a row.  
            failCount++;
            if(failCount==10)
                        break;
      }
      else
                        failCount=0; 
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place animals
   
   // Define and place moose herds
   int mooseHerdID=rmCreateObjectDef("moose herd");
   rmAddObjectDefItem(mooseHerdID, "moose", rmRandInt(5,6), 8.0);
   rmSetObjectDefCreateHerd(mooseHerdID, true);
   rmSetObjectDefMinDistance(mooseHerdID, 0.0);
   rmSetObjectDefMaxDistance(mooseHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(mooseHerdID, classAnimals);
   rmAddObjectDefConstraint(mooseHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(mooseHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(mooseHerdID, animalConstraint);
   rmAddObjectDefConstraint(mooseHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(mooseHerdID, avoidTradeSockets);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(mooseHerdID, 0, 0.5, 0.5);

   // Define and place wolf herds
   int wolfHerdID=rmCreateObjectDef("wolf herd");
   rmAddObjectDefItem(wolfHerdID, "elk", rmRandInt(5,6), 8.0);
   rmSetObjectDefCreateHerd(wolfHerdID, true);
   rmSetObjectDefMinDistance(wolfHerdID, 0.0);
   rmSetObjectDefMaxDistance(wolfHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(wolfHerdID, classAnimals);
   rmAddObjectDefConstraint(wolfHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(wolfHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(wolfHerdID, animalConstraint);
   rmAddObjectDefConstraint(wolfHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(wolfHerdID, avoidTradeSockets);
   numTries=cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(wolfHerdID, 0, 0.5, 0.5);

   // Define and place fox herds
   int foxHerdID=rmCreateObjectDef("fox herd");
   rmAddObjectDefItem(foxHerdID, "deer", rmRandInt(5,6), 8.0);
   rmSetObjectDefMinDistance(foxHerdID, 0.0);
   rmSetObjectDefMaxDistance(foxHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(foxHerdID, classAnimals);
   rmAddObjectDefConstraint(foxHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(foxHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(foxHerdID, animalConstraint);
   rmAddObjectDefConstraint(foxHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(foxHerdID, avoidTradeSockets);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(foxHerdID, 0, 0.5, 0.5);


   // Define and place silver mines

   if (cNumberNonGaiaPlayers <= 5)
      int silverCount = 3*cNumberNonGaiaPlayers;
   else
      silverCount = 2*cNumberNonGaiaPlayers;

   for(i=0; < silverCount)
   {
      int silverID=rmCreateObjectDef("silver mine"+i);
      rmAddObjectDefItem(silverID, "mine", 1, 0.0);
      rmSetObjectDefMinDistance(silverID, 0.0);
      rmSetObjectDefMaxDistance(silverID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(silverID, classMines);
      rmAddObjectDefConstraint(silverID, avoidImpassableLand);
      rmAddObjectDefConstraint(silverID, playerConstraint);
      rmAddObjectDefConstraint(silverID, avoidMines);
      rmAddObjectDefConstraint(silverID, avoidTradeRoute);
      rmAddObjectDefConstraint(silverID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(silverID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.90);
	

   // Define and place fish

   int fishID=rmCreateObjectDef("fish");
   rmAddObjectDefItem(fishID, "FishSalmon", 1, 4.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.5));
   rmAddObjectDefConstraint(fishID, fishVsFish);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 5*cNumberNonGaiaPlayers);


   // Text
   rmSetStatusText("",0.99);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget2, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget3, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget4, avoidTradeSockets);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Define and place cow herds

   int cowID=rmCreateObjectDef("cow");
   rmAddObjectDefItem(cowID, "cow", 2, 4.0);
   rmSetObjectDefMinDistance(cowID, 0.0);
   rmSetObjectDefMaxDistance(cowID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(cowID, avoidCow);
   rmAddObjectDefConstraint(cowID, playerConstraint);
   rmAddObjectDefConstraint(cowID, avoidImpassableLand);
   rmAddObjectDefConstraint(cowID, avoidTradeRoute);
   rmAddObjectDefConstraint(cowID, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(cowID, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);


   // Place King's Hill if KOTH game to middle - stays on island we created before

   if(rmGetIsKOTH()) 
   {
      float xLoc = 0.5;
      float yLoc = 0.5;
      float walk = 0.01;

      ypKingsHillPlacer(xLoc, yLoc, walk, 0);
   }


   // Text
   rmSetStatusText("",1.0); 
}  
