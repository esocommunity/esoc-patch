// RM script of Dalmatia
// For K&B mod
// By AOE_Fan

// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Chooses the 1 native, always Avars

   // Native 1
   rmSetSubCiv(0, "Comanche");


   // Picks the map size
   int playerTiles = 20000;
   if (cNumberNonGaiaPlayers == 8)
	playerTiles = 14000;
   else if (cNumberNonGaiaPlayers == 7)
	playerTiles = 15000;
   else if (cNumberNonGaiaPlayers == 6)
	playerTiles = 16000;
   else if (cNumberNonGaiaPlayers == 5)
	playerTiles = 17000;
   else if (cNumberNonGaiaPlayers == 4)
	playerTiles = 18000;
   else if (cNumberNonGaiaPlayers == 3)
	playerTiles = 19000;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("Caribbean");


   // Picks default terrain and map type
   rmSetMapElevationParameters(cElevTurbulence, 0.10, 4, 0.4, 8.0);
   rmSetSeaType("great lakes");
   rmTerrainInitialize("water");
   rmSetWindMagnitude(15.0);
   rmSetMapType("grass");
   rmSetMapType("water");
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   int classIsland=rmDefineClass("island");
   int classLand=rmDefineClass("land");
   int classSockets=rmDefineClass("sockets");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("nuggets");
   rmDefineClass("natives");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 30.0);
   int longForestConstraint=rmCreateClassDistanceConstraint("forest vs. forest long", rmClassID("classForest"), 40.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 60.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 55.0);
   int longAnimalConstraint=rmCreateClassDistanceConstraint("long avoid all animals", classAnimals, 70.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 6.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 40.0);

   // Area constraints
   int islandConstraint=rmCreateClassDistanceConstraint("stay away from islands", classIsland, 30.0);
   int shortLandConstraint=rmCreateClassDistanceConstraint("short stay away from land", classLand, 10.0);
   int landConstraint=rmCreateClassDistanceConstraint("stay away from land", classLand, 20.0);
   int longLandConstraint=rmCreateClassDistanceConstraint("long stay away from land", classLand, 25.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 6.0);

   // Trade route avoidance
   int avoidTradeRoute=rmCreateTradeRouteDistanceConstraint("trade route", 8.0);
   int avoidTradeSockets=rmCreateTypeDistanceConstraint("avoid trade sockets", "SPCDrydock", 7.0);
   int socketsAvoidTradeSockets=rmCreateClassDistanceConstraint("sockets avoid trade sockets", classSockets, 60.0);

   // Native avoidance
   int avoidNatives=rmCreateClassDistanceConstraint("things avoids natives", rmClassID("natives"), 7.0);

   // Water objects avoidance	
   int fishVsFish=rmCreateTypeDistanceConstraint("fish v fish", "FishSardine", 20.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish land", "land", true, 2.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag vs land", "land", true, 10.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid same", "HomeCityWaterSpawnFlag", 25.0);


   // Text
   rmSetStatusText("",0.10);


   // Define and place land to north-east

   int landID=rmCreateArea("Land");
   rmSetAreaMix(landID, "nwt_grass1");
   rmSetAreaSize(landID, 0.25, 0.25);
   rmSetAreaCoherence(landID, 0.5);
   rmSetAreaLocation(landID, 1.0, 0.5);
   rmAddAreaInfluenceSegment(landID, 1.0, 0.0, 1.0, 1.0); 
   rmSetAreaBaseHeight(landID, 1.0);
   rmSetAreaSmoothDistance(landID, 20);
   rmSetAreaHeightBlend(landID, 2);
   rmSetAreaObeyWorldCircleConstraint(landID, false);
   rmAddAreaToClass(landID, classLand);
   rmSetAreaElevationType(landID, cElevTurbulence);
   rmSetAreaElevationVariation(landID, 11.0);
   rmSetAreaElevationMinFrequency(landID, 0.06);
   rmSetAreaElevationOctaves(landID, 4);
   rmSetAreaElevationPersistence(landID, 0.7);
   rmSetAreaElevationNoiseBias(landID, 1);
   rmBuildArea(landID);


   // Define and place water trade route

   // Create the water trade route
   int tradeRouteID = rmCreateTradeRoute();
   int socketID=rmCreateObjectDef("sockets to dock trade posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

   // Define the sockets
   rmAddObjectDefItem(socketID, "SPCDrydock", 1, 5.0);
   rmSetObjectDefAllowOverlap(socketID, true);
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 300.0);
   rmAddObjectDefToClass(socketID, classSockets);
   rmAddObjectDefConstraint(socketID, avoidImpassableLand);
   rmAddObjectDefConstraint(socketID, avoidAll);
   rmAddObjectDefConstraint(socketID, socketsAvoidTradeSockets);
   rmAddObjectDefConstraint(socketID, longLandConstraint);

   // Set the water trade route waypoints
   rmAddTradeRouteWaypoint(tradeRouteID, 0.4, 1.0);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.4, 0.0, 45, 45);
   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route");


   // Define and place random islands

   float A = rmRandFloat(0.09, 0.56);
   float B = rmRandFloat(0.09, 0.91);
   float C = rmRandFloat(0.09, 0.56);
   float D = rmRandFloat(0.09, 0.91);
   float E = rmRandFloat(0.09, 0.56);
   float F = rmRandFloat(0.09, 0.91);
   float G = rmRandFloat(0.09, 0.56);
   float H = rmRandFloat(0.09, 0.91);
   float I = rmRandFloat(0.09, 0.56);
   float J = rmRandFloat(0.09, 0.91);
   float K = rmRandFloat(0.09, 0.56);
   float L = rmRandFloat(0.09, 0.91);

   float islandFraction=rmAreaTilesToFraction(1500);

   int island=rmCreateArea("random island");
   rmSetAreaSize(island, islandFraction, islandFraction);
   rmSetAreaLocation(island, A, B);
   rmAddAreaInfluenceSegment(island, A, B-0.08, A, B+0.08);
   rmSetAreaMix(island, "nwt_grass1");
   rmSetAreaBaseHeight(island, 1);
   rmSetAreaCoherence(island, 0.6);
   rmAddAreaConstraint(island, islandConstraint);
   rmAddAreaConstraint(island, avoidTradeRoute);
   rmAddAreaToClass(island, classIsland);
   rmSetAreaElevationType(island, cElevTurbulence);
   rmSetAreaElevationVariation(island, 5.0);
   rmSetAreaElevationMinFrequency(island, 0.10);
   rmSetAreaElevationOctaves(island, 3);
   rmSetAreaElevationPersistence(island, 0.2);
   rmSetAreaElevationNoiseBias(island, 1);
   rmSetAreaObeyWorldCircleConstraint(island, false);

   int islandA=rmCreateArea("random island A");
   rmSetAreaSize(islandA, islandFraction, islandFraction);
   rmSetAreaLocation(islandA, C, D);
   rmAddAreaInfluenceSegment(islandA, C, D-0.08, C, D+0.08);
   rmSetAreaMix(islandA, "nwt_grass1");
   rmSetAreaBaseHeight(islandA, 1);
   rmSetAreaCoherence(islandA, 0.6);
   rmAddAreaConstraint(islandA, islandConstraint);
   rmAddAreaConstraint(islandA, avoidTradeRoute);
   rmAddAreaToClass(islandA, classIsland);
   rmSetAreaElevationType(islandA, cElevTurbulence);
   rmSetAreaElevationVariation(islandA, 5.0);
   rmSetAreaElevationMinFrequency(islandA, 0.10);
   rmSetAreaElevationOctaves(islandA, 3);
   rmSetAreaElevationPersistence(islandA, 0.2);
   rmSetAreaElevationNoiseBias(islandA, 1);
   rmSetAreaObeyWorldCircleConstraint(islandA, false);

   int islandB=rmCreateArea("random island B");
   rmSetAreaSize(islandB, islandFraction, islandFraction);
   rmSetAreaLocation(islandB, E, F);
   rmAddAreaInfluenceSegment(islandB, E, F-0.08, E, F+0.08);
   rmSetAreaMix(islandB, "nwt_grass1");
   rmSetAreaBaseHeight(islandB, 1);
   rmSetAreaCoherence(islandB, 0.6);
   rmAddAreaConstraint(islandB, islandConstraint);
   rmAddAreaConstraint(islandB, avoidTradeRoute);
   rmAddAreaToClass(islandB, classIsland);
   rmSetAreaElevationType(islandB, cElevTurbulence);
   rmSetAreaElevationVariation(islandB, 5.0);
   rmSetAreaElevationMinFrequency(islandB, 0.10);
   rmSetAreaElevationOctaves(islandB, 3);
   rmSetAreaElevationPersistence(islandB, 0.2);
   rmSetAreaElevationNoiseBias(islandB, 1);
   rmSetAreaObeyWorldCircleConstraint(islandB, false);

   int islandC=rmCreateArea("random island C");
   rmSetAreaSize(islandC, islandFraction, islandFraction);
   rmSetAreaLocation(islandC, G, H);
   rmAddAreaInfluenceSegment(islandC, G, H-0.08, G, H+0.08);
   rmSetAreaMix(islandC, "nwt_grass1");
   rmSetAreaBaseHeight(islandC, 1);
   rmSetAreaCoherence(islandC, 0.6);
   rmAddAreaConstraint(islandC, islandConstraint);
   rmAddAreaConstraint(islandC, avoidTradeRoute);
   rmAddAreaToClass(islandC, classIsland);
   rmSetAreaElevationType(islandC, cElevTurbulence);
   rmSetAreaElevationVariation(islandC, 5.0);
   rmSetAreaElevationMinFrequency(islandC, 0.10);
   rmSetAreaElevationOctaves(islandC, 3);
   rmSetAreaElevationPersistence(islandC, 0.2);
   rmSetAreaElevationNoiseBias(islandC, 1);
   rmSetAreaObeyWorldCircleConstraint(islandC, false);

   int islandD=rmCreateArea("random island D");
   rmSetAreaSize(islandD, islandFraction, islandFraction);
   rmSetAreaLocation(islandD, I, J);
   rmAddAreaInfluenceSegment(islandD, I, J-0.08, I, J+0.08);
   rmSetAreaMix(islandD, "nwt_grass1");
   rmSetAreaBaseHeight(islandD, 1);
   rmSetAreaCoherence(islandD, 0.6);
   rmAddAreaConstraint(islandD, islandConstraint);
   rmAddAreaConstraint(islandD, avoidTradeRoute);
   rmAddAreaToClass(islandD, classIsland);
   rmSetAreaElevationType(islandD, cElevTurbulence);
   rmSetAreaElevationVariation(islandD, 5.0);
   rmSetAreaElevationMinFrequency(islandD, 0.10);
   rmSetAreaElevationOctaves(islandD, 3);
   rmSetAreaElevationPersistence(islandD, 0.2);
   rmSetAreaElevationNoiseBias(islandD, 1);
   rmSetAreaObeyWorldCircleConstraint(islandD, false);

   int islandE=rmCreateArea("random island E");
   rmSetAreaSize(islandE, islandFraction, islandFraction);
   rmSetAreaLocation(islandE, K, L);
   rmAddAreaInfluenceSegment(islandE, K, L-0.08, K, L+0.08);
   rmSetAreaMix(islandE, "nwt_grass1");
   rmSetAreaBaseHeight(islandE, 1);
   rmSetAreaCoherence(islandE, 0.6);
   rmAddAreaConstraint(islandE, islandConstraint);
   rmAddAreaConstraint(islandE, avoidTradeRoute);
   rmAddAreaToClass(islandE, classIsland);
   rmSetAreaElevationType(islandE, cElevTurbulence);
   rmSetAreaElevationVariation(islandE, 5.0);
   rmSetAreaElevationMinFrequency(islandE, 0.10);
   rmSetAreaElevationOctaves(islandE, 3);
   rmSetAreaElevationPersistence(islandE, 0.2);
   rmSetAreaElevationNoiseBias(islandE, 1);
   rmSetAreaObeyWorldCircleConstraint(islandE, false);


   // Build the areas.
   rmBuildAllAreas();


   // Define and place random islands

   float islandFraction2=rmAreaTilesToFraction(2500);
   for(i=1; <30)
   {
      int islands=rmCreateArea("random island"+i);
      rmSetAreaSize(islands, islandFraction2, islandFraction2);
      rmAddAreaToClass(islands, classIsland);
      rmSetAreaMix(islands, "nwt_grass1");
      rmSetAreaBaseHeight(islands, 1);
      rmAddAreaConstraint(islands, islandConstraint); 
      rmAddAreaConstraint(islands, landConstraint);
      rmAddAreaConstraint(islands, avoidTradeRoute);
      rmSetAreaCoherence(islands, 0.6);
      rmSetAreaElevationType(islands, cElevTurbulence);
      rmSetAreaElevationVariation(islands, 5.0);
      rmSetAreaElevationMinFrequency(islands, 0.10);
      rmSetAreaElevationOctaves(islands, 3);
      rmSetAreaElevationPersistence(islands, 0.2);
      rmSetAreaElevationNoiseBias(islands, 1);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.40);


   // Place players - in team and FFA games

   // Player placement if FFA (Free For All) - place players in line
   if(cNumberTeams > 2)
      rmPlacePlayersLine(0.8, 0.85, 0.8, 0.15, 0, 0);

   // Player placement if teams - place teams in lines
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      rmPlacePlayersLine(0.8, 0.85, 0.8, 0.6, 0, 0);

      rmSetPlacementTeam(1);
      rmPlacePlayersLine(0.8, 0.15, 0.8, 0.4, 0, 0);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.50);
		

   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "TreeTexasDirt", 4, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 12);
   rmSetObjectDefMaxDistance(startAreaTreeID, 16);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startDeerID=rmCreateObjectDef("starting deer");
   rmAddObjectDefItem(startDeerID, "deer", 3, 4.0);
   rmSetObjectDefCreateHerd(startDeerID, true);
   rmSetObjectDefMinDistance(startDeerID, 14);
   rmSetObjectDefMaxDistance(startDeerID, 18);
   rmAddObjectDefConstraint(startDeerID, avoidStartResource);
   rmAddObjectDefConstraint(startDeerID, avoidImpassableLand);
   rmAddObjectDefConstraint(startDeerID, shortAvoidStartingUnits);

   int startCoalID=rmCreateObjectDef("player coal");
   rmAddObjectDefItem(startCoalID, "mine", 1, 5.0);
   rmSetObjectDefMinDistance(startCoalID, 14);
   rmSetObjectDefMaxDistance(startCoalID, 18);
   rmAddObjectDefConstraint(startCoalID, avoidStartResource);
   rmAddObjectDefConstraint(startCoalID, avoidImpassableLand);
   rmAddObjectDefConstraint(startCoalID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startDeerID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startCoalID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
      rmAddClosestPointConstraint(flagVsFlag);
      rmAddClosestPointConstraint(flagLand);		
      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Text
   rmSetStatusText("",0.70);


   // Define and place the 1 native to land, always Avars

   int nativeVillageType = rmRandInt(1,5);
   int nativeVillageID = rmCreateGrouping("avars village", "native comanche village "+nativeVillageType);
   rmSetGroupingMinDistance(nativeVillageID, 0.0);
   rmSetGroupingMaxDistance(nativeVillageID, 10.0);
   rmAddGroupingToClass(nativeVillageID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillageID, 0, 0.95, 0.5);


   // Define and place forests

   // Define and place forests for land area
   int numTries=8*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i, rmAreaID("Land"));
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(100), rmAreaTilesToFraction(150));
      rmSetAreaForestType(forest, "texas forest dirt");
      rmSetAreaForestDensity(forest, 0.5);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.5);
      rmSetAreaForestUnderbrush(forest, 0.0);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, longForestConstraint);
      rmAddAreaConstraint(forest, avoidTradeSockets);
      rmAddAreaConstraint(forest, avoidNatives);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.  
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }

   // Define and place forests to islands
   numTries=30*cNumberNonGaiaPlayers;
   failCount=0;
   for (i=0; <numTries)
   {   
      int forestIsland=rmCreateArea("forest island"+i);
      rmSetAreaWarnFailure(forestIsland, false);
      rmSetAreaSize(forestIsland, rmAreaTilesToFraction(100), rmAreaTilesToFraction(150));
      rmSetAreaForestType(forestIsland, "texas forest");
      rmSetAreaForestDensity(forestIsland, 0.8);
      rmAddAreaToClass(forestIsland, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forestIsland, 0.3);
      rmSetAreaForestUnderbrush(forestIsland, 0.0);
      rmSetAreaCoherence(forestIsland, 0.4);
      rmAddAreaConstraint(forestIsland, avoidImpassableLand);
      rmAddAreaConstraint(forestIsland, somePlayerConstraint);
      rmAddAreaConstraint(forestIsland, forestConstraint);
      rmAddAreaConstraint(forestIsland, shortLandConstraint);
      rmAddAreaConstraint(forestIsland, avoidTradeSockets);
      if(rmBuildArea(forestIsland)==false)
      {
            // Stop trying once we fail 10 times in a row.  
            failCount++;
            if(failCount==10)
                        break;
      }
      else
                        failCount=0; 
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place animals

   // Define and place deer herds to land
   int deerHerdID=rmCreateObjectDef("deer herd");
   rmAddObjectDefItem(deerHerdID, "deer", rmRandInt(6,7), 8.0);
   rmSetObjectDefCreateHerd(deerHerdID, true);
   rmSetObjectDefMinDistance(deerHerdID, 0.0);
   rmSetObjectDefMaxDistance(deerHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(deerHerdID, classAnimals);
   rmAddObjectDefConstraint(deerHerdID, mediumPlayerConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(deerHerdID, animalConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidTradeSockets);
   rmAddObjectDefConstraint(deerHerdID, avoidNatives);
   if (cNumberNonGaiaPlayers <= 3)
      numTries=4;
   else
      numTries=6;
   for (i=0; <numTries)
      rmPlaceObjectDefInArea(deerHerdID, 0, rmAreaID("Land"));

   // Define and place berry bushes to islands
   int berryBushID=rmCreateObjectDef("berry bush");
   rmAddObjectDefItem(berryBushID, "berrybush", 4, 6.0);
   rmSetObjectDefMinDistance(berryBushID, 0.0);
   rmSetObjectDefMaxDistance(berryBushID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(berryBushID, classAnimals);
   rmAddObjectDefConstraint(berryBushID, somePlayerConstraint);
   rmAddObjectDefConstraint(berryBushID, avoidImpassableLand);
   rmAddObjectDefConstraint(berryBushID, longAnimalConstraint);
   rmAddObjectDefConstraint(berryBushID, shortLandConstraint);
   rmAddObjectDefConstraint(berryBushID, avoidTradeSockets);
   if (cNumberNonGaiaPlayers <= 6)
      numTries=2*cNumberNonGaiaPlayers;
   else
      numTries=12;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(berryBushID, 0, 0.5, 0.5);


   // Define and place coal mines

   // Mines for land area
   if (cNumberNonGaiaPlayers <= 4)
      int coalCount = 6;
   else
      coalCount = 10;

   for(i=0; < coalCount)
   {
      int coalID=rmCreateObjectDef("coal mine"+i);
      rmAddObjectDefItem(coalID, "mine", 1, 0.0);
      rmSetObjectDefMinDistance(coalID, 0.0);
      rmSetObjectDefMaxDistance(coalID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(coalID, classMines);
      rmAddObjectDefConstraint(coalID, avoidImpassableLand);
      rmAddObjectDefConstraint(coalID, playerConstraint);
      rmAddObjectDefConstraint(coalID, avoidMines);
      rmAddObjectDefConstraint(coalID, avoidTradeSockets);
      rmAddObjectDefConstraint(coalID, avoidNatives);
      rmPlaceObjectDefInArea(coalID, 0, rmAreaID("Land"));
   }

   // Mines for islands
   int coalCount2 = 15;

   for(i=0; < coalCount2)
   {
      int coalIslandID=rmCreateObjectDef("coal mine island"+i);
      rmAddObjectDefItem(coalIslandID, "mine", 1, 0.0);
      rmSetObjectDefMinDistance(coalIslandID, 0.0);
      rmSetObjectDefMaxDistance(coalIslandID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(coalIslandID, classMines);
      rmAddObjectDefConstraint(coalIslandID, avoidImpassableLand);
      rmAddObjectDefConstraint(coalIslandID, playerConstraint);
      rmAddObjectDefConstraint(coalIslandID, avoidMines);
      rmAddObjectDefConstraint(coalIslandID, shortLandConstraint);
      rmAddObjectDefConstraint(coalIslandID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(coalIslandID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.90);
	

   // Define and place fish

   int fishID=rmCreateObjectDef("fish");
   rmAddObjectDefItem(fishID, "FishSardine", 1, 4.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.5));
   rmAddObjectDefConstraint(fishID, fishVsFish);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 10*cNumberNonGaiaPlayers);


   // Text
   rmSetStatusText("",0.99);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 15.0);
   rmSetObjectDefMaxDistance(nugget1, 20.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures - everywhere
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget2, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);

   // Hard treasures - to islands only
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, shortLandConstraint);
   rmAddObjectDefConstraint(nugget3, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget3, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, 1.5*cNumberNonGaiaPlayers);

   // Very hard treasures - to islands only
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, shortLandConstraint);
   rmAddObjectDefConstraint(nugget4, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget4, avoidNatives);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Place sockets to trade route

   vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.1);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.25);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.4);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.55);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.7);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.9);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);


   // Text
   rmSetStatusText("",1.0); 
}  
