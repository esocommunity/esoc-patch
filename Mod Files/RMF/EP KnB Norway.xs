// RM script of Norway
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Chooses the 2 natives, always Jutes

   // Native 1
   rmSetSubCiv(0, "Comanche");

   // Native 2
   rmSetSubCiv(1, "Comanche");


   // Picks the map size
   int playerTiles = 20000;
   if (cNumberNonGaiaPlayers == 8)
	playerTiles = 14000;
   else if (cNumberNonGaiaPlayers == 7)
	playerTiles = 15000;
   else if (cNumberNonGaiaPlayers == 6)
	playerTiles = 16000;
   else if (cNumberNonGaiaPlayers == 5)
	playerTiles = 17000;
   else if (cNumberNonGaiaPlayers == 4)
	playerTiles = 18000;
   else if (cNumberNonGaiaPlayers == 3)
	playerTiles = 19000;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("london sunset");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.10, 4, 0.4, 5.0);
   rmTerrainInitialize("yukon\ground1_yuk", 1.0);
   rmEnableLocalWater(false);
   rmSetWindMagnitude(0.0);
   rmSetMapType("snow");
   rmSetMapType("water");
   rmSetMapType("greatPlains");
   

   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   int classCliff=rmDefineClass("cliff");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("nuggets");
   rmDefineClass("natives");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 40.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 60.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 95.0);
   int avoidCow=rmCreateTypeDistanceConstraint("avoid cows", "cow", 65.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 5.0);

   // Area avoidance
   int cliffConstraint=rmCreateClassDistanceConstraint("avoid cliffs", classCliff, 5.0);
   int cliffPlayerConstraint=rmCreateClassDistanceConstraint("cliffs avoid player", classPlayer, 25.0);
   int shortCliffPlayerConstraint=rmCreateClassDistanceConstraint("cliffs avoid player short", classPlayer, 15.0);
   int shortestCliffPlayerConstraint=rmCreateClassDistanceConstraint("cliffs avoid player shortest", classPlayer, 5.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 40.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);

   // Trade route avoidance
   int avoidTradeRoute=rmCreateTradeRouteDistanceConstraint("trade route", 5.0);
   int avoidTradeSockets=rmCreateTypeDistanceConstraint("avoid trade sockets", "SocketTradeRoute", 5.0);

   // Native avoidance
   int avoidNatives=rmCreateClassDistanceConstraint("things avoids natives", rmClassID("natives"), 5.0);

   // Water objects avoidance
   int fishVsFish=rmCreateTypeDistanceConstraint("fish v fish", "FishSalmon", 30.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish land", "land", true, 5.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag vs land", "land", true, 15.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid same", "HomeCityWaterSpawnFlag", 25);


   // Text
   rmSetStatusText("",0.10);


   // Define and place water areas

   // Define and place the base of fjord
   int fjordBaseID=rmCreateArea("fjord base");
   rmSetAreaWaterType(fjordBaseID, "great lakes ice");
   rmSetAreaSize(fjordBaseID, 0.09, 0.09);
   rmSetAreaCoherence(fjordBaseID, 0.8);
   rmSetAreaLocation(fjordBaseID, 0.0, 0.5);
   rmSetAreaBaseHeight(fjordBaseID, 0.0);
   rmSetAreaObeyWorldCircleConstraint(fjordBaseID, false);
   rmSetAreaMinBlobs(fjordBaseID, 1);
   rmSetAreaMaxBlobs(fjordBaseID, 1);
   rmSetAreaMinBlobDistance(fjordBaseID, 1);
   rmSetAreaMaxBlobDistance(fjordBaseID, 1);
   rmSetAreaSmoothDistance(fjordBaseID, 10);
   rmBuildArea(fjordBaseID);

   // Define and place fjord 2
   int fjord2ID=rmCreateArea("fjord 2");
   rmSetAreaWaterType(fjord2ID, "great lakes ice");
   rmSetAreaSize(fjord2ID, 0.1, 0.1);
   rmSetAreaCoherence(fjord2ID, 0.9);
   rmSetAreaLocation(fjord2ID, 0.3, 0.5);
   rmAddAreaInfluenceSegment(fjord2ID, 0.2, 0.5, 0.4, 0.5);
   rmSetAreaBaseHeight(fjord2ID, 0.0);
   rmSetAreaObeyWorldCircleConstraint(fjord2ID, false);
   rmSetAreaMinBlobs(fjord2ID, 20);
   rmSetAreaMaxBlobs(fjord2ID, 20);
   rmSetAreaMinBlobDistance(fjord2ID, 20);
   rmSetAreaMaxBlobDistance(fjord2ID, 20);
   rmSetAreaSmoothDistance(fjord2ID, 3);
   rmBuildArea(fjord2ID);

   // Define and place fjord 3
   int fjord3ID=rmCreateArea("fjord 3");
   rmSetAreaWaterType(fjord3ID, "great lakes ice");
   rmSetAreaSize(fjord3ID, 0.06, 0.06);
   rmSetAreaCoherence(fjord3ID, 0.8);
   rmSetAreaLocation(fjord3ID, 0.5, 0.5);
   rmAddAreaInfluenceSegment(fjord3ID, 0.4, 0.5, 0.7, 0.5);
   rmSetAreaBaseHeight(fjord3ID, 0.0);
   rmSetAreaObeyWorldCircleConstraint(fjord3ID, false);
   rmSetAreaMinBlobs(fjord3ID, 20);
   rmSetAreaMaxBlobs(fjord3ID, 20);
   rmSetAreaMinBlobDistance(fjord3ID, 20);
   rmSetAreaMaxBlobDistance(fjord3ID, 20);
   rmSetAreaSmoothDistance(fjord3ID, 3);
   rmBuildArea(fjord3ID);


   // Text
   rmSetStatusText("",0.20);


   // Player placement in team and FFA games

   // Place players in team games - in lines
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      rmPlacePlayersLine(0.35, 0.85, 0.65, 0.8, 5.0, 15.0);

      rmSetPlacementTeam(1);
      rmPlacePlayersLine(0.35, 0.15, 0.65, 0.2, 5.0, 15.0);
   }

   // Place players in FFA games - in circle
   if(cNumberTeams > 2)
   {
      rmSetPlacementSection(0.90, 0.60);
      rmSetTeamSpacingModifier(0.7);
      if (cNumberNonGaiaPlayers <= 4)
            rmPlacePlayersCircular(0.40, 0.40, 0.0);
      if (cNumberNonGaiaPlayers == 5)
            rmPlacePlayersCircular(0.35, 0.35, 0.0);
      if (cNumberNonGaiaPlayers >= 6)
            rmPlacePlayersCircular(0.33, 0.33, 0.0);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(1200);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaTerrainType(id, "rockies\ground5_roc");
      rmSetAreaMinBlobs(id, 5);
      rmSetAreaMaxBlobs(id, 10);
      rmSetAreaCoherence(id, 0.9);
      rmSetAreaSmoothDistance(id, 5);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.30);


   // Define and place trade route - only in FFA

   if (rmGetIsFFA() == true)
   {
      // Create the trade route
      int tradeRouteID = rmCreateTradeRoute();
      int socketID=rmCreateObjectDef("sockets to dock trade posts");
      rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

      // Define the sockets
      rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
      rmSetObjectDefAllowOverlap(socketID, true);
      rmSetObjectDefMinDistance(socketID, 0.0);
      rmSetObjectDefMaxDistance(socketID, 12.0);

      // Set the trade route waypoints
      rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.8);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.86, 0.6);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.90, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.86, 0.4);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.2);
      bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
      if(placedTradeRoute == false)
            rmEchoError("Failed to place trade route"); 
  
      // Place sockets to trade route - between every 2 players
      if (cNumberNonGaiaPlayers == 2)
      {
            vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      }
      if (cNumberNonGaiaPlayers == 3)
      {
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.3);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.7);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      }
      if (cNumberNonGaiaPlayers == 4)
      {
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.25);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.75);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      }
      if (cNumberNonGaiaPlayers == 5)
      {
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.23);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.37);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.63);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.77);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      }
      if (cNumberNonGaiaPlayers == 6)
      {
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.23);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.33);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.67);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.77);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      }
      if (cNumberNonGaiaPlayers == 7)
      {
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.23);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.31);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.4);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.6);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.69);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.77);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      }
      if (cNumberNonGaiaPlayers == 8)
      {
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.21);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.3);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.36);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.64);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.7);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.79);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      }
   }


   // Text
   rmSetStatusText("",0.40);


   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmSetObjectDefMinDistance(startingTCID, 5.0);
   rmSetObjectDefMaxDistance(startingTCID, 10.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefConstraint(startingTCID, avoidTradeRoute);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "TreeGreatLakesSnow", 8, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 12);
   rmSetObjectDefMaxDistance(startAreaTreeID, 16);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startMooseID=rmCreateObjectDef("starting moose");
   rmAddObjectDefItem(startMooseID, "moose", 5, 4.0);
   rmSetObjectDefCreateHerd(startMooseID, true);
   rmSetObjectDefMinDistance(startMooseID, 14);
   rmSetObjectDefMaxDistance(startMooseID, 18);
   rmAddObjectDefConstraint(startMooseID, avoidStartResource);
   rmAddObjectDefConstraint(startMooseID, avoidImpassableLand);
   rmAddObjectDefConstraint(startMooseID, shortAvoidStartingUnits);

   int startSilverID=rmCreateObjectDef("player silver");
   rmAddObjectDefItem(startSilverID, "Mine", 1, 5.0);
   rmSetObjectDefMinDistance(startSilverID, 14);
   rmSetObjectDefMaxDistance(startSilverID, 18);
   rmAddObjectDefConstraint(startSilverID, avoidStartResource);
   rmAddObjectDefConstraint(startSilverID, avoidImpassableLand);
   rmAddObjectDefConstraint(startSilverID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startMooseID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startSilverID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
      rmAddClosestPointConstraint(flagVsFlag);
      rmAddClosestPointConstraint(flagLand);		
      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Define and place the 2 natives, always Jutes - only in team games

   if (rmGetIsFFA() == false)
   {
      // Native 1, to western side
      int nativeVillageType = rmRandInt(1,5);
      int nativeVillageID = rmCreateGrouping("jutes village", "native comanche village "+nativeVillageType);
      rmSetGroupingMinDistance(nativeVillageID, 0.0);
      rmSetGroupingMaxDistance(nativeVillageID, 10.0);
      rmAddGroupingToClass(nativeVillageID, rmClassID("natives"));
      rmPlaceGroupingAtLoc(nativeVillageID, 0, 0.9, 0.6);

      // Native 2, to eastern side
      int nativeVillage2Type = rmRandInt(1,5);
      int nativeVillage2ID = rmCreateGrouping("jutes village 2", "native comanche village "+nativeVillage2Type);
      rmSetGroupingMinDistance(nativeVillage2ID, 0.0);
      rmSetGroupingMaxDistance(nativeVillage2ID, 10.0);
      rmAddGroupingToClass(nativeVillage2ID, rmClassID("natives"));
      rmPlaceGroupingAtLoc(nativeVillage2ID, 0, 0.9, 0.4);
   }


   // Text
   rmSetStatusText("",0.50);


   // Define and place cliffs for team 1 (left side)

   // Lower cliffs
   int cliffLeftID=rmCreateArea("cliff left side");
   rmSetAreaSize(cliffLeftID, 0.13, 0.13);
   rmSetAreaLocation(cliffLeftID, 0.5, 1.0);
   rmAddAreaInfluenceSegment(cliffLeftID, 0.2, 0.95, 0.9, 0.85);
   rmSetAreaCoherence(cliffLeftID, 0.8);
   rmSetAreaSmoothDistance(cliffLeftID, 10);
   rmAddAreaToClass(cliffLeftID, classCliff);
   rmSetAreaCliffType(cliffLeftID, "rocky mountain2");
   rmSetAreaCliffEdge(cliffLeftID, 1, 1.0, 0.0, 0.0, 0);
   rmSetAreaCliffHeight(cliffLeftID, 4, 0.0, 0.3);
   rmSetAreaCliffPainting(cliffLeftID, true, false, true);
   rmSetAreaObeyWorldCircleConstraint(cliffLeftID, false);
   rmAddAreaConstraint(cliffLeftID, shortestCliffPlayerConstraint);
   rmBuildArea(cliffLeftID);

   // Higher cliffs
   int cliffLeft2ID=rmCreateArea("cliff left side 2");
   rmSetAreaSize(cliffLeft2ID, 0.1, 0.1);
   rmSetAreaLocation(cliffLeft2ID, 0.5, 1.0);
   rmAddAreaInfluenceSegment(cliffLeft2ID, 0.2, 0.95, 0.9, 0.85);
   rmSetAreaCoherence(cliffLeft2ID, 0.8);
   rmSetAreaSmoothDistance(cliffLeft2ID, 10);
   rmAddAreaToClass(cliffLeft2ID, classCliff);
   rmSetAreaCliffType(cliffLeft2ID, "rocky mountain2");
   rmSetAreaCliffEdge(cliffLeft2ID, 1, 1.0, 0.0, 0.0, 0);
   rmSetAreaCliffHeight(cliffLeft2ID, 4, 0.0, 0.3);
   rmSetAreaCliffPainting(cliffLeft2ID, true, false, true);
   rmSetAreaObeyWorldCircleConstraint(cliffLeft2ID, false);
   rmAddAreaConstraint(cliffLeft2ID, shortCliffPlayerConstraint);
   rmBuildArea(cliffLeft2ID);

   // Highest cliffs
   int cliffLeft3ID=rmCreateArea("cliff left side 3");
   rmSetAreaSize(cliffLeft3ID, 0.07, 0.07);
   rmSetAreaLocation(cliffLeft3ID, 0.5, 1.0);
   rmAddAreaInfluenceSegment(cliffLeft3ID, 0.2, 0.95, 0.9, 0.85);
   rmSetAreaCoherence(cliffLeft3ID, 0.8);
   rmSetAreaSmoothDistance(cliffLeft3ID, 10);
   rmAddAreaToClass(cliffLeft3ID, classCliff);
   rmSetAreaCliffType(cliffLeft3ID, "rocky mountain2");
   rmSetAreaCliffEdge(cliffLeft3ID, 1, 1.0, 0.0, 0.0, 0);
   rmSetAreaCliffHeight(cliffLeft3ID, 4, 0.0, 0.3);
   rmSetAreaCliffPainting(cliffLeft3ID, true, false, true);
   rmSetAreaObeyWorldCircleConstraint(cliffLeft3ID, false);
   rmAddAreaConstraint(cliffLeft3ID, cliffPlayerConstraint);
   rmBuildArea(cliffLeft3ID);


   // Define and place cliffs for team 2 (right side)

   // Lower cliffs
   int cliffRightID=rmCreateArea("cliff right side");
   rmSetAreaSize(cliffRightID, 0.13, 0.13);
   rmSetAreaLocation(cliffRightID, 0.5, 0.0);
   rmAddAreaInfluenceSegment(cliffRightID, 0.2, 0.05, 0.9, 0.15);
   rmSetAreaCoherence(cliffRightID, 0.8);
   rmSetAreaSmoothDistance(cliffRightID, 10);
   rmAddAreaToClass(cliffRightID, classCliff);
   rmSetAreaCliffType(cliffRightID, "rocky mountain2");
   rmSetAreaCliffEdge(cliffRightID, 1, 1.0, 0.0, 0.0, 0);
   rmSetAreaCliffHeight(cliffRightID, 4, 0.0, 0.3);
   rmSetAreaCliffPainting(cliffRightID, true, false, true);
   rmSetAreaObeyWorldCircleConstraint(cliffRightID, false);
   rmAddAreaConstraint(cliffRightID, shortestCliffPlayerConstraint);
   rmBuildArea(cliffRightID);

   // Higher cliffs
   int cliffRight2ID=rmCreateArea("cliff right side 2");
   rmSetAreaSize(cliffRight2ID, 0.1, 0.1);
   rmSetAreaLocation(cliffRight2ID, 0.5, 0.0);
   rmAddAreaInfluenceSegment(cliffRight2ID, 0.2, 0.05, 0.9, 0.15);
   rmSetAreaCoherence(cliffRight2ID, 0.8);
   rmSetAreaSmoothDistance(cliffRight2ID, 10);
   rmAddAreaToClass(cliffRight2ID, classCliff);
   rmSetAreaCliffType(cliffRight2ID, "rocky mountain2");
   rmSetAreaCliffEdge(cliffRight2ID, 1, 1.0, 0.0, 0.0, 0);
   rmSetAreaCliffHeight(cliffRight2ID, 4, 0.0, 0.3);
   rmSetAreaCliffPainting(cliffRight2ID, true, false, true);
   rmSetAreaObeyWorldCircleConstraint(cliffRight2ID, false);
   rmAddAreaConstraint(cliffRight2ID, shortCliffPlayerConstraint);
   rmBuildArea(cliffRight2ID);

   // Highest cliffs
   int cliffRight3ID=rmCreateArea("cliff right side 3");
   rmSetAreaSize(cliffRight3ID, 0.07, 0.07);
   rmSetAreaLocation(cliffRight3ID, 0.5, 0.0);
   rmAddAreaInfluenceSegment(cliffRight3ID, 0.2, 0.05, 0.9, 0.15);
   rmSetAreaCoherence(cliffRight3ID, 0.8);
   rmSetAreaSmoothDistance(cliffRight3ID, 10);
   rmAddAreaToClass(cliffRight3ID, classCliff);
   rmSetAreaCliffType(cliffRight3ID, "rocky mountain2");
   rmSetAreaCliffEdge(cliffRight3ID, 1, 1.0, 0.0, 0.0, 0);
   rmSetAreaCliffHeight(cliffRight3ID, 4, 0.0, 0.3);
   rmSetAreaCliffPainting(cliffRight3ID, true, false, true);
   rmSetAreaObeyWorldCircleConstraint(cliffRight3ID, false);
   rmAddAreaConstraint(cliffRight3ID, cliffPlayerConstraint);
   rmBuildArea(cliffRight3ID);


   // Text
   rmSetStatusText("",0.60);


   // Define and place forests
	
   int numTries=30*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaSize(forest, rmAreaTilesToFraction(450), rmAreaTilesToFraction(500));
      rmSetAreaForestType(forest, "great lakes forest snow");
      rmSetAreaForestDensity(forest, 1.0);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 1.0);
      rmSetAreaForestUnderbrush(forest, 0.3);
      rmSetAreaBaseHeight(forest, 1.0);
      rmSetAreaMinBlobs(forest, 1);
      rmSetAreaMaxBlobs(forest, 3);
      rmSetAreaMinBlobDistance(forest, 10.0);
      rmSetAreaMaxBlobDistance(forest, 15.0);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, playerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, cliffConstraint);
      rmAddAreaConstraint(forest, avoidTradeRoute);
      rmAddAreaConstraint(forest, avoidTradeSockets);
      rmAddAreaConstraint(forest, avoidNatives);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.  
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }


   // Text
   rmSetStatusText("",0.70);


   // Define and place moose herds

   int mooseHerdID=rmCreateObjectDef("moose herd");
   rmAddObjectDefItem(mooseHerdID, "moose", rmRandInt(6,7), 8.0);
   rmSetObjectDefCreateHerd(mooseHerdID, true);
   rmSetObjectDefMinDistance(mooseHerdID, 0.0);
   rmSetObjectDefMaxDistance(mooseHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(mooseHerdID, classAnimals);
   rmAddObjectDefConstraint(mooseHerdID, shortPlayerConstraint);
   rmAddObjectDefConstraint(mooseHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(mooseHerdID, animalConstraint);
   rmAddObjectDefConstraint(mooseHerdID, cliffConstraint);
   rmAddObjectDefConstraint(mooseHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(mooseHerdID, avoidTradeSockets);
   rmAddObjectDefConstraint(mooseHerdID, avoidNatives);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(mooseHerdID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.80);


   // Define and place silver mines

   int silverCount = 2*cNumberNonGaiaPlayers;

   for(i=0; < silverCount)
   {
      int silverID=rmCreateObjectDef("silver mine"+i);
      rmAddObjectDefItem(silverID, "mine", 1, 0.0);
      rmSetObjectDefMinDistance(silverID, 0.0);
      rmSetObjectDefMaxDistance(silverID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(silverID, classMines);
      rmAddObjectDefConstraint(silverID, avoidImpassableLand);
      rmAddObjectDefConstraint(silverID, playerConstraint);
      rmAddObjectDefConstraint(silverID, cliffConstraint);
      rmAddObjectDefConstraint(silverID, avoidMines);
      rmAddObjectDefConstraint(silverID, avoidTradeRoute);
      rmAddObjectDefConstraint(silverID, avoidTradeSockets);
      rmAddObjectDefConstraint(silverID, avoidNatives);
      rmPlaceObjectDefAtLoc(silverID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.90);
	

   // Define and place fish

   int fishID=rmCreateObjectDef("fish");
   rmAddObjectDefItem(fishID, "FishSalmon", 1, 4.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.5));
   rmAddObjectDefConstraint(fishID, fishVsFish);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 8*cNumberNonGaiaPlayers);


   // Text
   rmSetStatusText("",0.99);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget1, avoidTradeSockets);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, cliffConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget2, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget2, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, cliffConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget3, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget3, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, cliffConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget4, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget4, avoidNatives);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Define and place cow herds

   int cowID=rmCreateObjectDef("cow");
   rmAddObjectDefItem(cowID, "cow", 2, 4.0);
   rmSetObjectDefMinDistance(cowID, 0.0);
   rmSetObjectDefMaxDistance(cowID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(cowID, avoidCow);
   rmAddObjectDefConstraint(cowID, playerConstraint);
   rmAddObjectDefConstraint(cowID, cliffConstraint);
   rmAddObjectDefConstraint(cowID, avoidImpassableLand);
   rmAddObjectDefConstraint(cowID, avoidTradeRoute);
   rmAddObjectDefConstraint(cowID, avoidTradeSockets);
   rmAddObjectDefConstraint(cowID, avoidNatives);
   rmPlaceObjectDefAtLoc(cowID, 0, 0.5, 0.5, cNumberNonGaiaPlayers);
   

   // Text
   rmSetStatusText("",1.0); 
}  
