// *********************************************************************************************************************************************************
// ****************************************************** B O N N I E   S P R I N G S **********************************************************************
// *********************************************************************************************************************************************************

// ------------------------------------------------------ Comentaries ---------------------------------------------------------------------------
// Work done by Rikikipu - February 2016



// ------------------------------------------------------ Starting the OP UI --------------------------------------------------------------------

// No  UI in this version


// ------------------------------------------------------ Initialization ------------------------------------------------------------------------
include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   rmSetStatusText("",0.01);

   int playerTiles=11500;
   if (cNumberNonGaiaPlayers >2)
	    playerTiles = 11500;
   if (cNumberNonGaiaPlayers >4)
		playerTiles = 10800;
   if (cNumberNonGaiaPlayers >6)
      playerTiles = 10000;

	int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
	rmSetMapSize(size, size*0.75);
	rmSetSeaLevel(0.0);
	rmSetMapElevationParameters(cElevTurbulence, 0.06, 2, 0.1, 1.0);
	rmSetBaseTerrainMix("texas_grass");
	rmTerrainInitialize("texas\ground1_tex", 5);
	rmSetLightingSet("ottoman morning");
	rmSetMapType("texas");
	rmSetMapType("land");
	rmSetWorldCircleConstraint(false);
	rmSetMapType("grass");

	int mapVersion =-1;
	if (rmRandFloat(0,1)>0.5)
		mapVersion = 1;
	else if (rmRandFloat(0,1)>0.5)
		mapVersion = 2;
	else
		mapVersion = 3;
	
	
   int numTries = -1;
   int failCount = -1;

	chooseMercs();


// ------------------------------------------------------ Contraints ---------------------------------------------------------------------------
	int classPlayer=rmDefineClass("player");
	rmDefineClass("classPatch");
	rmDefineClass("starting settlement");
	rmDefineClass("startingUnit");
	rmDefineClass("classForest");
	rmDefineClass("importantItem");
	rmDefineClass("secrets");
	rmDefineClass("natives");	
	rmDefineClass("socketClass");
	rmDefineClass("nuggets");
    rmDefineClass("classCliff");
	int pondClass=rmDefineClass("pond");

   
   // Map edge constraints
   int avoidCenter = rmCreatePieConstraint("Avoid Center",0.5,0.5, rmXFractionToMeters(0.09),rmXFractionToMeters(0.5), rmDegreesToRadians(0),rmDegreesToRadians(360));
   int avoidEdgeGold = rmCreatePieConstraint("Avoid Edge1",0.5,0.5, rmXFractionToMeters(0.05),rmXFractionToMeters(0.1), rmDegreesToRadians(0),rmDegreesToRadians(360));
   int avoidEdge = rmCreateBoxConstraint("player edge of map1", rmXTilesToFraction(0.25), rmZTilesToFraction(3), 1.0-rmXTilesToFraction(0.25), 1.0-rmZTilesToFraction(3), 1.0);

   // For Gold

// X marks the spot

   int avoidCenterGold3 = rmCreatePieConstraint("Avoid Center gold 3",0.5,0.5, rmXFractionToMeters(0.43),rmXFractionToMeters(0.48), rmDegreesToRadians(0),rmDegreesToRadians(360));
   // Player constraints
   int playerConstraint=rmCreateClassDistanceConstraint("player vs. player", classPlayer, 10.0);
   int nuggetPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players a lot", rmClassID("startingUnit"), 50.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 25.0);
   int avoidCoinShort=rmCreateTypeDistanceConstraint("avoids coin short", "gold", 8.0);
   int avoidDeer=rmCreateTypeDistanceConstraint("Deer avoids food", "Deer", 50.0);
   int avoidDeerPond=rmCreateTypeDistanceConstraint("Deer avoids food1", "Deer", 10.0);
   int avoidBison=rmCreateTypeDistanceConstraint("Bison avoids food", "Bison", 50.0);
   int avoidBisonShort=rmCreateTypeDistanceConstraint("Bison avoids food short", "Bison", 25.0);
   int avoidCoin=rmCreateTypeDistanceConstraint("coin avoids coin", "gold", 62.0);
   int avoidCoinPond=rmCreateTypeDistanceConstraint("pond avoids coin", "gold", 12.0);
   int avoidStartingCoin=rmCreateTypeDistanceConstraint("starting coin avoids coin", "gold", 28.0);
   int avoidNugget=rmCreateTypeDistanceConstraint("nugget avoid nugget", "AbstractNugget", 43.0);
   int avoidNuggetSmall=rmCreateTypeDistanceConstraint("avoid nuggets by a little", "AbstractNugget", 6.0);
   int avoidNuggetSmall1=rmCreateTypeDistanceConstraint("avoid nuggets by a little1", "AbstractNugget", 20.0);
   int avoidFastCoin=rmCreateTypeDistanceConstraint("fast coin avoids coin", "gold", 63);
   int avoidFastCoinTeam=rmCreateTypeDistanceConstraint("fast coin avoids coin team", "gold", 72);
   int avoidDeerShort = rmCreateTypeDistanceConstraint("avoid Deer short", "Deer", 15.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 6.0);
   int shortAvoidImpassableLand=rmCreateTerrainDistanceConstraint("short avoid impassable land", "Land", false, 2.0);
   int patchConstraint=rmCreateClassDistanceConstraint("patch vs. patch", rmClassID("classPatch"), 5.0);

   // Unit avoidance - for things that aren't in the starting resources.
   int avoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units", rmClassID("startingUnit"), 30.0);
   int avoidStartingUnitsTree=rmCreateClassDistanceConstraint("objects avoid starting units1", rmClassID("startingUnit"), 10.0);
   int avoidStartingUnitsSmall=rmCreateClassDistanceConstraint("objects avoid starting units small", rmClassID("startingUnit"), 5.0);
   int avoidTownCenterFar=rmCreateTypeDistanceConstraint("avoid Town Center Far", "townCenter", 58.0);
   int avoidTownCenterFar1=rmCreateTypeDistanceConstraint("avoid Town Center Far 1", "townCenter", 30.0);
   int avoidTownCenterFar2=rmCreateTypeDistanceConstraint("avoid Town Center Far team", "townCenter", 45.0);
   int avoidPondMine=rmCreateClassDistanceConstraint("mines avoid Pond", pondClass, 20.0);
   
   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 6.0);

   // VP avoidance
   int avoidTradeRoute = rmCreateTradeRouteDistanceConstraint("trade route", 6.0);
   int avoidTradeRouteSmall = rmCreateTradeRouteDistanceConstraint("trade route small", 4.0);
   int avoidImportantItem=rmCreateClassDistanceConstraint("important stuff avoids each other", rmClassID("importantItem"), 15.0);
   int avoidSocket=rmCreateClassDistanceConstraint("socket avoidance", rmClassID("socketClass"), 6.0);
   int avoidSocketMore=rmCreateClassDistanceConstraint("bigger socket avoidance", rmClassID("socketClass"), 10.0);

   // Constraint to avoid water.
   int avoidWater = rmCreateTerrainDistanceConstraint("avoid water long", "Land", false, 50.0);

   // Avoid the Cliffs.
   int avoidCliffs = rmCreateClassDistanceConstraint("avoid Cliffs", rmClassID("classCliff"), 10.0);
   int avoidCliffs1 = rmCreateClassDistanceConstraint("avoid Cliffs1", rmClassID("classCliff"), 7.0);
   int avoidCliffsFar = rmCreateClassDistanceConstraint("avoid Cliffs far", rmClassID("classCliff"), 15.0);
   int avoidPatch = rmCreateClassDistanceConstraint("avoid patch", rmClassID("classPatch"), 2.0);
	
   // natives avoid natives
   int avoidNatives = rmCreateClassDistanceConstraint("avoid Natives", rmClassID("natives"), 10.0);
   int avoidNativesWood = rmCreateClassDistanceConstraint("avoid Natives wood", rmClassID("natives"), 6.0);
   int avoidNativesNuggets = rmCreateClassDistanceConstraint("nuggets avoid Natives", rmClassID("natives"), 20.0);

   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));


  									 rmSetStatusText("",0.10);

	
// ------------------------------------------------------ Trade Route ---------------------------------------------------------------------------
   int tradeRouteID = rmCreateTradeRoute();
   int tradeRouteID1 = rmCreateTradeRoute();
   
   int socketID=rmCreateObjectDef("sockets to dock Trade Posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);
   rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socketID, true);
   rmAddObjectDefToClass(socketID, rmClassID("socketClass"));
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 6.0);

   int socketID1=rmCreateObjectDef("sockets to dock Trade Posts1");
   rmSetObjectDefTradeRouteID(socketID1, tradeRouteID1);
   rmAddObjectDefItem(socketID1, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socketID1, true);
   rmAddObjectDefToClass(socketID1, rmClassID("socketClass"));
   rmSetObjectDefMinDistance(socketID1, 0.0);
   rmSetObjectDefMaxDistance(socketID1, 6.0);
   
   rmAddTradeRouteWaypoint(tradeRouteID,0.99, 0.5);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.9, 0.5, 0, 0);
   rmAddTradeRouteWaypoint(tradeRouteID1,0.0, 0.5);
   rmAddRandomTradeRouteWaypoints(tradeRouteID1, 0.1, 0.5, 0, 0);

  
if (mapVersion ==1)
{	   
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.6, 0.2, 4, 1);
   rmAddRandomTradeRouteWaypoints(tradeRouteID1, 0.4, 0.8, 4, 1);
}
else
{
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.7, 0.25, 4, 1);
   rmAddRandomTradeRouteWaypoints(tradeRouteID1, 0.31, 0.73, 4, 1);	
}
   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
   bool placedTradeRoute1 = rmBuildTradeRoute(tradeRouteID1, "dirt");

    vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.15);
    rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);	
    socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 1.0);
    rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
	
	vector socketLoc1 = rmGetTradeRouteWayPoint(tradeRouteID1, 0.15);
    rmPlaceObjectDefAtPoint(socketID1, 0, socketLoc1);
	socketLoc1 = rmGetTradeRouteWayPoint(tradeRouteID1, 1.0);
    rmPlaceObjectDefAtPoint(socketID1, 0, socketLoc1);
	
    rmSetStatusText("",0.20);

// ------------------------------------------------------ Cliffs ----------------------------------------------------------------------------------

    int areaCliff1 = rmCreateArea("area for cliff1");
   rmSetAreaLocation(areaCliff1, 0.95, 0.5);
   rmAddAreaInfluenceSegment(areaCliff1, 1.0, 0.5, 0.9, 0.5);   
   rmSetAreaWarnFailure(areaCliff1, false);
   rmSetAreaSize(areaCliff1, 0.03, 0.04);
   rmSetAreaCoherence(areaCliff1, 0.99);
   rmSetAreaSmoothDistance(areaCliff1, 15);  
   rmSetAreaEdgeFilling(areaCliff1, 1.0);
   rmSetAreaObeyWorldCircleConstraint(areaCliff1, false);
   	rmAddAreaToClass(areaCliff1, rmClassID("classPatch"));
   rmSetAreaTerrainType(areaCliff1, "texas\ground1_tex");
   rmSetAreaMix(areaCliff1, "texas grass");
   rmBuildArea(areaCliff1);
   
    int areaCliff2 = rmCreateArea("area for cliff2");
   rmSetAreaLocation(areaCliff2, 0.05, 0.5); 
   rmSetAreaWarnFailure(areaCliff2, false);
   rmAddAreaInfluenceSegment(areaCliff2, 0.0, 0.5, 0.1, 0.5); 
   rmSetAreaSize(areaCliff2, 0.03, 0.04);
   rmSetAreaCoherence(areaCliff2, 0.99);
   rmSetAreaSmoothDistance(areaCliff2, 15);  
   rmSetAreaEdgeFilling(areaCliff2, 1.0);
   rmSetAreaObeyWorldCircleConstraint(areaCliff2, false);
   rmAddAreaToClass(areaCliff2, rmClassID("classPatch"));
   rmSetAreaTerrainType(areaCliff2, "texas\ground1_tex");
   rmSetAreaMix(areaCliff2, "texas grass");
   rmBuildArea(areaCliff2);
 
   int areaCliff3 = rmCreateArea("area for cliffs3");
   rmSetAreaLocation(areaCliff3, 0.5, 0.5); 
   rmSetAreaWarnFailure(areaCliff3, false);
   rmSetAreaSize(areaCliff3, 0.65, 0.65);
   rmSetAreaCoherence(areaCliff3, 0.99);
   rmSetAreaSmoothDistance(areaCliff3, 15);  
   rmSetAreaObeyWorldCircleConstraint(areaCliff3, false);
   rmAddAreaConstraint(areaCliff3, avoidEdge);
   rmAddAreaToClass(areaCliff3, rmClassID("classPatch"));
   rmSetAreaTerrainType(areaCliff3, "texas\ground1_tex");
   rmSetAreaMix(areaCliff3, "texas grass");
   rmBuildArea(areaCliff3);
 
    int cliffIDL=rmCreateArea("cliff left");
	rmSetAreaLocation(cliffIDL, 0.98, 0.98);
	rmSetAreaSize(cliffIDL, 0.15, 0.15);
    rmSetAreaWarnFailure(cliffIDL, false);
	rmSetAreaCliffType(cliffIDL, "texas");
    rmSetAreaCliffEdge(cliffIDL, 1, 1);
    rmSetAreaCliffPainting(cliffIDL, true, true, true, 1.0, true);
	rmSetAreaEdgeFilling(cliffIDL, 1.0);
    rmSetAreaCliffHeight(cliffIDL, 5, 1.0, 0.5);
    rmSetAreaHeightBlend(cliffIDL, 1);
    rmSetAreaSmoothDistance(cliffIDL, 10);
    rmAddAreaConstraint(cliffIDL, avoidPatch);
	rmAddAreaToClass(cliffIDL, rmClassID("classCliff"));
    rmSetAreaCoherence(cliffIDL, 0.7);
    rmBuildArea(cliffIDL);

	int cliffIDR=rmCreateArea("cliff right");
	rmSetAreaLocation(cliffIDR, 0.02, 0.02);
	rmSetAreaSize(cliffIDR, 0.15, 0.15);
    rmSetAreaWarnFailure(cliffIDR, false);
	rmSetAreaCliffType(cliffIDR, "texas");
    rmSetAreaCliffEdge(cliffIDR, 1, 1);
    rmSetAreaCliffPainting(cliffIDR, true, true, true, 1.0, true);
	rmSetAreaEdgeFilling(cliffIDR, 1.0);
    rmSetAreaCliffHeight(cliffIDR, 5, 1.0, 0.5);
    rmSetAreaHeightBlend(cliffIDR, 1);
    rmSetAreaSmoothDistance(cliffIDR, 10);
    rmAddAreaConstraint(cliffIDR, avoidPatch);
	rmAddAreaToClass(cliffIDR, rmClassID("classCliff"));
    rmSetAreaCoherence(cliffIDR, 0.7);
    rmBuildArea(cliffIDR);
	
   int PatchC = rmCreateArea("patch 3");
   rmSetAreaLocation(PatchC, 0.5, 0.5); 
   rmSetAreaWarnFailure(PatchC, false);
   rmSetAreaSize(PatchC, 0.15, 0.18);
   rmSetAreaCoherence(PatchC, 0.8);
   rmSetAreaSmoothDistance(PatchC, 10);  
   rmSetAreaObeyWorldCircleConstraint(PatchC, false);
   rmSetAreaTerrainType(PatchC, "texas\ground5_tex");
   rmSetAreaMix(PatchC, "texas grass");
   rmBuildArea(PatchC);
   
   int PatchB = rmCreateArea("patch 2");
   rmSetAreaLocation(PatchB, 0.7, 0.2); 
   rmSetAreaWarnFailure(PatchB, false);
   rmSetAreaSize(PatchB, 0.05, 0.07);
   rmSetAreaCoherence(PatchB, 0.8);
   rmSetAreaSmoothDistance(PatchB, 10);  
   rmSetAreaObeyWorldCircleConstraint(PatchB, false);
   rmSetAreaTerrainType(PatchB, "texas\ground5_tex");
   rmSetAreaMix(PatchB, "texas grass");
   rmBuildArea(PatchB);
   
   int PatchA = rmCreateArea("patch 1");
   rmSetAreaLocation(PatchA, 0.3, 0.8); 
   rmSetAreaWarnFailure(PatchA, false);
   rmSetAreaSize(PatchA, 0.05, 0.08);
   rmSetAreaCoherence(PatchA, 0.8);
   rmSetAreaSmoothDistance(PatchA, 10);  
   rmSetAreaObeyWorldCircleConstraint(PatchA, false);
   rmSetAreaTerrainType(PatchA, "texas\ground5_tex");
   rmSetAreaMix(PatchA, "texas grass");
   rmBuildArea(PatchA);

// ------------------------------------------------------ Player Location and Specific Area ---------------------------------------------------------------------------


    int VillageID =-1;
if (mapVersion==1)
	VillageID = rmCreateGrouping("center location", "middleBigCity_EP"); 
else if (mapVersion==2)
	VillageID = rmCreateGrouping("center location", "middleLittleCity1_EP");
else if (mapVersion==3)
	VillageID = rmCreateGrouping("center location", "middleLittleCity2_EP");
    rmSetGroupingMinDistance(VillageID, 0.0);
    rmSetGroupingMaxDistance(VillageID, rmXFractionToMeters(0.0));
	rmPlaceGroupingAtLoc(VillageID, 0, 0.5, 0.5);

	int VillageID2 =-1;
if (rmRandFloat(0,1)<0.5)
	VillageID2 = rmCreateGrouping("right location", "mineIndian_EP"); //   X marks the spot
else
	VillageID2 = rmCreateGrouping("right location", "mineCamp_EP");
    rmSetGroupingMinDistance(VillageID2, 0.0);
    rmSetGroupingMaxDistance(VillageID2, rmXFractionToMeters(0.0));
	rmPlaceGroupingAtLoc(VillageID2, 0, 0.75, 0.17);
	
	int VillageID3 = rmCreateGrouping("left location", "mineBarn_EP"); //   X marks the spot
    rmSetGroupingMinDistance(VillageID3, 0.0);
    rmSetGroupingMaxDistance(VillageID3, rmXFractionToMeters(0.0));
	rmPlaceGroupingAtLoc(VillageID3, 0, 0.28, 0.85);
	
	
// ------------------------------------------------------ Starting Ressources ---------------------------------------------------------------------------


if (cNumberTeams ==2)
{

	if(cNumberNonGaiaPlayers==2)
	{
		if (rmRandFloat(0,1)<0.5)
		{		
		rmSetPlacementTeam(0);
		rmSetPlacementSection(0.1, 0.22);
		rmPlacePlayersCircular(0.38, 0.39, 0);

		rmSetPlacementTeam(1);
		rmSetPlacementSection(0.6, 0.72);
		rmPlacePlayersCircular(0.38, 0.39, 0);	
		}
		else
		{
		rmSetPlacementTeam(1);
		rmSetPlacementSection(0.1, 0.22);
		rmPlacePlayersCircular(0.38, 0.39, 0);

		rmSetPlacementTeam(0);
		rmSetPlacementSection(0.6, 0.72);
		rmPlacePlayersCircular(0.38, 0.39, 0);				
		}	
	}
	else if (cNumberNonGaiaPlayers==4)
	{
   rmSetPlacementTeam(0);
   rmSetPlacementSection(0.02, 0.14);
   rmPlacePlayersCircular(0.37, 0.38, 0);

   rmSetPlacementTeam(1);
   rmSetPlacementSection(0.52, 0.64);
   rmPlacePlayersCircular(0.37, 0.38, 0);			
	}
	else
	{
   rmSetPlacementTeam(0);
   rmSetPlacementSection(0.98, 0.14);
   rmPlacePlayersCircular(0.37, 0.38, 0);

   rmSetPlacementTeam(1);
   rmSetPlacementSection(0.48, 0.64);
   rmPlacePlayersCircular(0.37, 0.38, 0);	
	}		
}
else
{
	rmPlacePlayer(1, 0.13, 0.48);
	rmPlacePlayer(2, 0.87, 0.52);
	rmPlacePlayer(3, 0.5, 0.9);
	rmPlacePlayer(4, 0.5, 0.1);
	rmPlacePlayer(5, 0.23, 0.23);
	rmPlacePlayer(6, 0.77, 0.77);
	rmPlacePlayer(7, 0.35, 0.65);
	rmPlacePlayer(8, 0.65, 0.35);	
}	  

	int startingUnits = rmCreateStartingUnitsObjectDef(5.0);
	rmSetObjectDefMinDistance(startingUnits, 0.0);
	rmSetObjectDefMaxDistance(startingUnits, 0.0);

	int startingTCID = rmCreateObjectDef("startingTC");
	if ( rmGetNomadStart())
	{
		rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 0.0);
	}
	else
	{
		rmAddObjectDefItem(startingTCID, "townCenter", 1, 0.0);
	}
	rmAddObjectDefToClass(startingTCID, rmClassID("startingUnit"));
	rmSetObjectDefMinDistance(startingTCID, 0.0);
	rmSetObjectDefMaxDistance(startingTCID, 1.0);

	int StartAreaTreeID=rmCreateObjectDef("starting trees");
	rmAddObjectDefItem(StartAreaTreeID, "TreeTexas", 2, 0.0);
	rmSetObjectDefMinDistance(StartAreaTreeID, 12.0);
	rmSetObjectDefMaxDistance(StartAreaTreeID, 20.0);
	rmAddObjectDefConstraint(StartAreaTreeID, avoidStartingUnitsSmall);

	int StartDeerID1=rmCreateObjectDef("starting Deer");
	rmAddObjectDefItem(StartDeerID1, "Deer", 5, 5.0);
	rmSetObjectDefMinDistance(StartDeerID1, 10.0);
	rmSetObjectDefMaxDistance(StartDeerID1, 12.0);
	rmSetObjectDefCreateHerd(StartDeerID1, true);
	rmAddObjectDefConstraint(StartDeerID1, avoidStartingUnitsSmall);
	rmAddObjectDefConstraint(StartDeerID1, avoidCliffs1);

	int StartDeerID11=rmCreateObjectDef("starting Deer1");
	rmAddObjectDefItem(StartDeerID11, "Bison", 8, 9.0);
	rmSetObjectDefMinDistance(StartDeerID11, 32.0);
	rmSetObjectDefMaxDistance(StartDeerID11, 34.0);
	rmSetObjectDefCreateHerd(StartDeerID11, true);
	rmAddObjectDefConstraint(StartDeerID11, avoidStartingUnitsSmall);
	rmAddObjectDefConstraint(StartDeerID11, avoidBisonShort);
	rmAddObjectDefConstraint(StartDeerID11, avoidCliffs);


	int playerNuggetID=rmCreateObjectDef("player nugget");
	rmAddObjectDefItem(playerNuggetID, "nugget", 1, 0.0);
	rmAddObjectDefToClass(playerNuggetID, rmClassID("nuggets"));
	rmAddObjectDefToClass(playerNuggetID, rmClassID("startingUnit"));
    rmSetObjectDefMinDistance(playerNuggetID, 23.0);
    rmSetObjectDefMaxDistance(playerNuggetID, 32.0);
	rmAddObjectDefConstraint(playerNuggetID, avoidNugget);
	rmAddObjectDefConstraint(playerNuggetID, avoidNativesNuggets);
	rmAddObjectDefConstraint(playerNuggetID, avoidTradeRouteSmall);
	rmAddObjectDefConstraint(playerNuggetID, circleConstraint);
	// rmAddObjectDefConstraint(playerNuggetID, avoidImportantItem);

	
	int silverType = -1;
	int playerGoldID = -1;

 	for(i=1; <=cNumberNonGaiaPlayers)
	{
		rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
    
    if(ypIsAsian(i) && rmGetNomadStart() == false)
      rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
    
		rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

		// Everyone gets two ore ObjectDefs, one pretty close, the other a little further away.

		silverType = rmRandInt(1,10);
		playerGoldID = rmCreateObjectDef("player silver closer "+i);
		rmAddObjectDefItem(playerGoldID, "MineCopper", 1, 0.0);
		rmAddObjectDefConstraint(playerGoldID, avoidTradeRoute);
		rmAddObjectDefConstraint(playerGoldID, avoidStartingCoin);
		rmAddObjectDefConstraint(playerGoldID, avoidStartingUnitsSmall);
		rmAddObjectDefConstraint(playerGoldID, avoidCliffs1);
		rmSetObjectDefMinDistance(playerGoldID, 15.0);
		rmSetObjectDefMaxDistance(playerGoldID, 16.0);
		

		// Place gold mine

		rmPlaceObjectDefAtLoc(playerGoldID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

		// Placing starting trees...
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

		rmPlaceObjectDefAtLoc(StartDeerID1, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartDeerID11, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartDeerID11, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

		rmSetNuggetDifficulty(1, 1);
		rmPlaceObjectDefAtLoc(playerNuggetID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(playerNuggetID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	}



							   rmSetStatusText("",0.60);


// ------------------------------------------------------ Ressources ---------------------------------------------------------------------------

// ********************* Mines ***************************************

	for (i=0; < cNumberNonGaiaPlayers*3)
	{
	int silverID2 = rmCreateObjectDef("4 other mines"+i);
	rmAddObjectDefItem(silverID2, "MineCopper", 1, 0);
	rmSetObjectDefMinDistance(silverID2, 0.0);
	rmSetObjectDefMaxDistance(silverID2, rmXFractionToMeters(0.41));
	rmAddObjectDefConstraint(silverID2, avoidTradeRoute);
	rmAddObjectDefConstraint(silverID2, avoidSocketMore);
	rmAddObjectDefConstraint(silverID2, avoidCliffs1);
	rmAddObjectDefConstraint(silverID2, avoidCoin);
	rmAddObjectDefConstraint(silverID2, avoidTownCenterFar);
	rmPlaceObjectDefAtLoc(silverID2, 0, 0.5, 0.5);
    }

	
// ****************** Forest ********************************************
   int forestTreeID = 0;
    failCount=0;
   for (i=0; <cNumberNonGaiaPlayers*10)
      {   
         int forest=rmCreateArea("forest "+i);
         rmSetAreaWarnFailure(forest, false);
         rmSetAreaSize(forest, rmAreaTilesToFraction(150), rmAreaTilesToFraction(350));
         rmSetAreaForestType(forest, "texas forest");
         rmSetAreaForestDensity(forest, 0.5);
         rmSetAreaForestClumpiness(forest, 0.0);
         rmSetAreaForestUnderbrush(forest, 0.0);
         rmSetAreaMinBlobs(forest, 1);
         rmSetAreaMaxBlobs(forest, 4);
         rmSetAreaMinBlobDistance(forest, 16.0);
         rmSetAreaMaxBlobDistance(forest, 20.0);
         rmSetAreaCoherence(forest, 0.4);
         rmSetAreaSmoothDistance(forest, 10);
         rmAddAreaToClass(forest, rmClassID("classForest")); 
         rmAddAreaConstraint(forest, forestConstraint);
         rmAddAreaConstraint(forest, avoidAll);
		 rmAddAreaConstraint(forest, avoidCenter);
		 rmAddAreaConstraint(forest, avoidCoinShort);
         rmAddAreaConstraint(forest, shortAvoidImpassableLand);
         rmAddAreaConstraint(forest, avoidTradeRouteSmall);
rmAddAreaConstraint(forest, avoidTownCenterFar1);			 
         if(rmBuildArea(forest)==false)
         {
            // Stop trying once we fail 3 times in a row.
            failCount++;
            if(failCount==5)
               break;
         }
         else
            failCount=0; 
      }

// ***************************** Herds ********************************	
if (cNumberNonGaiaPlayers==2)
{	     
	int BisonID=rmCreateObjectDef("Bison herd 1v1");
    rmAddObjectDefItem(BisonID, "Bison", rmRandInt(8,9), 8.0);
    rmSetObjectDefMinDistance(BisonID, 0.0);
    rmSetObjectDefMaxDistance(BisonID, 15);
	rmAddObjectDefConstraint(BisonID, avoidAll);
    rmAddObjectDefConstraint(BisonID, avoidImpassableLand);
	rmAddObjectDefConstraint(BisonID, avoidSocket);
	rmAddObjectDefConstraint(BisonID, avoidCliffs1);
	rmAddObjectDefConstraint(BisonID, avoidCoinShort);
    rmSetObjectDefCreateHerd(BisonID, true);
	rmPlaceObjectDefAtLoc(BisonID, 0, 0.55, 0.45, 1);
	rmPlaceObjectDefAtLoc(BisonID, 0, 0.45, 0.55, 1);
	rmPlaceObjectDefAtLoc(BisonID, 0, 0.95, 0.5, 1);
	rmPlaceObjectDefAtLoc(BisonID, 0, 0.05, 0.5, 1);

    int DeerID=rmCreateObjectDef("Deer herd 1v1");
    rmAddObjectDefItem(DeerID, "Deer", rmRandInt(7,9), 8.0);
    rmSetObjectDefMinDistance(DeerID, 0.0);
    rmSetObjectDefMaxDistance(DeerID, 10);
	rmAddObjectDefConstraint(DeerID, avoidAll);
    rmAddObjectDefConstraint(DeerID, avoidImpassableLand);
	rmAddObjectDefConstraint(DeerID, avoidSocket);
	rmAddObjectDefConstraint(DeerID, avoidCliffs1);
	rmAddObjectDefConstraint(DeerID, avoidCoinShort);
    rmSetObjectDefCreateHerd(DeerID, true);
	
	if (mapVersion==1)
	{
	rmPlaceObjectDefAtLoc(DeerID, 0, 0.72, 0.15, 1);
	rmPlaceObjectDefAtLoc(DeerID, 0, 0.7, 0.2, 1);
	rmPlaceObjectDefAtLoc(DeerID, 0, 0.3, 0.8, 1);
	rmPlaceObjectDefAtLoc(DeerID, 0, 0.3, 0.8, 1);
	}
	else
	{
	rmPlaceObjectDefAtLoc(DeerID, 0, 0.6, 0.25, 1);
	rmPlaceObjectDefAtLoc(DeerID, 0, 0.25, 0.6, 1);
    rmPlaceObjectDefAtLoc(DeerID, 0, 0.8, 0.4, 1);
	rmPlaceObjectDefAtLoc(DeerID, 0, 0.4, 0.8, 1);
	}
}
else
{
	int BisonIDT=rmCreateObjectDef("Bison team");
    rmAddObjectDefItem(BisonIDT, "Bison", rmRandInt(9,11), 8.0);
    rmSetObjectDefMinDistance(BisonIDT, 0.0);
    rmSetObjectDefMaxDistance(BisonIDT, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(BisonIDT, avoidAll);
    rmAddObjectDefConstraint(BisonIDT, avoidImpassableLand);
	rmAddObjectDefConstraint(BisonIDT, avoidSocket);
	rmAddObjectDefConstraint(BisonIDT, avoidTownCenterFar);
	rmAddObjectDefConstraint(BisonIDT, avoidCliffs1);
	rmAddObjectDefConstraint(BisonIDT, avoidDeer);
	rmAddObjectDefConstraint(BisonIDT, avoidBison);
	rmAddObjectDefConstraint(BisonIDT, avoidCoinShort);
    rmSetObjectDefCreateHerd(BisonIDT, true);
	rmPlaceObjectDefAtLoc(BisonIDT, 0, 0.5, 0.5, cNumberNonGaiaPlayers*3);

    int DeerIDT=rmCreateObjectDef("Deer team");
    rmAddObjectDefItem(DeerIDT, "Deer", rmRandInt(9,11), 8.0);
    rmSetObjectDefMinDistance(DeerIDT, 0.0);
    rmSetObjectDefMaxDistance(DeerIDT, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(DeerIDT, avoidAll);
    rmAddObjectDefConstraint(DeerIDT, avoidImpassableLand);
	rmAddObjectDefConstraint(DeerIDT, avoidDeer);
	rmAddObjectDefConstraint(DeerIDT, avoidBison);
	rmAddObjectDefConstraint(DeerIDT, avoidSocket);
	rmAddObjectDefConstraint(DeerIDT, avoidTownCenterFar);
	rmAddObjectDefConstraint(DeerIDT, avoidCliffs1);
	rmAddObjectDefConstraint(DeerIDT, avoidCoinShort);
    rmSetObjectDefCreateHerd(DeerIDT, true);
	rmPlaceObjectDefAtLoc(DeerIDT, 0, 0.4, 0.7, cNumberNonGaiaPlayers*2);	
	
}	
// ------------------------------------------------------ Natives & Nuggets & Design --------------------------------------------------------------------------- 


//	---------------------------------------------------- Nuggets ----------------------------------------------------------------------------------------

		rmSetNuggetDifficulty(1, 1);		
		int nugget3= rmCreateObjectDef("nugget easy center"); 
		rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
		rmSetNuggetDifficulty(1, 1);
		rmSetObjectDefMinDistance(nugget3, 0.0);
		rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.1));
		rmAddObjectDefToClass(nugget3, rmClassID("classNugget"));		
		rmAddObjectDefConstraint(nugget3, avoidNuggetSmall1);
		rmAddObjectDefConstraint(nugget3, avoidAll);
		rmAddObjectDefConstraint(nugget3, avoidTradeRoute);
		rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);


		rmSetNuggetDifficulty(2, 2);
		int nugget4= rmCreateObjectDef("nugget hard");
		rmSetNuggetDifficulty(2, 2);
		rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
		rmSetObjectDefMinDistance(nugget4, 0.3);
		rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.5));
		rmAddObjectDefToClass(nugget4, rmClassID("classNugget"));
		rmAddObjectDefConstraint(nugget4, avoidNugget);
		rmAddObjectDefConstraint(nugget4, avoidTownCenterFar2);
		rmAddObjectDefConstraint(nugget4, avoidCliffs);
		rmAddObjectDefConstraint(nugget4, avoidAll);
		rmAddObjectDefConstraint(nugget4, avoidTradeRoute);
		rmAddAreaConstraint(nugget4, avoidCoinShort);
		rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3*cNumberNonGaiaPlayers);


// ------------------------------------------------------ TRIGGER and OP TAPIR---------------------------------------------------------------------------
/*
    rmCreateTrigger("NoExp");
    rmSwitchToTrigger(rmTriggerID("NoExp"));
    rmSetTriggerPriority(4);
    rmSetTriggerActive(true);
    rmSetTriggerRunImmediately(true);
    rmSetTriggerLoop(false);   
    rmAddTriggerCondition("Always");
    rmAddTriggerEffect("Modify Protounit");
	rmSetTriggerEffectParam("ProtoUnit", "Saloon");
    rmSetTriggerEffectParamInt("PlayerID", 0);
    rmSetTriggerEffectParamInt("Field", 5);
	rmSetTriggerEffectParamFloat("Delta", 100);*/
// ------------------------------------------------------ KOTH for noobs ---------------------------------------------------------------------

  if(rmGetIsKOTH()) {
    
    int randLoc = rmRandInt(1,3);
    float xLoc = 0.5;
    float yLoc = 0.5;
    float walk = 0.075;
    
    ypKingsHillPlacer(xLoc, yLoc, walk, 0);
    rmEchoInfo("XLOC = "+xLoc);
    rmEchoInfo("XLOC = "+yLoc);
  }
// ------------------------------------------------------ Finishing the OP UI ---------------------------------------------------------------------------


  							 rmSetStatusText("",1.0);
}  


// ***********************************************************************************************************************************************
// ****************************************************** E N D **********************************************************************************
// ***********************************************************************************************************************************************