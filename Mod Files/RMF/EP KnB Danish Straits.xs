// RM script of Danish Straits
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Chooses the 2 natives: 1 Jutes and 1 Frisians

   // Native 1
   rmSetSubCiv(0, "Comanche");

   // Native 2
   rmSetSubCiv(1, "Comanche");


   // Picks the map size
   int playerTiles = 20000;
   if (cNumberNonGaiaPlayers == 8)
	playerTiles = 14000;
   else if (cNumberNonGaiaPlayers == 7)
	playerTiles = 15000;
   else if (cNumberNonGaiaPlayers == 6)
	playerTiles = 16000;
   else if (cNumberNonGaiaPlayers == 5)
	playerTiles = 17000;
   else if (cNumberNonGaiaPlayers == 4)
	playerTiles = 18000;
   else if (cNumberNonGaiaPlayers == 3)
	playerTiles = 19000;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses the side teams start
   int teamSide = rmRandInt(1,2);


   // Very VERY complex FFA player placement random integers, only I can really understand these! :P
   int playerPlace3 = rmRandInt(1,3);
   int playerPlace4 = rmRandInt(1,2);
   int playerPlace5 = rmRandInt(1,5);
   int playerPlace6 = rmRandInt(1,3);
   int playerPlace7 = rmRandInt(1,7);
   int playerPlace8 = rmRandInt(1,4);


   // Chooses the side the players start in FFA - this is also complex! :)
   int playerSide = rmRandInt(1,2);


   // If 5 or 7 player FFA game, changes the starting place of the last (5th or 7th) player. Bad explanation, isn't it? :D
   int oddPlayer = rmRandInt(1,2);


   // Chooses summer or winter 
   int seasonPicker = rmRandInt(0,1); // 0 = summer, 1 = winter


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("new england");


   // Picks default terrain and map type
   rmSetMapElevationParameters(cElevTurbulence, 0.10, 4, 0.4, 8.0);
   if (seasonPicker == 0)
   {
      rmSetSeaType("great lakes");
      rmTerrainInitialize("water");
      rmSetMapType("grass");
      rmSetMapType("water");
   }
   else
   {
      rmSetBaseTerrainMix("yukon_snow");
      rmTerrainInitialize("great_lakes\ground_ice1_gl", 1.0);
      rmSetMapType("snow");
      rmSetGlobalSnow( 1.0 );
   }
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   int classIsland=rmDefineClass("island");
   int classLand=rmDefineClass("land");
   int classSockets=rmDefineClass("sockets");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("nuggets");
   rmDefineClass("natives");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));
   int edgeConstraint=rmCreatePieConstraint("edge Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.44), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 35.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 60.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 55.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 5.0);
   int avoidIce=rmCreateTerrainDistanceConstraint("avoid ice", "Water", true, 5.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 40.0);

   // Area constraints
   int islandConstraint=rmCreateClassDistanceConstraint("stay away from islands", classIsland, 50.0);
   int shortIslandConstraint=rmCreateClassDistanceConstraint("stay away from islands short", classIsland, 20.0);
   int landConstraint=rmCreateClassDistanceConstraint("stay away from land", classLand, 30.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);

   // Trade route avoidance
   int avoidTradeRoute=rmCreateTradeRouteDistanceConstraint("trade route", 10.0);
   int avoidTradeSockets=rmCreateTypeDistanceConstraint("avoid trade sockets", "SPCDrydock", 5.0);
   int socketsAvoidTradeSockets=rmCreateClassDistanceConstraint("sockets avoid trade sockets", classSockets, 110.0);

   // Native avoidance
   int avoidNatives=rmCreateClassDistanceConstraint("things avoids natives", rmClassID("natives"), 5.0);

   // Water objects avoidance	
   int fishVsFish=rmCreateTypeDistanceConstraint("fish v fish", "FishSalmon", 25.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish land", "land", true, 5.0);
   int whaleVsWhale=rmCreateTypeDistanceConstraint("whale vs whale", "HumpbackWhale", 60.0);
   int whaleLand = rmCreateTerrainDistanceConstraint("whale land", "land", true, 10.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag vs land", "land", true, 15.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid same", "HomeCityWaterSpawnFlag", 25.0);


   // Text
   rmSetStatusText("",0.10);


   // Define and place land to north-east (Sweden)

   int landID=rmCreateArea("Sweden");
   if (seasonPicker == 0)
      rmSetAreaMix(landID, "carolina_grass");
   else
   {
      rmSetAreaTerrainType(landID, "yukon\ground1_yuk");
      rmSetAreaMix(landID, "yukon_snow");
   }
   rmSetAreaSize(landID, 0.10, 0.15);
   rmSetAreaLocation(landID, 1.0, 0.5);
   rmAddAreaInfluenceSegment(landID, 0.8, 0.55, 0.8, 0.45); 
   rmSetAreaBaseHeight(landID, 1.0);
   rmSetAreaCoherence(landID, 0.5);
   rmSetAreaSmoothDistance(landID, 20);
   rmSetAreaMinBlobs(landID, 5);
   rmSetAreaMaxBlobs(landID, 20);
   rmSetAreaMinBlobDistance(landID, 5);
   rmSetAreaMaxBlobDistance(landID, 20);
   rmSetAreaObeyWorldCircleConstraint(landID, false);
   rmAddAreaToClass(landID, classLand);
   rmSetAreaElevationType(landID, cElevTurbulence);
   rmSetAreaElevationVariation(landID, 5.0);
   rmSetAreaElevationMinFrequency(landID, 0.10);
   rmSetAreaElevationOctaves(landID, 3);
   rmSetAreaElevationPersistence(landID, 0.2);
   rmSetAreaElevationNoiseBias(landID, 1);
   rmBuildArea(landID);


   // Define and place land to south-west (Denmark)

   int land2ID=rmCreateArea("Denmark");
   if (seasonPicker == 0)
      rmSetAreaMix(land2ID, "carolina_grass");
   else
   {
      rmSetAreaTerrainType(land2ID, "yukon\ground1_yuk");
      rmSetAreaMix(land2ID, "yukon_snow");
   }
   rmSetAreaSize(land2ID, 0.1, 0.1);
   rmSetAreaLocation(land2ID, 0.0, 0.5);
   rmAddAreaInfluenceSegment(land2ID, 0.0, 0.5, 0.1, 1.0); 
   rmSetAreaBaseHeight(land2ID, 1.0);
   rmSetAreaCoherence(land2ID, 0.5);
   rmSetAreaSmoothDistance(land2ID, 10);
   rmSetAreaMinBlobs(land2ID, 5);
   rmSetAreaMaxBlobs(land2ID, 20);
   rmSetAreaMinBlobDistance(land2ID, 5);
   rmSetAreaMaxBlobDistance(land2ID, 20);
   rmSetAreaObeyWorldCircleConstraint(land2ID, false);
   rmAddAreaToClass(land2ID, classLand);
   rmSetAreaElevationType(land2ID, cElevTurbulence);
   rmSetAreaElevationVariation(land2ID, 5.0);
   rmSetAreaElevationMinFrequency(land2ID, 0.10);
   rmSetAreaElevationOctaves(land2ID, 3);
   rmSetAreaElevationPersistence(land2ID, 0.2);
   rmSetAreaElevationNoiseBias(land2ID, 1);
   rmBuildArea(land2ID);

   int land3ID=rmCreateArea("Denmark2");
   if (seasonPicker == 0)
      rmSetAreaMix(land3ID, "carolina_grass");
   else
   {
      rmSetAreaTerrainType(land3ID, "yukon\ground1_yuk");
      rmSetAreaMix(land3ID, "yukon_snow");
   }
   rmSetAreaSize(land3ID, 0.1, 0.1);
   rmSetAreaLocation(land3ID, 0.0, 0.5);
   rmAddAreaInfluenceSegment(land3ID, 0.0, 0.5, 0.3, 0.0); 
   rmSetAreaBaseHeight(land3ID, 1.0);
   rmSetAreaCoherence(land3ID, 0.5);
   rmSetAreaSmoothDistance(land3ID, 10);
   rmSetAreaMinBlobs(land3ID, 5);
   rmSetAreaMaxBlobs(land3ID, 20);
   rmSetAreaMinBlobDistance(land3ID, 5);
   rmSetAreaMaxBlobDistance(land3ID, 20);
   rmSetAreaObeyWorldCircleConstraint(land3ID, false);
   rmAddAreaToClass(land3ID, classLand);
   rmSetAreaElevationType(land3ID, cElevTurbulence);
   rmSetAreaElevationVariation(land3ID, 5.0);
   rmSetAreaElevationMinFrequency(land3ID, 0.10);
   rmSetAreaElevationOctaves(land3ID, 3);
   rmSetAreaElevationPersistence(land3ID, 0.2);
   rmSetAreaElevationNoiseBias(land3ID, 1);
   rmBuildArea(land3ID);


   // Define and place water trade route

   if (seasonPicker == 0)
   {
      // Create the water trade route
      int tradeRouteID = rmCreateTradeRoute();
      int socketID=rmCreateObjectDef("sockets to dock trade posts");
      rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

      // Define the sockets
      rmAddObjectDefItem(socketID, "SPCDrydock", 1, 5.0);
      rmSetObjectDefAllowOverlap(socketID, true);
      rmSetObjectDefMinDistance(socketID, 0.0);
      rmSetObjectDefMaxDistance(socketID, 300.0);
      rmAddObjectDefToClass(socketID, classSockets);
      rmAddObjectDefConstraint(socketID, avoidImpassableLand);
      rmAddObjectDefConstraint(socketID, avoidAll);
      rmAddObjectDefConstraint(socketID, socketsAvoidTradeSockets);
      rmAddObjectDefConstraint(socketID, landConstraint);

      // Set the water trade route waypoints
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 1.0);
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.5, 0.0, 45, 45);
      bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
      if(placedTradeRoute == false)
            rmEchoError("Failed to place trade route");
   }


   // Define and place random islands

   // Big islands
   float islandFraction2=rmAreaTilesToFraction(7000);
   for(i=1; < 3)
   {
      int islands2=rmCreateArea("random island big"+i);
      rmSetAreaSize(islands2, islandFraction2, islandFraction2);
      rmAddAreaToClass(islands2, classIsland);
      if (seasonPicker == 0)
            rmSetAreaMix(islands2, "greatlakes_grass");
      else
      {
            rmSetAreaTerrainType(islands2, "yukon\ground1_yuk");
            rmSetAreaMix(islands2, "yukon_snow");
      }
      rmSetAreaBaseHeight(islands2, 1.0);
      rmAddAreaConstraint(islands2, islandConstraint); 
      rmAddAreaConstraint(islands2, landConstraint);
      rmAddAreaConstraint(islands2, avoidTradeRoute);
      rmAddAreaConstraint(islands2, edgeConstraint);
      rmSetAreaCoherence(islands2, 0.7);
      rmSetAreaElevationType(islands2, cElevTurbulence);
      rmSetAreaElevationVariation(islands2, 5.0);
      rmSetAreaElevationMinFrequency(islands2, 0.10);
      rmSetAreaElevationOctaves(islands2, 3);
      rmSetAreaElevationPersistence(islands2, 0.2);
      rmSetAreaElevationNoiseBias(islands2, 1);
      rmBuildArea(islands2);
   }

   // Small islands
   float islandFraction=rmAreaTilesToFraction(1500);
   for(i=1; < 6)
   {
      int islands=rmCreateArea("random island"+i);
      rmSetAreaSize(islands, islandFraction, islandFraction);
      rmAddAreaToClass(islands, classIsland);
      if (seasonPicker == 0)
            rmSetAreaMix(islands, "greatlakes_grass");
      else
      {
            rmSetAreaTerrainType(islands, "yukon\ground1_yuk");
            rmSetAreaMix(islands, "yukon_snow");
      }
      rmSetAreaBaseHeight(islands, 1.0);
      rmAddAreaConstraint(islands, islandConstraint); 
      rmAddAreaConstraint(islands, landConstraint);
      rmAddAreaConstraint(islands, avoidTradeRoute);
      rmAddAreaConstraint(islands, edgeConstraint);
      rmSetAreaCoherence(islands, 0.6);
      rmSetAreaElevationType(islands, cElevTurbulence);
      rmSetAreaElevationVariation(islands, 5.0);
      rmSetAreaElevationMinFrequency(islands, 0.10);
      rmSetAreaElevationOctaves(islands, 3);
      rmSetAreaElevationPersistence(islands, 0.2);
      rmSetAreaElevationNoiseBias(islands, 1);
      rmBuildArea(islands);
   }

   // Tiny islands
   float islandFraction3=rmAreaTilesToFraction(500);
   for(i=1; < 15)
   {
      int islands3=rmCreateArea("random island tiny"+i);
      rmSetAreaSize(islands3, islandFraction3, islandFraction3);
      rmAddAreaToClass(islands3, classIsland);
      if (seasonPicker == 0)
            rmSetAreaMix(islands3, "greatlakes_grass");
      else
      {
            rmSetAreaTerrainType(islands3, "yukon\ground1_yuk");
            rmSetAreaMix(islands3, "yukon_snow");
      }
      rmSetAreaBaseHeight(islands3, 1.0);
      rmAddAreaConstraint(islands3, shortIslandConstraint); 
      rmAddAreaConstraint(islands3, landConstraint);
      rmAddAreaConstraint(islands3, avoidTradeRoute);
      rmAddAreaConstraint(islands3, edgeConstraint);
      rmSetAreaCoherence(islands3, 0.6);
      rmSetAreaElevationType(islands3, cElevTurbulence);
      rmSetAreaElevationVariation(islands3, 5.0);
      rmSetAreaElevationMinFrequency(islands3, 0.10);
      rmSetAreaElevationOctaves(islands3, 3);
      rmSetAreaElevationPersistence(islands3, 0.2);
      rmSetAreaElevationNoiseBias(islands3, 1);
   }


   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.40);


   // Place players - in team and FFA games

   // Player placement if FFA (Free For All) OR two player game - manual placement
   if(cNumberTeams > 2 || cNumberNonGaiaPlayers == 2)
   {
      if (cNumberNonGaiaPlayers == 2) // Place in middle of both "sides"
      {
            if (teamSide == 1)
            {
                        rmPlacePlayer(1, 0.8, 0.5);
                        rmPlacePlayer(2, 0.1, 0.5);
            }
            else if (teamSide == 2)
            {
                        rmPlacePlayer(1, 0.1, 0.5);
                        rmPlacePlayer(2, 0.8, 0.5);
            }
      }
      if (cNumberNonGaiaPlayers == 3) // One player in middle of own "side", two shares their side
      {
            if (playerPlace3 == 1) // Player 2 in middle of own "side"
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.1, 0.4);
                                                rmPlacePlayer(2, 0.8, 0.5);
                                                rmPlacePlayer(3, 0.1, 0.6);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.8, 0.4);
                                                rmPlacePlayer(2, 0.1, 0.5);
                                                rmPlacePlayer(3, 0.8, 0.6);
                        }
            }
            else if (playerPlace3 == 2) // Player 1 in middle of own "side"
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.8, 0.5);
                                                rmPlacePlayer(2, 0.1, 0.4);
                                                rmPlacePlayer(3, 0.1, 0.6);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.1, 0.5);
                                                rmPlacePlayer(2, 0.8, 0.4);
                                                rmPlacePlayer(3, 0.8, 0.6);
                        }
            }
            else if (playerPlace3 == 3) // Player 3 in middle of own "side"
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.1, 0.4);
                                                rmPlacePlayer(2, 0.1, 0.6);
                                                rmPlacePlayer(3, 0.8, 0.5);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.8, 0.4);
                                                rmPlacePlayer(2, 0.8, 0.6);
                                                rmPlacePlayer(3, 0.1, 0.5);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 4) // Normal balanced placement
      {
            if (playerPlace4 == 1)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.8, 0.4);
                                                rmPlacePlayer(2, 0.8, 0.6);
                                                rmPlacePlayer(3, 0.1, 0.4);
                                                rmPlacePlayer(4, 0.1, 0.6);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.1, 0.4);
                                                rmPlacePlayer(2, 0.1, 0.6);
                                                rmPlacePlayer(3, 0.8, 0.4);
                                                rmPlacePlayer(4, 0.8, 0.6);
                        }
            }
            else if (playerPlace4 == 2)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.8, 0.6);
                                                rmPlacePlayer(2, 0.8, 0.4);
                                                rmPlacePlayer(3, 0.1, 0.6);
                                                rmPlacePlayer(4, 0.1, 0.4);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.1, 0.6);
                                                rmPlacePlayer(2, 0.1, 0.4);
                                                rmPlacePlayer(3, 0.8, 0.6);
                                                rmPlacePlayer(4, 0.8, 0.4);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 5) // Same as 4-player, but one near the sea
      {
            if (playerPlace5 == 1) // Player 1 near sea
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(1, 0.7, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(1, 0.1, 0.5);
                                                rmPlacePlayer(2, 0.9, 0.4);
                                                rmPlacePlayer(3, 0.9, 0.6);
                                                rmPlacePlayer(4, 0.1, 0.35);
                                                rmPlacePlayer(5, 0.1, 0.65);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(1, 0.1, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(1, 0.7, 0.5);
                                                rmPlacePlayer(2, 0.1, 0.35);
                                                rmPlacePlayer(3, 0.1, 0.65);
                                                rmPlacePlayer(4, 0.9, 0.4);
                                                rmPlacePlayer(5, 0.9, 0.6);
                        }
            }
            else if (playerPlace5 == 2) // Player 2 near sea
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.9, 0.4);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(2, 0.7, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(2, 0.1, 0.5);
                                                rmPlacePlayer(3, 0.9, 0.6);
                                                rmPlacePlayer(4, 0.1, 0.35);
                                                rmPlacePlayer(5, 0.1, 0.65);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.1, 0.35);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(2, 0.1, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(2, 0.7, 0.5);
                                                rmPlacePlayer(3, 0.1, 0.65);
                                                rmPlacePlayer(4, 0.9, 0.4);
                                                rmPlacePlayer(5, 0.9, 0.6);
                        }
            }
            else if (playerPlace5 == 3) // Player 3 near sea
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.9, 0.4);
                                                rmPlacePlayer(2, 0.9, 0.6);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(3, 0.7, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(3, 0.1, 0.5);
                                                rmPlacePlayer(4, 0.1, 0.35);
                                                rmPlacePlayer(5, 0.1, 0.65);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.1, 0.35);
                                                rmPlacePlayer(2, 0.1, 0.65);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(3, 0.1, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(3, 0.7, 0.5);
                                                rmPlacePlayer(4, 0.9, 0.4);
                                                rmPlacePlayer(5, 0.9, 0.6);
                        }
            }
            else if (playerPlace5 == 4) // Player 4 near sea
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.9, 0.4);
                                                rmPlacePlayer(2, 0.9, 0.6);
                                                rmPlacePlayer(3, 0.1, 0.35);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(4, 0.7, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(4, 0.1, 0.5);
                                                rmPlacePlayer(5, 0.1, 0.65);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.1, 0.35);
                                                rmPlacePlayer(2, 0.1, 0.65);
                                                rmPlacePlayer(3, 0.9, 0.4);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(4, 0.1, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(4, 0.7, 0.5);
                                                rmPlacePlayer(5, 0.9, 0.6);
                        }
            }
            else if (playerPlace5 == 5) // Player 5 near sea
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.9, 0.4);
                                                rmPlacePlayer(2, 0.9, 0.6);
                                                rmPlacePlayer(3, 0.1, 0.35);
                                                rmPlacePlayer(4, 0.1, 0.65);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(5, 0.7, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(5, 0.1, 0.5);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.1, 0.35);
                                                rmPlacePlayer(2, 0.1, 0.65);
                                                rmPlacePlayer(3, 0.9, 0.4);
                                                rmPlacePlayer(4, 0.9, 0.6);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(5, 0.1, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(5, 0.7, 0.5);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 6) // Normal balanced placement
      {
            if (playerPlace6 == 1)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.7, 0.5);
                                                rmPlacePlayer(2, 0.1, 0.5);
                                                rmPlacePlayer(3, 0.9, 0.4);
                                                rmPlacePlayer(4, 0.9, 0.6);
                                                rmPlacePlayer(5, 0.1, 0.35);
                                                rmPlacePlayer(6, 0.1, 0.65);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.1, 0.5);
                                                rmPlacePlayer(2, 0.7, 0.5);
                                                rmPlacePlayer(3, 0.1, 0.35);
                                                rmPlacePlayer(4, 0.1, 0.65);
                                                rmPlacePlayer(5, 0.9, 0.4);
                                                rmPlacePlayer(6, 0.9, 0.6);
                        }
            }
            else if (playerPlace6 == 2)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(5, 0.7, 0.5);
                                                rmPlacePlayer(6, 0.1, 0.5);
                                                rmPlacePlayer(1, 0.9, 0.4);
                                                rmPlacePlayer(2, 0.9, 0.6);
                                                rmPlacePlayer(3, 0.1, 0.35);
                                                rmPlacePlayer(4, 0.1, 0.65);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(5, 0.1, 0.5);
                                                rmPlacePlayer(6, 0.7, 0.5);
                                                rmPlacePlayer(1, 0.1, 0.35);
                                                rmPlacePlayer(2, 0.1, 0.65);
                                                rmPlacePlayer(3, 0.9, 0.4);
                                                rmPlacePlayer(4, 0.9, 0.6);
                        }
            }
            else if (playerPlace6 == 3)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(3, 0.7, 0.5);
                                                rmPlacePlayer(4, 0.1, 0.5);
                                                rmPlacePlayer(5, 0.9, 0.4);
                                                rmPlacePlayer(6, 0.9, 0.6);
                                                rmPlacePlayer(1, 0.1, 0.35);
                                                rmPlacePlayer(2, 0.1, 0.65);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(3, 0.1, 0.5);
                                                rmPlacePlayer(4, 0.7, 0.5);
                                                rmPlacePlayer(5, 0.1, 0.35);
                                                rmPlacePlayer(6, 0.1, 0.65);
                                                rmPlacePlayer(1, 0.9, 0.4);
                                                rmPlacePlayer(2, 0.9, 0.6);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 7) // 4 players in square in other side, 3 players in one side like 5-player
      {
            if (playerPlace7 == 1) // Player 1 in either side
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.9, 0.4);
                                                rmPlacePlayer(2, 0.9, 0.6);
                                                rmPlacePlayer(3, 0.7, 0.4);
                                                rmPlacePlayer(4, 0.7, 0.6);
                                                rmPlacePlayer(5, 0.1, 0.35);
                                                rmPlacePlayer(6, 0.1, 0.5);
                                                rmPlacePlayer(7, 0.1, 0.65);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.1, 0.5);
                                                rmPlacePlayer(2, 0.1, 0.35);
                                                rmPlacePlayer(3, 0.1, 0.65);
                                                rmPlacePlayer(4, 0.9, 0.4);
                                                rmPlacePlayer(5, 0.9, 0.6);
                                                rmPlacePlayer(6, 0.7, 0.4);
                                                rmPlacePlayer(7, 0.7, 0.6);
                        }
            }
            else if (playerPlace7 == 2) // Player 2 in either side
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(7, 0.9, 0.4);
                                                rmPlacePlayer(1, 0.9, 0.6);
                                                rmPlacePlayer(2, 0.7, 0.4);
                                                rmPlacePlayer(3, 0.7, 0.6);
                                                rmPlacePlayer(4, 0.1, 0.35);
                                                rmPlacePlayer(5, 0.1, 0.5);
                                                rmPlacePlayer(6, 0.1, 0.65);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(7, 0.1, 0.5);
                                                rmPlacePlayer(1, 0.1, 0.35);
                                                rmPlacePlayer(2, 0.1, 0.65);
                                                rmPlacePlayer(3, 0.9, 0.4);
                                                rmPlacePlayer(4, 0.9, 0.6);
                                                rmPlacePlayer(5, 0.7, 0.4);
                                                rmPlacePlayer(6, 0.7, 0.6);
                        }
            }
            else if (playerPlace7 == 3) // Player 3 in either side
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(6, 0.9, 0.4);
                                                rmPlacePlayer(7, 0.9, 0.6);
                                                rmPlacePlayer(1, 0.7, 0.4);
                                                rmPlacePlayer(2, 0.7, 0.6);
                                                rmPlacePlayer(3, 0.1, 0.35);
                                                rmPlacePlayer(4, 0.1, 0.5);
                                                rmPlacePlayer(5, 0.1, 0.65);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(6, 0.1, 0.5);
                                                rmPlacePlayer(7, 0.1, 0.35);
                                                rmPlacePlayer(1, 0.1, 0.65);
                                                rmPlacePlayer(2, 0.9, 0.4);
                                                rmPlacePlayer(3, 0.9, 0.6);
                                                rmPlacePlayer(4, 0.7, 0.4);
                                                rmPlacePlayer(5, 0.7, 0.6);
                        }
            }
            else if (playerPlace7 == 4) // Player 4 in either side
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(5, 0.9, 0.4);
                                                rmPlacePlayer(6, 0.9, 0.6);
                                                rmPlacePlayer(7, 0.7, 0.4);
                                                rmPlacePlayer(1, 0.7, 0.6);
                                                rmPlacePlayer(2, 0.1, 0.35);
                                                rmPlacePlayer(3, 0.1, 0.5);
                                                rmPlacePlayer(4, 0.1, 0.65);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(5, 0.1, 0.5);
                                                rmPlacePlayer(6, 0.1, 0.35);
                                                rmPlacePlayer(7, 0.1, 0.65);
                                                rmPlacePlayer(1, 0.9, 0.4);
                                                rmPlacePlayer(2, 0.9, 0.6);
                                                rmPlacePlayer(3, 0.7, 0.4);
                                                rmPlacePlayer(4, 0.7, 0.6);
                        }
            }
            else if (playerPlace7 == 5) // Player 5 in either side
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(4, 0.9, 0.4);
                                                rmPlacePlayer(5, 0.9, 0.6);
                                                rmPlacePlayer(6, 0.7, 0.4);
                                                rmPlacePlayer(7, 0.7, 0.6);
                                                rmPlacePlayer(1, 0.1, 0.35);
                                                rmPlacePlayer(2, 0.1, 0.5);
                                                rmPlacePlayer(3, 0.1, 0.65);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(4, 0.1, 0.5);
                                                rmPlacePlayer(5, 0.1, 0.35);
                                                rmPlacePlayer(6, 0.1, 0.65);
                                                rmPlacePlayer(7, 0.9, 0.4);
                                                rmPlacePlayer(1, 0.9, 0.6);
                                                rmPlacePlayer(2, 0.7, 0.4);
                                                rmPlacePlayer(3, 0.7, 0.6);
                        }
            }
            else if (playerPlace7 == 6) // Player 6 in either side
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(3, 0.9, 0.4);
                                                rmPlacePlayer(4, 0.9, 0.6);
                                                rmPlacePlayer(5, 0.7, 0.4);
                                                rmPlacePlayer(6, 0.7, 0.6);
                                                rmPlacePlayer(7, 0.1, 0.35);
                                                rmPlacePlayer(1, 0.1, 0.5);
                                                rmPlacePlayer(2, 0.1, 0.65);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(3, 0.1, 0.5);
                                                rmPlacePlayer(4, 0.1, 0.35);
                                                rmPlacePlayer(5, 0.1, 0.65);
                                                rmPlacePlayer(6, 0.9, 0.4);
                                                rmPlacePlayer(7, 0.9, 0.6);
                                                rmPlacePlayer(1, 0.7, 0.4);
                                                rmPlacePlayer(2, 0.7, 0.6);
                        }
            }
            else if (playerPlace7 == 7) // Player 7 in either side
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(2, 0.9, 0.4);
                                                rmPlacePlayer(3, 0.9, 0.6);
                                                rmPlacePlayer(4, 0.7, 0.4);
                                                rmPlacePlayer(5, 0.7, 0.6);
                                                rmPlacePlayer(6, 0.1, 0.35);
                                                rmPlacePlayer(7, 0.1, 0.5);
                                                rmPlacePlayer(1, 0.1, 0.65);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(2, 0.1, 0.5);
                                                rmPlacePlayer(3, 0.1, 0.35);
                                                rmPlacePlayer(4, 0.1, 0.65);
                                                rmPlacePlayer(5, 0.9, 0.4);
                                                rmPlacePlayer(6, 0.9, 0.6);
                                                rmPlacePlayer(7, 0.7, 0.4);
                                                rmPlacePlayer(1, 0.7, 0.6);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 8) // Normal balanced placement
      {
            if (playerPlace8 == 1)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.9, 0.4);
                                                rmPlacePlayer(2, 0.9, 0.6);
                                                rmPlacePlayer(3, 0.7, 0.4);
                                                rmPlacePlayer(4, 0.7, 0.6);
                                                rmPlacePlayer(5, 0.1, 0.3);
                                                rmPlacePlayer(6, 0.1, 0.45);
                                                rmPlacePlayer(7, 0.1, 0.55);
                                                rmPlacePlayer(8, 0.1, 0.7);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.1, 0.3);
                                                rmPlacePlayer(2, 0.1, 0.45);
                                                rmPlacePlayer(3, 0.1, 0.55);
                                                rmPlacePlayer(4, 0.1, 0.7);
                                                rmPlacePlayer(5, 0.9, 0.4);
                                                rmPlacePlayer(6, 0.9, 0.6);
                                                rmPlacePlayer(7, 0.7, 0.4);
                                                rmPlacePlayer(8, 0.7, 0.6);
                        }
            }
            else if (playerPlace8 == 2)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(7, 0.9, 0.4);
                                                rmPlacePlayer(8, 0.9, 0.6);
                                                rmPlacePlayer(1, 0.7, 0.4);
                                                rmPlacePlayer(2, 0.7, 0.6);
                                                rmPlacePlayer(3, 0.1, 0.3);
                                                rmPlacePlayer(4, 0.1, 0.45);
                                                rmPlacePlayer(5, 0.1, 0.55);
                                                rmPlacePlayer(6, 0.1, 0.7);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(7, 0.1, 0.3);
                                                rmPlacePlayer(8, 0.1, 0.45);
                                                rmPlacePlayer(1, 0.1, 0.55);
                                                rmPlacePlayer(2, 0.1, 0.7);
                                                rmPlacePlayer(3, 0.9, 0.4);
                                                rmPlacePlayer(4, 0.9, 0.6);
                                                rmPlacePlayer(5, 0.7, 0.4);
                                                rmPlacePlayer(6, 0.7, 0.6);
                        }
            }
            else if (playerPlace8 == 3)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(5, 0.9, 0.4);
                                                rmPlacePlayer(6, 0.9, 0.6);
                                                rmPlacePlayer(7, 0.7, 0.4);
                                                rmPlacePlayer(8, 0.7, 0.6);
                                                rmPlacePlayer(1, 0.1, 0.3);
                                                rmPlacePlayer(2, 0.1, 0.45);
                                                rmPlacePlayer(3, 0.1, 0.55);
                                                rmPlacePlayer(4, 0.1, 0.7);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(5, 0.1, 0.3);
                                                rmPlacePlayer(6, 0.1, 0.45);
                                                rmPlacePlayer(7, 0.1, 0.55);
                                                rmPlacePlayer(8, 0.1, 0.7);
                                                rmPlacePlayer(1, 0.9, 0.4);
                                                rmPlacePlayer(2, 0.9, 0.6);
                                                rmPlacePlayer(3, 0.7, 0.4);
                                                rmPlacePlayer(4, 0.7, 0.6);
                        }
            }
            else if (playerPlace8 == 4)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(3, 0.9, 0.4);
                                                rmPlacePlayer(4, 0.9, 0.6);
                                                rmPlacePlayer(5, 0.7, 0.4);
                                                rmPlacePlayer(6, 0.7, 0.6);
                                                rmPlacePlayer(7, 0.1, 0.3);
                                                rmPlacePlayer(8, 0.1, 0.45);
                                                rmPlacePlayer(1, 0.1, 0.55);
                                                rmPlacePlayer(2, 0.1, 0.7);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(3, 0.1, 0.3);
                                                rmPlacePlayer(4, 0.1, 0.45);
                                                rmPlacePlayer(5, 0.1, 0.55);
                                                rmPlacePlayer(6, 0.1, 0.7);
                                                rmPlacePlayer(7, 0.9, 0.4);
                                                rmPlacePlayer(8, 0.9, 0.6);
                                                rmPlacePlayer(1, 0.7, 0.4);
                                                rmPlacePlayer(2, 0.7, 0.6);
                        }
            }
      }
   }

   // Player placement if teams - place teams in lines
   if(cNumberTeams == 2  && cNumberNonGaiaPlayers >= 3)
   {
      rmSetPlacementTeam(0);
      if (teamSide == 1)
            rmPlacePlayersLine(0.8, 0.35, 0.8, 0.65, 0, 0);
      else
            rmPlacePlayersLine(0.1, 0.35, 0.1, 0.65, 0, 0);

      rmSetPlacementTeam(1);
      if (teamSide == 1)
            rmPlacePlayersLine(0.1, 0.35, 0.1, 0.65, 0, 0);
      else
            rmPlacePlayersLine(0.8, 0.35, 0.8, 0.65, 0, 0);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.50);
		

   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   if (seasonPicker == 0)
      rmAddObjectDefItem(startAreaTreeID, "TreeGreatLakes", 6, 4.0);
   else
      rmAddObjectDefItem(startAreaTreeID, "TreeGreatLakesSnow", 6, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 12);
   rmSetObjectDefMaxDistance(startAreaTreeID, 16);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startDeerID=rmCreateObjectDef("starting deer");
   rmAddObjectDefItem(startDeerID, "deer", 6, 4.0);
   rmSetObjectDefCreateHerd(startDeerID, true);
   rmSetObjectDefMinDistance(startDeerID, 14);
   rmSetObjectDefMaxDistance(startDeerID, 18);
   rmAddObjectDefConstraint(startDeerID, avoidStartResource);
   rmAddObjectDefConstraint(startDeerID, avoidImpassableLand);
   rmAddObjectDefConstraint(startDeerID, shortAvoidStartingUnits);

   int startCopperID=rmCreateObjectDef("player copper");
   rmAddObjectDefItem(startCopperID, "MineCopper", 1, 5.0);
   rmSetObjectDefMinDistance(startCopperID, 14);
   rmSetObjectDefMaxDistance(startCopperID, 18);
   rmAddObjectDefConstraint(startCopperID, avoidStartResource);
   rmAddObjectDefConstraint(startCopperID, avoidImpassableLand);
   rmAddObjectDefConstraint(startCopperID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startDeerID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startCopperID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
      rmAddClosestPointConstraint(flagVsFlag);
      rmAddClosestPointConstraint(flagLand);		
      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Text
   rmSetStatusText("",0.70);


   // Define and place the 2 natives

   int nativeVillageType = rmRandInt(1,5);
   int nativeVillageID = rmCreateGrouping("jutes village", "native comanche village "+nativeVillageType);
   rmSetGroupingMinDistance(nativeVillageID, 0.0);
   rmSetGroupingMaxDistance(nativeVillageID, 10.0);
   rmAddGroupingToClass(nativeVillageID, rmClassID("natives"));
   rmAddGroupingConstraint(nativeVillageID, playerConstraint);
   rmPlaceGroupingInArea(nativeVillageID, 0, rmAreaID("Sweden"));

   int nativeVillageType2 = rmRandInt(1,5);
   int nativeVillage2ID = rmCreateGrouping("frisians village 2", "native comanche village "+nativeVillageType2);
   rmSetGroupingMinDistance(nativeVillage2ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage2ID, 10.0);
   rmAddGroupingToClass(nativeVillage2ID, rmClassID("natives"));
   rmAddGroupingConstraint(nativeVillage2ID, playerConstraint);
   if (rmRandInt(1,2) == 1)
      rmPlaceGroupingInArea(nativeVillage2ID, 0, rmAreaID("Denmark"));
   else
      rmPlaceGroupingInArea(nativeVillage2ID, 0, rmAreaID("Denmark2"));


   // Define and place forests
	
   int numTries=10*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(250), rmAreaTilesToFraction(300));
      if (seasonPicker == 0)
            rmSetAreaForestType(forest, "great lakes forest");
      else
            rmSetAreaForestType(forest, "great lakes forest snow");
      rmSetAreaForestDensity(forest, 1.0);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.2);
      rmSetAreaForestUnderbrush(forest, 0.3);
      rmSetAreaBaseHeight(forest, 1.0);
      rmSetAreaMinBlobs(forest, 1);
      rmSetAreaMaxBlobs(forest, 3);
      rmSetAreaMinBlobDistance(forest, 10);
      rmSetAreaMaxBlobDistance(forest, 15);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, avoidNatives);
      rmAddAreaConstraint(forest, avoidTradeSockets);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.  
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place animals

   // Define and place deer herds to everywhere
   int deerHerdID=rmCreateObjectDef("deer herd");
   rmAddObjectDefItem(deerHerdID, "deer", rmRandInt(6,7), 8.0);
   rmSetObjectDefCreateHerd(deerHerdID, true);
   rmSetObjectDefMinDistance(deerHerdID, 0.0);
   rmSetObjectDefMaxDistance(deerHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(deerHerdID, classAnimals);
   rmAddObjectDefConstraint(deerHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(deerHerdID, animalConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidTradeSockets);
   rmAddObjectDefConstraint(deerHerdID, avoidNatives);
   numTries=cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(deerHerdID, 0, 0.5, 0.5);

   // Define and place elk herds to Denmark
   int elkHerdID=rmCreateObjectDef("elk herd");
   rmAddObjectDefItem(elkHerdID, "elk", rmRandInt(6,7), 8.0);
   rmSetObjectDefCreateHerd(elkHerdID, true);
   rmSetObjectDefMinDistance(elkHerdID, 0.0);
   rmSetObjectDefMaxDistance(elkHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(elkHerdID, classAnimals);
   rmAddObjectDefConstraint(elkHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(elkHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(elkHerdID, animalConstraint);
   rmAddObjectDefConstraint(elkHerdID, avoidTradeSockets);
   rmAddObjectDefConstraint(elkHerdID, avoidNatives);
   numTries=cNumberNonGaiaPlayers/2;
   for (i=0; <numTries)
   {
      rmPlaceObjectDefInArea(elkHerdID, 0, rmAreaID("Denmark"));
      rmPlaceObjectDefInArea(elkHerdID, 0, rmAreaID("Denmark2"));
   }

   // Define and place moose herds to Sweden
   int mooseHerdID=rmCreateObjectDef("moose herd");
   rmAddObjectDefItem(mooseHerdID, "moose", rmRandInt(6,7), 8.0);
   rmSetObjectDefCreateHerd(mooseHerdID, true);
   rmSetObjectDefMinDistance(mooseHerdID, 0.0);
   rmSetObjectDefMaxDistance(mooseHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(mooseHerdID, classAnimals);
   rmAddObjectDefConstraint(mooseHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(mooseHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(mooseHerdID, animalConstraint);
   rmAddObjectDefConstraint(mooseHerdID, avoidTradeSockets);
   rmAddObjectDefConstraint(mooseHerdID, avoidNatives);
   numTries=cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefInArea(mooseHerdID, 0, rmAreaID("Sweden"));


   // Define and place copper mines

   int copperCount = 3*cNumberNonGaiaPlayers;

   for(i=0; < copperCount)
   {
      int copperID=rmCreateObjectDef("copper mine"+i);
      rmAddObjectDefItem(copperID, "MineCopper", 1, 0.0);
      rmSetObjectDefMinDistance(copperID, 0.0);
      rmSetObjectDefMaxDistance(copperID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(copperID, classMines);
      rmAddObjectDefConstraint(copperID, avoidImpassableLand);
      rmAddObjectDefConstraint(copperID, playerConstraint);
      rmAddObjectDefConstraint(copperID, avoidMines);
      rmAddObjectDefConstraint(copperID, avoidTradeSockets);
      rmAddObjectDefConstraint(copperID, avoidNatives);
      rmPlaceObjectDefAtLoc(copperID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.90);
	

   // Define and place fish

   if (seasonPicker == 0)
   {
      int fishID=rmCreateObjectDef("fish");
      rmAddObjectDefItem(fishID, "FishSalmon", 1, 4.0);
      rmSetObjectDefMinDistance(fishID, 0.0);
      rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.5));
      rmAddObjectDefConstraint(fishID, fishVsFish);
      rmAddObjectDefConstraint(fishID, fishLand);
      rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 10*cNumberNonGaiaPlayers);

      // Define and place whales
      int whaleID=rmCreateObjectDef("whale");
      rmAddObjectDefItem(whaleID, "HumpbackWhale", 1, 4.0);
      rmSetObjectDefMinDistance(whaleID, 0.0);
      rmSetObjectDefMaxDistance(whaleID, rmXFractionToMeters(0.47));
      rmAddObjectDefConstraint(whaleID, whaleVsWhale);
      rmAddObjectDefConstraint(whaleID, whaleLand);
      rmPlaceObjectDefAtLoc(whaleID, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);
   }


   // Text
   rmSetStatusText("",0.99);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 15.0);
   rmSetObjectDefMaxDistance(nugget1, 20.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget2, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget3, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget4, avoidNatives);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Place sockets to trade route

   if (seasonPicker == 0)
   {
      vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.2);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.8);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   }
   

   // Text
   rmSetStatusText("",1.0); 
}  
