// RM script of Andalusia
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

   // Set up void to shorten creation of hills

   int number=0;
   void Hill(float Size=0.0, float Height=0.0, string Terrain="", float LocX=0.0, float LocZ=0.0, float A=0.0, float B=0.0, float C=0.0, float D=0.0)
   {
      int areaID=rmCreateArea("area"+number);
      rmSetAreaLocation(areaID, LocX, LocZ);
      rmSetAreaSize(areaID, Size, Size);
      rmAddAreaInfluenceSegment(areaID, A, B, C, D);
      rmSetAreaBaseHeight(areaID, Height);
      rmSetAreaMix(areaID, Terrain);
      rmSetAreaCoherence(areaID, 0.8);
      rmSetAreaSmoothDistance(areaID, 10);
      rmSetAreaElevationType(areaID, cElevTurbulence);
      rmSetAreaElevationVariation(areaID, 3.0);
      rmSetAreaHeightBlend(areaID, 2);
      rmSetAreaObeyWorldCircleConstraint(areaID, false);
      rmBuildArea(areaID);
      number=number+1;
   }

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Picks the map size
   int playerTiles = 20000;
   if (cNumberNonGaiaPlayers == 8)
	playerTiles = 14000;
   else if (cNumberNonGaiaPlayers == 7)
	playerTiles = 15000;
   else if (cNumberNonGaiaPlayers == 6)
	playerTiles = 16000;
   else if (cNumberNonGaiaPlayers == 5)
	playerTiles = 17000;
   else if (cNumberNonGaiaPlayers == 4)
	playerTiles = 18000;
   else if (cNumberNonGaiaPlayers == 3)
	playerTiles = 19000;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   int longSide=1.15*size;
   rmSetMapSize(longSide, size);


   // Set map the map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses the side the players start in FFA - this is also complex! :)
   int playerSide = rmRandInt(1,2);


   // If 5 or 7 player FFA game, changes the starting place of the last (5th or 7th) player. Bad explanation, isn't it? :D
   int oddPlayer = rmRandInt(1,2);


   // Very VERY complex FFA player placement random integers, only I (AOE_Fan) can really understand these! :P
   int playerPlace3 = rmRandInt(1,3);
   int playerPlace4 = rmRandInt(1,2);
   int playerPlace5 = rmRandInt(1,5);
   int playerPlace6 = rmRandInt(1,3);
   int playerPlace7 = rmRandInt(1,7);
   int playerPlace8 = rmRandInt(1,4);


   // Chooses the side teams start - only in team games
   int teamSide = rmRandInt(1,2);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Pick lighting
   rmSetLightingSet("great plains");


   // Picks default terrain and map type
   rmSetMapElevationParameters(cElevTurbulence, 0.1, 4, 0.4, 5.0);
   rmTerrainInitialize("patagonia\groundforest_pat", 0.0);
   rmSetMapType("grass");
   rmSetMapType("land");
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint
   rmSetWorldCircleConstraint(true);


   // Define classes, these are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   int classSea=rmDefineClass("sea");
   rmDefineClass("starting settlement");
   rmDefineClass("startingUnit");
   rmDefineClass("nuggets");
   rmDefineClass("classMountain");
   rmDefineClass("classForest");
   rmDefineClass("classTreeStraggler");


   // Define constraints - used for things to avoid certain things
   
   // Map edge and centre constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // All player things avoidance
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);

   // All resource avoidance
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 55.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 55.0);
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 40.0);
   int shortForestConstraint=rmCreateClassDistanceConstraint("forest vs. forest short", rmClassID("classForest"), 10.0);
   int stragglerTreeConstraint=rmCreateClassDistanceConstraint("straggler tree constraint", rmClassID("classTreeStraggler"), 20.0);
   int avoidSheep=rmCreateTypeDistanceConstraint("sheep avoids sheep", "sheep", 65.0);

   // Impassable land avoidance
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 8.0);

   // Nuggets avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("nuggets avoids nuggets", rmClassID("nuggets"), 40.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 4.0);

   // Trade route avoidance
   int avoidTradeRoute=rmCreateTradeRouteDistanceConstraint("trade route", 5.0);
   int avoidTradeSockets=rmCreateTypeDistanceConstraint("avoid trade sockets", "SocketTradeRoute", 5.0);

   // Water objects avoidance
   int fishVsFish=rmCreateTypeDistanceConstraint("fish vs fish", "ypFishTuna", 20.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish land", "land", true, 6.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid same", "HomeCityWaterSpawnFlag", 50.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag vs land", "land", true, 25.0);
   int seaConstraint=rmCreateClassDistanceConstraint("avoid sea", classSea, 20.0);


   // Text
   rmSetStatusText("",0.10);


   // Define and place terrain mix - overwrites the terrain initialize, is actually the base map's terrain!
   int MixID = rmCreateArea("Mix"); 
   rmSetAreaSize(MixID, 1.0, 1.0);
   rmSetAreaLocation(MixID, 0.5, 0.5);  
   rmSetAreaCoherence(MixID, 1.0);
   rmSetAreaSmoothDistance(MixID, 10);
   rmSetAreaBaseHeight(MixID, 1.0);
   rmSetAreaWarnFailure(MixID, false);
   rmSetAreaMix(MixID, "patagonia_grass");
   rmBuildArea(MixID);


   // Text
   rmSetStatusText("",0.20);


   // Place players - in team and FFA games

   // Player placement if FFA (Free For All) OR two player game - manual placement
   if(cNumberTeams > 2 || cNumberNonGaiaPlayers == 2)
   {
      if (cNumberNonGaiaPlayers == 2) // Place in middle to different sides
      {
            if (teamSide == 1)
            {
                        rmPlacePlayer(1, 0.5, 0.25);
                        rmPlacePlayer(2, 0.5, 0.75);
            }
            else if (teamSide == 2)
            {
                        rmPlacePlayer(1, 0.5, 0.75);
                        rmPlacePlayer(2, 0.5, 0.25);
            }
      }
      if (cNumberNonGaiaPlayers == 3) // One player is in middle
      {
            if (playerPlace3 == 1) // Player 2 in middle
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.25, 0.25);
                                                rmPlacePlayer(2, 0.5, 0.75);
                                                rmPlacePlayer(3, 0.75, 0.25);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.25, 0.75);
                                                rmPlacePlayer(2, 0.5, 0.25);
                                                rmPlacePlayer(3, 0.75, 0.75);
                        }
            }
            else if (playerPlace3 == 2) // Player 1 in middle
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.5, 0.25);
                                                rmPlacePlayer(2, 0.25, 0.75);
                                                rmPlacePlayer(3, 0.75, 0.75);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.5, 0.75);
                                                rmPlacePlayer(2, 0.25, 0.25);
                                                rmPlacePlayer(3, 0.75, 0.25);
                        }
            }
            else if (playerPlace3 == 3) // Player 3 in middle
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.75, 0.25);
                                                rmPlacePlayer(2, 0.25, 0.25);
                                                rmPlacePlayer(3, 0.5, 0.75);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.75, 0.75);
                                                rmPlacePlayer(2, 0.25, 0.75);
                                                rmPlacePlayer(3, 0.5, 0.25);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 4) // Normal balanced placement
      {
            if (playerPlace4 == 1)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.25, 0.25);
                                                rmPlacePlayer(2, 0.25, 0.75);
                                                rmPlacePlayer(3, 0.75, 0.25);
                                                rmPlacePlayer(4, 0.75, 0.75);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.25, 0.75);
                                                rmPlacePlayer(2, 0.25, 0.25);
                                                rmPlacePlayer(3, 0.75, 0.75);
                                                rmPlacePlayer(4, 0.75, 0.25);
                        }
            }
            else if (playerPlace4 == 2)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.75, 0.25);
                                                rmPlacePlayer(2, 0.75, 0.75);
                                                rmPlacePlayer(3, 0.25, 0.25);
                                                rmPlacePlayer(4, 0.25, 0.75);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.75, 0.75);
                                                rmPlacePlayer(2, 0.75, 0.25);
                                                rmPlacePlayer(3, 0.25, 0.75);
                                                rmPlacePlayer(4, 0.25, 0.25);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 5) // One player is in middle
      {
            if (playerPlace5 == 1) // Player 4 in middle
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.25, 0.25);
                                                rmPlacePlayer(2, 0.25, 0.75);
                                                rmPlacePlayer(3, 0.75, 0.25);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(4, 0.5, 0.75);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(4, 0.5, 0.25);
                                                rmPlacePlayer(5, 0.75, 0.75);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.25, 0.75);
                                                rmPlacePlayer(2, 0.25, 0.25);
                                                rmPlacePlayer(3, 0.75, 0.75);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(4, 0.5, 0.25);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(4, 0.5, 0.75);
                                                rmPlacePlayer(5, 0.75, 0.25);
                        }
            }
            else if (playerPlace5 == 2) // Player 5 in middle
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.75, 0.25);
                                                rmPlacePlayer(2, 0.75, 0.75);
                                                rmPlacePlayer(3, 0.25, 0.25);
                                                rmPlacePlayer(4, 0.25, 0.75);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(5, 0.5, 0.75);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(5, 0.5, 0.25);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.75, 0.75);
                                                rmPlacePlayer(2, 0.75, 0.25);
                                                rmPlacePlayer(3, 0.25, 0.75);
                                                rmPlacePlayer(4, 0.25, 0.25);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(5, 0.5, 0.25);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(5, 0.5, 0.75);
                        }
            }
            else if (playerPlace5 == 3) // Player 1 in middle
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(1, 0.5, 0.25);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(1, 0.5, 0.75);
                                                rmPlacePlayer(2, 0.25, 0.75);
                                                rmPlacePlayer(3, 0.75, 0.25);
                                                rmPlacePlayer(4, 0.75, 0.75);
                                                rmPlacePlayer(5, 0.25, 0.25);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(1, 0.5, 0.75);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(1, 0.5, 0.25);
                                                rmPlacePlayer(2, 0.25, 0.25);
                                                rmPlacePlayer(3, 0.75, 0.75);
                                                rmPlacePlayer(4, 0.75, 0.25);
                                                rmPlacePlayer(5, 0.25, 0.75);
                        }
            }
            else if (playerPlace5 == 4) // Player 2 in middle
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.25, 0.25);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(2, 0.5, 0.25);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(2, 0.5, 0.75);
                                                rmPlacePlayer(3, 0.75, 0.25);
                                                rmPlacePlayer(4, 0.25, 0.75);
                                                rmPlacePlayer(5, 0.75, 0.75);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.25, 0.75);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(2, 0.5, 0.75);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(2, 0.5, 0.25);
                                                rmPlacePlayer(3, 0.75, 0.75);
                                                rmPlacePlayer(4, 0.25, 0.25);
                                                rmPlacePlayer(5, 0.75, 0.25);
                        }
            }
            else if (playerPlace5 == 5) // Player 3 in middle
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.75, 0.25);
                                                rmPlacePlayer(2, 0.75, 0.75);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(3, 0.5, 0.25);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(3, 0.5, 0.75);
                                                rmPlacePlayer(4, 0.25, 0.75);
                                                rmPlacePlayer(5, 0.25, 0.25);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.75, 0.75);
                                                rmPlacePlayer(2, 0.75, 0.25);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(3, 0.5, 0.75);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(3, 0.5, 0.25);
                                                rmPlacePlayer(4, 0.25, 0.25);
                                                rmPlacePlayer(5, 0.25, 0.75);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 6) // Normal balanced placement
      {
            if (playerPlace6 == 1)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.25, 0.25);
                                                rmPlacePlayer(2, 0.25, 0.75);
                                                rmPlacePlayer(3, 0.5, 0.25);
                                                rmPlacePlayer(4, 0.5, 0.75);
                                                rmPlacePlayer(5, 0.75, 0.25);
                                                rmPlacePlayer(6, 0.75, 0.75);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.25, 0.75);
                                                rmPlacePlayer(2, 0.25, 0.25);
                                                rmPlacePlayer(3, 0.5, 0.75);
                                                rmPlacePlayer(4, 0.5, 0.25);
                                                rmPlacePlayer(5, 0.75, 0.75);
                                                rmPlacePlayer(6, 0.75, 0.25);
                        }
            }
            else if (playerPlace6 == 2)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.75, 0.75);
                                                rmPlacePlayer(2, 0.75, 0.25);
                                                rmPlacePlayer(3, 0.25, 0.25);
                                                rmPlacePlayer(4, 0.25, 0.75);
                                                rmPlacePlayer(5, 0.5, 0.75);
                                                rmPlacePlayer(6, 0.5, 0.25);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.75, 0.25);
                                                rmPlacePlayer(2, 0.75, 0.75);
                                                rmPlacePlayer(3, 0.25, 0.75);
                                                rmPlacePlayer(4, 0.25, 0.25);
                                                rmPlacePlayer(5, 0.5, 0.25);
                                                rmPlacePlayer(6, 0.5, 0.75);
                        }
            }
            else if (playerPlace6 == 3)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.5, 0.75);
                                                rmPlacePlayer(2, 0.5, 0.25);
                                                rmPlacePlayer(3, 0.75, 0.75);
                                                rmPlacePlayer(4, 0.75, 0.25);
                                                rmPlacePlayer(5, 0.25, 0.75);
                                                rmPlacePlayer(6, 0.25, 0.25);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.5, 0.25);
                                                rmPlacePlayer(2, 0.5, 0.75);
                                                rmPlacePlayer(3, 0.75, 0.25);
                                                rmPlacePlayer(4, 0.75, 0.75);
                                                rmPlacePlayer(5, 0.25, 0.25);
                                                rmPlacePlayer(6, 0.25, 0.75);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 7) // One player is in middle
      {
            if (playerPlace7 == 1) // Player 4 in middle
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.25, 0.25);
                                                rmPlacePlayer(2, 0.25, 0.75);
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(3, 0.42, 0.25);
                                                                                                rmPlacePlayer(4, 0.5, 0.75);
                                                                                                rmPlacePlayer(5, 0.58, 0.25);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(3, 0.42, 0.75);
                                                                                                rmPlacePlayer(4, 0.5, 0.25);
                                                                                                rmPlacePlayer(5, 0.58, 0.75);
                                                }
                                                rmPlacePlayer(6, 0.75, 0.75);
                                                rmPlacePlayer(7, 0.75, 0.25);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.25, 0.75);
                                                rmPlacePlayer(2, 0.25, 0.25);
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(3, 0.42, 0.75);
                                                                                                rmPlacePlayer(4, 0.5, 0.25);
                                                                                                rmPlacePlayer(5, 0.58, 0.75);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(3, 0.42, 0.25);
                                                                                                rmPlacePlayer(4, 0.5, 0.75);
                                                                                                rmPlacePlayer(5, 0.58, 0.25);
                                                }
                                                rmPlacePlayer(6, 0.75, 0.25);
                                                rmPlacePlayer(7, 0.75, 0.75);
                        }
            }
            if (playerPlace7 == 2) // Player 5 in middle
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.75, 0.75);
                                                rmPlacePlayer(2, 0.75, 0.25);
                                                rmPlacePlayer(3, 0.25, 0.25);
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(4, 0.42, 0.75);
                                                                                                rmPlacePlayer(5, 0.5, 0.25);
                                                                                                rmPlacePlayer(6, 0.58, 0.75);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(4, 0.42, 0.25);
                                                                                                rmPlacePlayer(5, 0.5, 0.75);
                                                                                                rmPlacePlayer(6, 0.58, 0.25);
                                                }
                                                rmPlacePlayer(7, 0.25, 0.75);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.75, 0.25);
                                                rmPlacePlayer(2, 0.75, 0.75);
                                                rmPlacePlayer(3, 0.25, 0.75);
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(4, 0.42, 0.25);
                                                                                                rmPlacePlayer(5, 0.5, 0.75);
                                                                                                rmPlacePlayer(6, 0.58, 0.25);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(4, 0.42, 0.75);
                                                                                                rmPlacePlayer(5, 0.5, 0.25);
                                                                                                rmPlacePlayer(6, 0.58, 0.75);
                                                }
                                                rmPlacePlayer(7, 0.25, 0.25);
                        }
            }
            if (playerPlace7 == 3) // Player 6 in middle
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.25, 0.25);
                                                rmPlacePlayer(2, 0.25, 0.75);
                                                rmPlacePlayer(3, 0.75, 0.75);
                                                rmPlacePlayer(4, 0.75, 0.25);
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(5, 0.42, 0.75);
                                                                                                rmPlacePlayer(6, 0.5, 0.25);
                                                                                                rmPlacePlayer(7, 0.58, 0.75);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(5, 0.42, 0.25);
                                                                                                rmPlacePlayer(6, 0.5, 0.75);
                                                                                                rmPlacePlayer(7, 0.58, 0.25);
                                                }
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.25, 0.75);
                                                rmPlacePlayer(2, 0.25, 0.25);
                                                rmPlacePlayer(3, 0.75, 0.25);
                                                rmPlacePlayer(4, 0.75, 0.75);
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(5, 0.42, 0.25);
                                                                                                rmPlacePlayer(6, 0.5, 0.75);
                                                                                                rmPlacePlayer(7, 0.58, 0.25);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(5, 0.42, 0.75);
                                                                                                rmPlacePlayer(6, 0.5, 0.25);
                                                                                                rmPlacePlayer(7, 0.58, 0.75);
                                                }
                        }
            }
            if (playerPlace7 == 4) // Player 7 in middle
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(2, 0.75, 0.25);
                                                rmPlacePlayer(3, 0.25, 0.25);
                                                rmPlacePlayer(4, 0.25, 0.75);
                                                rmPlacePlayer(5, 0.75, 0.75);
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(6, 0.42, 0.75);
                                                                                                rmPlacePlayer(7, 0.5, 0.25);
                                                                                                rmPlacePlayer(1, 0.58, 0.75);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(6, 0.42, 0.25);
                                                                                                rmPlacePlayer(7, 0.5, 0.75);
                                                                                                rmPlacePlayer(1, 0.58, 0.25);
                                                }
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(2, 0.75, 0.75);
                                                rmPlacePlayer(3, 0.25, 0.75);
                                                rmPlacePlayer(4, 0.25, 0.25);
                                                rmPlacePlayer(5, 0.75, 0.25);
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(6, 0.42, 0.25);
                                                                                                rmPlacePlayer(7, 0.5, 0.75);
                                                                                                rmPlacePlayer(1, 0.58, 0.25);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(6, 0.42, 0.75);
                                                                                                rmPlacePlayer(7, 0.5, 0.25);
                                                                                                rmPlacePlayer(1, 0.58, 0.75);
                                                }
                        }
            }
            if (playerPlace7 == 5) // Player 1 in middle
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(3, 0.75, 0.75);
                                                rmPlacePlayer(4, 0.75, 0.25);
                                                rmPlacePlayer(5, 0.25, 0.25);
                                                rmPlacePlayer(6, 0.25, 0.75);
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(7, 0.42, 0.75);
                                                                                                rmPlacePlayer(1, 0.5, 0.25);
                                                                                                rmPlacePlayer(2, 0.58, 0.75);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(7, 0.42, 0.25);
                                                                                                rmPlacePlayer(1, 0.5, 0.75);
                                                                                                rmPlacePlayer(2, 0.58, 0.25);
                                                }
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(3, 0.75, 0.25);
                                                rmPlacePlayer(4, 0.75, 0.75);
                                                rmPlacePlayer(5, 0.25, 0.75);
                                                rmPlacePlayer(6, 0.25, 0.25);
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(7, 0.42, 0.25);
                                                                                                rmPlacePlayer(1, 0.5, 0.75);
                                                                                                rmPlacePlayer(2, 0.58, 0.25);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(7, 0.42, 0.75);
                                                                                                rmPlacePlayer(1, 0.5, 0.25);
                                                                                                rmPlacePlayer(2, 0.58, 0.75);
                                                }
                        }
            }
            if (playerPlace7 == 6) // Player 2 in middle
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(4, 0.25, 0.75);
                                                rmPlacePlayer(5, 0.75, 0.75);
                                                rmPlacePlayer(6, 0.75, 0.25);
                                                rmPlacePlayer(7, 0.25, 0.25);
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(1, 0.42, 0.75);
                                                                                                rmPlacePlayer(2, 0.5, 0.25);
                                                                                                rmPlacePlayer(3, 0.58, 0.75);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(1, 0.42, 0.25);
                                                                                                rmPlacePlayer(2, 0.5, 0.75);
                                                                                                rmPlacePlayer(3, 0.58, 0.25);
                                                }
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(4, 0.25, 0.25);
                                                rmPlacePlayer(5, 0.75, 0.25);
                                                rmPlacePlayer(6, 0.75, 0.75);
                                                rmPlacePlayer(7, 0.25, 0.75);
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(1, 0.42, 0.25);
                                                                                                rmPlacePlayer(2, 0.5, 0.75);
                                                                                                rmPlacePlayer(3, 0.58, 0.25);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(1, 0.42, 0.75);
                                                                                                rmPlacePlayer(2, 0.5, 0.25);
                                                                                                rmPlacePlayer(3, 0.58, 0.75);
                                                }
                        }
            }
            if (playerPlace7 == 7) // Player 3 in middle
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(5, 0.25, 0.25);
                                                rmPlacePlayer(6, 0.25, 0.75);
                                                rmPlacePlayer(7, 0.75, 0.75);
                                                rmPlacePlayer(1, 0.75, 0.25);
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(2, 0.42, 0.75);
                                                                                                rmPlacePlayer(3, 0.5, 0.25);
                                                                                                rmPlacePlayer(4, 0.58, 0.75);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(2, 0.42, 0.25);
                                                                                                rmPlacePlayer(3, 0.5, 0.75);
                                                                                                rmPlacePlayer(4, 0.58, 0.25);
                                                }
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(5, 0.25, 0.75);
                                                rmPlacePlayer(6, 0.25, 0.25);
                                                rmPlacePlayer(7, 0.75, 0.25);
                                                rmPlacePlayer(1, 0.75, 0.75);
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(2, 0.42, 0.25);
                                                                                                rmPlacePlayer(3, 0.5, 0.75);
                                                                                                rmPlacePlayer(4, 0.58, 0.25);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(2, 0.42, 0.75);
                                                                                                rmPlacePlayer(3, 0.5, 0.25);
                                                                                                rmPlacePlayer(4, 0.58, 0.75);
                                                }
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 8) // Normal balanced placement
      {
            if (playerPlace8 == 1)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.25, 0.25);
                                                rmPlacePlayer(2, 0.25, 0.75);
                                                rmPlacePlayer(3, 0.42, 0.25);
                                                rmPlacePlayer(4, 0.42, 0.75);
                                                rmPlacePlayer(5, 0.58, 0.25);
                                                rmPlacePlayer(6, 0.58, 0.75);
                                                rmPlacePlayer(7, 0.75, 0.25);
                                                rmPlacePlayer(8, 0.75, 0.75);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.25, 0.75);
                                                rmPlacePlayer(2, 0.25, 0.25);
                                                rmPlacePlayer(3, 0.42, 0.75);
                                                rmPlacePlayer(4, 0.42, 0.25);
                                                rmPlacePlayer(5, 0.58, 0.75);
                                                rmPlacePlayer(6, 0.58, 0.25);
                                                rmPlacePlayer(7, 0.75, 0.75);
                                                rmPlacePlayer(8, 0.75, 0.25);
                        }
            }
            else if (playerPlace8 == 2)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.75, 0.75);
                                                rmPlacePlayer(2, 0.75, 0.25);
                                                rmPlacePlayer(3, 0.25, 0.25);
                                                rmPlacePlayer(4, 0.25, 0.75);
                                                rmPlacePlayer(5, 0.42, 0.25);
                                                rmPlacePlayer(6, 0.42, 0.75);
                                                rmPlacePlayer(7, 0.58, 0.25);
                                                rmPlacePlayer(8, 0.58, 0.75);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.75, 0.25);
                                                rmPlacePlayer(2, 0.75, 0.75);
                                                rmPlacePlayer(3, 0.25, 0.75);
                                                rmPlacePlayer(4, 0.25, 0.25);
                                                rmPlacePlayer(5, 0.42, 0.75);
                                                rmPlacePlayer(6, 0.42, 0.25);
                                                rmPlacePlayer(7, 0.58, 0.75);
                                                rmPlacePlayer(8, 0.58, 0.25);
                        }
            }
            else if (playerPlace8 == 3)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.58, 0.75);
                                                rmPlacePlayer(2, 0.58, 0.25);
                                                rmPlacePlayer(3, 0.75, 0.25);
                                                rmPlacePlayer(4, 0.75, 0.75);
                                                rmPlacePlayer(5, 0.25, 0.25);
                                                rmPlacePlayer(6, 0.25, 0.75);
                                                rmPlacePlayer(7, 0.42, 0.25);
                                                rmPlacePlayer(8, 0.42, 0.75);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.58, 0.25);
                                                rmPlacePlayer(2, 0.58, 0.75);
                                                rmPlacePlayer(3, 0.75, 0.75);
                                                rmPlacePlayer(4, 0.75, 0.25);
                                                rmPlacePlayer(5, 0.25, 0.75);
                                                rmPlacePlayer(6, 0.25, 0.25);
                                                rmPlacePlayer(7, 0.42, 0.75);
                                                rmPlacePlayer(8, 0.42, 0.25);
                        }
            }
            else if (playerPlace8 == 4)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.42, 0.75);
                                                rmPlacePlayer(2, 0.42, 0.25);
                                                rmPlacePlayer(3, 0.58, 0.25);
                                                rmPlacePlayer(4, 0.58, 0.75);
                                                rmPlacePlayer(5, 0.75, 0.25);
                                                rmPlacePlayer(6, 0.75, 0.75);
                                                rmPlacePlayer(7, 0.25, 0.25);
                                                rmPlacePlayer(8, 0.25, 0.75);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.42, 0.25);
                                                rmPlacePlayer(2, 0.42, 0.75);
                                                rmPlacePlayer(3, 0.58, 0.75);
                                                rmPlacePlayer(4, 0.58, 0.25);
                                                rmPlacePlayer(5, 0.75, 0.75);
                                                rmPlacePlayer(6, 0.75, 0.25);
                                                rmPlacePlayer(7, 0.25, 0.75);
                                                rmPlacePlayer(8, 0.25, 0.25);
                        }
            }
      }
   }

   // Player placement if teams - place both teams to lines in other side of map
   if(cNumberTeams == 2 && cNumberNonGaiaPlayers >= 3)
   {
      rmSetPlacementTeam(0);
      if (teamSide == 1)
      {
         if (rmGetNumberPlayersOnTeam(0) == 2)
            rmPlacePlayersLine(0.4, 0.75, 0.6, 0.75, 0, 15);
         else
            rmPlacePlayersLine(0.25, 0.75, 0.75, 0.75, 0, 15);
      }
      else if (teamSide == 2)
      {
         if (rmGetNumberPlayersOnTeam(0) == 2)
            rmPlacePlayersLine(0.4, 0.25, 0.6, 0.25, 0, 15);
         else
            rmPlacePlayersLine(0.25, 0.25, 0.75, 0.25, 0, 15);
      }

      rmSetPlacementTeam(1);
      if (teamSide == 1)
      {
         if (rmGetNumberPlayersOnTeam(1) == 2)
            rmPlacePlayersLine(0.4, 0.25, 0.6, 0.25, 0, 15);
         else
            rmPlacePlayersLine(0.25, 0.25, 0.75, 0.25, 0, 15);
      }
      else if (teamSide == 2)
      {
         if (rmGetNumberPlayersOnTeam(1) == 2)
            rmPlacePlayersLine(0.4, 0.75, 0.6, 0.75, 0, 15);
         else
            rmPlacePlayersLine(0.25, 0.75, 0.75, 0.75, 0, 15);
      }
   }


   // Text
   rmSetStatusText("",0.25);


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaBaseHeight(id, 1);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand); 
      rmSetAreaCoherence(id, 0.9);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }


   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.30);


   // Define different sizes of mountains - scales with the size of map
   int radius = size * 0.5;
   int mountainPeaksCornerSize = radius * 10.0;
   int mountainPeaksLongSideSize = radius * 15.0;
   int mountainPeaksShortSideSize = radius * 20.0;


   // Define and place mountain terrains - softenes the height change between mountains peaks and base map terrain

   Hill(rmAreaTilesToFraction(size*0.5*68.0), 5.0, "patagonia_grass", 0.5, 1.0, 0.0, 1.0, 1.0, 1.0);
   Hill(rmAreaTilesToFraction(size*0.5*64.0), 6.0, "patagonia_grass", 0.5, 1.0, 0.0, 1.0, 1.0, 1.0);
   Hill(rmAreaTilesToFraction(size*0.5*60.0), 7.0, "patagonia_grass", 0.5, 1.0, 0.0, 1.0, 1.0, 1.0);
   Hill(rmAreaTilesToFraction(size*0.5*56.0), 8.0, "patagonia_grass", 0.5, 1.0, 0.0, 1.0, 1.0, 1.0);
   Hill(rmAreaTilesToFraction(size*0.5*52.0), 9.0, "patagonia_grass", 0.5, 1.0, 0.0, 1.0, 1.0, 1.0);
   Hill(rmAreaTilesToFraction(size*0.5*48.0), 10.0, "patagonia_grass", 0.5, 1.0, 0.0, 1.0, 1.0, 1.0);
   Hill(rmAreaTilesToFraction(size*0.5*44.0), 11.0, "patagonia_grass", 0.5, 1.0, 0.0, 1.0, 1.0, 1.0);
   Hill(rmAreaTilesToFraction(size*0.5*40.0), 12.0, "patagonia_grass", 0.5, 1.0, 0.0, 1.0, 1.0, 1.0);

   Hill(rmAreaTilesToFraction(size*0.5*68.0), 5.0, "patagonia_grass", 0.5, 0.0, 0.0, 0.0, 1.0, 0.0);
   Hill(rmAreaTilesToFraction(size*0.5*64.0), 6.0, "patagonia_grass", 0.5, 0.0, 0.0, 0.0, 1.0, 0.0);
   Hill(rmAreaTilesToFraction(size*0.5*60.0), 7.0, "patagonia_grass", 0.5, 0.0, 0.0, 0.0, 1.0, 0.0);
   Hill(rmAreaTilesToFraction(size*0.5*56.0), 8.0, "patagonia_grass", 0.5, 0.0, 0.0, 0.0, 1.0, 0.0);
   Hill(rmAreaTilesToFraction(size*0.5*52.0), 9.0, "patagonia_grass", 0.5, 0.0, 0.0, 0.0, 1.0, 0.0);
   Hill(rmAreaTilesToFraction(size*0.5*48.0), 10.0, "patagonia_grass", 0.5, 0.0, 0.0, 0.0, 1.0, 0.0);
   Hill(rmAreaTilesToFraction(size*0.5*44.0), 11.0, "patagonia_grass", 0.5, 0.0, 0.0, 0.0, 1.0, 0.0);
   Hill(rmAreaTilesToFraction(size*0.5*40.0), 12.0, "patagonia_grass", 0.5, 0.0, 0.0, 0.0, 1.0, 0.0);

   Hill(rmAreaTilesToFraction(size*0.5*68.0), 5.0, "patagonia_grass", 1.0, 0.5, 1.0, 0.0, 1.0, 1.0);
   Hill(rmAreaTilesToFraction(size*0.5*64.0), 6.0, "patagonia_grass", 1.0, 0.5, 1.0, 0.0, 1.0, 1.0);
   Hill(rmAreaTilesToFraction(size*0.5*60.0), 7.0, "patagonia_grass", 1.0, 0.5, 1.0, 0.0, 1.0, 1.0);
   Hill(rmAreaTilesToFraction(size*0.5*56.0), 8.0, "patagonia_grass", 1.0, 0.5, 1.0, 0.0, 1.0, 1.0);
   Hill(rmAreaTilesToFraction(size*0.5*52.0), 9.0, "patagonia_grass", 1.0, 0.5, 1.0, 0.0, 1.0, 1.0);
   Hill(rmAreaTilesToFraction(size*0.5*48.0), 10.0, "patagonia_grass", 1.0, 0.5, 1.0, 0.0, 1.0, 1.0);
   Hill(rmAreaTilesToFraction(size*0.5*44.0), 11.0, "patagonia_grass", 1.0, 0.5, 1.0, 0.0, 1.0, 1.0);
   Hill(rmAreaTilesToFraction(size*0.5*40.0), 12.0, "patagonia_grass", 1.0, 0.5, 1.0, 0.0, 1.0, 1.0);


   // Text
   rmSetStatusText("",0.35);


   // Define and place mountain peaks - impassable and unbuildable!

   // Define and place west mountain peaks
   int mountainPeaksWID = rmCreateArea("mountain peaks west");
   rmSetAreaSize(mountainPeaksWID, rmAreaTilesToFraction(mountainPeaksLongSideSize), rmAreaTilesToFraction(mountainPeaksLongSideSize));
   rmAddAreaToClass(mountainPeaksWID, rmClassID("classMountain"));
   rmSetAreaLocation(mountainPeaksWID, 0.5, 1.0);
   rmAddAreaInfluenceSegment(mountainPeaksWID, 0.0, 1.0, 1.0, 1.0);
   rmSetAreaCoherence(mountainPeaksWID, 0.6); 
   rmSetAreaSmoothDistance(mountainPeaksWID, 10);
   rmSetAreaBaseHeight(mountainPeaksWID, 17);
   rmSetAreaElevationType(mountainPeaksWID, cElevTurbulence);
   rmSetAreaElevationVariation(mountainPeaksWID, 9.0);
   rmSetAreaHeightBlend(mountainPeaksWID, 1);
   rmSetAreaWarnFailure(mountainPeaksWID, false);
   rmSetAreaTerrainType(mountainPeaksWID, "terrain\rockies\clifftop_roc");
   rmBuildArea(mountainPeaksWID);

   // Define and place east mountain peaks
   int mountainPeaksEID = rmCreateArea("mountain peaks east");
   rmSetAreaSize(mountainPeaksEID, rmAreaTilesToFraction(mountainPeaksLongSideSize), rmAreaTilesToFraction(mountainPeaksLongSideSize));
   rmAddAreaToClass(mountainPeaksEID, rmClassID("classMountain"));
   rmSetAreaLocation(mountainPeaksEID, 0.5, 0.0);     
   rmAddAreaInfluenceSegment(mountainPeaksEID, 0.0, 0.0, 1.0, 0.0);  
   rmSetAreaCoherence(mountainPeaksEID, 0.6); 
   rmSetAreaSmoothDistance(mountainPeaksEID, 10);
   rmSetAreaBaseHeight(mountainPeaksEID, 17);
   rmSetAreaElevationType(mountainPeaksEID, cElevTurbulence);
   rmSetAreaElevationVariation(mountainPeaksEID, 9.0);
   rmSetAreaHeightBlend(mountainPeaksEID, 1);
   rmSetAreaWarnFailure(mountainPeaksEID, false);
   rmSetAreaTerrainType(mountainPeaksEID, "terrain\rockies\clifftop_roc");
   rmBuildArea(mountainPeaksEID);

   // Define and place north mountain peaks
   int mountainPeaksNID = rmCreateArea("mountain peaks north");
   rmSetAreaSize(mountainPeaksNID, rmAreaTilesToFraction(mountainPeaksShortSideSize), rmAreaTilesToFraction(mountainPeaksShortSideSize));
   rmAddAreaToClass(mountainPeaksNID, rmClassID("classMountain"));
   rmSetAreaLocation(mountainPeaksNID, 1.0, 0.5);     
   rmAddAreaInfluenceSegment(mountainPeaksNID, 0.98, 0.0, 0.98, 1.0);  
   rmSetAreaCoherence(mountainPeaksNID, 0.6); 
   rmSetAreaSmoothDistance(mountainPeaksNID, 10);
   rmSetAreaBaseHeight(mountainPeaksNID, 17);
   rmSetAreaElevationType(mountainPeaksNID, cElevTurbulence);
   rmSetAreaElevationVariation(mountainPeaksNID, 9.0);
   rmSetAreaHeightBlend(mountainPeaksNID, 1);
   rmSetAreaWarnFailure(mountainPeaksNID, false);
   rmSetAreaTerrainType(mountainPeaksNID, "terrain\rockies\clifftop_roc");
   rmSetAreaObeyWorldCircleConstraint(mountainPeaksNID, false);
   rmBuildArea(mountainPeaksNID);

   // Define and place mountain peaks to north-west corner
   int mountainPeaksNWID = rmCreateArea("mountain peaks northwest");
   rmSetAreaSize(mountainPeaksNWID, rmAreaTilesToFraction(mountainPeaksCornerSize), rmAreaTilesToFraction(mountainPeaksCornerSize));
   rmAddAreaToClass(mountainPeaksNWID, rmClassID("classMountain"));
   rmSetAreaLocation(mountainPeaksNWID, 1.0, 1.0);     
   rmAddAreaInfluencePoint(mountainPeaksNWID, 0.98, 0.9);  
   rmSetAreaCoherence(mountainPeaksNWID, 0.8); 
   rmSetAreaSmoothDistance(mountainPeaksNWID, 5);
   rmSetAreaBaseHeight(mountainPeaksNWID, 17);
   rmSetAreaElevationType(mountainPeaksNWID, cElevTurbulence);
   rmSetAreaElevationVariation(mountainPeaksNWID, 9.0);
   rmSetAreaHeightBlend(mountainPeaksNWID, 1);
   rmSetAreaWarnFailure(mountainPeaksNWID, false);
   rmSetAreaTerrainType(mountainPeaksNWID, "terrain\rockies\clifftop_roc");
   rmSetAreaObeyWorldCircleConstraint(mountainPeaksNWID, false);
   rmBuildArea(mountainPeaksNWID);

   // Define and place mountain peaks to north-east corner
   int mountainPeaksNEID = rmCreateArea("mountain peaks northeast");
   rmSetAreaSize(mountainPeaksNEID, rmAreaTilesToFraction(mountainPeaksCornerSize), rmAreaTilesToFraction(mountainPeaksCornerSize));
   rmAddAreaToClass(mountainPeaksNEID, rmClassID("classMountain"));
   rmSetAreaLocation(mountainPeaksNEID, 1.0, 0.0);     
   rmAddAreaInfluencePoint(mountainPeaksNEID, 0.98, 0.1);  
   rmSetAreaCoherence(mountainPeaksNEID, 0.8); 
   rmSetAreaSmoothDistance(mountainPeaksNEID, 5);
   rmSetAreaBaseHeight(mountainPeaksNEID, 17);
   rmSetAreaElevationType(mountainPeaksNEID, cElevTurbulence);
   rmSetAreaElevationVariation(mountainPeaksNEID, 9.0);
   rmSetAreaHeightBlend(mountainPeaksNEID, 1);
   rmSetAreaWarnFailure(mountainPeaksNEID, false);
   rmSetAreaTerrainType(mountainPeaksNEID, "terrain\rockies\clifftop_roc");
   rmSetAreaObeyWorldCircleConstraint(mountainPeaksNEID, false);
   rmBuildArea(mountainPeaksNEID);


   // Text
   rmSetStatusText("",0.40);


   // Define and place sea to south

   int seaID=rmCreateArea("Sea");
   rmSetAreaWaterType(seaID, "great lakes");
   rmSetAreaSize(seaID, 0.11, 0.11);
   rmSetAreaCoherence(seaID, 0.5);
   rmSetAreaLocation(seaID, 0.02, 0.5);
   rmAddAreaInfluenceSegment(seaID, 0.02, 0.0, 0.02, 1.0); 
   rmSetAreaBaseHeight(seaID, 0.0);
   rmSetAreaMinBlobs(seaID, 4);
   rmSetAreaMaxBlobs(seaID, 5);
   rmSetAreaMinBlobDistance(seaID, 4);
   rmSetAreaMaxBlobDistance(seaID, 5);
   rmSetAreaSmoothDistance(seaID, 50);
   rmSetAreaHeightBlend(seaID, 2);
   rmAddAreaToClass(seaID, classSea);
   rmBuildArea(seaID);


   // Text
   rmSetStatusText("",0.50);


   // Define and place trade route - splits the map in middle

   // Create the trade route
   int tradeRouteID = rmCreateTradeRoute();
   int socketID=rmCreateObjectDef("sockets to dock trade posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

   // Define the sockets
   rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socketID, true);
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 12.0);

   // Set the trade route waypoints
   rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.5);
   if (cNumberNonGaiaPlayers <= 3)
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.85, 0.5, 18, 18);
   if (cNumberNonGaiaPlayers == 4)
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.87, 0.5, 18, 18);
   if (cNumberNonGaiaPlayers >= 5)
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.9, 0.5, 18, 18);
   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route"); 
  
   // Place sockets to trade route - to inland only!
   vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.4);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.65);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.9);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);


   // Text
   rmSetStatusText("",0.60);


   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startTreeID, "TreeMadrone", 8, 4.0);
   rmSetObjectDefMinDistance(startTreeID, 18);
   rmSetObjectDefMaxDistance(startTreeID, 25);
   rmAddObjectDefConstraint(startTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startTreeID, shortAvoidStartingUnits);

   int startIbexID=rmCreateObjectDef("starting ibex");
   rmAddObjectDefItem(startIbexID, "ypIbex", 6, 4.0);
   rmSetObjectDefCreateHerd(startIbexID, true);
   rmSetObjectDefMinDistance(startIbexID, 18);
   rmSetObjectDefMaxDistance(startIbexID, 25);
   rmAddObjectDefConstraint(startIbexID, avoidStartResource);
   rmAddObjectDefConstraint(startIbexID, avoidImpassableLand);
   rmAddObjectDefConstraint(startIbexID, shortAvoidStartingUnits);

   int startCopperID=rmCreateObjectDef("player copper");
   rmAddObjectDefItem(startCopperID, "MineCopper", 1, 5.0);
   rmSetObjectDefMinDistance(startCopperID, 18);
   rmSetObjectDefMaxDistance(startCopperID, 22);
   rmAddObjectDefConstraint(startCopperID, avoidStartResource);
   rmAddObjectDefConstraint(startCopperID, avoidImpassableLand);
   rmAddObjectDefConstraint(startCopperID, shortAvoidStartingUnits);

   // Place starting objects and resources

   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startCopperID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startIbexID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 1.0);
      rmSetObjectDefMinDistance(waterFlagID, 0.0);
      rmSetObjectDefMaxDistance(waterFlagID, rmZFractionToMeters(1.0));
      rmAddObjectDefConstraint(waterFlagID, flagLand);
      rmAddObjectDefConstraint(waterFlagID, flagVsFlag);
      rmPlaceObjectDefAtLoc(waterFlagID, i, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.70);


   // Define and place olive tree forests

   int numTries=20*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(200), rmAreaTilesToFraction(300));
      rmSetAreaForestType(forest, "california madrone forest");
      rmSetAreaForestDensity(forest, 0.8);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.5);
      rmSetAreaForestUnderbrush(forest, 0.2);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, seaConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, avoidTradeRoute);
      rmAddAreaConstraint(forest, avoidTradeSockets);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row 
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }


   // Define and place straggler trees
   for (i=0; < 40)
   { 
      int stragglerTreeID=rmCreateObjectDef("straggler tree olive"+i);
      rmAddObjectDefItem(stragglerTreeID, "TreeMadrone", rmRandInt(1,5), 10.0);
      rmSetObjectDefMinDistance(stragglerTreeID, 0.0);
      rmSetObjectDefMaxDistance(stragglerTreeID, rmZFractionToMeters(0.47));
      rmAddObjectDefToClass(stragglerTreeID, rmClassID("classForest"));
      rmAddObjectDefToClass(stragglerTreeID, rmClassID("classTreeStraggler"));
      rmAddObjectDefConstraint(stragglerTreeID, somePlayerConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, avoidImpassableLand);
      rmAddObjectDefConstraint(stragglerTreeID, shortForestConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, stragglerTreeConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, avoidTradeRoute);
      rmAddObjectDefConstraint(stragglerTreeID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(stragglerTreeID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place huntables
   
   // Define and place mouflon herds
   int mouflonHerdID=rmCreateObjectDef("mouflon herd");
   rmAddObjectDefItem(mouflonHerdID, "BighornSheep", rmRandInt(7,8), 8.0);
   rmSetObjectDefCreateHerd(mouflonHerdID, true);
   rmSetObjectDefMinDistance(mouflonHerdID, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(mouflonHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(mouflonHerdID, classAnimals);
   rmAddObjectDefConstraint(mouflonHerdID, animalConstraint);
   rmAddObjectDefConstraint(mouflonHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(mouflonHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(mouflonHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(mouflonHerdID, avoidTradeSockets);
   numTries=cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(mouflonHerdID, 0, 0.5, 0.5);

   // Define and place spanish ibex herds
   int spanishIbexHerdID=rmCreateObjectDef("spanish ibex herd");
   rmAddObjectDefItem(spanishIbexHerdID, "ypIbex", rmRandInt(7,8), 8.0);
   rmSetObjectDefCreateHerd(spanishIbexHerdID, true);
   rmSetObjectDefMinDistance(spanishIbexHerdID, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(spanishIbexHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(spanishIbexHerdID, classAnimals);
   rmAddObjectDefConstraint(spanishIbexHerdID, animalConstraint);
   rmAddObjectDefConstraint(spanishIbexHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(spanishIbexHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(spanishIbexHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(spanishIbexHerdID, avoidTradeSockets);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(spanishIbexHerdID, 0, 0.5, 0.5);

   // Define and place fallow deer herds
   int fallowDeerHerdID=rmCreateObjectDef("fallow deer herd");
   rmAddObjectDefItem(fallowDeerHerdID, "deer", rmRandInt(7,8), 8.0);
   rmSetObjectDefCreateHerd(fallowDeerHerdID, true);
   rmSetObjectDefMinDistance(fallowDeerHerdID, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(fallowDeerHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(fallowDeerHerdID, classAnimals);
   rmAddObjectDefConstraint(fallowDeerHerdID, animalConstraint);
   rmAddObjectDefConstraint(fallowDeerHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(fallowDeerHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(fallowDeerHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(fallowDeerHerdID, avoidTradeSockets);
   numTries=cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(fallowDeerHerdID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.85);


   // Define and place copper mines

   int copperCount = 3*cNumberNonGaiaPlayers;

   for(i=0; < copperCount)
   {
      int copperID=rmCreateObjectDef("copper mine"+i);
      rmAddObjectDefItem(copperID, "MineCopper", 1, 0.0);
      rmSetObjectDefMinDistance(copperID, rmXFractionToMeters(0.00));
      rmSetObjectDefMaxDistance(copperID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(copperID, classMines);
      rmAddObjectDefConstraint(copperID, playerConstraint);
      rmAddObjectDefConstraint(copperID, avoidMines);
      rmAddObjectDefConstraint(copperID, avoidImpassableLand);
      rmAddObjectDefConstraint(copperID, avoidTradeRoute);
      rmAddObjectDefConstraint(copperID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(copperID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.90);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget1, avoidTradeSockets);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget2, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget3, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget4, avoidTradeSockets);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Text
   rmSetStatusText("",0.95);


   // Define and place sheep herds 

   int sheepID=rmCreateObjectDef("sheep");
   rmAddObjectDefItem(sheepID, "sheep", 2, 4.0);
   rmSetObjectDefMinDistance(sheepID, 0.0);
   rmSetObjectDefMaxDistance(sheepID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(sheepID, avoidSheep);
   rmAddObjectDefConstraint(sheepID, playerConstraint);
   rmAddObjectDefConstraint(sheepID, avoidImpassableLand);
   rmAddObjectDefConstraint(sheepID, avoidTradeRoute);
   rmAddObjectDefConstraint(sheepID, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(sheepID, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);


   // Define and place fish to sea

   int fishID=rmCreateObjectDef("fish");
   rmAddObjectDefItem(fishID, "ypFishTuna", 1, 4.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.5));
   rmAddObjectDefConstraint(fishID, fishVsFish);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 7*cNumberNonGaiaPlayers);


   // Place King's Hill if KOTH game to middle - automatically avoids the trade route

   if(rmGetIsKOTH()) 
   {
      float xLoc = 0.5;
      float yLoc = 0.5;
      float walk = 0.05;

      ypKingsHillPlacer(xLoc, yLoc, walk, 0);
   }


   // Text
   rmSetStatusText("",1.0);
}  
