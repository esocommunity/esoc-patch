// ***********************************************************************************************************************************************
// ****************************************************** M A N C H A C **************************************************************************
// ***********************************************************************************************************************************************

// ------------------------------------------------------ Comentaries ---------------------------------------------------------------------------

// ------------------------------------------------------ Initialization ------------------------------------------------------------------------


include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";


void main(void)
{
   rmSetStatusText("",0.01);

   int numPlayer = cNumberNonGaiaPlayers ;

   // Picks the map size
   int playerTiles = 10500;
	if (numPlayer >4)
		playerTiles = 9500;
	if (numPlayer >6)
		playerTiles = 8900;		
    int size=2.0*sqrt(numPlayer*playerTiles);
    rmSetMapSize(size, size);
    rmSetSeaLevel(1.0);
    rmSetLightingSet("Bayou");

	rmSetMapElevationParameters(cElevTurbulence, 0.1, 4, 0.3, 2.0);
	rmSetSeaType("bayou");
	rmEnableLocalWater(false);
	rmSetBaseTerrainMix("bayou_grass");
	rmTerrainInitialize("water");
	rmSetMapType("bayou");
	rmSetMapType("water");
	rmSetMapType("grass");
	rmDefineClass("natives");
	rmSetWorldCircleConstraint(true);

	// Choose mercs.
	chooseMercs();

	// Define some classes. These are used later for constraints.
	int classPlayer=rmDefineClass("player");
	rmDefineClass("classCliff");
	rmDefineClass("classPatch");
	int classbigContinent=rmDefineClass("big continent");
	rmDefineClass("classForest");
	rmDefineClass("importantItem");
	rmDefineClass("secrets");
	rmDefineClass("startingUnit");
	rmDefineClass("GoldMine");
	int classBay=rmDefineClass("bay");

	int classIsland=rmDefineClass("island");
	int classBonusIsland=rmDefineClass("bonus island");
	rmDefineClass("corner");



// ------------------------------------------------------ Contraints ---------------------------------------------------------------------------

	// Map edge constraints
	int longPlayerConstraint=rmCreateClassDistanceConstraint("continent stays away from players", classPlayer, 24.0);
	int avoidTC=rmCreateTypeDistanceConstraint("avoid TC", "townCenter", 35.0);
	int avoidTCFar1=rmCreateTypeDistanceConstraint("avoid TC far1", "townCenter", 55.0);
	int avoidTCFar2=rmCreateTypeDistanceConstraint("avoid TC far2", "townCenter", 47.0);
	int avoidTCFar=rmCreateTypeDistanceConstraint("avoid TC far", "townCenter", 85.0);
	int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));
    int avoidCenterIsland = rmCreatePieConstraint("Avoid Center",0.5,0.5, rmXFractionToMeters(0.25),rmXFractionToMeters(0.5), rmDegreesToRadians(0),rmDegreesToRadians(360));
	int stayEdge = rmCreatePieConstraint("stay edge test",0.5,0.5, rmXFractionToMeters(0.47),rmXFractionToMeters(0.5), rmDegreesToRadians(0),rmDegreesToRadians(360));
	int stayCenterIsland = rmCreatePieConstraint("Stay Center",0.5,0.5, rmXFractionToMeters(0.0),rmXFractionToMeters(0.12), rmDegreesToRadians(0),rmDegreesToRadians(360));
	int avoidEdgeGold = rmCreatePieConstraint("Avoid Edge1",0.5,0.5, rmXFractionToMeters(0.27),rmXFractionToMeters(0.47), rmDegreesToRadians(0),rmDegreesToRadians(360));
	int avoidEdgeFood = rmCreatePieConstraint("Avoid Edge Food",0.5,0.5, rmXFractionToMeters(0.3),rmXFractionToMeters(0.47), rmDegreesToRadians(0),rmDegreesToRadians(360));


	// Player constraints
	int avoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units", rmClassID("startingUnit"), 8.0);
	int avoidStartingUnitsSmall=rmCreateClassDistanceConstraint("objects avoid starting unitss", rmClassID("startingUnit"), 5.0);


	// Resource avoidance
	int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 20.0);
	int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 1.0);
	int avoidCoin=rmCreateTypeDistanceConstraint("avoid coin", "gold", 70.0);
    int avoidCoinNorth=rmCreateTypeDistanceConstraint("avoid coin north", "gold", 35.0);
	int farAvoidCoin=rmCreateTypeDistanceConstraint("silver avoid coin", "gold", 65.0);
	int shortAvoidCoin=rmCreateTypeDistanceConstraint("silver avoid coin short", "MineTin", 30.0);
	int avoidNugget=rmCreateTypeDistanceConstraint("nugget avoid nugget", "AbstractNugget", 50.0);
	int avoidNuggetSmall=rmCreateTypeDistanceConstraint("something avoid nugget", "AbstractNugget", 8.0);
	int avoidelk=rmCreateTypeDistanceConstraint("food avoids food", "elk", 48.0);
	int avoidTurkey=rmCreateTypeDistanceConstraint("food avoids turkey", "turkey", 48.0);
	int avoidelkShort=rmCreateTypeDistanceConstraint("food avoids food short", "elk", 18.0);
	int avoidTurkeyShort=rmCreateTypeDistanceConstraint("food avoids turkey short", "turkey", 18.0);	
	int avoidelkTree=rmCreateTypeDistanceConstraint("Tree avoids food", "elk", 6.0);
	int avoidTurkeyTree=rmCreateTypeDistanceConstraint("Tree avoids turkey", "turkey", 6.0);
	int fishVsFishID=rmCreateTypeDistanceConstraint("fish v fish", "fish", 18.0);
	int fishLand = rmCreateTerrainDistanceConstraint("fish land", "land", true, 6.0);
	int avoidResource=rmCreateTypeDistanceConstraint("resource avoid resource", "resource", 10.0);
  	int avoidCoinTree=rmCreateTypeDistanceConstraint("tree avoids coin", "gold", 8.0);
	int avoidCoinTree1=rmCreateTypeDistanceConstraint("tree avoids coin1", "gold", 10.0);
  	int avoidStartingCoin=rmCreateTypeDistanceConstraint("starting coin avoids coin", "gold", 30.0);

	// Avoid impassable land
	int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 10.0);
	int shortAvoidImpassableLand=rmCreateTerrainDistanceConstraint("short avoid impassable land", "Land", false, 3.0);
	int avoidCliffs=rmCreateClassDistanceConstraint("cliff vs. cliff", rmClassID("classCliff"), 12.0);
	int avoidCliffsMin=rmCreateClassDistanceConstraint("cliff vs. cliff min", rmClassID("classCliff"), 1.0);
    int avoidTradeRoute = rmCreateTradeRouteDistanceConstraint("trade route", 6.0);
	int avoidTradeRoute1 = rmCreateTradeRouteDistanceConstraint("trade route1", 20.0);
    int avoidTradeRouteSmall = rmCreateTradeRouteDistanceConstraint("trade route small", 4.0);
	int avoidSocket=rmCreateClassDistanceConstraint("socket avoidance", rmClassID("socketClass"), 8.0);
    int avoidNatives = rmCreateClassDistanceConstraint("avoid Natives", rmClassID("natives"), 35.0);
    int avoidNativesShort = rmCreateClassDistanceConstraint("avoid Natives short", rmClassID("natives"), 10.0);	

	// Decoration avoidance
	int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 6.0);

	// VP avoidance
	int avoidImportantItem = rmCreateClassDistanceConstraint("secrets etc avoid each other", rmClassID("importantItem"), 50.0);
	int shortAvoidImportantItem = rmCreateClassDistanceConstraint("secrets etc avoid each other short", rmClassID("importantItem"), 8.0);
    int shorterAvoidImportantItem = rmCreateClassDistanceConstraint("secrets etc avoid each other shorter", rmClassID("importantItem"), 4.0);


	// Gold

									rmSetStatusText("",0.10);


// ------------------------------------------------------ Design & Trade Route ---------------------------------------------------------------------------
   int lakeID=rmCreateArea("Shallows lake");
   rmSetAreaSize(lakeID, 0.1, 0.1);
   rmSetAreaLocation(lakeID, 0.5, 0.5);
   rmSetAreaWaterType(lakeID, "bayou");
   rmSetAreaBaseHeight(lakeID, 4.0); // Was 10
   rmSetAreaMinBlobs(lakeID, 8);
   rmSetAreaMaxBlobs(lakeID, 10);
   rmSetAreaMinBlobDistance(lakeID, 10);
   rmSetAreaMaxBlobDistance(lakeID, 20);
   rmSetAreaSmoothDistance(lakeID, 50);
   rmSetAreaCoherence(lakeID, 0.25);
   rmAddAreaToClass(lakeID, rmClassID("bay"));
   rmSetAreaObeyWorldCircleConstraint(lakeID, false);
   rmBuildArea(lakeID);

    int centerIsland=rmCreateArea("center Island");
    rmSetAreaSize(centerIsland, 0.063, 0.063);
    rmSetAreaLocation(centerIsland, 0.5, 0.5);
    rmSetAreaTerrainType(centerIsland, "bayou\ground6_bay");
	 rmSetAreaMix(centerIsland, "bayou_grass");
    rmSetAreaWarnFailure(centerIsland, false);
	rmAddAreaToClass(centerIsland, classIsland);
	rmAddAreaToClass(centerIsland, classBonusIsland);
	rmSetAreaCoherence(centerIsland, 0.75);
	rmSetAreaSmoothDistance(centerIsland, 6);
	rmSetAreaElevationType(centerIsland, cElevTurbulence);
	rmSetAreaElevationVariation(centerIsland, 2.0);
	rmSetAreaBaseHeight(centerIsland, 5.0);
	rmSetAreaElevationMinFrequency(centerIsland, 0.09);
	rmSetAreaElevationOctaves(centerIsland, 3);
	rmSetAreaElevationPersistence(centerIsland, 0.2);      
	rmSetAreaObeyWorldCircleConstraint(centerIsland, false);
    rmBuildArea(centerIsland);   
   
    int landAround=rmCreateArea("land around");
    rmSetAreaSize(landAround, 0.35, 0.35);
    rmSetAreaLocation(landAround, 0.5, 0.1);
    rmSetAreaMix(landAround, "bayou_grass");
    rmSetAreaWarnFailure(landAround, false);
    rmAddAreaConstraint(landAround, avoidCenterIsland);
	rmAddAreaToClass(landAround, classIsland);
	rmSetAreaCoherence(landAround, 0.9);
	rmSetAreaSmoothDistance(landAround, 5);
	rmSetAreaElevationType(landAround, cElevTurbulence);
	rmSetAreaElevationVariation(landAround, 2.0);
	rmSetAreaBaseHeight(landAround, 4.0);
	rmSetAreaElevationMinFrequency(landAround, 0.09);
	rmSetAreaElevationOctaves(landAround, 3);
	rmSetAreaElevationPersistence(landAround, 0.2);      
	rmSetAreaObeyWorldCircleConstraint(landAround, false);
        rmBuildArea(landAround);
	
	int topIsland=rmCreateArea("top small island");
    rmSetAreaSize(topIsland, 0.06, 0.06);
    rmSetAreaLocation(topIsland, 0.5, 0.95);
	rmAddAreaInfluenceSegment(topIsland, 0.6, 0.9, 0.4, 0.9);
    rmSetAreaMix(topIsland, "bayou_grass");
    rmSetAreaWarnFailure(topIsland, false);
    rmAddAreaConstraint(topIsland, avoidCenterIsland);
	rmAddAreaToClass(topIsland, classIsland);
	rmSetAreaCoherence(topIsland, 0.7);
	rmSetAreaSmoothDistance(topIsland, 1);
	rmSetAreaElevationType(topIsland, cElevTurbulence);
	rmSetAreaElevationVariation(topIsland, 2.0);
	rmSetAreaBaseHeight(topIsland, 2.0);
	rmSetAreaElevationMinFrequency(topIsland, 0.09);
	rmSetAreaElevationOctaves(topIsland, 3);
	rmSetAreaElevationPersistence(topIsland, 0.2);      
	rmSetAreaObeyWorldCircleConstraint(topIsland, false);
	rmSetAreaEdgeFilling(topIsland, 1.0);
    rmBuildArea(topIsland);	
	
		int landAround2=rmCreateArea("land around2 top");
    rmSetAreaSize(landAround2, 0.014, 0.014);
    rmSetAreaLocation(landAround2, 0.5, 0.99);
    rmSetAreaMix(landAround2, "bayou_grass");
    rmSetAreaWarnFailure(landAround2, false);
    rmAddAreaConstraint(landAround2, stayEdge);
	rmAddAreaToClass(landAround2, classIsland);
	rmSetAreaCoherence(landAround2, 0.99);
	rmSetAreaSmoothDistance(landAround2, 12);
	rmSetAreaElevationType(landAround2, cElevTurbulence);
	rmSetAreaElevationVariation(landAround2, 2.0);
	rmSetAreaBaseHeight(landAround2, 6.0);
	rmSetAreaElevationMinFrequency(landAround2, 0.09);
	rmSetAreaElevationOctaves(landAround2, 3);
	rmSetAreaElevationPersistence(landAround2, 0.2);      
	rmSetAreaObeyWorldCircleConstraint(landAround2, false);
	rmSetAreaEdgeFilling(landAround2, 1.0);
    rmBuildArea(landAround2);
	
	int landAround3=rmCreateArea("land around3 bottom");
    rmSetAreaSize(landAround3, 0.05, 0.05);
    rmSetAreaLocation(landAround3, 0.5, 0.01);
    rmSetAreaMix(landAround3, "bayou_grass");
    rmSetAreaWarnFailure(landAround3, false);
    rmAddAreaConstraint(landAround3, stayEdge);
	rmAddAreaToClass(landAround3, classIsland);
	rmSetAreaCoherence(landAround3, 0.99);
	rmSetAreaSmoothDistance(landAround3, 12);
	rmSetAreaElevationType(landAround3, cElevTurbulence);
	rmSetAreaElevationVariation(landAround3, 2.0);
	rmSetAreaBaseHeight(landAround3, 6.0);
	rmSetAreaElevationMinFrequency(landAround3, 0.09);
	rmSetAreaElevationOctaves(landAround3, 3);
	rmSetAreaElevationPersistence(landAround3, 0.2);      
	rmSetAreaObeyWorldCircleConstraint(landAround3, false);
	rmSetAreaEdgeFilling(landAround3, 1.0);
    rmBuildArea(landAround3);		
	
   int tradeRouteID = rmCreateTradeRoute();
   int socketID=rmCreateObjectDef("sockets to dock Trade Posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);
   rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socketID, true);
   rmAddObjectDefToClass(socketID, rmClassID("socketClass"));
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 7.0);

   rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.4);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.3, 0.3, 0, 0);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.48, 0.48, 0, 0);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.7, 0.3, 0, 0);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 1.0, 0.4, 0, 0);   


   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
    vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.05);
    rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
    socketLoc = rmGetTradeRouteWayPoint(tradeRouteID,0.46);
    rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

    socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.9);
    rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

   for(i=0; <numPlayer+3)
   {
   int newGrass1 = rmCreateArea("new grass1"+i);
   rmSetAreaLocation(newGrass1, rmRandFloat(0.1,0.9), rmRandFloat(0.1,0.9)); 
   rmSetAreaWarnFailure(newGrass1, false);
   rmSetAreaSize(newGrass1, rmXMetersToFraction(5), rmXMetersToFraction(5));
   rmSetAreaCoherence(newGrass1, 0.7);
   rmSetAreaObeyWorldCircleConstraint(newGrass1, false);
   rmAddAreaConstraint(newGrass1, avoidCenterIsland);
   rmSetAreaTerrainType(newGrass1, "bayou\ground3_bay");
   rmBuildArea(newGrass1);
   }

   for(i=0; <10)
   {
	int cliffID=rmCreateArea("cliff"+i);
	rmSetAreaSize(cliffID, 0.0017, 0.0017);
	if (i==0)
	{
        rmSetAreaLocation(cliffID, 0.7, 0.5);
	}
	else if (i==1)
	{
        rmSetAreaLocation(cliffID, 0.66, 0.61);
	}
	else if (i==2)
	{
        rmSetAreaLocation(cliffID, 0.56, 0.69);
	}
	else if (i==3)
	{
        rmSetAreaLocation(cliffID, 0.44, 0.69);
	}
	else if (i==4)
	{
        rmSetAreaLocation(cliffID, 0.34, 0.62);
	}
	else if (i==5)
	{
        rmSetAreaLocation(cliffID, 0.3, 0.5);
	}
	else if (i==6)
	{
        rmSetAreaLocation(cliffID, 0.33, 0.41);
	}
	else if (i==7)
	{
        rmSetAreaLocation(cliffID, 0.44, 0.31);
	}
	else if (i==8)
	{
        rmSetAreaLocation(cliffID, 0.56, 0.31);
	}
	else
	{
        rmSetAreaLocation(cliffID, 0.67, 0.4);
	}
	rmSetAreaWarnFailure(cliffID, false);
	rmSetAreaCliffType(cliffID, "bayou");
	rmAddAreaToClass(cliffID, rmClassID("classCliff"));
        rmAddAreaConstraint(cliffID, avoidAll);
	rmSetAreaCliffEdge(cliffID, 1, 1.0, 0.1, 1.0, 0);
	rmSetAreaCliffHeight(cliffID, 8, 1.0, 1.0);
	rmSetAreaMinBlobDistance(cliffID, 3.0);
	rmSetAreaMaxBlobDistance(cliffID, 5.0);
	rmSetAreaCoherence(cliffID, 0.5);
	rmSetAreaObeyWorldCircleConstraint(cliffID, false);
        rmBuildArea(cliffID);
   }


   								rmSetStatusText("",0.50);

	int SeminolesVillageID1 = -1;
	int SeminolesVillageType = rmRandInt(1,5);
	SeminolesVillageID1 = rmCreateGrouping("Seminole village", "native seminole village "+SeminolesVillageType);
	rmSetGroupingMinDistance(SeminolesVillageID1, 0.0);
	rmSetGroupingMaxDistance(SeminolesVillageID1, 0.0);
	rmAddGroupingToClass(SeminolesVillageID1, rmClassID("importantItem"));
	rmAddGroupingToClass(SeminolesVillageID1, rmClassID("natives"));
	rmPlaceGroupingAtLoc(SeminolesVillageID1, 0, 0.5, 0.12);								
								
	int	SeminolesVillageID = rmCreateGrouping("Seminole village1", "native seminole village "+3);
	rmSetGroupingMinDistance(SeminolesVillageID, 0.0);
	rmSetGroupingMaxDistance(SeminolesVillageID, 0.0);
	rmAddGroupingToClass(SeminolesVillageID, rmClassID("importantItem"));
	rmAddGroupingToClass(SeminolesVillageID, rmClassID("natives"));
	rmPlaceGroupingAtLoc(SeminolesVillageID, 0, 0.5, 0.91);								
							

// ------------------------------------------------------ Starting Ressources ---------------------------------------------------------------------------
 if (numPlayer==2 && cNumberTeams ==2)
 {
	if (rmRandFloat(0,1)>0.5)
	{
    rmSetPlacementTeam(0);
   	rmSetPlacementSection(0.24, 0.4);
   	rmPlacePlayersCircular(0.33, 0.34, 0);

   	rmSetPlacementTeam(1);
   	rmSetPlacementSection(0.77, 0.99);
   	rmPlacePlayersCircular(0.33, 0.34, 0);
	}
	else
	{
	rmSetPlacementTeam(1);
   	rmSetPlacementSection(0.24, 0.4);
   	rmPlacePlayersCircular(0.33, 0.34, 0);

   	rmSetPlacementTeam(0);
   	rmSetPlacementSection(0.77, 0.99);
   	rmPlacePlayersCircular(0.33, 0.34, 0);		
	}
}	
else if (numPlayer==8 && cNumberTeams ==2)
{
	rmSetPlacementTeam(1);
   	rmSetPlacementSection(0.24, 0.42);
   	rmPlacePlayersCircular(0.35, 0.36, 0);

   	rmSetPlacementTeam(0);
   	rmSetPlacementSection(0.58, 0.77);
   	rmPlacePlayersCircular(0.35, 0.36, 0);			
}
else if (cNumberTeams ==2)
{
	rmSetPlacementTeam(1);
   	rmSetPlacementSection(0.24, 0.4);
   	rmPlacePlayersCircular(0.35, 0.36, 0);

   	rmSetPlacementTeam(0);
   	rmSetPlacementSection(0.61, 0.77);
   	rmPlacePlayersCircular(0.35, 0.36, 0);			
}
else
{
	rmPlacePlayer(1, 0.82, 0.53);
	rmPlacePlayer(2, 0.18, 0.53);
	rmPlacePlayer(3, 0.4, 0.2);
	rmPlacePlayer(5, 0.65, 0.2);
	rmPlacePlayer(4, 0.61, 0.9);
	rmPlacePlayer(6, 0.39, 0.9);
	rmPlacePlayer(7, 0.15, 0.3);
	rmPlacePlayer(8, 0.85, 0.3);
}	



	int startingUnits = rmCreateStartingUnitsObjectDef(5.0);
	rmSetObjectDefMinDistance(startingUnits, 10.0);
	rmSetObjectDefMaxDistance(startingUnits, 15.0);
	rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));
	rmAddObjectDefToClass(startingUnits, rmClassID("player"));

	int TCID = rmCreateObjectDef("player TC");
	if (rmGetNomadStart())
		{
			rmAddObjectDefItem(TCID, "CoveredWagon", 1, 0.0);
		}
		else
		{
            rmAddObjectDefItem(TCID, "townCenter", 1, 0);
		}
	rmSetObjectDefMinDistance(TCID, 0.0);
	rmSetObjectDefMaxDistance(TCID, 0.0);
	rmAddObjectDefToClass(TCID, rmClassID("player"));
	rmAddObjectDefToClass(TCID, rmClassID("startingUnit"));

	int StartElkID=rmCreateObjectDef("starting elk");
	rmAddObjectDefItem(StartElkID, "elk", 5, 5.0);
	rmSetObjectDefMinDistance(StartElkID, 8.0);
	rmSetObjectDefMaxDistance(StartElkID, 10.0);
	rmSetObjectDefCreateHerd(StartElkID, true);
	rmAddObjectDefConstraint(StartElkID, avoidImpassableLand);
	rmAddObjectDefConstraint(StartElkID, avoidStartingUnits);
    rmAddObjectDefConstraint(StartElkID, avoidelkShort);
	rmAddObjectDefConstraint(StartElkID, avoidTurkeyShort);
	rmAddObjectDefConstraint(StartElkID, avoidSocket);

	int StartElkID1=rmCreateObjectDef("starting elk1");
	rmAddObjectDefItem(StartElkID1, "elk", rmRandInt(11,12), 9.0);
	rmSetObjectDefMinDistance(StartElkID1, 32.0);
	rmSetObjectDefMaxDistance(StartElkID1, 34.0);
	rmSetObjectDefCreateHerd(StartElkID1, true);
	rmAddObjectDefConstraint(StartElkID1, shortAvoidImpassableLand);
	rmAddObjectDefConstraint(StartElkID1, avoidStartingUnits);
	rmAddObjectDefConstraint(StartElkID1, avoidelkShort);
	rmAddObjectDefConstraint(StartElkID1, avoidTurkeyShort);
	rmAddObjectDefConstraint(StartElkID1, avoidSocket);
	rmAddObjectDefConstraint(StartElkID1, avoidCliffsMin);
	
    int StartTurkeyID=rmCreateObjectDef("starting turkey");
	rmAddObjectDefItem(StartTurkeyID, "turkey", rmRandInt(5,6), 6.0);
	rmSetObjectDefMinDistance(StartTurkeyID, 15.0);
	rmSetObjectDefMaxDistance(StartTurkeyID, 17.0);
	rmSetObjectDefCreateHerd(StartTurkeyID, true);
	rmAddObjectDefConstraint(StartTurkeyID, avoidImpassableLand);
	rmAddObjectDefConstraint(StartTurkeyID, avoidStartingUnits);
	rmAddObjectDefConstraint(StartTurkeyID, avoidelkShort);
	rmAddObjectDefConstraint(StartTurkeyID, avoidTurkeyShort);
	rmAddObjectDefConstraint(StartTurkeyID, avoidSocket);
	rmAddObjectDefConstraint(StartTurkeyID, avoidCliffsMin);
	
	int StartAreaTreeID=rmCreateObjectDef("starting trees");
	rmAddObjectDefItem(StartAreaTreeID, "TreeBayou", rmRandInt(2,4), 4.0);
	rmSetObjectDefMinDistance(StartAreaTreeID, 13);
	rmSetObjectDefMaxDistance(StartAreaTreeID, 19);
	rmAddObjectDefToClass(StartAreaTreeID, rmClassID("startingUnit"));
	rmAddObjectDefConstraint(StartAreaTreeID, avoidImpassableLand);
	rmAddObjectDefConstraint(StartAreaTreeID, avoidStartingUnits);
	rmAddObjectDefConstraint(StartAreaTreeID, avoidSocket);

	int startBerryID=rmCreateObjectDef("starting berries");
	rmAddObjectDefItem(startBerryID, "berrybush", 6, 5.0);
	rmSetObjectDefCreateHerd(startBerryID, true);
	rmSetObjectDefMinDistance(startBerryID, 10);
	rmSetObjectDefMaxDistance(startBerryID, 15);
	rmAddObjectDefToClass(startBerryID, rmClassID("startingUnit"));
	rmAddObjectDefConstraint(startBerryID, avoidImpassableLand);
	rmAddObjectDefConstraint(startBerryID, avoidStartingUnitsSmall);
	rmAddObjectDefConstraint(startBerryID, avoidResource);
	rmAddObjectDefConstraint(startBerryID, avoidSocket);

	int playerNuggetID=rmCreateObjectDef("player nugget");
	rmAddObjectDefItem(playerNuggetID, "nugget", 1, 0.0);
	rmAddObjectDefToClass(playerNuggetID, rmClassID("secrets"));
	rmAddObjectDefToClass(playerNuggetID, rmClassID("startingUnit"));
    rmSetObjectDefMinDistance(playerNuggetID, 23.0);
    rmSetObjectDefMaxDistance(playerNuggetID, 32.0);
	rmAddObjectDefConstraint(playerNuggetID, avoidNugget);
	rmAddObjectDefConstraint(playerNuggetID, avoidStartingUnitsSmall);
	rmAddObjectDefConstraint(playerNuggetID, avoidTradeRouteSmall);
	rmAddObjectDefConstraint(playerNuggetID, avoidCliffsMin);
	rmAddObjectDefConstraint(playerNuggetID, circleConstraint);
	rmAddObjectDefConstraint(playerNuggetID, avoidSocket);
	
	

   rmBuildAllAreas(); 
   
   
  // Text
   rmSetStatusText("",0.60);

   for(i=1; <cNumberPlayers)
	{
		
		int playerGoldID = rmCreateObjectDef("player silver closer "+i);
		rmAddObjectDefItem(playerGoldID, "MineTin", 1, 0.0);
		rmAddObjectDefConstraint(playerGoldID, avoidTradeRoute);
		rmAddObjectDefConstraint(playerGoldID, avoidStartingCoin);
		rmAddObjectDefConstraint(playerGoldID, avoidStartingUnitsSmall);
		rmSetObjectDefMinDistance(playerGoldID, 15.0);
		rmSetObjectDefMaxDistance(playerGoldID, 16.0);

		rmPlaceObjectDefAtLoc(TCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

		rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
if (numPlayer ==2)
{
		rmPlaceObjectDefAtLoc(playerGoldID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(playerGoldID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
}
		//rmPlaceObjectDefAtLoc(StartElkID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(startBerryID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartTurkeyID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartElkID1, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));	
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		
		rmSetNuggetDifficulty(1, 1);
		rmPlaceObjectDefAtLoc(playerNuggetID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(playerNuggetID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(playerNuggetID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
        
    if(ypIsAsian(i) && rmGetNomadStart() == false)
      rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		//vector closestPoint=rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingUnits, i));
		//rmSetHomeCityGatherPoint(i, closestPoint);
	}

if (numPlayer>2)
{
int mineb=rmAddFairLoc("TownCenter", false, false, 18, 19, 12, 5); 
rmAddObjectDefConstraint(mineb, avoidStartingCoin);
rmAddObjectDefConstraint(mineb, avoidStartingUnits);

	if(rmPlaceFairLocs())
	{
	mineb=rmCreateObjectDef("mine behind");
	rmAddObjectDefItem(mineb, "MineTin", 1, 0.0);
	for(i=1; <numPlayer+1)
		{
	for(j=0; <rmGetNumberFairLocs(i))
			{
	rmPlaceObjectDefAtLoc(mineb, i, rmFairLocXFraction(i, j), rmFairLocZFraction(i, j), 1);
			}
		}
	}

int minef=rmAddFairLoc("TownCenter", true, true, 18, 19, 12, 5);
rmAddObjectDefConstraint(minef, avoidStartingCoin);
rmAddObjectDefConstraint(minef, avoidStartingUnits);


	if(rmPlaceFairLocs())
	{
	minef=rmCreateObjectDef("forward mine");
	rmAddObjectDefItem(minef, "MineTin", 1, 0.0);
	for(i=1; <numPlayer+1)
	{
	for(j=1; <rmGetNumberFairLocs(i))
	{
	rmPlaceObjectDefAtLoc(minef, i, rmFairLocXFraction(i, j), rmFairLocZFraction(i, j), 1);
	}
	}
	}
}

  // check for KOTH game mode
  if(rmGetIsKOTH()) {
    
    int randLoc = rmRandInt(1,2);
    float xLoc = 0.0;
    
    if(randLoc == 1)
      xLoc = .6;
    
    else
      xLoc = .4;
    
    ypKingsHillPlacer(xLoc, .5, 0.1, longPlayerConstraint);
    rmEchoInfo("XLOC = "+xLoc);
  }

  							 rmSetStatusText("",0.70);

// ------------------------------------------------------ Natives & Nuggets & Design ---------------------------------------------------------------------------
    



// ------------------------------------------------------ Others Ressources ---------------------------------------------------------------------------

if (numPlayer==2)
    {
	int silverID1 = rmCreateObjectDef("North West small island mine");
	rmAddObjectDefItem(silverID1, "mine", 1, 0);
	rmSetObjectDefMinDistance(silverID1, 0.0);
	rmSetObjectDefMaxDistance(silverID1, rmXFractionToMeters(0.05));
	rmPlaceObjectDefAtLoc(silverID1, 0, 0.5, 0.9);

		int silverID4 = rmCreateObjectDef("Center island mine");
	rmAddObjectDefItem(silverID4, "mine", 1, 0);
	rmSetObjectDefMinDistance(silverID4, 0.0);
	rmSetObjectDefMaxDistance(silverID4, rmXFractionToMeters(0.05));
	rmAddObjectDefConstraint(silverID4, avoidTradeRoute);
	rmAddObjectDefConstraint(silverID4, avoidSocket);
	rmAddObjectDefConstraint(silverID4, avoidTCFar);
	rmAddObjectDefConstraint(silverID4, stayCenterIsland);
	rmPlaceObjectDefAtLoc(silverID4, 0, 0.5, 0.5);
	
	for (i=0; < 4)
	{
	int silverID2 = rmCreateObjectDef("4 other mines"+i);
	rmAddObjectDefItem(silverID2, "mine", 1, 0);
	rmSetObjectDefMinDistance(silverID2, 0.0);
	rmSetObjectDefMaxDistance(silverID2, rmXFractionToMeters(0.45));
	rmAddObjectDefConstraint(silverID2, avoidTradeRoute1);
	rmAddObjectDefConstraint(silverID2, avoidEdgeGold);
	rmAddObjectDefConstraint(silverID2, avoidCoin);
	rmAddObjectDefConstraint(silverID2, avoidTCFar1);
	rmAddObjectDefConstraint(silverID2, avoidNatives);
	rmPlaceObjectDefAtLoc(silverID2, 0, 0.5, 0.5);
	}


    }
else
    {
	for (i=0; < cNumberNonGaiaPlayers*3)
	{
	int silverIDT = rmCreateObjectDef("silver for team"+i);
	rmAddObjectDefItem(silverIDT, "mine", 1, 0);
	rmSetObjectDefMinDistance(silverIDT, 0.0);
	rmSetObjectDefMaxDistance(silverIDT, rmXFractionToMeters(0.45));
	rmAddObjectDefConstraint(silverIDT, farAvoidCoin);
	rmAddObjectDefConstraint(silverIDT, avoidAll);
	rmAddObjectDefConstraint(silverIDT, avoidCliffs);
	rmAddObjectDefConstraint(silverIDT, avoidTCFar1);
	rmAddObjectDefConstraint(silverIDT, avoidCoin);
	rmAddObjectDefConstraint(silverIDT, avoidStartingUnits);
	rmAddObjectDefConstraint(silverIDT, shortAvoidImportantItem);
	rmPlaceObjectDefAtLoc(silverIDT, 0, 0.5, 0.5);
	}
    }

   // ************************* Forest **************************
   int forestTreeID = 0;
   int numTries=6*numPlayer+6;
   int failCount=0;
   for (i=0; <numTries)
      {   
         int forest=rmCreateArea("forest "+i);
         rmSetAreaWarnFailure(forest, false);
         rmSetAreaSize(forest, rmAreaTilesToFraction(250), rmAreaTilesToFraction(350));
         rmSetAreaForestType(forest, "bayou swamp forest");
         rmSetAreaForestDensity(forest, 0.4);
         rmSetAreaForestClumpiness(forest, 0.0);
         rmSetAreaForestUnderbrush(forest, 0.0);
         rmSetAreaMinBlobs(forest, 1);
         rmSetAreaMaxBlobs(forest, 4);
         rmSetAreaMinBlobDistance(forest, 16.0);
         rmSetAreaMaxBlobDistance(forest, 20.0);
         rmSetAreaCoherence(forest, 0.4);
         rmSetAreaSmoothDistance(forest, 10);
         rmAddAreaToClass(forest, rmClassID("classForest")); 
         rmAddAreaConstraint(forest, forestConstraint);
         rmAddAreaConstraint(forest, avoidAll);
         rmAddAreaConstraint(forest, avoidCoinTree);
         rmAddAreaConstraint(forest, avoidSocket);
         rmAddAreaConstraint(forest, shortAvoidImpassableLand);
         rmAddAreaConstraint(forest, avoidTradeRouteSmall);
		 rmAddAreaConstraint(forest, avoidTC);
         rmAddAreaConstraint(forest, avoidNativesShort); 		 
         if(rmBuildArea(forest)==false)
         {
            // Stop trying once we fail 3 times in a row.
            failCount++;
            if(failCount==5)
               break;
         }
         else
            failCount=0; 
      }

    // ******************************* FOOD ************************************
	
	if (numPlayer==2)
	{
	int elkID=rmCreateObjectDef("elk herd");
	rmAddObjectDefItem(elkID, "elk", rmRandInt(8,10), 10.0);
	rmSetObjectDefMinDistance(elkID, 0.0);
	rmSetObjectDefMaxDistance(elkID, rmXFractionToMeters(0.1));
	rmAddObjectDefConstraint(elkID, avoidelkShort);
	rmAddObjectDefConstraint(elkID, avoidTurkeyShort);
	rmAddObjectDefConstraint(elkID, avoidAll);
	rmAddObjectDefConstraint(elkID, shortAvoidImpassableLand);
    rmAddObjectDefConstraint(elkID, avoidCoinTree);
	rmSetObjectDefCreateHerd(elkID, true);
	rmPlaceObjectDefAtLoc(elkID, 0, 0.5, 0.92, 1);

	int turkeyID=rmCreateObjectDef("turkey flock");
	rmAddObjectDefItem(turkeyID, "turkey", rmRandInt(10,14), 10.0);
	rmSetObjectDefMinDistance(turkeyID, 0.0);
	rmSetObjectDefMaxDistance(turkeyID, rmXFractionToMeters(0.1));
	rmAddObjectDefConstraint(turkeyID, avoidelkShort);
	rmAddObjectDefConstraint(turkeyID, avoidTurkeyShort);
	rmAddObjectDefConstraint(turkeyID, avoidAll);
	rmAddObjectDefConstraint(turkeyID, shortAvoidImpassableLand);
    rmAddObjectDefConstraint(turkeyID, avoidCoinTree);
	rmSetObjectDefCreateHerd(turkeyID, true);
	rmPlaceObjectDefAtLoc(turkeyID, 0, 0.5, 0.92, 1);
	
	int turkeyIDC=rmCreateObjectDef("turkey flockC");
	rmAddObjectDefItem(turkeyIDC, "turkey", rmRandInt(12,14), 10.0);
	rmSetObjectDefMinDistance(turkeyIDC, 0.0);
	rmSetObjectDefMaxDistance(turkeyIDC, rmXFractionToMeters(0.08));
	rmAddObjectDefConstraint(turkeyIDC, avoidAll);
	rmAddObjectDefConstraint(turkeyIDC, shortAvoidImpassableLand);
    rmAddObjectDefConstraint(turkeyIDC, avoidSocket);
	rmAddObjectDefConstraint(turkeyIDC, avoidTradeRouteSmall);
	rmAddObjectDefConstraint(turkeyIDC, stayCenterIsland);
	rmSetObjectDefCreateHerd(turkeyIDC, true);
	rmPlaceObjectDefAtLoc(turkeyIDC, 0, 0.5, 0.5, 1);

	int turkeyID1=rmCreateObjectDef("turkey flock1");
	rmAddObjectDefItem(turkeyID1, "turkey", rmRandInt(12,13), 14.0);
	rmSetObjectDefMinDistance(turkeyID1, 0.0);
	rmSetObjectDefMaxDistance(turkeyID1, 15);
	rmAddObjectDefConstraint(turkeyID1, avoidAll);
    rmAddObjectDefConstraint(turkeyID1, avoidCoinTree);
	rmSetObjectDefCreateHerd(turkeyID1, true);
	rmPlaceObjectDefAtLoc(turkeyID1, 0, 0.75, 0.8, 1);
	
	int turkeyID2=rmCreateObjectDef("turkey flock2");
	rmAddObjectDefItem(turkeyID2, "turkey", rmRandInt(12,13), 14.0);
	rmSetObjectDefMinDistance(turkeyID2, 0.0);
	rmSetObjectDefMaxDistance(turkeyID2, 15);
    rmAddObjectDefConstraint(turkeyID2, avoidAll);
    rmAddObjectDefConstraint(turkeyID2, avoidCoinTree);
	rmSetObjectDefCreateHerd(turkeyID2, true);
	rmPlaceObjectDefAtLoc(turkeyID2, 0, 0.25, 0.8, 1);
	
    int elkID1=rmCreateObjectDef("elk herd1");
	rmAddObjectDefItem(elkID1, "elk", rmRandInt(11,12), 14.0);
	rmSetObjectDefMinDistance(elkID1, 0.0);
	rmSetObjectDefMaxDistance(elkID1, 20);
	rmAddObjectDefConstraint(elkID1, avoidAll);
	rmAddObjectDefConstraint(elkID1, shortAvoidImpassableLand);
    rmAddObjectDefConstraint(elkID1, avoidCoinTree);
	rmSetObjectDefCreateHerd(elkID1, true); 
	rmPlaceObjectDefAtLoc(elkID1, 0, 0.75, 0.22, 1);

    int elkID2=rmCreateObjectDef("elk herd2");
	rmAddObjectDefItem(elkID2, "elk", rmRandInt(11,12), 14.0);
	rmSetObjectDefMinDistance(elkID2, 0.0);
	rmSetObjectDefMaxDistance(elkID2, 20);
	rmAddObjectDefConstraint(elkID2, avoidAll);
	rmAddObjectDefConstraint(elkID2, shortAvoidImpassableLand);
   rmAddObjectDefConstraint(elkID2, avoidCoinTree);
	rmSetObjectDefCreateHerd(elkID2, true); 
	rmPlaceObjectDefAtLoc(elkID2, 0, 0.25, 0.2, 1); 	
	
   }
else
   {
	turkeyID2=rmCreateObjectDef("turkey flock team");
	rmAddObjectDefItem(turkeyID2, "turkey", rmRandInt(12,13), 14.0);
	rmSetObjectDefMinDistance(turkeyID2, 0.0);
	rmSetObjectDefMaxDistance(turkeyID2, rmXFractionToMeters(0.5));
    rmAddObjectDefConstraint(turkeyID2, avoidAll);
    rmAddObjectDefConstraint(turkeyID2, avoidCoinTree);
	rmAddObjectDefConstraint(turkeyID2, avoidelk);
	rmAddObjectDefConstraint(turkeyID2, avoidTurkey);	
	rmAddObjectDefConstraint(turkeyID2, avoidSocket);
	rmAddObjectDefConstraint(turkeyID2, avoidTCFar1);
	rmAddObjectDefConstraint(turkeyID2, avoidCliffs);
	rmSetObjectDefCreateHerd(turkeyID2, true);
	rmPlaceObjectDefAtLoc(turkeyID2, 0, 0.5, 0.5, numPlayer*2);
	
    elkID1=rmCreateObjectDef("elk herd team");
	rmAddObjectDefItem(elkID1, "elk", rmRandInt(11,12), 14.0);
	rmSetObjectDefMinDistance(elkID1, 0.0);
	rmSetObjectDefMaxDistance(elkID1, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(elkID1, avoidAll);
	rmAddObjectDefConstraint(elkID1, avoidCliffs);
	rmAddObjectDefConstraint(elkID1, avoidTCFar1);
	rmAddObjectDefConstraint(elkID1, avoidTurkey);
	rmAddObjectDefConstraint(elkID1, avoidelk);
	rmAddObjectDefConstraint(elkID1, avoidSocket);
    rmAddObjectDefConstraint(elkID1, avoidCoinTree);
	rmSetObjectDefCreateHerd(elkID1, true); 
	rmPlaceObjectDefAtLoc(elkID1, 0, 0.5, 0.5, numPlayer*2);	
   }
	// Text
	rmSetStatusText("",0.90);

// ------------------------------------------------ Nuggets -------------------------------------------------------------------
		rmSetNuggetDifficulty(2, 2);
		int nugget3= rmCreateObjectDef("nugget hard"); 
		rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
		rmSetNuggetDifficulty(2, 2);
		rmSetObjectDefMinDistance(nugget3, 0.0);
		rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.5));
		rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
		rmAddObjectDefConstraint(nugget3, shortAvoidImportantItem);
		rmAddObjectDefConstraint(nugget3, avoidNugget);
		rmAddObjectDefConstraint(nugget3, circleConstraint);
		rmAddObjectDefConstraint(nugget3, avoidCliffs);
		rmAddObjectDefConstraint(nugget3, avoidAll);
		rmAddObjectDefConstraint(nugget3, avoidTradeRoute);
		rmAddAreaConstraint(nugget3, avoidCoinTree);
		rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, 4*numPlayer);

	
// ------------------------------------------------ Embelissment --------------------------------------------------------------
	int avoidEagles=rmCreateTypeDistanceConstraint("avoids Eagles", "EaglesNest", 40.0);

	int randomEagleTreeID=rmCreateObjectDef("random eagle tree");
	rmAddObjectDefItem(randomEagleTreeID, "EaglesNest", 1, 0.0);
	rmSetObjectDefMinDistance(randomEagleTreeID, 0.0);
	rmSetObjectDefMaxDistance(randomEagleTreeID, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(randomEagleTreeID, avoidAll);
	rmAddObjectDefConstraint(randomEagleTreeID, shortAvoidImpassableLand);
	rmAddObjectDefConstraint(randomEagleTreeID, avoidEagles);
	rmAddObjectDefConstraint(randomEagleTreeID, avoidCoinTree);
	rmPlaceObjectDefAtLoc(randomEagleTreeID, 0, 0.5, 0.5, 2*numPlayer);

	int treeVsLand = rmCreateTerrainDistanceConstraint("tree v. land", "land", true, 2.0);
	int nearShore=rmCreateTerrainMaxDistanceConstraint("tree v. water", "land", true, 14.0);

	int randomWaterTreeID=rmCreateObjectDef("random tree in water");
	rmAddObjectDefItem(randomWaterTreeID, "treeBayouMarsh", 3, 0.0);
	rmSetObjectDefMinDistance(randomWaterTreeID, 0.0);
	rmSetObjectDefMaxDistance(randomWaterTreeID, rmXFractionToMeters(0.5));
	//rmAddObjectDefConstraint(randomWaterTreeID, nearShore);
	rmAddObjectDefConstraint(randomWaterTreeID, treeVsLand);
	rmAddObjectDefConstraint(randomWaterTreeID, avoidCoinTree);

	int randomTurtlesID=rmCreateObjectDef("random turtles in water");
	rmAddObjectDefItem(randomTurtlesID, "propTurtles", 1, 3.0);
	rmSetObjectDefMinDistance(randomTurtlesID, 0.0);
	rmSetObjectDefMaxDistance(randomTurtlesID, rmXFractionToMeters(0.5));
	//rmAddObjectDefConstraint(randomTurtlesID, nearShore);
	rmAddObjectDefConstraint(randomTurtlesID, treeVsLand);
	rmAddObjectDefConstraint(randomTurtlesID, avoidCoinTree);

	int randomWaterRocksID=rmCreateObjectDef("random rocks in water");
	rmAddObjectDefItem(randomWaterRocksID, "underbrushLake", rmRandInt(3,6), 3.0);
	rmSetObjectDefMinDistance(randomWaterRocksID, 0.0);
	rmSetObjectDefMaxDistance(randomWaterRocksID, rmXFractionToMeters(0.5));
	//rmAddObjectDefConstraint(randomWaterRocksID, nearShore);
	rmAddObjectDefConstraint(randomWaterRocksID, treeVsLand);
	rmAddObjectDefConstraint(randomWaterRocksID, avoidCoinTree);

	int avoidGeese=rmCreateTypeDistanceConstraint("avoids geese", "PropSwan", 80.0);
	int avoidDucks=rmCreateTypeDistanceConstraint("avoids ducks", "DuckFamily", 50.0);

	int randomGeeseID=rmCreateObjectDef("random Geese in water");
	rmAddObjectDefItem(randomGeeseID, "PropSwan", 1, 0.0);
	rmSetObjectDefMinDistance(randomGeeseID, 0.0);
	rmSetObjectDefMaxDistance(randomGeeseID, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(randomGeeseID, avoidGeese);
	rmAddObjectDefConstraint(randomGeeseID, treeVsLand);
	rmAddObjectDefConstraint(randomGeeseID, avoidCoinTree);

	int randomDucksID=rmCreateObjectDef("random ducks in water");
	rmAddObjectDefItem(randomDucksID, "DuckFamily", 1, 0.0);
	rmSetObjectDefMinDistance(randomDucksID, 0.0);
	rmSetObjectDefMaxDistance(randomDucksID, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(randomDucksID, avoidGeese);
	rmAddObjectDefConstraint(randomDucksID, avoidDucks);
	rmAddObjectDefConstraint(randomDucksID, treeVsLand);
	rmAddObjectDefConstraint(randomDucksID, avoidCoinTree);

	int fishID=rmCreateObjectDef("fish");
	rmAddObjectDefItem(fishID, "FishSalmon", 3, 9.0);
	rmSetObjectDefMinDistance(fishID, 0.0);
	rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(fishID, fishVsFishID);
	rmAddObjectDefConstraint(fishID, fishLand);
	rmAddObjectDefConstraint(fishID, avoidCoinTree);

	rmPlaceObjectDefAtLoc(randomWaterTreeID, 0, 0.5, 0.5, 10*numPlayer);
	rmPlaceObjectDefAtLoc(randomTurtlesID, 0, 0.5, 0.5, 2*numPlayer);
	rmPlaceObjectDefAtLoc(randomWaterRocksID, 0, 0.5, 0.5, 20*numPlayer);
	rmPlaceObjectDefAtLoc(randomGeeseID, 0, 0.5, 0.5, 2*numPlayer);
	rmPlaceObjectDefAtLoc(randomDucksID, 0, 0.5, 0.5, 2*numPlayer);	
	
   rmSetStatusText("",0.99);
}
