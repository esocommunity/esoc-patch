// RM script of Lombardy
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Chooses the 3 natives: 2 Franks and 1 Goths

   // Native 1
   rmSetSubCiv(0, "Comanche");

   // Native 2
   rmSetSubCiv(1, "Comanche");

   // Native 3
   rmSetSubCiv(2, "Comanche");


   // Picks the map size
   int playerTiles = 11000;
   if (cNumberNonGaiaPlayers > 4)
      playerTiles = 10000;
   if (cNumberNonGaiaPlayers > 6)
      playerTiles = 8500;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);
   

   // Chooses the side where teams start in team games
   int teamSide = rmRandInt(1,2);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("honshu");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.10, 4, 0.4, 5.0);
   rmTerrainInitialize("california\ground5_cal", 1.0);
   rmSetMapType("grass");
   rmSetMapType("water");
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   int classCliff=rmDefineClass("cliff");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("classTreeStraggler");
   rmDefineClass("nuggets");
   rmDefineClass("natives");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));
   int edgeConstraint=rmCreatePieConstraint("edge Constraint", 0.5, 0.5, rmZFractionToMeters(0.45), rmZFractionToMeters(0.50), rmDegreesToRadians(10), rmDegreesToRadians(140));
   int edgeConstraint2=rmCreatePieConstraint("edge Constraint 2", 0.5, 0.5, rmZFractionToMeters(0.45), rmZFractionToMeters(0.50), rmDegreesToRadians(190), rmDegreesToRadians(320));
   int edgeConstraint3=rmCreatePieConstraint("edge Constraint 3", 0.5, 0.5, rmZFractionToMeters(0.40), rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));
   // Player constraints
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 60.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 45.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 35.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 30.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 15.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 40.0);
   int shortForestConstraint=rmCreateClassDistanceConstraint("forest vs. forest short", rmClassID("classForest"), 10.0);
   int stragglerTreeConstraint=rmCreateClassDistanceConstraint("straggler tree constraint", rmClassID("classTreeStraggler"), 20.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 60.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 55.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 5.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 60.0);

   // Area avoidance
   int cliffConstraint=rmCreateClassDistanceConstraint("avoid cliff", classCliff, 5.0);

   // Native avoidance
   int avoidNatives=rmCreateClassDistanceConstraint("things avoids natives", rmClassID("natives"), 5.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);

   // Water objects avoidance	
   int fishVsFish=rmCreateTypeDistanceConstraint("fish avoid fish", "ypFishTuna", 25.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish avoid land", "land", true, 5.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag avoid land", "land", true, 15.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid flags", "HomeCityWaterSpawnFlag", 25);


   // Text
   rmSetStatusText("",0.10);


   // Define and place cliffs to edges

   // Cliffs to north-east
   int cliffID=rmCreateArea("cliff");
   rmSetAreaSize(cliffID, 0.1, 0.1);
   //rmSetAreaLocation(cliffID, 0.5, 0.5);
   rmSetAreaBaseHeight(cliffID, 12);
   rmSetAreaTerrainType(cliffID, "patagonia\ground_snow1_pat");
   rmSetAreaCoherence(cliffID, 0.7);
   rmSetAreaSmoothDistance(cliffID, 10);
   rmSetAreaElevationType(cliffID, cElevTurbulence);
   rmSetAreaElevationVariation(cliffID, 15.0);
   rmSetAreaHeightBlend(cliffID, 1);
   rmAddAreaToClass(cliffID, classCliff);
   rmAddAreaConstraint(cliffID, edgeConstraint);
   rmSetAreaObeyWorldCircleConstraint(cliffID, false);
   rmBuildArea(cliffID);

   // Cliffs to south-West
   int cliff2ID=rmCreateArea("cliff 2");
   rmSetAreaSize(cliff2ID, 0.1, 0.1);
   //rmSetAreaLocation(cliff2ID, 0.5, 0.5);
   rmSetAreaBaseHeight(cliff2ID, 12);
   rmSetAreaTerrainType(cliff2ID, "patagonia\ground_snow1_pat");
   rmSetAreaCoherence(cliff2ID, 0.7);
   rmSetAreaSmoothDistance(cliff2ID, 10);
   rmSetAreaElevationType(cliff2ID, cElevTurbulence);
   rmSetAreaElevationVariation(cliff2ID, 15.0);
   rmSetAreaHeightBlend(cliff2ID, 1);
   rmAddAreaToClass(cliff2ID, classCliff);
   rmAddAreaConstraint(cliff2ID, edgeConstraint2);
   rmSetAreaObeyWorldCircleConstraint(cliff2ID, false);
   rmBuildArea(cliff2ID);



   // Define and place water areas

   // If FFA --> don't place rivers
   if (rmGetIsFFA() == false)
   {
      // Define and place river to up
      int river = rmRiverCreate(-1, "great lakes", 10, 10, 16, 16);
      rmRiverSetConnections(river, 0.5, 0.8, 0.3, 1.0);
      rmRiverBuild(river);

      // Define and place river 2 to down
      int river2 = rmRiverCreate(-1, "great lakes", 10, 10, 16, 16);
      rmRiverSetConnections(river2, 0.5, 0.2, 0.7, 0.0);
      rmRiverBuild(river2);
   }

   // Define and place lake
   int lakeID=rmCreateArea("lake");
   rmSetAreaWaterType(lakeID, "great lakes");
   rmSetAreaSize(lakeID, 0.25, 0.25);
   rmSetAreaLocation(lakeID, 0.5, 0.5);
   rmAddAreaInfluenceSegment(lakeID, 0.5, 0.7, 0.5, 0.3);
   rmSetAreaBaseHeight(lakeID, 0.0);
   rmSetAreaCoherence(lakeID, 0.7);
   rmSetAreaSmoothDistance(lakeID, 20);
   rmBuildArea(lakeID);


   // Define and place island, if more than 6 players

   if (cNumberNonGaiaPlayers >= 6)
   {
      int islandID=rmCreateArea("island");
      rmSetAreaTerrainType(islandID, "california\ground5_cal");
      rmSetAreaSize(islandID, 0.02, 0.02);
      rmSetAreaLocation(islandID, 0.5, 0.5);
      rmSetAreaBaseHeight(islandID, 1.0);
      rmSetAreaCoherence(islandID, 0.6);
      rmSetAreaSmoothDistance(islandID, 5);
      rmSetAreaElevationType(islandID, cElevTurbulence);
      rmSetAreaElevationVariation(islandID, 5.0);
      rmSetAreaElevationMinFrequency(islandID, 0.10);
      rmSetAreaElevationOctaves(islandID, 3);
      rmSetAreaElevationPersistence(islandID, 0.2);
      rmSetAreaElevationNoiseBias(islandID, 1);
      rmBuildArea(islandID);
   }


   // Text
   rmSetStatusText("",0.20);


   // Place players in FFA and team games

   // Player placement if FFA (Free For All) - place players in circle
   if(cNumberTeams > 2)
      rmPlacePlayersCircular(0.25, 0.25, 0.0);

   // Player placement if teams - place teams in circle to north-east or south-west
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      if (teamSide == 1)
            rmSetPlacementSection(0.15, 0.35);
      else if (teamSide == 2)
            rmSetPlacementSection(0.65, 0.85);
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.25, 0.25, 0.0);

      rmSetPlacementTeam(1);
      if (teamSide == 1)
            rmSetPlacementSection(0.65, 0.85);
      else if (teamSide == 2)
            rmSetPlacementSection(0.15, 0.35);
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.25, 0.25, 0.0);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(1800);
   float playerFraction2=rmAreaTilesToFraction(1500);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmSetAreaTerrainType(id, "california\ground5_cal");
      rmSetAreaCoherence(id, 0.8);
      rmSetAreaSmoothDistance(id, 5);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);

      int id2=rmCreateArea("Player 2"+i);
      rmSetPlayerArea(i, id2);
      rmSetAreaSize(id2, playerFraction2, playerFraction2);
      rmAddAreaToClass(id2, classPlayer);
      rmSetAreaCliffType(id2, "texas grass");
      rmSetAreaCliffEdge(id2, 1, 0.8, 0.1, 1.0, 1);
      rmSetAreaCliffHeight(id2, 12, 1.0, 0.5);
      rmSetAreaCliffPainting(id2, false, true, true, 1.5, true);
      rmSetAreaCoherence(id2, 0.8);
      rmSetAreaSmoothDistance(id2, 5);
      rmSetAreaMinBlobs(id2, 1);
      rmSetAreaMaxBlobs(id2, 1);
      rmSetAreaLocPlayer(id2, i);
      rmSetAreaWarnFailure(id2, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.30);
		

   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "TreeMadrone", 6, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 14);
   rmSetObjectDefMaxDistance(startAreaTreeID, 18);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startDeerID=rmCreateObjectDef("starting deer");
   rmAddObjectDefItem(startDeerID, "deer", 6, 6.0);
   rmSetObjectDefCreateHerd(startDeerID, true);
   rmSetObjectDefMinDistance(startDeerID, 14);
   rmSetObjectDefMaxDistance(startDeerID, 18);
   rmAddObjectDefConstraint(startDeerID, avoidStartResource);
   rmAddObjectDefConstraint(startDeerID, avoidImpassableLand);
   rmAddObjectDefConstraint(startDeerID, shortAvoidStartingUnits);

   int startCopperID=rmCreateObjectDef("player copper");
   rmAddObjectDefItem(startCopperID, "MineCopper", 1, 5.0);
   rmSetObjectDefMinDistance(startCopperID, 14);
   rmSetObjectDefMaxDistance(startCopperID, 18);
   rmAddObjectDefConstraint(startCopperID, avoidStartResource);
   rmAddObjectDefConstraint(startCopperID, avoidImpassableLand);
   rmAddObjectDefConstraint(startCopperID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startDeerID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startCopperID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
      rmAddClosestPointConstraint(flagVsFlag);
      rmAddClosestPointConstraint(flagLand);		
      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Text
   rmSetStatusText("",0.40);


   // Define and place the 3 natives: 2 Franks and 1 Goths

   // Native 1, to western side
   int nativeVillageType = rmRandInt(1,5);
   int nativeVillageID = rmCreateGrouping("franks village", "native comanche village "+nativeVillageType);
   rmSetGroupingMinDistance(nativeVillageID, 0.0);
   rmSetGroupingMaxDistance(nativeVillageID, 10.0);
   rmAddGroupingToClass(nativeVillageID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillageID, 0, 0.3, 0.88);

   // Native 2, to eastern side
   int nativeVillage2Type = rmRandInt(1,5);
   int nativeVillage2ID = rmCreateGrouping("franks village 2", "native comanche village "+nativeVillage2Type);
   rmSetGroupingMinDistance(nativeVillage2ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage2ID, 10.0);
   rmAddGroupingToClass(nativeVillage2ID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillage2ID, 0, 0.7, 0.12);

   // Native 3, to middle, only if more than 6 players
   if (cNumberNonGaiaPlayers >= 6)
   {
      int nativeVillage3Type = rmRandInt(1,5);
      int nativeVillage3ID = rmCreateGrouping("goths village", "native comanche village "+nativeVillage3Type);
      rmSetGroupingMinDistance(nativeVillage3ID, 0.0);
      rmSetGroupingMaxDistance(nativeVillage3ID, 10.0);
      rmAddGroupingToClass(nativeVillage3ID, rmClassID("natives"));
      rmPlaceGroupingAtLoc(nativeVillage3ID, 0, 0.5, 0.5);
   }


   // Define and place forests
	
   int numTries=30*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(300), rmAreaTilesToFraction(350));
      rmSetAreaForestType(forest, "california madrone forest");
      rmSetAreaForestDensity(forest, 0.8);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.5);
      rmSetAreaForestUnderbrush(forest, 0.2);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, avoidNatives);
      rmAddAreaConstraint(forest, cliffConstraint);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.  
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }

   // Define and place straggler trees
   for (i=0; < 40)
   { 
      int stragglerTreeID=rmCreateObjectDef("straggler tree olive"+i);
      rmAddObjectDefItem(stragglerTreeID, "TreeMadrone", rmRandInt(1,5), 10.0);
      rmSetObjectDefMinDistance(stragglerTreeID, 0.0);
      rmSetObjectDefMaxDistance(stragglerTreeID, rmZFractionToMeters(0.47));
      rmAddObjectDefToClass(stragglerTreeID, rmClassID("classForest"));
      rmAddObjectDefToClass(stragglerTreeID, rmClassID("classTreeStraggler"));
      rmAddObjectDefConstraint(stragglerTreeID, somePlayerConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, avoidImpassableLand);
      rmAddObjectDefConstraint(stragglerTreeID, shortForestConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, stragglerTreeConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, avoidNatives);
      rmAddObjectDefConstraint(stragglerTreeID, cliffConstraint);
      rmPlaceObjectDefAtLoc(stragglerTreeID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.50);


   // Define and place animals
   
   // Define and place deer herds
   int deerHerdID=rmCreateObjectDef("deer herd");
   rmAddObjectDefItem(deerHerdID, "deer", rmRandInt(7,8), 9.0);
   rmSetObjectDefCreateHerd(deerHerdID, true);
   rmSetObjectDefMinDistance(deerHerdID, 0.0);
   rmSetObjectDefMaxDistance(deerHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(deerHerdID, classAnimals);
   rmAddObjectDefConstraint(deerHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(deerHerdID, animalConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidNatives);
   rmAddObjectDefConstraint(deerHerdID, cliffConstraint);
   numTries=3*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(deerHerdID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.60);


   // Define and place copper mines

   int copperCount = 3*cNumberNonGaiaPlayers+2;

   for(i=0; < copperCount)
   {
      int copperID=rmCreateObjectDef("copper mine"+i);
      rmAddObjectDefItem(copperID, "MineCopper", 1, 0.0);
      rmSetObjectDefMinDistance(copperID, 0.0);
      rmSetObjectDefMaxDistance(copperID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(copperID, classMines);
      rmAddObjectDefConstraint(copperID, avoidImpassableLand);
      rmAddObjectDefConstraint(copperID, somePlayerConstraint);
      rmAddObjectDefConstraint(copperID, avoidMines);
      rmAddObjectDefConstraint(copperID, avoidNatives);
      rmAddObjectDefConstraint(copperID, edgeConstraint3);
      rmAddObjectDefConstraint(copperID, cliffConstraint);
      rmPlaceObjectDefAtLoc(copperID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.70);
	

   // Define and place fish

   int fishID=rmCreateObjectDef("fish");
   rmAddObjectDefItem(fishID, "ypFishTuna", 1, 4.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(fishID, fishVsFish);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 8*cNumberNonGaiaPlayers);


   // Text
   rmSetStatusText("",0.80);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidAll);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidNatives);
   rmAddObjectDefConstraint(nugget2, cliffConstraint);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidNatives);
   rmAddObjectDefConstraint(nugget3, cliffConstraint);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, 1.5*cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidNatives);
   rmAddObjectDefConstraint(nugget4, cliffConstraint);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);
	  

   // Text
   rmSetStatusText("",1.0); 
}