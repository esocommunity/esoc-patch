// RM script of Morea
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Chooses the 2 natives, always Goths

   // Native 1
   rmSetSubCiv(0, "Comanche");

   // Native 2
   rmSetSubCiv(1, "Comanche");


   // Picks the map size
   int playerTiles = 20000;
   if (cNumberNonGaiaPlayers == 8)
	playerTiles = 14000;
   else if (cNumberNonGaiaPlayers == 7)
	playerTiles = 15000;
   else if (cNumberNonGaiaPlayers == 6)
	playerTiles = 16000;
   else if (cNumberNonGaiaPlayers == 5)
	playerTiles = 17000;
   else if (cNumberNonGaiaPlayers == 4)
	playerTiles = 18000;
   else if (cNumberNonGaiaPlayers == 3)
	playerTiles = 19000;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses the side where teams start in team games
   int teamSide = rmRandInt(1,2);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("mongolia");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.10, 4, 0.4, 9.0);
   rmTerrainInitialize("texas\ground2_tex", 1.0);
   rmSetMapType("grass");
   rmSetMapType("water");
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   int classCliff=rmDefineClass("cliff");
   int classBay=rmDefineClass("bay");
   int classSea=rmDefineClass("sea");
   int classGrass=rmDefineClass("grass");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("nuggets");
   rmDefineClass("natives");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 40.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 70.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 60.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 5.0);
   int longAvoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land long", "Land", false, 10.0);
   int avoidLand = rmCreateTerrainDistanceConstraint("islands avoid land", "land", true, 10.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 40.0);

   // Area avoidance
   int cliffConstraint=rmCreateClassDistanceConstraint("avoid cliff", classCliff, 10.0);
   int bayConstraint=rmCreateClassDistanceConstraint("avoid side bays", classBay, 10.0);
   int seaConstraint=rmCreateClassDistanceConstraint("avoid middle sea", classSea, 10.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);
   int grassConstraint=rmCreateClassDistanceConstraint("grass vs. grass", classGrass, 60.0);

   // Native avoidance
   int avoidNatives=rmCreateClassDistanceConstraint("things avoids natives", rmClassID("natives"), 5.0);
   int longAvoidNatives=rmCreateClassDistanceConstraint("forests avoids natives long", rmClassID("natives"), 10.0);

   // Water objects avoidance	
   int fishVsFish=rmCreateTypeDistanceConstraint("fish avoid fish", "ypFishTuna", 25.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish avoid land", "land", true, 5.0);
   int whaleVsWhale=rmCreateTypeDistanceConstraint("whale vs whale", "HumpbackWhale", 45.0);
   int whaleLand = rmCreateTerrainDistanceConstraint("whale land", "land", true, 10.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag avoid land", "land", true, 15.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid flags", "HomeCityWaterSpawnFlag", 25);


   // Text
   rmSetStatusText("",0.10);


   // Define and place water areas

   // Define and place the major sea - to south
   int seaID=rmCreateArea("Sea");
   rmSetAreaWaterType(seaID, "texas pond");
   rmSetAreaSize(seaID, 0.3, 0.3);
   rmSetAreaCoherence(seaID, 0.8);
   rmSetAreaLocation(seaID, 0.0, 0.0);
   rmAddAreaInfluenceSegment(seaID, 0.0, 0.5, 0.5, 0.0); 
   rmSetAreaBaseHeight(seaID, 0.0);
   rmSetAreaObeyWorldCircleConstraint(seaID, false);
   rmSetAreaMinBlobs(seaID, 3);
   rmSetAreaMaxBlobs(seaID, 3);
   rmSetAreaMinBlobDistance(seaID, 3);
   rmSetAreaMaxBlobDistance(seaID, 3);
   rmSetAreaSmoothDistance(seaID, 15);
   rmAddAreaToClass(seaID, classSea);
   rmBuildArea(seaID);

   // Define and place sea 2 - the western "bay"
   int sea2ID=rmCreateArea("Sea 2");
   rmSetAreaWaterType(sea2ID, "texas pond");
   rmSetAreaSize(sea2ID, 0.07, 0.07);
   rmSetAreaCoherence(sea2ID, 0.75);
   rmSetAreaLocation(sea2ID, 0.0, 1.0);
   rmAddAreaInfluenceSegment(sea2ID, 0.0, 0.65, 0.25, 0.85);
   rmSetAreaBaseHeight(sea2ID, 0.0);
   rmSetAreaObeyWorldCircleConstraint(sea2ID, false);
   rmSetAreaMinBlobs(sea2ID, 3);
   rmSetAreaMaxBlobs(sea2ID, 3);
   rmSetAreaMinBlobDistance(sea2ID, 3);
   rmSetAreaMaxBlobDistance(sea2ID, 3);
   rmSetAreaSmoothDistance(sea2ID, 15);
   rmAddAreaToClass(sea2ID, classBay);
   rmBuildArea(sea2ID);

   // Define and place sea 3 - the eastern "bay"
   int sea3ID=rmCreateArea("Sea 3");
   rmSetAreaWaterType(sea3ID, "texas pond");
   rmSetAreaSize(sea3ID, 0.07, 0.07);
   rmSetAreaCoherence(sea3ID, 0.75);
   rmSetAreaLocation(sea3ID, 1.0, 0.0);
   rmAddAreaInfluenceSegment(sea3ID, 0.65, 0.0, 0.85, 0.25);
   rmSetAreaBaseHeight(sea3ID, 0.0);
   rmSetAreaObeyWorldCircleConstraint(sea3ID, false);
   rmSetAreaMinBlobs(sea3ID, 3);
   rmSetAreaMaxBlobs(sea3ID, 3);
   rmSetAreaMinBlobDistance(sea3ID, 3);
   rmSetAreaMaxBlobDistance(sea3ID, 3);
   rmSetAreaSmoothDistance(sea3ID, 15);
   rmAddAreaToClass(sea3ID, classBay);
   rmBuildArea(sea3ID);

   // Define and place bay to middle - big/south
   int bayID=rmCreateArea("Bay");
   rmSetAreaWaterType(bayID, "texas pond");
   rmSetAreaSize(bayID, 0.1, 0.1);
   rmSetAreaCoherence(bayID, 0.75);
   rmSetAreaLocation(bayID, 0.4, 0.4);
   rmSetAreaBaseHeight(bayID, 0.0);
   rmSetAreaMinBlobs(bayID, 3);
   rmSetAreaMaxBlobs(bayID, 3);
   rmSetAreaMinBlobDistance(bayID, 3);
   rmSetAreaMaxBlobDistance(bayID, 3);
   rmSetAreaSmoothDistance(bayID, 15);
   rmAddAreaToClass(bayID, classSea);
   rmBuildArea(bayID);

   // Define and place bay 2 to middle - medium/middle
   int bay2ID=rmCreateArea("Bay 2");
   rmSetAreaWaterType(bay2ID, "texas pond");
   rmSetAreaSize(bay2ID, 0.05, 0.05);
   rmSetAreaCoherence(bay2ID, 0.75);
   rmSetAreaLocation(bay2ID, 0.5, 0.5);
   rmSetAreaBaseHeight(bay2ID, 0.0);
   rmSetAreaMinBlobs(bay2ID, 3);
   rmSetAreaMaxBlobs(bay2ID, 3);
   rmSetAreaMinBlobDistance(bay2ID, 3);
   rmSetAreaMaxBlobDistance(bay2ID, 3);
   rmSetAreaSmoothDistance(bay2ID, 15);
   rmAddAreaToClass(bay2ID, classSea);
   rmBuildArea(bay2ID);

   // Define and place bay 3 to middle - medium/north
   int bay3ID=rmCreateArea("Bay 3");
   rmSetAreaWaterType(bay3ID, "texas pond");
   rmSetAreaSize(bay3ID, 0.05, 0.05);
   rmSetAreaCoherence(bay3ID, 0.75);
   rmSetAreaLocation(bay3ID, 0.55, 0.55);
   rmSetAreaBaseHeight(bay3ID, 0.0);
   rmSetAreaMinBlobs(bay3ID, 3);
   rmSetAreaMaxBlobs(bay3ID, 3);
   rmSetAreaMinBlobDistance(bay3ID, 3);
   rmSetAreaMaxBlobDistance(bay3ID, 3);
   rmSetAreaSmoothDistance(bay3ID, 15);
   rmAddAreaToClass(bay3ID, classSea);
   rmBuildArea(bay3ID);


   // Define and place peninsulas

   // Define and place peninsula - to west
   int peninsulaID=rmCreateArea("Peninsula");
   rmSetAreaTerrainType(peninsulaID, "texas\ground2_tex");
   rmSetAreaSize(peninsulaID, 0.015, 0.015);
   rmSetAreaCoherence(peninsulaID, 0.8);
   rmSetAreaLocation(peninsulaID, 0.25, 0.55);
   rmAddAreaInfluenceSegment(peninsulaID, 0.15, 0.45, 0.3, 0.6);
   rmSetAreaBaseHeight(peninsulaID, 0.0);
   rmSetAreaObeyWorldCircleConstraint(peninsulaID, false);
   rmBuildArea(peninsulaID);

   // Define and place peninsula 2 - to east
   int peninsula2ID=rmCreateArea("Peninsula 2");
   rmSetAreaTerrainType(peninsula2ID, "texas\ground2_tex");
   rmSetAreaSize(peninsula2ID, 0.015, 0.015);
   rmSetAreaCoherence(peninsula2ID, 0.8);
   rmSetAreaLocation(peninsula2ID, 0.55, 0.25);
   rmAddAreaInfluenceSegment(peninsula2ID, 0.45, 0.15, 0.6, 0.3);
   rmSetAreaBaseHeight(peninsula2ID, 0.0);
   rmSetAreaObeyWorldCircleConstraint(peninsula2ID, false);
   rmBuildArea(peninsula2ID);


   // Text
   rmSetStatusText("",0.20);


   // Define and place cliffs to west and east

   int radius = size * 0.5;
   int mountainPeaksLongSideSize = radius * 2.5;

   // Define and place cliff - vertical to west
   int cliffID=rmCreateArea("cliff");
   rmSetAreaSize(cliffID, rmAreaTilesToFraction(mountainPeaksLongSideSize), rmAreaTilesToFraction(mountainPeaksLongSideSize));
   rmSetAreaLocation(cliffID, 0.39, 0.75);
   rmAddAreaInfluenceSegment(cliffID, 0.45, 0.8, 0.29, 0.65);
   rmSetAreaBaseHeight(cliffID, 15);
   rmSetAreaTerrainType(cliffID, "terrain\rockies\clifftop_roc");
   rmSetAreaCoherence(cliffID, 0.7);
   rmSetAreaSmoothDistance(cliffID, 10);
   rmSetAreaElevationType(cliffID, cElevTurbulence);
   rmSetAreaElevationVariation(cliffID, 9.0);
   rmSetAreaHeightBlend(cliffID, 1);
   rmAddAreaToClass(cliffID, classCliff);
   rmSetAreaObeyWorldCircleConstraint(cliffID, false);
   rmBuildArea(cliffID);

   // Define and place cliff 2 - horizontal to west
   int cliff2ID=rmCreateArea("cliff 2");
   rmSetAreaSize(cliff2ID, rmAreaTilesToFraction(mountainPeaksLongSideSize), rmAreaTilesToFraction(mountainPeaksLongSideSize));
   rmSetAreaLocation(cliff2ID, 0.42, 0.9);
   rmAddAreaInfluenceSegment(cliff2ID, 0.4, 0.99, 0.45, 0.8);
   rmSetAreaBaseHeight(cliff2ID, 15);
   rmSetAreaTerrainType(cliff2ID, "terrain\rockies\clifftop_roc");
   rmSetAreaCoherence(cliff2ID, 0.7);
   rmSetAreaSmoothDistance(cliff2ID, 10);
   rmSetAreaElevationType(cliff2ID, cElevTurbulence);
   rmSetAreaElevationVariation(cliff2ID, 9.0);
   rmSetAreaHeightBlend(cliff2ID, 1);
   rmAddAreaToClass(cliff2ID, classCliff);
   rmSetAreaObeyWorldCircleConstraint(cliff2ID, false);
   rmBuildArea(cliff2ID);

   // Define and place cliff 3 - vertical to east
   int cliff3ID=rmCreateArea("cliff 3");
   rmSetAreaSize(cliff3ID, rmAreaTilesToFraction(mountainPeaksLongSideSize), rmAreaTilesToFraction(mountainPeaksLongSideSize));
   rmSetAreaLocation(cliff3ID, 0.75, 0.39);
   rmAddAreaInfluenceSegment(cliff3ID, 0.8, 0.45, 0.65, 0.29);
   rmSetAreaBaseHeight(cliff3ID, 15);
   rmSetAreaTerrainType(cliff3ID, "terrain\rockies\clifftop_roc");
   rmSetAreaCoherence(cliff3ID, 0.7);
   rmSetAreaSmoothDistance(cliff3ID, 10);
   rmSetAreaElevationType(cliff3ID, cElevTurbulence);
   rmSetAreaElevationVariation(cliff3ID, 9.0);
   rmSetAreaHeightBlend(cliff3ID, 1);
   rmAddAreaToClass(cliff3ID, classCliff);
   rmSetAreaObeyWorldCircleConstraint(cliff3ID, false);
   rmBuildArea(cliff3ID);

   // Define and place cliff 4 - horizontal to east
   int cliff4ID=rmCreateArea("cliff 4");
   rmSetAreaSize(cliff4ID, rmAreaTilesToFraction(mountainPeaksLongSideSize), rmAreaTilesToFraction(mountainPeaksLongSideSize));
   rmSetAreaLocation(cliff4ID, 0.9, 0.42);
   rmAddAreaInfluenceSegment(cliff4ID, 0.99, 0.4, 0.8, 0.45);
   rmSetAreaBaseHeight(cliff4ID, 15);
   rmSetAreaTerrainType(cliff4ID, "terrain\rockies\clifftop_roc");
   rmSetAreaCoherence(cliff4ID, 0.7);
   rmSetAreaSmoothDistance(cliff4ID, 10);
   rmSetAreaElevationType(cliff4ID, cElevTurbulence);
   rmSetAreaElevationVariation(cliff4ID, 9.0);
   rmSetAreaHeightBlend(cliff4ID, 1);
   rmAddAreaToClass(cliff4ID, classCliff);
   rmSetAreaObeyWorldCircleConstraint(cliff4ID, false);
   rmBuildArea(cliff4ID);


   // Define and place river Eurotas splitting the two land areas - only in team games

   if (rmGetIsFFA() == false)
   {
      int eurotasRiver = rmRiverCreate(-1, "texas pond", 20, 15, 10, 10);
      rmRiverSetConnections(eurotasRiver, 0.6, 0.6, 1.0, 1.0);
      rmRiverBuild(eurotasRiver);
   }


   // Define and place islands

   float A = rmRandFloat(0.15, 0.3);
   float B = rmRandFloat(0.15, 0.3);
   float C = rmRandFloat(0.4, 0.5);
   float D = rmRandFloat(0.4, 0.5);

   // Define and place island 1 - to south
   int island=rmCreateArea("island");
   rmSetAreaTerrainType(island, "texas\ground2_tex");
   rmSetAreaSize(island, 0.02, 0.02);
   rmSetAreaLocation(island, A, B);
   rmSetAreaBaseHeight(island, 0);
   rmSetAreaCoherence(island, 0.7);
   rmSetAreaMinBlobs(island, 10);
   rmSetAreaMaxBlobs(island, 20);
   rmSetAreaMinBlobDistance(island, 10);
   rmSetAreaMaxBlobDistance(island, 20);
   rmSetAreaSmoothDistance(island, 10);
   rmAddAreaConstraint(island, avoidLand);
   rmSetAreaObeyWorldCircleConstraint(island, false);
   rmSetAreaElevationType(island, cElevTurbulence);
   rmSetAreaElevationVariation(island, 5.0);
   rmSetAreaElevationMinFrequency(island, 0.10);
   rmSetAreaElevationOctaves(island, 3);
   rmSetAreaElevationPersistence(island, 0.2);
   rmSetAreaElevationNoiseBias(island, 1);
   rmBuildArea(island);

   // Define and place island 2 - to middle
   int island2=rmCreateArea("island 2");
   rmSetAreaTerrainType(island2, "texas\ground2_tex");
   rmSetAreaSize(island2, 0.02, 0.02);
   rmSetAreaLocation(island2, C, D);
   rmSetAreaBaseHeight(island2, 0);
   rmSetAreaCoherence(island2, 0.7);
   rmSetAreaMinBlobs(island2, 10);
   rmSetAreaMaxBlobs(island2, 20);
   rmSetAreaMinBlobDistance(island2, 10);
   rmSetAreaMaxBlobDistance(island2, 20);
   rmSetAreaSmoothDistance(island2, 10);
   rmAddAreaConstraint(island2, avoidLand);
   rmSetAreaObeyWorldCircleConstraint(island2, false);
   rmSetAreaElevationType(island2, cElevTurbulence);
   rmSetAreaElevationVariation(island2, 5.0);
   rmSetAreaElevationMinFrequency(island2, 0.10);
   rmSetAreaElevationOctaves(island2, 3);
   rmSetAreaElevationPersistence(island2, 0.2);
   rmSetAreaElevationNoiseBias(island2, 1);
   rmBuildArea(island2);


   // Define and place brown grass areas around the map

   int numTries=20;
   for (i=0; <numTries)
   { 
      int grass=rmCreateArea("grass"+i);
      rmSetAreaSize(grass, 0.05, 0.05);
      rmSetAreaForestType(grass, "great plains grass");
      rmSetAreaForestDensity(grass, 0.2);
      rmSetAreaForestClumpiness(grass, 0.0);
      rmSetAreaForestUnderbrush(grass, 0.3);
      rmSetAreaBaseHeight(grass, 1.0);
      rmSetAreaMinBlobs(grass, 7);
      rmSetAreaMaxBlobs(grass, 10);
      rmSetAreaMinBlobDistance(grass, 7);
      rmSetAreaMaxBlobDistance(grass, 10);
      rmSetAreaCoherence(grass, 0.4);
      rmAddAreaToClass(grass, classGrass);
      rmAddAreaConstraint(grass, avoidImpassableLand);
      rmAddAreaConstraint(grass, grassConstraint);
      rmAddAreaConstraint(grass, cliffConstraint);
      rmAddAreaConstraint(grass, shortPlayerConstraint);
      rmAddAreaConstraint(grass, avoidNatives);
      rmSetAreaElevationType(grass, cElevTurbulence);
      rmSetAreaElevationVariation(grass, 8.0);
      rmSetAreaElevationMinFrequency(grass, 0.06);
      rmSetAreaElevationOctaves(grass, 4);
      rmSetAreaElevationPersistence(grass, 0.4);
   }


   // Place players in FFA and team games

   // Player placement if FFA (Free For All) - place players in circle
   if(cNumberTeams > 2)
   {
      rmSetPlacementSection(0.0, 0.25);
      rmPlacePlayersCircular(0.40, 0.40, 0.0);
   }

   // Player placement if teams - place teams in circle to apart from each other
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      if (teamSide == 1)
            rmPlacePlayersLine(0.52, 0.95, 0.6, 0.75, 0, 0);
      else
            rmPlacePlayersLine(0.95, 0.5, 0.75, 0.62, 0, 0);

      rmSetPlacementTeam(1);
      if (teamSide == 1)
            rmPlacePlayersLine(0.95, 0.5, 0.75, 0.62, 0, 0);
      else
            rmPlacePlayersLine(0.52, 0.95, 0.6, 0.75, 0, 0);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.30);
		

   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "TreeMadrone", 4, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 14);
   rmSetObjectDefMaxDistance(startAreaTreeID, 18);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startDeerID=rmCreateObjectDef("starting deer");
   rmAddObjectDefItem(startDeerID, "deer", 3, 6.0);
   rmSetObjectDefCreateHerd(startDeerID, true);
   rmSetObjectDefMinDistance(startDeerID, 14);
   rmSetObjectDefMaxDistance(startDeerID, 18);
   rmAddObjectDefConstraint(startDeerID, avoidStartResource);
   rmAddObjectDefConstraint(startDeerID, avoidImpassableLand);
   rmAddObjectDefConstraint(startDeerID, shortAvoidStartingUnits);

   int startCopperID=rmCreateObjectDef("player copper");
   rmAddObjectDefItem(startCopperID, "MineCopper", 1, 5.0);
   rmSetObjectDefMinDistance(startCopperID, 14);
   rmSetObjectDefMaxDistance(startCopperID, 18);
   rmAddObjectDefConstraint(startCopperID, avoidStartResource);
   rmAddObjectDefConstraint(startCopperID, avoidImpassableLand);
   rmAddObjectDefConstraint(startCopperID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startDeerID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startCopperID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
      rmAddClosestPointConstraint(flagVsFlag);
      rmAddClosestPointConstraint(flagLand);
      rmAddClosestPointConstraint(cliffConstraint);
      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Text
   rmSetStatusText("",0.40);


   // Define and place the 2 natives, always Goths

   // Native 1, to island 1
   int nativeVillageID = -1;
   int nativeVillageType = rmRandInt(1,5);
   nativeVillageID = rmCreateGrouping("goths village", "native comanche village "+nativeVillageType);
   rmSetGroupingMinDistance(nativeVillageID, 0.0);
   rmSetGroupingMaxDistance(nativeVillageID, 10.0);
   rmAddGroupingConstraint(nativeVillageID, longAvoidImpassableLand);
   rmAddGroupingToClass(nativeVillageID, rmClassID("natives"));
   rmPlaceGroupingInArea(nativeVillageID, 0, rmAreaID("island"));

   // Native 2, to island 2
   int nativeVillage2ID = -1;
   int nativeVillage2Type = rmRandInt(1,5);
   nativeVillage2ID = rmCreateGrouping("goths village 2", "native comanche village "+nativeVillage2Type);
   rmSetGroupingMinDistance(nativeVillage2ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage2ID, 10.0);
   rmAddGroupingConstraint(nativeVillage2ID, longAvoidImpassableLand);
   rmAddGroupingToClass(nativeVillage2ID, rmClassID("natives"));
   rmPlaceGroupingInArea(nativeVillage2ID, 0, rmAreaID("island 2"));


   // Define and place forests
	
   numTries=30*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(150), rmAreaTilesToFraction(200));
      rmSetAreaForestType(forest, "california madrone forest");
      rmSetAreaForestDensity(forest, 0.8);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.5);
      rmSetAreaForestUnderbrush(forest, 0.2);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, cliffConstraint);
      rmAddAreaConstraint(forest, longAvoidNatives);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.  
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }


   // Text
   rmSetStatusText("",0.50);


   // Define and place animals
   
   // Define and place deer herds
   int deerHerdID=rmCreateObjectDef("deer herd");
   rmAddObjectDefItem(deerHerdID, "deer", rmRandInt(7,8), 9.0);
   rmSetObjectDefCreateHerd(deerHerdID, true);
   rmSetObjectDefMinDistance(deerHerdID, 0.0);
   rmSetObjectDefMaxDistance(deerHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(deerHerdID, classAnimals);
   rmAddObjectDefConstraint(deerHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(deerHerdID, animalConstraint);
   rmAddObjectDefConstraint(deerHerdID, cliffConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidNatives);
   numTries=cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(deerHerdID, 0, 0.5, 0.5);

   // Define and place berry bushes
   int berryBushID=rmCreateObjectDef("berry bush");
   rmAddObjectDefItem(berryBushID, "berrybush", rmRandInt(3,4), 6.0);
   rmSetObjectDefMinDistance(berryBushID, 0.0);
   rmSetObjectDefMaxDistance(berryBushID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(berryBushID, classAnimals);
   rmAddObjectDefConstraint(berryBushID, somePlayerConstraint);
   rmAddObjectDefConstraint(berryBushID, avoidImpassableLand);
   rmAddObjectDefConstraint(berryBushID, animalConstraint);
   rmAddObjectDefConstraint(berryBushID, cliffConstraint);
   rmAddObjectDefConstraint(berryBushID, avoidNatives);
   numTries=cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(berryBushID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.60);


   // Define and place copper mines

   int copperCount = 2*cNumberNonGaiaPlayers+3;

   for(i=0; < copperCount)
   {
      int copperID=rmCreateObjectDef("copper mine"+i);
      rmAddObjectDefItem(copperID, "MineCopper", 1, 0.0);
      rmSetObjectDefMinDistance(copperID, 0.0);
      rmSetObjectDefMaxDistance(copperID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(copperID, classMines);
      rmAddObjectDefConstraint(copperID, avoidImpassableLand);
      rmAddObjectDefConstraint(copperID, somePlayerConstraint);
      rmAddObjectDefConstraint(copperID, avoidMines);
      rmAddObjectDefConstraint(copperID, cliffConstraint);
      rmAddObjectDefConstraint(copperID, avoidNatives);
      rmPlaceObjectDefAtLoc(copperID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.70);


   // Define and place water animals

   // Define and place fish to middle - only some
   int fishMiddleID=rmCreateObjectDef("fish middle");
   rmAddObjectDefItem(fishMiddleID, "ypFishTuna", 1, 4.0);
   rmSetObjectDefMinDistance(fishMiddleID, 0.0);
   rmSetObjectDefMaxDistance(fishMiddleID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(fishMiddleID, fishVsFish);
   rmAddObjectDefConstraint(fishMiddleID, fishLand);
   rmAddObjectDefConstraint(fishMiddleID, bayConstraint);
   rmPlaceObjectDefAtLoc(fishMiddleID, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);

   // Define and place fish to side bays - many
   int fishBayID=rmCreateObjectDef("fish bay");
   rmAddObjectDefItem(fishBayID, "ypFishTuna", 1, 4.0);
   rmSetObjectDefMinDistance(fishBayID, 0.0);
   rmSetObjectDefMaxDistance(fishBayID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(fishBayID, fishVsFish);
   rmAddObjectDefConstraint(fishBayID, fishLand);
   rmAddObjectDefConstraint(fishBayID, seaConstraint);
   rmPlaceObjectDefAtLoc(fishBayID, 0, 0.5, 0.5, 10*cNumberNonGaiaPlayers);

   // Define and place whales - some everywhere
   int whaleID=rmCreateObjectDef("whale");
   rmAddObjectDefItem(whaleID, "HumpbackWhale", 1, 4.0);
   rmSetObjectDefMinDistance(whaleID, 0.0);
   rmSetObjectDefMaxDistance(whaleID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(whaleID, whaleVsWhale);
   rmAddObjectDefConstraint(whaleID, whaleLand);
   rmPlaceObjectDefAtLoc(whaleID, 0, 0.5, 0.5, 3*cNumberNonGaiaPlayers);


   // Text
   rmSetStatusText("",0.80);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidAll);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, cliffConstraint);
   rmAddObjectDefConstraint(nugget2, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, cliffConstraint);
   rmAddObjectDefConstraint(nugget3, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, cliffConstraint);
   rmAddObjectDefConstraint(nugget4, avoidNatives);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Text
   rmSetStatusText("",1.0);
}