// RM script of Portugal
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Chooses the 3 natives, always Visigoths

   // Native 1
   rmSetSubCiv(0, "Comanche");

   // Native 2
   rmSetSubCiv(1, "Comanche");

   // Native 3
   rmSetSubCiv(2, "Comanche");


   // Picks the map size
   int playerTiles = 11000;
   if (cNumberNonGaiaPlayers > 4)
      playerTiles = 10000;
   if (cNumberNonGaiaPlayers > 6)
      playerTiles = 8500;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses if there is barbarians or a trade route in middle, 0 = trade route, 1 = barbarians
   int middleBonus = rmRandInt(0,1);


   // Chooses the side teams start
   int teamSide = rmRandInt(1,2);


   // Very VERY complex FFA player placement random integers, only I can really understand these! :P
   int playerPlace3 = rmRandInt(1,3);
   int playerPlace4 = rmRandInt(1,2);
   int playerPlace5 = rmRandInt(1,5);
   int playerPlace6 = rmRandInt(1,3);
   int playerPlace7 = rmRandInt(1,7);
   int playerPlace8 = rmRandInt(1,4);


   // Chooses the side the players start in FFA - this is also complex! :)
   int playerSide = rmRandInt(1,2);


   // Picks a default water height
   rmSetSeaLevel(-15.0);


   // Lighting
   rmSetLightingSet("new england");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.10, 4, 0.4, 8.0);
   rmSetBaseTerrainMix("patagonia_grass");
   rmTerrainInitialize("patagonia\ground_grass2_pat", 1.0);
   rmEnableLocalWater(false);
   rmSetMapType("grass");
   rmSetMapType("water");
   rmSetMapType("greatPlains");
   

   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("nuggets");
   rmDefineClass("natives");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 40.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 75.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 60.0);
   int avoidCow=rmCreateTypeDistanceConstraint("herds avoids cows", "cow", 60.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 6.0);
   int avoidImpassableLandLong=rmCreateTerrainDistanceConstraint("avoid impassable land long", "Land", false, 18.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 40.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 6.0);

   // Trade route avoidance
   int avoidTradeRoute=rmCreateTradeRouteDistanceConstraint("trade route", 5.0);
   int avoidTradeSockets=rmCreateTypeDistanceConstraint("avoid trade sockets", "SocketTradeRoute", 5.0);

   // Native avoidance
   int avoidNatives=rmCreateClassDistanceConstraint("things avoids natives", rmClassID("natives"), 5.0);

   // Water objects avoidance	
   int fishVsFish=rmCreateTypeDistanceConstraint("fish v fish", "ypFishTuna", 25.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish land", "land", true, 6.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag vs land", "land", true, 15.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid same", "HomeCityWaterSpawnFlag", 25);


   // Text
   rmSetStatusText("",0.10);


   // Define and place two rivers, Tagus and Douro

   // Define and place river Douro to upper part of map, pretty straight
   int douroRiver = rmRiverCreate(-1, "hudson bay", 15, 15, 9, 9);
   rmRiverSetConnections(douroRiver, 0.3, 1.0, 1.0, 0.3);
   rmRiverSetShallowRadius(douroRiver, 10);
   rmRiverAddShallow(douroRiver, 0.2);
   rmRiverAddShallow(douroRiver, 0.8);
   if (cNumberTeams > 2 && cNumberNonGaiaPlayers == 7 && teamSide == 1) // If FFA, 7 players, and 7th player starts in north: EXTRA SHALLOW TO MIDDLE
      rmRiverAddShallow(douroRiver, 0.5);
   if (cNumberTeams > 2 && cNumberNonGaiaPlayers == 8) // If FFA and 8 players: EXTRA SHALLOW TO MIDDLE
      rmRiverAddShallow(douroRiver, 0.5);
   if (cNumberTeams == 2 && cNumberNonGaiaPlayers >= 7) // If team game and 7-8 players: EXTRA SHALLOW TO MIDDLE
      rmRiverAddShallow(douroRiver, 0.5);
   rmRiverBuild(douroRiver);
   rmRiverReveal(douroRiver, 2);


   // Define and place river Tagus to lower part of map, more curly than Douro
   int tagusRiver = rmRiverCreate(-1, "hudson bay", 15, 10, 9, 9);
   rmRiverAddWaypoint(tagusRiver, 0.83, 0.1);
   rmRiverAddWaypoint(tagusRiver, 0.4, 0.4);
   rmRiverAddWaypoint(tagusRiver, 0.0, 0.58);
   rmRiverSetShallowRadius(tagusRiver, 10);
   rmRiverAddShallow(tagusRiver, 0.2);
   rmRiverAddShallow(tagusRiver, 0.8);
   if (cNumberTeams > 2 && cNumberNonGaiaPlayers == 7 && teamSide == 2) // If FFA, 7 players, and 7th player starts in south: EXTRA SHALLOW TO MIDDLE
      rmRiverAddShallow(tagusRiver, 0.5);
   if (cNumberTeams > 2 && cNumberNonGaiaPlayers == 8) // If FFA and 8 players: EXTRA SHALLOW TO MIDDLE
      rmRiverAddShallow(tagusRiver, 0.5);
   if (cNumberTeams == 2 && cNumberNonGaiaPlayers >= 7) // If team game and 7-8 players: EXTRA SHALLOW TO MIDDLE
      rmRiverAddShallow(tagusRiver, 0.5);
   rmRiverBuild(tagusRiver);
   rmRiverReveal(tagusRiver, 2);


   // Text
   rmSetStatusText("",0.40);


   // Place players

   // Player placement if teams - place teams into lines
   if(cNumberTeams == 2 && cNumberNonGaiaPlayers >= 3)
   {
      rmSetPlacementTeam(0);
      if (teamSide == 1)
            rmPlacePlayersLine(0.6, 0.9, 0.9, 0.6, 0, 0);
      else if (teamSide == 2)
            rmPlacePlayersLine(0.15, 0.35, 0.55, 0.15, 0, 0);

      rmSetPlacementTeam(1);
      if (teamSide == 1)
            rmPlacePlayersLine(0.15, 0.35, 0.55, 0.15, 0, 0);
      else if (teamSide == 2)
            rmPlacePlayersLine(0.6, 0.9, 0.9, 0.6, 0, 0);
   }

   // Player placement if FFA (Free For All) OR two player game - manual placement
   if(cNumberTeams > 2 || cNumberNonGaiaPlayers == 2)
   {
      if (cNumberNonGaiaPlayers == 2) // Place in middle to different sides
      {
            if (teamSide == 1)
            {
                        rmPlacePlayer(1, 0.77, 0.77);
                        rmPlacePlayer(2, 0.33, 0.23);
            }
            else if (teamSide == 2)
            {
                        rmPlacePlayer(1, 0.33, 0.23);
                        rmPlacePlayer(2, 0.77, 0.77);
            }
      }
      if (cNumberNonGaiaPlayers == 3) // One player is in middle
      {
            if (playerPlace3 == 1) // Player 1 in middle
            {
                        if (playerSide == 1) // Middle in south
                        {
                                                rmPlacePlayer(1, 0.5, 0.2);
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(2, 0.6, 0.9);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(2, 0.9, 0.6);
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(3, 0.9, 0.6);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(3, 0.6, 0.9);
                        }
                        else if (playerSide == 2) // Middle in north
                        {
                                                rmPlacePlayer(1, 0.9, 0.9);
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(2, 0.15, 0.35);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(2, 0.55, 0.15);

                                                if (teamSide == 1)
                                                                        rmPlacePlayer(3, 0.55, 0.15);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(3, 0.15, 0.35);

                        }
            }
            else if (playerPlace3 == 2) // Player 2 in middle
            {
                        if (playerSide == 1) // Middle in south
                        {
                                                rmPlacePlayer(2, 0.5, 0.2);
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(3, 0.6, 0.9);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(3, 0.9, 0.6);
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(1, 0.9, 0.6);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(1, 0.6, 0.9);
                        }
                        else if (playerSide == 2) // Middle in north
                        {
                                                rmPlacePlayer(2, 0.9, 0.9);
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(3, 0.15, 0.35);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(3, 0.55, 0.15);

                                                if (teamSide == 1)
                                                                        rmPlacePlayer(1, 0.55, 0.15);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(1, 0.15, 0.35);

                        }
            }
            else if (playerPlace3 == 3) // Player 3 in middle
            {
                        if (playerSide == 1) // Middle in south
                        {
                                                rmPlacePlayer(3, 0.5, 0.2);
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(1, 0.6, 0.9);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(1, 0.9, 0.6);
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(2, 0.9, 0.6);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(2, 0.6, 0.9);
                        }
                        else if (playerSide == 2) // Middle in north
                        {
                                                rmPlacePlayer(3, 0.9, 0.9);
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(1, 0.15, 0.35);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(1, 0.55, 0.15);

                                                if (teamSide == 1)
                                                                        rmPlacePlayer(2, 0.55, 0.15);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(2, 0.15, 0.35);

                        }
            }
      }
      if (cNumberNonGaiaPlayers == 4) // Normal balanced placement
      {
            if (playerPlace4 == 1) // Players 1-2 in south, 3-4 in north
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.15, 0.35);
                                                rmPlacePlayer(2, 0.55, 0.15);
                                                rmPlacePlayer(3, 0.6, 0.9);
                                                rmPlacePlayer(4, 0.9, 0.6);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.55, 0.15);
                                                rmPlacePlayer(2, 0.15, 0.35);
                                                rmPlacePlayer(3, 0.9, 0.6);
                                                rmPlacePlayer(4, 0.6, 0.9);
                        }
            }
            else if (playerPlace4 == 2) // Players 1-2 in north, 3-4 in south
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.6, 0.9);
                                                rmPlacePlayer(2, 0.9, 0.6);
                                                rmPlacePlayer(3, 0.15, 0.35);
                                                rmPlacePlayer(4, 0.55, 0.15);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.9, 0.6);
                                                rmPlacePlayer(2, 0.6, 0.9);
                                                rmPlacePlayer(3, 0.55, 0.15);
                                                rmPlacePlayer(4, 0.15, 0.35);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 5) // One player in each corner + one in middle, ALWAYS in LEFT!
      {
            if (playerPlace5 == 1) // Player 1 in middle
            {
                        if (playerSide == 1) // Players 2-3 in north, 4-5 in south
                        {
                                                rmPlacePlayer(1, 0.25, 0.75);
                                                rmPlacePlayer(2, 0.6, 0.9);
                                                rmPlacePlayer(3, 0.9, 0.6);
                                                rmPlacePlayer(4, 0.15, 0.35);
                                                rmPlacePlayer(5, 0.55, 0.15);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.25, 0.75);
                                                rmPlacePlayer(2, 0.9, 0.6);
                                                rmPlacePlayer(3, 0.6, 0.9);
                                                rmPlacePlayer(4, 0.55, 0.15);
                                                rmPlacePlayer(5, 0.15, 0.35);
                        }
            }
            else if (playerPlace5 == 2) // Player 2 in middle
            {
                        if (playerSide == 1) // Players 3-4 in north, 5-1 in south
                        {
                                                rmPlacePlayer(2, 0.25, 0.75);
                                                rmPlacePlayer(3, 0.6, 0.9);
                                                rmPlacePlayer(4, 0.9, 0.6);
                                                rmPlacePlayer(5, 0.15, 0.35);
                                                rmPlacePlayer(1, 0.55, 0.15);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(2, 0.25, 0.75);
                                                rmPlacePlayer(3, 0.9, 0.6);
                                                rmPlacePlayer(4, 0.6, 0.9);
                                                rmPlacePlayer(5, 0.55, 0.15);
                                                rmPlacePlayer(1, 0.15, 0.35);
                        }
            }
            else if (playerPlace5 == 3) // Player 3 in middle
            {
                        if (playerSide == 1) // Players 4-5 in north, 1-2 in south
                        {
                                                rmPlacePlayer(3, 0.25, 0.75);
                                                rmPlacePlayer(4, 0.6, 0.9);
                                                rmPlacePlayer(5, 0.9, 0.6);
                                                rmPlacePlayer(1, 0.15, 0.35);
                                                rmPlacePlayer(2, 0.55, 0.15);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(3, 0.25, 0.75);
                                                rmPlacePlayer(4, 0.9, 0.6);
                                                rmPlacePlayer(5, 0.6, 0.9);
                                                rmPlacePlayer(1, 0.55, 0.15);
                                                rmPlacePlayer(2, 0.15, 0.35);
                        }
            }
            else if (playerPlace5 == 4) // Player 4 in middle
            {
                        if (playerSide == 1) // Players 5-1 in north, 2-3 in south
                        {
                                                rmPlacePlayer(4, 0.25, 0.75);
                                                rmPlacePlayer(5, 0.6, 0.9);
                                                rmPlacePlayer(1, 0.9, 0.6);
                                                rmPlacePlayer(2, 0.15, 0.35);
                                                rmPlacePlayer(3, 0.55, 0.15);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(4, 0.25, 0.75);
                                                rmPlacePlayer(5, 0.9, 0.6);
                                                rmPlacePlayer(1, 0.6, 0.9);
                                                rmPlacePlayer(2, 0.55, 0.15);
                                                rmPlacePlayer(3, 0.15, 0.35);
                        }
            }
            else if (playerPlace5 == 5) // Player 5 in middle
            {
                        if (playerSide == 1) // Players 1-2 in north, 3-4 in south
                        {
                                                rmPlacePlayer(5, 0.25, 0.75);
                                                rmPlacePlayer(1, 0.6, 0.9);
                                                rmPlacePlayer(2, 0.9, 0.6);
                                                rmPlacePlayer(3, 0.15, 0.35);
                                                rmPlacePlayer(4, 0.55, 0.15);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(5, 0.25, 0.75);
                                                rmPlacePlayer(1, 0.9, 0.6);
                                                rmPlacePlayer(2, 0.6, 0.9);
                                                rmPlacePlayer(3, 0.55, 0.15);
                                                rmPlacePlayer(4, 0.15, 0.35);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 6) // One player in each corner + two in middle, in left AND right
      {
            if (playerPlace6 == 1) // Players 1-2 in middle
            {
                        if (playerSide == 1) // Player 1 in left, 2 in right | Players 3-4 in north, 5-6 in south
                        {
                                                rmPlacePlayer(1, 0.25, 0.75);
                                                rmPlacePlayer(2, 0.78, 0.3);
                                                rmPlacePlayer(3, 0.6, 0.9);
                                                rmPlacePlayer(4, 0.9, 0.6);
                                                rmPlacePlayer(5, 0.15, 0.35);
                                                rmPlacePlayer(6, 0.55, 0.15);
                        }
                        else if (playerSide == 2) // Player 1 in right, 2 in left
                        {
                                                rmPlacePlayer(1, 0.78, 0.3);
                                                rmPlacePlayer(2, 0.25, 0.75);
                                                rmPlacePlayer(3, 0.9, 0.6);
                                                rmPlacePlayer(4, 0.6, 0.9);
                                                rmPlacePlayer(5, 0.55, 0.15);
                                                rmPlacePlayer(6, 0.15, 0.35);
                        }
            }
            else if (playerPlace6 == 2) // Players 3-4 in middle
            {
                        if (playerSide == 1) // Player 3 in left, 4 in right | Players 5-6 in north, 1-2 in south
                        {
                                                rmPlacePlayer(3, 0.25, 0.75);
                                                rmPlacePlayer(4, 0.78, 0.3);
                                                rmPlacePlayer(5, 0.6, 0.9);
                                                rmPlacePlayer(6, 0.9, 0.6);
                                                rmPlacePlayer(1, 0.15, 0.35);
                                                rmPlacePlayer(2, 0.55, 0.15);
                        }
                        else if (playerSide == 2) // Player 3 in right, 4 in left
                        {
                                                rmPlacePlayer(3, 0.78, 0.3);
                                                rmPlacePlayer(4, 0.25, 0.75);
                                                rmPlacePlayer(5, 0.9, 0.6);
                                                rmPlacePlayer(6, 0.6, 0.9);
                                                rmPlacePlayer(1, 0.55, 0.15);
                                                rmPlacePlayer(2, 0.15, 0.35);
                        }
            }
            else if (playerPlace6 == 3) // Players 5-6 in middle
            {
                        if (playerSide == 1) // Player 5 in left, 6 in right | Players 1-2 in north, 3-4 in south
                        {
                                                rmPlacePlayer(5, 0.25, 0.75);
                                                rmPlacePlayer(6, 0.78, 0.3);
                                                rmPlacePlayer(1, 0.6, 0.9);
                                                rmPlacePlayer(2, 0.9, 0.6);
                                                rmPlacePlayer(3, 0.15, 0.35);
                                                rmPlacePlayer(4, 0.55, 0.15);
                        }
                        else if (playerSide == 2) // Player 5 in right, 6 in left
                        {
                                                rmPlacePlayer(5, 0.78, 0.3);
                                                rmPlacePlayer(6, 0.25, 0.75);
                                                rmPlacePlayer(1, 0.9, 0.6);
                                                rmPlacePlayer(2, 0.6, 0.9);
                                                rmPlacePlayer(3, 0.55, 0.15);
                                                rmPlacePlayer(4, 0.15, 0.35);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 7) // One player in each corner + two in middle, in left AND right + one player in middle in north or south
      {
            if (playerPlace7 == 1) // Player 1 in upper/lower middle
            {
                        if (playerSide == 1) // Player 2 in middle left, 3 in middle right | 4-5 in north, 6-7 in south
                        {
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(1, 0.75, 0.75);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(1, 0.35, 0.25);
                                                rmPlacePlayer(2, 0.25, 0.75);
                                                rmPlacePlayer(3, 0.78, 0.3);
                                                rmPlacePlayer(4, 0.6, 0.9);
                                                rmPlacePlayer(5, 0.9, 0.6);
                                                rmPlacePlayer(6, 0.15, 0.35);
                                                rmPlacePlayer(7, 0.55, 0.15);
                        }
                        else if (playerSide == 2) // Player 2 in middle right, 3 in middle left
                        {
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(1, 0.75, 0.75);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(1, 0.35, 0.25);
                                                rmPlacePlayer(2, 0.78, 0.3);
                                                rmPlacePlayer(3, 0.25, 0.75);
                                                rmPlacePlayer(4, 0.9, 0.6);
                                                rmPlacePlayer(5, 0.6, 0.9);
                                                rmPlacePlayer(6, 0.55, 0.15);
                                                rmPlacePlayer(7, 0.15, 0.35);
                        }
            }
            else if (playerPlace7 == 2) // Player 2 in upper/lower middle
            {
                        if (playerSide == 1) // Player 3 in middle left, 4 in middle right | 5-6 in north, 7-1 in south
                        {
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(2, 0.75, 0.75);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(2, 0.35, 0.25);
                                                rmPlacePlayer(3, 0.25, 0.75);
                                                rmPlacePlayer(4, 0.78, 0.3);
                                                rmPlacePlayer(5, 0.6, 0.9);
                                                rmPlacePlayer(6, 0.9, 0.6);
                                                rmPlacePlayer(7, 0.15, 0.35);
                                                rmPlacePlayer(1, 0.55, 0.15);
                        }
                        else if (playerSide == 2) // Player 3 in middle right, 4 in middle left
                        {
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(2, 0.75, 0.75);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(2, 0.35, 0.25);
                                                rmPlacePlayer(3, 0.78, 0.3);
                                                rmPlacePlayer(4, 0.25, 0.75);
                                                rmPlacePlayer(5, 0.9, 0.6);
                                                rmPlacePlayer(6, 0.6, 0.9);
                                                rmPlacePlayer(7, 0.55, 0.15);
                                                rmPlacePlayer(1, 0.15, 0.35);
                        }
            }
            else if (playerPlace7 == 3) // Player 3 in upper/lower middle
            {
                        if (playerSide == 1) // Player 4 in middle left, 5 in middle right | 6-7 in north, 1-2 in south
                        {
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(3, 0.75, 0.75);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(3, 0.35, 0.25);
                                                rmPlacePlayer(4, 0.25, 0.75);
                                                rmPlacePlayer(5, 0.78, 0.3);
                                                rmPlacePlayer(6, 0.6, 0.9);
                                                rmPlacePlayer(7, 0.9, 0.6);
                                                rmPlacePlayer(1, 0.15, 0.35);
                                                rmPlacePlayer(2, 0.55, 0.15);
                        }
                        else if (playerSide == 2) // Player 4 in middle right, 5 in middle left
                        {
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(3, 0.75, 0.75);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(3, 0.35, 0.25);
                                                rmPlacePlayer(4, 0.78, 0.3);
                                                rmPlacePlayer(5, 0.25, 0.75);
                                                rmPlacePlayer(6, 0.9, 0.6);
                                                rmPlacePlayer(7, 0.6, 0.9);
                                                rmPlacePlayer(1, 0.55, 0.15);
                                                rmPlacePlayer(2, 0.15, 0.35);
                        }
            }
            else if (playerPlace7 == 4) // Player 4 in upper/lower middle
            {
                        if (playerSide == 1) // Player 5 in middle left, 6 in middle right | 7-1 in north, 2-3 in south
                        {
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(4, 0.75, 0.75);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(4, 0.35, 0.25);
                                                rmPlacePlayer(5, 0.25, 0.75);
                                                rmPlacePlayer(6, 0.78, 0.3);
                                                rmPlacePlayer(7, 0.6, 0.9);
                                                rmPlacePlayer(1, 0.9, 0.6);
                                                rmPlacePlayer(2, 0.15, 0.35);
                                                rmPlacePlayer(3, 0.55, 0.15);
                        }
                        else if (playerSide == 2) // Player 5 in middle right, 6 in middle left
                        {
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(4, 0.75, 0.75);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(4, 0.35, 0.25);
                                                rmPlacePlayer(5, 0.78, 0.3);
                                                rmPlacePlayer(6, 0.25, 0.75);
                                                rmPlacePlayer(7, 0.9, 0.6);
                                                rmPlacePlayer(1, 0.6, 0.9);
                                                rmPlacePlayer(2, 0.55, 0.15);
                                                rmPlacePlayer(3, 0.15, 0.35);
                        }
            }
            else if (playerPlace7 == 5) // Player 5 in upper/lower middle
            {
                        if (playerSide == 1) // Player 6 in middle left, 7 in middle right | 1-2 in north, 3-4 in south
                        {
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(5, 0.75, 0.75);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(5, 0.35, 0.25);
                                                rmPlacePlayer(6, 0.25, 0.75);
                                                rmPlacePlayer(7, 0.78, 0.3);
                                                rmPlacePlayer(1, 0.6, 0.9);
                                                rmPlacePlayer(2, 0.9, 0.6);
                                                rmPlacePlayer(3, 0.15, 0.35);
                                                rmPlacePlayer(4, 0.55, 0.15);
                        }
                        else if (playerSide == 2) // Player 6 in middle right, 7 in middle left
                        {
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(5, 0.75, 0.75);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(5, 0.35, 0.25);
                                                rmPlacePlayer(6, 0.78, 0.3);
                                                rmPlacePlayer(7, 0.25, 0.75);
                                                rmPlacePlayer(1, 0.9, 0.6);
                                                rmPlacePlayer(2, 0.6, 0.9);
                                                rmPlacePlayer(3, 0.55, 0.15);
                                                rmPlacePlayer(4, 0.15, 0.35);
                        }
            }
            else if (playerPlace7 == 6) // Player 6 in upper/lower middle
            {
                        if (playerSide == 1) // Player 7 in middle left, 1 in middle right | 2-3 in north, 4-5 in south
                        {
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(6, 0.75, 0.75);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(6, 0.35, 0.25);
                                                rmPlacePlayer(7, 0.25, 0.75);
                                                rmPlacePlayer(1, 0.78, 0.3);
                                                rmPlacePlayer(2, 0.6, 0.9);
                                                rmPlacePlayer(3, 0.9, 0.6);
                                                rmPlacePlayer(4, 0.15, 0.35);
                                                rmPlacePlayer(5, 0.55, 0.15);
                        }
                        else if (playerSide == 2) // Player 7 in middle right, 1 in middle left
                        {
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(6, 0.75, 0.75);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(6, 0.35, 0.25);
                                                rmPlacePlayer(7, 0.78, 0.3);
                                                rmPlacePlayer(1, 0.25, 0.75);
                                                rmPlacePlayer(2, 0.9, 0.6);
                                                rmPlacePlayer(3, 0.6, 0.9);
                                                rmPlacePlayer(4, 0.55, 0.15);
                                                rmPlacePlayer(5, 0.15, 0.35);
                        }
            }
            else if (playerPlace7 == 7) // Player 7 in upper/lower middle
            {
                        if (playerSide == 1) // Player 1 in middle left, 2 in middle right | 3-4 in north, 5-6 in south
                        {
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(7, 0.75, 0.75);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(7, 0.35, 0.25);
                                                rmPlacePlayer(1, 0.25, 0.75);
                                                rmPlacePlayer(2, 0.78, 0.3);
                                                rmPlacePlayer(3, 0.6, 0.9);
                                                rmPlacePlayer(4, 0.9, 0.6);
                                                rmPlacePlayer(5, 0.15, 0.35);
                                                rmPlacePlayer(6, 0.55, 0.15);
                        }
                        else if (playerSide == 2) // Player 1 in middle right, 2 in middle left
                        {
                                                if (teamSide == 1)
                                                                        rmPlacePlayer(7, 0.75, 0.75);
                                                else if (teamSide == 2)
                                                                        rmPlacePlayer(7, 0.35, 0.25);
                                                rmPlacePlayer(1, 0.78, 0.3);
                                                rmPlacePlayer(2, 0.25, 0.75);
                                                rmPlacePlayer(3, 0.9, 0.6);
                                                rmPlacePlayer(4, 0.6, 0.9);
                                                rmPlacePlayer(5, 0.55, 0.15);
                                                rmPlacePlayer(6, 0.15, 0.35);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 8) // One player in each corner + two in middle in left AND right + two players in upper or lower middle
      {
            if (playerPlace8 == 1) // Players 1-2 in upper/lower middle
            {
                        if (playerSide == 1) // Player 1 in upper, 2 in lower | 3 in middle left, 4 in middle right | 5-6 in north, 7-8 in south
                        {
                                                rmPlacePlayer(1, 0.75, 0.75);
                                                rmPlacePlayer(2, 0.35, 0.25);
                                                rmPlacePlayer(3, 0.25, 0.75);
                                                rmPlacePlayer(4, 0.78, 0.3);
                                                rmPlacePlayer(5, 0.6, 0.9);
                                                rmPlacePlayer(6, 0.9, 0.6);
                                                rmPlacePlayer(7, 0.15, 0.35);
                                                rmPlacePlayer(8, 0.55, 0.15);
                        }
                        else if (playerSide == 2) // Player 1 in lower, 2 in upper | 3 in middle right, 4 in middle left
                        {
                                                rmPlacePlayer(1, 0.35, 0.25);
                                                rmPlacePlayer(2, 0.75, 0.75);
                                                rmPlacePlayer(3, 0.78, 0.3);
                                                rmPlacePlayer(4, 0.25, 0.75);
                                                rmPlacePlayer(5, 0.9, 0.6);
                                                rmPlacePlayer(6, 0.6, 0.9);
                                                rmPlacePlayer(7, 0.55, 0.15);
                                                rmPlacePlayer(8, 0.15, 0.35);
                        }
            }
            else if (playerPlace8 == 2) // Players 3-4 in upper/lower middle
            {
                        if (playerSide == 1) // Player 3 in upper, 4 in lower | 5 in middle left, 6 in middle right | 7-8 in north, 1-2 in south
                        {
                                                rmPlacePlayer(3, 0.75, 0.75);
                                                rmPlacePlayer(4, 0.35, 0.25);
                                                rmPlacePlayer(5, 0.25, 0.75);
                                                rmPlacePlayer(6, 0.78, 0.3);
                                                rmPlacePlayer(7, 0.6, 0.9);
                                                rmPlacePlayer(8, 0.9, 0.6);
                                                rmPlacePlayer(1, 0.15, 0.35);
                                                rmPlacePlayer(2, 0.55, 0.15);
                        }
                        else if (playerSide == 2) // Player 3 in lower, 4 in upper | 5 in middle right, 6 in middle left
                        {
                                                rmPlacePlayer(3, 0.35, 0.25);
                                                rmPlacePlayer(4, 0.75, 0.75);
                                                rmPlacePlayer(5, 0.78, 0.3);
                                                rmPlacePlayer(6, 0.25, 0.75);
                                                rmPlacePlayer(7, 0.9, 0.6);
                                                rmPlacePlayer(8, 0.6, 0.9);
                                                rmPlacePlayer(1, 0.55, 0.15);
                                                rmPlacePlayer(2, 0.15, 0.35);
                        }
            }
            else if (playerPlace8 == 3) // Players 5-6 in upper/lower middle
            {
                        if (playerSide == 1) // Player 5 in upper, 6 in lower | 7 in middle left, 8 in middle right | 1-2 in north, 3-4 in south
                        {
                                                rmPlacePlayer(5, 0.75, 0.75);
                                                rmPlacePlayer(6, 0.35, 0.25);
                                                rmPlacePlayer(7, 0.25, 0.75);
                                                rmPlacePlayer(8, 0.78, 0.3);
                                                rmPlacePlayer(1, 0.6, 0.9);
                                                rmPlacePlayer(2, 0.9, 0.6);
                                                rmPlacePlayer(3, 0.15, 0.35);
                                                rmPlacePlayer(4, 0.55, 0.15);
                        }
                        else if (playerSide == 2) // Player 5 in lower, 6 in upper | 7 in middle right, 8 in middle left
                        {
                                                rmPlacePlayer(5, 0.35, 0.25);
                                                rmPlacePlayer(6, 0.75, 0.75);
                                                rmPlacePlayer(7, 0.78, 0.3);
                                                rmPlacePlayer(8, 0.25, 0.75);
                                                rmPlacePlayer(1, 0.9, 0.6);
                                                rmPlacePlayer(2, 0.6, 0.9);
                                                rmPlacePlayer(3, 0.55, 0.15);
                                                rmPlacePlayer(4, 0.15, 0.35);
                        }
            }
            else if (playerPlace8 == 4) // Players 7-8 in upper/lower middle
            {
                        if (playerSide == 1) // Player 7 in upper, 8 in lower | 1 in middle left, 2 in middle right | 3-4 in north, 5-6 in south
                        {
                                                rmPlacePlayer(7, 0.75, 0.75);
                                                rmPlacePlayer(8, 0.35, 0.25);
                                                rmPlacePlayer(1, 0.25, 0.75);
                                                rmPlacePlayer(2, 0.78, 0.3);
                                                rmPlacePlayer(3, 0.6, 0.9);
                                                rmPlacePlayer(4, 0.9, 0.6);
                                                rmPlacePlayer(5, 0.15, 0.35);
                                                rmPlacePlayer(6, 0.55, 0.15);
                        }
                        else if (playerSide == 2) // Player 7 in lower, 8 in upper | 1 in middle right, 2 in middle left
                        {
                                                rmPlacePlayer(7, 0.35, 0.25);
                                                rmPlacePlayer(8, 0.75, 0.75);
                                                rmPlacePlayer(1, 0.78, 0.3);
                                                rmPlacePlayer(2, 0.25, 0.75);
                                                rmPlacePlayer(3, 0.9, 0.6);
                                                rmPlacePlayer(4, 0.6, 0.9);
                                                rmPlacePlayer(5, 0.55, 0.15);
                                                rmPlacePlayer(6, 0.15, 0.35);
                        }
            }
      }
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.50);
		

   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "TreeGreatPlains", 6, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 12);
   rmSetObjectDefMaxDistance(startAreaTreeID, 16);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startFoxID=rmCreateObjectDef("starting fox");
   rmAddObjectDefItem(startFoxID, "deer", 6, 6.0);
   rmSetObjectDefCreateHerd(startFoxID, true);
   rmSetObjectDefMinDistance(startFoxID, 14);
   rmSetObjectDefMaxDistance(startFoxID, 18);
   rmAddObjectDefConstraint(startFoxID, avoidStartResource);
   rmAddObjectDefConstraint(startFoxID, avoidImpassableLand);
   rmAddObjectDefConstraint(startFoxID, shortAvoidStartingUnits);

   int startCopperID=rmCreateObjectDef("player copper");
   rmAddObjectDefItem(startCopperID, "MineCopper", 1, 5.0);
   rmSetObjectDefMinDistance(startCopperID, 14);
   rmSetObjectDefMaxDistance(startCopperID, 18);
   rmAddObjectDefConstraint(startCopperID, avoidStartResource);
   rmAddObjectDefConstraint(startCopperID, avoidImpassableLand);
   rmAddObjectDefConstraint(startCopperID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startFoxID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startCopperID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
      rmAddClosestPointConstraint(flagVsFlag);
      rmAddClosestPointConstraint(flagLand);		
      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Text
   rmSetStatusText("",0.60);


   // Place the trade route/barbarian settlements to middle area

   if (middleBonus == 0)
   {
      // Place the trade route, if middleBonus == 0

      // Create the trade route
      int tradeRouteID = rmCreateTradeRoute();
      int socketID=rmCreateObjectDef("sockets to dock trade posts");
      rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

      // Define the sockets
      rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
      rmSetObjectDefAllowOverlap(socketID, true);
      rmSetObjectDefMinDistance(socketID, 0.0);
      rmSetObjectDefMaxDistance(socketID, 10.0);

      // Set the trade route waypoints
      if (cNumberTeams == 2)
      {
            rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.9);
            rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.53, 0.53, 10, 10);
            rmAddRandomTradeRouteWaypoints(tradeRouteID, 1.0, 0.13, 10, 10);
      }
      if (cNumberTeams > 2 && cNumberNonGaiaPlayers <= 5) // If FFA, and 2-5 players: STARTS MORE FROM MIDDLE
      {
            rmAddTradeRouteWaypoint(tradeRouteID, 0.32, 0.67);
            rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.53, 0.53, 10, 10);
            rmAddRandomTradeRouteWaypoints(tradeRouteID, 1.0, 0.13, 10, 10);
      }
      if (cNumberTeams > 2 && cNumberNonGaiaPlayers >= 6) // If FFA, and 6-8 players: STARTS AND ENDS MORE NEAR MIDDLE 
      {
            rmAddTradeRouteWaypoint(tradeRouteID, 0.32, 0.67);
            rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.53, 0.53, 10, 10);
            rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.7, 0.4, 10, 10);
      }
      bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
      if(placedTradeRoute == false)
            rmEchoError("Failed to place trade route"); 

      // Place sockets to trade route
      if (cNumberTeams == 2)
      {
            vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.2);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.8);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      }
      if (cNumberTeams > 2 && cNumberNonGaiaPlayers == 5)
      {
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.1);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.4);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.7);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      }
      if (cNumberTeams > 2 && cNumberNonGaiaPlayers >= 6)
      {
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.1);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
            socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.9);
            rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      }
   }
   else if (middleBonus == 1)
   {
      // Define and place the 3 natives, always Visigoths, if middleBonus == 1

      // Native 1, to western side
      int nativeVillageID = -1;
      int nativeVillageType = rmRandInt(1,5);
      nativeVillageID = rmCreateGrouping("visigoth village", "native comanche village "+nativeVillageType);
      rmSetGroupingMinDistance(nativeVillageID, 0.0);
      rmSetGroupingMaxDistance(nativeVillageID, 10.0);
      rmAddGroupingToClass(nativeVillageID, rmClassID("natives"));
      if (cNumberTeams == 2 || cNumberNonGaiaPlayers <= 4) // In team games: ALWAYS | In FFA: ONLY IF THERE IS 2-4 PLAYERS >>TO LEFT<<
            rmPlaceGroupingAtLoc(nativeVillageID, 0, 0.25, 0.75);
      if (cNumberTeams > 2 && cNumberNonGaiaPlayers >= 6) // In 6-8 player FFA >>TO MIDDLE WESTERN<<
            rmPlaceGroupingAtLoc(nativeVillageID, 0, 0.44, 0.60);

      // Native 2, to middle
      int nativeVillage2ID = -1;
      int nativeVillage2Type = rmRandInt(1,5);
      nativeVillage2ID = rmCreateGrouping("visigoth village 2", "native comanche village "+nativeVillage2Type);
      rmSetGroupingMinDistance(nativeVillage2ID, 0.0);
      rmSetGroupingMaxDistance(nativeVillage2ID, 10.0);
      rmAddGroupingToClass(nativeVillage2ID, rmClassID("natives"));
      if (cNumberTeams == 2 || cNumberNonGaiaPlayers <= 5) // In team games: ALWAYS | In FFA: ONLY IF THERE IS 2-5 PLAYERS >>TO EXACT CENTER<<
            rmPlaceGroupingAtLoc(nativeVillage2ID, 0, 0.52, 0.52);
      if (cNumberTeams > 2  && cNumberNonGaiaPlayers >= 6) // In 6-8 player FFA >>TO MIDDLE EASTERN<<
            rmPlaceGroupingAtLoc(nativeVillage2ID, 0, 0.60, 0.44);

      // Native 3, to eastern side
      int nativeVillage3ID = -1;
      int nativeVillage3Type = rmRandInt(1,5);
      nativeVillage3ID = rmCreateGrouping("visigoth village 3", "native comanche village "+nativeVillage3Type);
      rmSetGroupingMinDistance(nativeVillage3ID, 0.0);
      rmSetGroupingMaxDistance(nativeVillage3ID, 10.0);
      rmAddGroupingToClass(nativeVillage3ID, rmClassID("natives"));
      if (cNumberTeams == 2 || cNumberNonGaiaPlayers <= 5) // In team games: ALWAYS | In FFA: ONLY IF THERE IS 2-5 PLAYERS! >>TO RIGHT<<
            rmPlaceGroupingAtLoc(nativeVillage3ID, 0, 0.78, 0.3);
   }


   // Text
   rmSetStatusText("",0.70);


   // Define and place forests
	
   int numTries=20*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(300), rmAreaTilesToFraction(350));
      rmSetAreaForestType(forest, "great plains forest");
      rmSetAreaForestDensity(forest, 0.9);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.5);
      rmSetAreaForestUnderbrush(forest, 0.3);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLandLong);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, avoidTradeRoute);
      rmAddAreaConstraint(forest, avoidTradeSockets);
      rmAddAreaConstraint(forest, avoidNatives);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.  
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place animals
   
   // Define and place wolf herds
   int wolfHerdID=rmCreateObjectDef("wolf herd");
   rmAddObjectDefItem(wolfHerdID, "moose", rmRandInt(5,6), 8.0);
   rmSetObjectDefCreateHerd(wolfHerdID, true);
   rmSetObjectDefMinDistance(wolfHerdID, 0.0);
   rmSetObjectDefMaxDistance(wolfHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(wolfHerdID, classAnimals);
   rmAddObjectDefConstraint(wolfHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(wolfHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(wolfHerdID, animalConstraint);
   rmAddObjectDefConstraint(wolfHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(wolfHerdID, avoidTradeSockets);
   rmAddObjectDefConstraint(wolfHerdID, avoidNatives);
   numTries=cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(wolfHerdID, 0, 0.5, 0.5);

   // Define and place fox herds
   int foxHerdID=rmCreateObjectDef("fox herd");
   rmAddObjectDefItem(foxHerdID, "deer", rmRandInt(5,6), 8.0);
   rmSetObjectDefMinDistance(foxHerdID, 0.0);
   rmSetObjectDefMaxDistance(foxHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(foxHerdID, classAnimals);
   rmAddObjectDefConstraint(foxHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(foxHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(foxHerdID, animalConstraint);
   rmAddObjectDefConstraint(foxHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(foxHerdID, avoidTradeSockets);
   rmAddObjectDefConstraint(foxHerdID, avoidNatives);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(foxHerdID, 0, 0.5, 0.5);

   // Define and place spanish ibex herds
   int spanishIbexHerdID=rmCreateObjectDef("spanish ibex herd");
   rmAddObjectDefItem(spanishIbexHerdID, "ypIbex", rmRandInt(7,8), 8.0);
   rmSetObjectDefCreateHerd(spanishIbexHerdID, true);
   rmSetObjectDefMinDistance(spanishIbexHerdID, 0.0);
   rmSetObjectDefMaxDistance(spanishIbexHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(spanishIbexHerdID, classAnimals);
   rmAddObjectDefConstraint(spanishIbexHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(spanishIbexHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(spanishIbexHerdID, animalConstraint);
   rmAddObjectDefConstraint(spanishIbexHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(spanishIbexHerdID, avoidTradeSockets);
   rmAddObjectDefConstraint(spanishIbexHerdID, avoidNatives);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(spanishIbexHerdID, 0, 0.5, 0.5);


   // Define and place gold mines

   int goldCount = 2*cNumberNonGaiaPlayers+3;

   for(i=0; < goldCount)
   {
      int goldID=rmCreateObjectDef("gold mine"+i);
      rmAddObjectDefItem(goldID, "mineGold", 1, 0.0);
      rmSetObjectDefMinDistance(goldID, 0.0);
      rmSetObjectDefMaxDistance(goldID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(goldID, classMines);
      rmAddObjectDefConstraint(goldID, avoidImpassableLandLong);
      rmAddObjectDefConstraint(goldID, playerConstraint);
      rmAddObjectDefConstraint(goldID, avoidMines);
      rmAddObjectDefConstraint(goldID, avoidTradeRoute);
      rmAddObjectDefConstraint(goldID, avoidTradeSockets);
      rmAddObjectDefConstraint(goldID, avoidNatives);
      rmPlaceObjectDefAtLoc(goldID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.90);
	

   // Define and place tuna fish

   int fishID=rmCreateObjectDef("fish");
   rmAddObjectDefItem(fishID, "ypFishTuna", 1, 4.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.5));
   rmAddObjectDefConstraint(fishID, fishVsFish);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 7*cNumberNonGaiaPlayers);


   // Text
   rmSetStatusText("",0.99);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLandLong);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget1, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget1, avoidNatives);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLandLong);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget2, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget2, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLandLong);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget3, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget3, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLandLong);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget4, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget4, avoidNatives);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Define and place cow herds 

   int cowID=rmCreateObjectDef("cow");
   rmAddObjectDefItem(cowID, "cow", 2, 4.0);
   rmSetObjectDefMinDistance(cowID, 0.0);
   rmSetObjectDefMaxDistance(cowID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(cowID, avoidCow);
   rmAddObjectDefConstraint(cowID, playerConstraint);
   rmAddObjectDefConstraint(cowID, avoidImpassableLand);
   rmAddObjectDefConstraint(cowID, avoidTradeRoute);
   rmAddObjectDefConstraint(cowID, avoidTradeSockets);
   rmAddObjectDefConstraint(cowID, avoidNatives);
   rmPlaceObjectDefAtLoc(cowID, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);


   // Text
   rmSetStatusText("",1.0); 
}  
