// RM script of Finland
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Picks the map size
   int playerTiles = 11000;
   if (cNumberNonGaiaPlayers > 4)
      playerTiles = 10000;
   if (cNumberNonGaiaPlayers > 6)
      playerTiles = 8500;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);

   // Set map smoothness
   rmSetMapElevationHeightBlend(1);
	

   // Picks a default water height
   rmSetSeaLevel(0.0);

   
   // Chooses summer or winter 
   int seasonPicker = rmRandInt(0,1); // 0 = summer, 1 = winter


   // Chooses the side where teams start in team games
   int teamSide = rmRandInt(1,4);


   // Picks default terrain and lighting - summer/winter
   if (seasonPicker == 0)
   {
      rmSetMapElevationParameters(cElevTurbulence, 0.09, 4, 0.7, 8.0);
      rmTerrainInitialize("texas\ground1_tex", 1.0);
      rmSetLightingSet("malta");
      rmSetMapType("greatPlains");
      rmSetMapType("grass");
      rmSetMapType("land");
   }
   else
   {
      rmSetMapElevationParameters(cElevTurbulence, 0.05, 4, 0.7, 8.0);
      rmSetBaseTerrainMix("yukon_snow");
      rmTerrainInitialize("yukon\ground1_yuk", 1.0);
      rmSetLightingSet("Yukon");
      rmSetMapType("greatPlains");
      rmSetMapType("snow");
      rmSetMapType("land");
      rmSetGlobalSnow( 1.0 );
   }


   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);


   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   int classLake=rmDefineClass("lake");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("nuggets");

   // Define constraints
   
   // Map edge and centre constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // All player things avoidance
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);

   // All resource avoidance
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 65.0);
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 15.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 55.0);
   int avoidCow=rmCreateTypeDistanceConstraint("cow avoids cow", "cow", 70.0);

   // Impassable land avoidance
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 5.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);

   // Lakes avoidance
   int lakeConstraint=rmCreateClassDistanceConstraint("lakes avoid lakes", classLake, 40.0);
   int shortLakeConstraint=rmCreateClassDistanceConstraint("short lakes avoid lakes", classLake, 20.0);
   int shortestLakeConstraint=rmCreateClassDistanceConstraint("forests avoid lakes", classLake, 6.0);

   // Nuggets avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("nuggets avoids nuggets", rmClassID("nuggets"), 40.0);

   // Water objects avoidance
   int fishVsFish=rmCreateTypeDistanceConstraint("fish vs fish", "FishSalmon", 20.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish land", "land", true, 5.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid same", "HomeCityWaterSpawnFlag", 25.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag vs land", "land", true, 15.0);


   // Text
   rmSetStatusText("",0.10);


   // Define and place invisible "middle" area where the lakes are placed

   int middleID=rmCreateArea("middle");
   rmSetAreaSize(middleID, 0.2, 0.2);
   rmSetAreaLocation(middleID, 0.5, 0.5);
   if (seasonPicker == 0)
      rmSetAreaTerrainType(middleID, "texas\ground1_tex");
   else 
   {
      rmSetAreaTerrainType(middleID, "yukon\ground1_yuk");
      rmSetAreaMix(middleID, "yukon_snow");
   }
   rmSetAreaBaseHeight(middleID, -3.0);
   rmSetAreaCoherence(middleID, 0.4);
   rmSetAreaElevationType(middleID, cElevTurbulence);
   rmSetAreaElevationVariation(middleID, 5.0);
   rmSetAreaElevationMinFrequency(middleID, 0.02);
   rmSetAreaElevationOctaves(middleID, 4);
   rmSetAreaElevationPersistence(middleID, 0.7);
   rmSetAreaElevationNoiseBias(middleID, 1);
   rmBuildArea(middleID);


   // Define and place lakes

   // 4 BIG random lakes
   for(i=1; < 5)
   {
      int lakeBig=rmCreateArea("random lake big"+i, rmAreaID("middle"));
      rmSetAreaSize(lakeBig, 0.015, 0.017);
      rmAddAreaToClass(lakeBig, classLake);
      if (seasonPicker == 0)
            rmSetAreaWaterType(lakeBig, "great lakes");
      else
            rmSetAreaWaterType(lakeBig, "great lakes ice");
      rmSetAreaBaseHeight(lakeBig, 0.0);
      rmSetAreaMinBlobs(lakeBig, 5);
      rmSetAreaMaxBlobs(lakeBig, 5);
      rmAddAreaConstraint(lakeBig, lakeConstraint); 
      rmSetAreaCoherence(lakeBig, 0.4);
      rmBuildArea(lakeBig);
   }

   // 8 SMALL random lakes
   for(i=1; < 9)
   {
      int lakeSmall=rmCreateArea("random lake small"+i, rmAreaID("middle"));
      rmSetAreaSize(lakeSmall, 0.005, 0.007);
      rmAddAreaToClass(lakeSmall, classLake);
      if (seasonPicker == 0)
            rmSetAreaWaterType(lakeSmall, "great lakes");
      else
            rmSetAreaTerrainType(lakeSmall, "great_lakes\ground_ice1_gl");
      rmSetAreaBaseHeight(lakeSmall, 0.0);
      rmSetAreaMinBlobs(lakeSmall, 5);
      rmSetAreaMaxBlobs(lakeSmall, 5);
      rmAddAreaConstraint(lakeSmall, shortLakeConstraint); 
      rmSetAreaCoherence(lakeSmall, 0.4);
      rmBuildArea(lakeSmall);
   }


   // Text
   rmSetStatusText("",0.40);


   // Place players - in team and FFA games

   // Player placement if FFA (Free For All) - place players in circle
   if(cNumberTeams > 2)
   {
      rmSetTeamSpacingModifier(0.7);
      rmPlacePlayersCircular(0.40, 0.40, 0.0);
   }

   // Player placement if teams - place teams in circle to apart from each other
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      if (rmGetNumberPlayersOnTeam(0) == 2)
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.05, 0.20);
         else if (teamSide == 2)
            rmSetPlacementSection(0.30, 0.45);
         else if (teamSide == 3)
            rmSetPlacementSection(0.55, 0.70);
         else if (teamSide == 4)
            rmSetPlacementSection(0.80, 0.95);
      }
      else
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.00, 0.25);
         else if (teamSide == 2)
            rmSetPlacementSection(0.25, 0.50);
         else if (teamSide == 3)
            rmSetPlacementSection(0.50, 0.75);
         else if (teamSide == 4)
            rmSetPlacementSection(0.75, 1.00);
      }
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.4, 0.4, 0.0);

      rmSetPlacementTeam(1);
      if (rmGetNumberPlayersOnTeam(1) == 2)
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.55, 0.70);
         else if (teamSide == 2)
            rmSetPlacementSection(0.80, 0.95);
         else if (teamSide == 3)
            rmSetPlacementSection(0.05, 0.20);
         else if (teamSide == 4)
            rmSetPlacementSection(0.30, 0.45);
      }
      else
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.50, 0.75);
         else if (teamSide == 2)
            rmSetPlacementSection(0.75, 1.00);
         else if (teamSide == 3)
            rmSetPlacementSection(0.00, 0.25);
         else if (teamSide == 4)
            rmSetPlacementSection(0.25, 0.50);
      }
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.4, 0.4, 0.0);
   }


   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      // Create the player areas.
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint);  
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.50);
		

   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   if (seasonPicker == 0)
      rmAddObjectDefItem(startAreaTreeID, "TreeGreatLakes", 6, 4.0);
   else
      rmAddObjectDefItem(startAreaTreeID, "TreeGreatLakesSnow", 6, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 14);
   rmSetObjectDefMaxDistance(startAreaTreeID, 18);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startMooseID=rmCreateObjectDef("starting moose");
   rmAddObjectDefItem(startMooseID, "moose", 6, 6.0);
   rmSetObjectDefCreateHerd(startMooseID, true);
   rmSetObjectDefMinDistance(startMooseID, 14);
   rmSetObjectDefMaxDistance(startMooseID, 18);
   rmAddObjectDefConstraint(startMooseID, avoidStartResource);
   rmAddObjectDefConstraint(startMooseID, avoidImpassableLand);
   rmAddObjectDefConstraint(startMooseID, shortAvoidStartingUnits);

   int startSilverID=rmCreateObjectDef("player silver");
   rmAddObjectDefItem(startSilverID, "Mine", 1, 5.0);
   rmSetObjectDefMinDistance(startSilverID, 14);
   rmSetObjectDefMaxDistance(startSilverID, 18);
   rmAddObjectDefConstraint(startSilverID, avoidStartResource);
   rmAddObjectDefConstraint(startSilverID, avoidImpassableLand);
   rmAddObjectDefConstraint(startSilverID, shortAvoidStartingUnits);

   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startMooseID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startSilverID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
      rmAddClosestPointConstraint(flagVsFlag);
      rmAddClosestPointConstraint(flagLand);		
      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Text
   rmSetStatusText("",0.60);


   // Define and place forests

   int numTries=500*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(1200), rmAreaTilesToFraction(1250));
      if (seasonPicker == 0)
            rmSetAreaForestType(forest, "great lakes forest");
      else
            rmSetAreaForestType(forest, "great lakes forest snow");
      rmSetAreaForestDensity(forest, 1.0);
      rmSetAreaForestClumpiness(forest, 0.5);
      rmSetAreaForestUnderbrush(forest, 0.0);
      rmSetAreaBaseHeight(forest, 0.0);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaMinBlobs(forest, 1);
      rmSetAreaMaxBlobs(forest, 3);
      rmSetAreaMinBlobDistance(forest, 10.0);
      rmSetAreaMaxBlobDistance(forest, 15.0);
      rmSetAreaCoherence(forest, 0.5);
      rmAddAreaConstraint(forest, shortPlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, shortestLakeConstraint);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 10 times in a row.  
            failCount++;
            if(failCount == 10)
                        break;
      }
      else
                        failCount=0; 
   }


   // Text
   rmSetStatusText("",0.70);


   // Define and place silver mines

   int silverCount = 3*cNumberNonGaiaPlayers;

   for(i=0; < silverCount)
   {
      int silverID=rmCreateObjectDef("silver mine"+i);
      rmAddObjectDefItem(silverID, "Mine", 1, 0.0);
      rmSetObjectDefMinDistance(silverID, 0.0);
      rmSetObjectDefMaxDistance(silverID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(silverID, classMines);
      rmAddObjectDefConstraint(silverID, avoidImpassableLand);
      rmAddObjectDefConstraint(silverID, avoidMines);
      rmAddObjectDefConstraint(silverID, playerConstraint);
      rmPlaceObjectDefAtLoc(silverID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place animals

   // Define and place deer herds
   int deerHerdID=rmCreateObjectDef("deer herd");
   rmAddObjectDefItem(deerHerdID, "deer", rmRandInt(7,8), 9.0);
   rmSetObjectDefCreateHerd(deerHerdID, true);
   rmSetObjectDefMinDistance(deerHerdID, 0.0);
   rmSetObjectDefMaxDistance(deerHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(deerHerdID, classAnimals);
   rmAddObjectDefConstraint(deerHerdID, playerConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(deerHerdID, animalConstraint);
   if (seasonPicker == 0)
      numTries=cNumberNonGaiaPlayers;
   else
      numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(deerHerdID, 0, 0.5, 0.5);

   // Define and place bear herds - ONLY IF SUMMER
   if (seasonPicker == 0)
   {
      int bearHerdID=rmCreateObjectDef("bear herd");
      rmAddObjectDefItem(bearHerdID, "elk", rmRandInt(7,8), 9.0);
      rmSetObjectDefMinDistance(bearHerdID, 0.0);
      rmSetObjectDefMaxDistance(bearHerdID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(bearHerdID, classAnimals);
      rmAddObjectDefConstraint(bearHerdID, playerConstraint);
      rmAddObjectDefConstraint(bearHerdID, avoidImpassableLand);
      rmAddObjectDefConstraint(bearHerdID, animalConstraint);
      numTries=cNumberNonGaiaPlayers;
      for (i=0; <numTries)
            rmPlaceObjectDefAtLoc(bearHerdID, 0, 0.5, 0.5);
   }

   // Define and place moose herds
   int mooseHerdID=rmCreateObjectDef("moose herd");
   rmAddObjectDefItem(mooseHerdID, "moose", rmRandInt(7,8), 9.0);
   rmSetObjectDefCreateHerd(mooseHerdID, true);
   rmSetObjectDefMinDistance(mooseHerdID, 0.0);
   rmSetObjectDefMaxDistance(mooseHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(mooseHerdID, classAnimals);
   rmAddObjectDefConstraint(mooseHerdID, playerConstraint);   
   rmAddObjectDefConstraint(mooseHerdID, avoidImpassableLand);   
   rmAddObjectDefConstraint(mooseHerdID, animalConstraint);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(mooseHerdID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.90);
	

   // Define and place fish

   int fishID=rmCreateObjectDef("fish");
   rmAddObjectDefItem(fishID, "FishSalmon", 1, 4.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(fishID, fishVsFish);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 6*cNumberNonGaiaPlayers);


   // Text
   rmSetStatusText("",0.99);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 25.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidAll);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Define and place cow herds 

   int cowID=rmCreateObjectDef("cow");
   rmAddObjectDefItem(cowID, "cow", 2, 4.0);
   rmSetObjectDefMinDistance(cowID, 0.0);
   rmSetObjectDefMaxDistance(cowID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(cowID, avoidCow);
   rmAddObjectDefConstraint(cowID, playerConstraint);
   rmAddObjectDefConstraint(cowID, avoidImpassableLand);
   rmPlaceObjectDefAtLoc(cowID, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);


   // Place King's Hill if KOTH game to middle - automatically avoids the lakes

   if(rmGetIsKOTH()) 
   {
      float xLoc = 0.5;
      float yLoc = 0.5;
      float walk = 0.05;

      ypKingsHillPlacer(xLoc, yLoc, walk, 0);
   }
   

   // Text
   rmSetStatusText("",1.0); 

}