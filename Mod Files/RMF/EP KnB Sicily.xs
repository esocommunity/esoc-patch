// RM script of Sicily
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);

   // Chooses the 1 native, always Sicilians

   // Native 1
   rmSetSubCiv(0, "Comanche");


   // Picks the map size
   int playerTiles = 20000;
   if (cNumberNonGaiaPlayers == 8)
	playerTiles = 14000;
   else if (cNumberNonGaiaPlayers == 7)
	playerTiles = 15000;
   else if (cNumberNonGaiaPlayers == 6)
	playerTiles = 16000;
   else if (cNumberNonGaiaPlayers == 5)
	playerTiles = 17000;
   else if (cNumberNonGaiaPlayers == 4)
	playerTiles = 18000;
   else if (cNumberNonGaiaPlayers == 3)
	playerTiles = 19000;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses the side where teams start in team games
   int teamSide = rmRandInt(1,2);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("saguenay");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.10, 4, 0.4, 9.0);
   rmSetSeaType("great lakes");
   rmTerrainInitialize("water");
   rmSetMapType("grass");
   rmSetMapType("water");
   rmSetMapType("greatPlains");
   

   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classMines=rmDefineClass("mines");
   int classCliff=rmDefineClass("cliff");
   int classAnimals=rmDefineClass("animals");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("nuggets");
   rmDefineClass("natives");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 45.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 60.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 60.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 5.0);
   int longAvoidImpassableLand=rmCreateTerrainDistanceConstraint("long avoid impassable land", "Land", false, 30.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 60.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);

   // Cliff avoidance
   int cliffConstraint=rmCreateClassDistanceConstraint("avoid cliff", classCliff, 5.0);
   int longCliffConstraint=rmCreateClassDistanceConstraint("long avoid cliffs", classCliff, 70.0);

   // Trade route avoidance
   int avoidTradeSockets=rmCreateTypeDistanceConstraint("avoid trade sockets", "SPCDrydock", 5.0);

   // Native avoidance
   int avoidNatives=rmCreateClassDistanceConstraint("things avoids natives", rmClassID("natives"), 5.0);

   // Water objects avoidance	
   int fishVsFish=rmCreateTypeDistanceConstraint("fish avoid fish", "ypFishTuna", 25.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish avoid land", "land", true, 5.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag avoid land", "land", true, 15.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid flags", "HomeCityWaterSpawnFlag", 25);


   // Text
   rmSetStatusText("",0.10);


   // Define and place the island

   int islandID=rmCreateArea("Island");
   rmSetAreaTerrainType(islandID, "california\ground8_cal");
   rmSetAreaMix(islandID, "scorched_ground");
   rmSetAreaSize(islandID, 0.3, 0.3);
   rmSetAreaCoherence(islandID, 0.6);
   rmSetAreaLocation(islandID, 0.5, 0.5);
   rmAddAreaInfluenceSegment(islandID, 0.4, 0.6, 0.6, 0.4); 
   rmSetAreaBaseHeight(islandID, 1.0);
   rmSetAreaMinBlobs(islandID, 10);
   rmSetAreaMaxBlobs(islandID, 20);
   rmSetAreaMinBlobDistance(islandID, 10);
   rmSetAreaMaxBlobDistance(islandID, 20);
   rmSetAreaSmoothDistance(islandID, 25);
   rmSetAreaElevationType(islandID, cElevTurbulence);
   rmSetAreaElevationVariation(islandID, 5.0);
   rmSetAreaElevationMinFrequency(islandID, 0.10);
   rmSetAreaElevationOctaves(islandID, 3);
   rmSetAreaElevationPersistence(islandID, 0.2);
   rmSetAreaElevationNoiseBias(islandID, 1);
   rmBuildArea(islandID);


   // Text
   rmSetStatusText("",0.20);


   // Place players in FFA and team games

   // Player placement if FFA (Free For All) - place players in different positions
   if(cNumberTeams > 2)
   {
      if (cNumberNonGaiaPlayers == 8)
      {
            rmSetPlacementSection(0.06, 0.94);
            rmPlacePlayersSquare(0.20, 0, 0);
      }
      if (cNumberNonGaiaPlayers == 7)
      {
            rmSetPlacementSection(0.01, 1.0);
            rmPlacePlayersSquare(0.17, 0, 0);
      }
      if (cNumberNonGaiaPlayers == 6)
      {
            rmSetPlacementSection(0.26, 1.25);
            rmPlacePlayersSquare(0.17, 0, 0);
      }
      if (cNumberNonGaiaPlayers == 5)
      {
            rmSetPlacementSection(0.26, 1.25);
            rmPlacePlayersSquare(0.20, 0, 0);
      }
      if (cNumberNonGaiaPlayers == 4)
      {
            rmSetPlacementSection(0.13, 1.12);
            rmPlacePlayersSquare(0.17, 0, 0);
      }
      if (cNumberNonGaiaPlayers == 3)
      {
            rmSetPlacementSection(0.26, 1.25);
            rmPlacePlayersSquare(0.17, 0, 0);
      }
      if (cNumberNonGaiaPlayers == 2)
      {
            rmSetPlacementSection(0.26, 1.25);
            rmPlacePlayersSquare(0.17, 0, 0);
      }
   }

   // Player placement if teams - place teams in lines to apart from each other
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      rmPlacePlayersLine(0.45, 0.75, 0.75, 0.45, 0, 0);

      rmSetPlacementTeam(1);
      rmPlacePlayersLine(0.55, 0.25, 0.25, 0.55, 0, 0);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.30);
		

   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "deer", 6, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 14);
   rmSetObjectDefMaxDistance(startAreaTreeID, 18);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startAreaTree2ID=rmCreateObjectDef("starting trees 2");
   rmAddObjectDefItem(startAreaTree2ID, "TreeGreatPlains", 4, 4.0);
   rmSetObjectDefMinDistance(startAreaTree2ID, 14);
   rmSetObjectDefMaxDistance(startAreaTree2ID, 18);
   rmAddObjectDefConstraint(startAreaTree2ID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTree2ID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTree2ID, shortAvoidStartingUnits);

   int startSilverID=rmCreateObjectDef("player silver");
   rmAddObjectDefItem(startSilverID, "Mine", 1, 5.0);
   rmSetObjectDefMinDistance(startSilverID, 14);
   rmSetObjectDefMaxDistance(startSilverID, 18);
   rmAddObjectDefConstraint(startSilverID, avoidStartResource);
   rmAddObjectDefConstraint(startSilverID, avoidImpassableLand);
   rmAddObjectDefConstraint(startSilverID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTree2ID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startSilverID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
      rmAddClosestPointConstraint(flagVsFlag);
      rmAddClosestPointConstraint(flagLand);		
      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Text
   rmSetStatusText("",0.40);


   int numCliffs = rmRandInt(5,7);
   for (i=0; <numCliffs)
   {  
      int cliffID=rmCreateArea("cliff"+i);
      rmSetAreaSize(cliffID, rmAreaTilesToFraction(700), rmAreaTilesToFraction(1000));
      rmSetAreaCliffType(cliffID, "texas grass");
      rmSetAreaCliffEdge(cliffID, 1, 0.8, 0.1, 1.0, 0);
      rmSetAreaCliffPainting(cliffID, false, true, true, 1.5, true);
      rmSetAreaCliffHeight(cliffID, 7, 4.0, 0.3);
      rmSetAreaCoherence(cliffID, 0.6);
      rmSetAreaSmoothDistance(cliffID, 10);
      rmSetAreaHeightBlend(cliffID, 1);
      rmSetAreaMinBlobs(cliffID, 10);
      rmSetAreaMaxBlobs(cliffID, 15);
      rmSetAreaMinBlobDistance(cliffID, 10);
      rmSetAreaMaxBlobDistance(cliffID, 15);
      rmAddAreaToClass(cliffID, classCliff);
      rmAddAreaConstraint(cliffID, playerConstraint);
      rmAddAreaConstraint(cliffID, longAvoidImpassableLand);
      rmAddAreaConstraint(cliffID, longCliffConstraint);
      rmBuildArea(cliffID);
   }


   // Define and place the 1 native to middle, always Sicilians

   int nativeVillageType = rmRandInt(1,5);
   int nativeVillageID = rmCreateGrouping("sicilian village", "native comanche village "+nativeVillageType);
   rmSetGroupingMinDistance(nativeVillageID, 0.0);
   rmSetGroupingMaxDistance(nativeVillageID, 10.0);
   rmAddGroupingToClass(nativeVillageID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillageID, 0, 0.5, 0.5);


   // Define and place trade route - goes around the island

   // Create the trade route
   int tradeRouteID = rmCreateTradeRoute();
   int socketID=rmCreateObjectDef("sockets to dock trade posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

   // Define the sockets
   rmAddObjectDefItem(socketID, "SPCDrydock", 1, 5.0);
   rmSetObjectDefAllowOverlap(socketID, true);
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 12.0);
   rmAddObjectDefConstraint(socketID, avoidImpassableLand);
   rmAddObjectDefConstraint(socketID, avoidAll);

   // Set the trade route waypoints
   rmAddTradeRouteWaypoint(tradeRouteID, 0.05, 0.5);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.15, 0.85);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.95);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.8, 0.8);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.95, 0.5);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.85, 0.15);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.05);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.2);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.05, 0.5);
   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route"); 
  
   // Place sockets to trade route
   rmPlaceObjectDefAtLoc(socketID, 0, 0.25, 0.75);
   rmPlaceObjectDefAtLoc(socketID, 0, 0.75, 0.25);
   rmPlaceObjectDefAtLoc(socketID, 0, 0.67, 0.67);
   rmPlaceObjectDefAtLoc(socketID, 0, 0.33, 0.33);


   // Define and place forests
	
   int numTries=10*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(200), rmAreaTilesToFraction(250));
      rmSetAreaForestType(forest, "great plains forest");
      rmSetAreaForestDensity(forest, 0.9);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.5);
      rmSetAreaForestUnderbrush(forest, 0.2);
      rmSetAreaBaseHeight(forest, 1.0);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, avoidTradeSockets);
      rmAddAreaConstraint(forest, cliffConstraint);
      rmAddAreaConstraint(forest, avoidNatives);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.  
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }


   // Text
   rmSetStatusText("",0.50);
   
   
   // Define and place fox herds
   int foxHerdID=rmCreateObjectDef("fox herd");
   rmAddObjectDefItem(foxHerdID, "deer", rmRandInt(7,8), 8.0);
   rmSetObjectDefMinDistance(foxHerdID, 0.0);
   rmSetObjectDefMaxDistance(foxHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(foxHerdID, classAnimals);
   rmAddObjectDefConstraint(foxHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(foxHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(foxHerdID, animalConstraint);
   rmAddObjectDefConstraint(foxHerdID, avoidTradeSockets);
   rmAddObjectDefConstraint(foxHerdID, avoidNatives);
   numTries=2.5*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(foxHerdID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.60);


   // Define and place silver mines

   int silverCount = 2*cNumberNonGaiaPlayers+3;

   for(i=0; < silverCount)
   {
      int silverID=rmCreateObjectDef("silver mine"+i);
      rmAddObjectDefItem(silverID, "Mine", 1, 0.0);
      rmSetObjectDefMinDistance(silverID, 0.0);
      rmSetObjectDefMaxDistance(silverID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(silverID, classMines);
      rmAddObjectDefConstraint(silverID, avoidImpassableLand);
      rmAddObjectDefConstraint(silverID, somePlayerConstraint);
      rmAddObjectDefConstraint(silverID, avoidMines);
      rmAddObjectDefConstraint(silverID, avoidNatives);
      rmAddObjectDefConstraint(silverID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(silverID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.70);
	

   // Define and place fish

   int fishID=rmCreateObjectDef("fish");
   rmAddObjectDefItem(fishID, "ypFishTuna", 1, 4.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(fishID, fishVsFish);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 13*cNumberNonGaiaPlayers);


   // Text
   rmSetStatusText("",0.80);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidAll);
   rmAddObjectDefConstraint(nugget1, avoidTradeSockets);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidNatives);
   rmAddObjectDefConstraint(nugget2, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidNatives);
   rmAddObjectDefConstraint(nugget3, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidNatives);
   rmAddObjectDefConstraint(nugget4, avoidTradeSockets);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Text
   rmSetStatusText("",1.0); 
}