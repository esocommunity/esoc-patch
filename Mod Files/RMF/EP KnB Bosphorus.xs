// RM script of Bosphorus
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Picks the map size
   int playerTiles = 11000;
   if (cNumberNonGaiaPlayers > 4)
      playerTiles = 10000;
   if (cNumberNonGaiaPlayers > 6)
      playerTiles = 8500;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);


   // Set map the map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses the side of naval trade route sockets
   int socketSide = rmRandInt(1,2);


   // Chooses the side teams start
   int teamSide = rmRandInt(1,2);


   // Very VERY complex FFA player placement random integers, only I (AOE_Fan) can really understand these! :P
   int playerPlace3 = rmRandInt(1,3);
   int playerPlace4 = rmRandInt(1,2);
   int playerPlace5 = rmRandInt(1,5);
   int playerPlace6 = rmRandInt(1,3);
   int playerPlace7 = rmRandInt(1,7);
   int playerPlace8 = rmRandInt(1,4);


   // Chooses the side the players start in FFA - this is also complex! :)
   int playerSide = rmRandInt(1,2);


   // If 5 or 7 player FFA game, changes the starting place of the last (5th or 7th) player. Bad explanation, isn't it? :D
   int oddPlayer = rmRandInt(1,2);


   // Picks a default water height
   rmSetSeaLevel(-1.0);


   // Pick lighting
   rmSetLightingSet("texas");


   // Picks default terrain and map type
   rmSetMapElevationParameters(cElevTurbulence, 0.1, 4, 0.4, 5.0);
   rmTerrainInitialize("california\desert6_cal", 0.0);
   rmSetMapType("grass");
   rmSetMapType("land");
   rmSetMapType("water");
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint
   rmSetWorldCircleConstraint(true);


   // Define classes, these are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   int classGrass=rmDefineClass("grass");
   rmDefineClass("starting settlement");
   rmDefineClass("startingUnit");
   rmDefineClass("nuggets");
   rmDefineClass("classForest");


   // Define constraints - used for things to avoid certain things
   
   // Map edge and centre constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // All player things avoidance
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortestPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players shortest", classPlayer, 10.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);

   // All resource avoidance
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 60.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 55.0);
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 35.0);
   int avoidSheep=rmCreateTypeDistanceConstraint("sheep avoids sheep", "sheep", 55.0);

   // Impassable land avoidance
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 8.0);
   int shortAvoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land short", "Land", false, 4.0);

   // Nuggets avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("nuggets avoids nuggets", rmClassID("nuggets"), 40.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 6.0);
   int grassConstraint=rmCreateClassDistanceConstraint("grass vs. grass", classGrass, 60.0);

   // Trade route avoidance
   int avoidTradeRoute=rmCreateTradeRouteDistanceConstraint("trade route", 5.0);
   int avoidTradeSockets=rmCreateTypeDistanceConstraint("avoid trade sockets", "SPCDrydock", 5.0);

   // Water objects avoidance
   int fishVsFish=rmCreateTypeDistanceConstraint("fish vs fish", "ypFishTuna", 20.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish land", "land", true, 6.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid same", "HomeCityWaterSpawnFlag", 25.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag vs land", "land", true, 20.0);


   // Text
   rmSetStatusText("",0.10);


   // Define and place water areas

   // Northern sea (Black Sea)
   int seaID=rmCreateArea("Sea");
   rmSetAreaWaterType(seaID, "new england coast");
   rmSetAreaSize(seaID, 0.15, 0.15);
   rmSetAreaCoherence(seaID, 0.5);
   rmSetAreaLocation(seaID, 0.9, 0.9);
   rmAddAreaInfluenceSegment(seaID, 1.0, 0.6, 0.6, 1.0); 
   rmSetAreaObeyWorldCircleConstraint(seaID, false);
   rmSetAreaBaseHeight(seaID, 0.0);
   rmSetAreaMinBlobs(seaID, 35);
   rmSetAreaMaxBlobs(seaID, 40);
   rmSetAreaMinBlobDistance(seaID, 35);
   rmSetAreaMaxBlobDistance(seaID, 40);
   rmSetAreaSmoothDistance(seaID, 30);
   rmBuildArea(seaID);

   // Southern sea (Sea of Marmara)
   int sea2ID=rmCreateArea("Sea 2");
   rmSetAreaWaterType(sea2ID, "new england coast");
   rmSetAreaSize(sea2ID, 0.15, 0.15);
   rmSetAreaCoherence(sea2ID, 0.5);
   rmSetAreaLocation(sea2ID, 0.1, 0.1);
   rmAddAreaInfluenceSegment(sea2ID, 0.0, 0.4, 0.4, 0.0); 
   rmSetAreaObeyWorldCircleConstraint(sea2ID, false);
   rmSetAreaBaseHeight(sea2ID, 0.0);
   rmSetAreaMinBlobs(sea2ID, 35);
   rmSetAreaMaxBlobs(sea2ID, 40);
   rmSetAreaMinBlobDistance(sea2ID, 35);
   rmSetAreaMaxBlobDistance(sea2ID, 40);
   rmSetAreaSmoothDistance(sea2ID, 30);
   rmBuildArea(sea2ID);

   // Bosphorus strait (connects the both seas)
   int sea3ID=rmCreateArea("Sea 3");
   rmSetAreaWaterType(sea3ID, "new england coast");
   rmSetAreaSize(sea3ID, 0.12, 0.12);
   rmSetAreaCoherence(sea3ID, 0.6);
   rmSetAreaLocation(sea3ID, 0.5, 0.5);
   rmAddAreaInfluenceSegment(sea3ID, 1.0, 0.9, 0.0, 0.1); 
   rmSetAreaObeyWorldCircleConstraint(sea3ID, false);
   rmSetAreaBaseHeight(sea3ID, 0.0);
   rmSetAreaMinBlobs(sea3ID, 1);
   rmSetAreaMaxBlobs(sea3ID, 1);
   rmSetAreaMinBlobDistance(sea3ID, 1);
   rmSetAreaMaxBlobDistance(sea3ID, 1);
   rmSetAreaSmoothDistance(sea3ID, 30);
   rmBuildArea(sea3ID);


   // Text
   rmSetStatusText("",0.25);


   // Define and place some grass batches around the map - also makes the map more hilly!

   int numTries=20;
   for (i=0; <numTries)
   { 
      int grass=rmCreateArea("grass"+i);
      rmSetAreaSize(grass, 0.05, 0.05);
      rmSetAreaForestType(grass, "Great Plains grass");
      rmSetAreaForestDensity(grass, 0.15);
      rmSetAreaForestClumpiness(grass, 0.0);
      rmSetAreaForestUnderbrush(grass, 0.0);
      rmSetAreaBaseHeight(grass, 0.0);
      rmSetAreaMinBlobs(grass, 7);
      rmSetAreaMaxBlobs(grass, 10);
      rmSetAreaMinBlobDistance(grass, 7);
      rmSetAreaMaxBlobDistance(grass, 10);
      rmSetAreaCoherence(grass, 0.4);
      rmAddAreaToClass(grass, classGrass);
      rmAddAreaConstraint(grass, shortAvoidImpassableLand);
      rmAddAreaConstraint(grass, grassConstraint);
      rmAddAreaConstraint(grass, shortestPlayerConstraint);
      rmSetAreaElevationType(grass, cElevTurbulence);
      rmSetAreaElevationVariation(grass, 4.0);
      rmSetAreaElevationMinFrequency(grass, 0.06);
      rmSetAreaElevationOctaves(grass, 4);
      rmSetAreaElevationPersistence(grass, 0.4);
   }


   // Place players - in team and FFA games

   // Player placement if FFA (Free For All) OR two player game - manual placement
   if(cNumberTeams > 2 || cNumberNonGaiaPlayers == 2)
   {
      if (cNumberNonGaiaPlayers == 2) // Place in middle of both "sides"
      {
            if (teamSide == 1)
            {
                        rmPlacePlayer(1, 0.3, 0.7);
                        rmPlacePlayer(2, 0.7, 0.3);
            }
            else if (teamSide == 2)
            {
                        rmPlacePlayer(1, 0.7, 0.3);
                        rmPlacePlayer(2, 0.3, 0.7);
            }
      }
      if (cNumberNonGaiaPlayers == 3) // One player in middle of own "side", two shares their side
      {
            if (playerPlace3 == 1) // Player 2 in middle of own "side"
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.8, 0.4);
                                                rmPlacePlayer(2, 0.3, 0.7);
                                                rmPlacePlayer(3, 0.6, 0.2);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.4, 0.8);
                                                rmPlacePlayer(2, 0.7, 0.3);
                                                rmPlacePlayer(3, 0.2, 0.6);
                        }
            }
            else if (playerPlace3 == 2) // Player 1 in middle of own "side"
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.3, 0.7);
                                                rmPlacePlayer(2, 0.6, 0.2);
                                                rmPlacePlayer(3, 0.8, 0.4);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.7, 0.3);
                                                rmPlacePlayer(2, 0.2, 0.6);
                                                rmPlacePlayer(3, 0.4, 0.8);
                        }
            }
            else if (playerPlace3 == 3) // Player 3 in middle of own "side"
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.6, 0.2);
                                                rmPlacePlayer(2, 0.8, 0.4);
                                                rmPlacePlayer(3, 0.3, 0.7);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.2, 0.6);
                                                rmPlacePlayer(2, 0.4, 0.8);
                                                rmPlacePlayer(3, 0.7, 0.3);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 4) // Normal balanced placement
      {
            if (playerPlace4 == 1)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.6, 0.2);
                                                rmPlacePlayer(2, 0.8, 0.4);
                                                rmPlacePlayer(3, 0.2, 0.6);
                                                rmPlacePlayer(4, 0.4, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.2, 0.6);
                                                rmPlacePlayer(2, 0.4, 0.8);
                                                rmPlacePlayer(3, 0.6, 0.2);
                                                rmPlacePlayer(4, 0.8, 0.4);
                        }
            }
            else if (playerPlace4 == 2)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.8, 0.4);
                                                rmPlacePlayer(2, 0.6, 0.2);
                                                rmPlacePlayer(3, 0.4, 0.8);
                                                rmPlacePlayer(4, 0.2, 0.6);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.4, 0.8);
                                                rmPlacePlayer(2, 0.2, 0.6);
                                                rmPlacePlayer(3, 0.8, 0.4);
                                                rmPlacePlayer(4, 0.6, 0.2);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 5) // Same as 4-player, but one near sockets in middle
      {
            if (playerPlace5 == 1) // Player 1 near sockets
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(1, 0.4, 0.6);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(1, 0.6, 0.4);
                                                rmPlacePlayer(2, 0.6, 0.15);
                                                rmPlacePlayer(3, 0.85, 0.4);
                                                rmPlacePlayer(4, 0.15, 0.6);
                                                rmPlacePlayer(5, 0.4, 0.85);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(1, 0.6, 0.4);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(1, 0.4, 0.6);
                                                rmPlacePlayer(2, 0.15, 0.6);
                                                rmPlacePlayer(3, 0.4, 0.85);
                                                rmPlacePlayer(4, 0.6, 0.15);
                                                rmPlacePlayer(5, 0.85, 0.4);
                        }
            }
            else if (playerPlace5 == 2) // Player 2 near sockets
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.4, 0.85);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(2, 0.4, 0.6);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(2, 0.6, 0.4);
                                                rmPlacePlayer(3, 0.6, 0.15);
                                                rmPlacePlayer(4, 0.85, 0.4);
                                                rmPlacePlayer(5, 0.15, 0.6);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.85, 0.4);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(2, 0.6, 0.4);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(2, 0.4, 0.6);
                                                rmPlacePlayer(3, 0.15, 0.6);
                                                rmPlacePlayer(4, 0.4, 0.85);
                                                rmPlacePlayer(5, 0.6, 0.15);
                        }
            }
            else if (playerPlace5 == 3) // Player 3 near sockets
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.15, 0.6);
                                                rmPlacePlayer(2, 0.4, 0.85);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(3, 0.4, 0.6);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(3, 0.6, 0.4);
                                                rmPlacePlayer(4, 0.6, 0.15);
                                                rmPlacePlayer(5, 0.85, 0.4);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.6, 0.15);
                                                rmPlacePlayer(2, 0.85, 0.4);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(3, 0.6, 0.4);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(3, 0.4, 0.6);
                                                rmPlacePlayer(4, 0.15, 0.6);
                                                rmPlacePlayer(5, 0.4, 0.85);
                        }
            }
            else if (playerPlace5 == 4) // Player 4 near sockets
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.85, 0.4);
                                                rmPlacePlayer(2, 0.15, 0.6);
                                                rmPlacePlayer(3, 0.4, 0.85);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(4, 0.4, 0.6);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(4, 0.6, 0.4);
                                                rmPlacePlayer(5, 0.6, 0.15);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.4, 0.85);
                                                rmPlacePlayer(2, 0.6, 0.15);
                                                rmPlacePlayer(3, 0.85, 0.4);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(4, 0.6, 0.4);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(4, 0.4, 0.6);
                                                rmPlacePlayer(5, 0.15, 0.6);
                        }
            }
            else if (playerPlace5 == 5) // Player 5 near sockets
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.6, 0.15);
                                                rmPlacePlayer(2, 0.85, 0.4);
                                                rmPlacePlayer(3, 0.15, 0.6);
                                                rmPlacePlayer(4, 0.4, 0.85);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(5, 0.4, 0.6);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(5, 0.6, 0.4);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.15, 0.6);
                                                rmPlacePlayer(2, 0.4, 0.85);
                                                rmPlacePlayer(3, 0.6, 0.15);
                                                rmPlacePlayer(4, 0.85, 0.4);
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(5, 0.6, 0.4);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(5, 0.4, 0.6);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 6) // Normal balanced placement
      {
            if (playerPlace6 == 1)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.4, 0.6);
                                                rmPlacePlayer(2, 0.6, 0.4);
                                                rmPlacePlayer(3, 0.6, 0.15);
                                                rmPlacePlayer(4, 0.85, 0.4);
                                                rmPlacePlayer(5, 0.15, 0.6);
                                                rmPlacePlayer(6, 0.4, 0.85);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.6, 0.4);
                                                rmPlacePlayer(2, 0.4, 0.6);
                                                rmPlacePlayer(3, 0.15, 0.6);
                                                rmPlacePlayer(4, 0.4, 0.85);
                                                rmPlacePlayer(5, 0.6, 0.15);
                                                rmPlacePlayer(6, 0.85, 0.4);
                        }
            }
            else if (playerPlace6 == 2)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.15, 0.6);
                                                rmPlacePlayer(2, 0.4, 0.85);
                                                rmPlacePlayer(3, 0.4, 0.6);
                                                rmPlacePlayer(4, 0.6, 0.4);
                                                rmPlacePlayer(5, 0.6, 0.15);
                                                rmPlacePlayer(6, 0.85, 0.4);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.6, 0.15);
                                                rmPlacePlayer(2, 0.85, 0.4);
                                                rmPlacePlayer(3, 0.6, 0.4);
                                                rmPlacePlayer(4, 0.4, 0.6);
                                                rmPlacePlayer(5, 0.15, 0.6);
                                                rmPlacePlayer(6, 0.4, 0.85);
                        }
            }
            else if (playerPlace6 == 3)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.6, 0.15);
                                                rmPlacePlayer(2, 0.85, 0.4);
                                                rmPlacePlayer(3, 0.15, 0.6);
                                                rmPlacePlayer(4, 0.4, 0.85);
                                                rmPlacePlayer(5, 0.4, 0.6);
                                                rmPlacePlayer(6, 0.6, 0.4);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.15, 0.6);
                                                rmPlacePlayer(2, 0.4, 0.85);
                                                rmPlacePlayer(3, 0.6, 0.15);
                                                rmPlacePlayer(4, 0.85, 0.4);
                                                rmPlacePlayer(5, 0.6, 0.4);
                                                rmPlacePlayer(6, 0.4, 0.6);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 7) // 4 players in "X formation" in other side, 3 players in one side like 5-player
      {
            if (playerPlace7 == 1) // Player 1 in either side
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(1, 0.2, 0.8);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(1, 0.8, 0.2);
                                                rmPlacePlayer(2, 0.4, 0.6);
                                                rmPlacePlayer(3, 0.6, 0.4);
                                                rmPlacePlayer(4, 0.6, 0.15);
                                                rmPlacePlayer(5, 0.85, 0.4);
                                                rmPlacePlayer(6, 0.15, 0.6);
                                                rmPlacePlayer(7, 0.4, 0.85);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(1, 0.8, 0.2);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(1, 0.2, 0.8);
                                                rmPlacePlayer(2, 0.6, 0.4);
                                                rmPlacePlayer(3, 0.4, 0.6);
                                                rmPlacePlayer(4, 0.15, 0.6);
                                                rmPlacePlayer(5, 0.4, 0.85);
                                                rmPlacePlayer(6, 0.6, 0.15);
                                                rmPlacePlayer(7, 0.85, 0.4);
                        }
            }
            else if (playerPlace7 == 2) // Player 2 in either side
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(2, 0.2, 0.8);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(2, 0.8, 0.2);
                                                rmPlacePlayer(3, 0.4, 0.6);
                                                rmPlacePlayer(4, 0.6, 0.4);
                                                rmPlacePlayer(5, 0.6, 0.15);
                                                rmPlacePlayer(6, 0.85, 0.4);
                                                rmPlacePlayer(7, 0.15, 0.6);
                                                rmPlacePlayer(1, 0.4, 0.85);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(2, 0.8, 0.2);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(2, 0.2, 0.8);
                                                rmPlacePlayer(3, 0.6, 0.4);
                                                rmPlacePlayer(4, 0.4, 0.6);
                                                rmPlacePlayer(5, 0.15, 0.6);
                                                rmPlacePlayer(6, 0.4, 0.85);
                                                rmPlacePlayer(7, 0.6, 0.15);
                                                rmPlacePlayer(1, 0.85, 0.4);
                        }
            }
            else if (playerPlace7 == 3) // Player 3 in either side
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(3, 0.2, 0.8);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(3, 0.8, 0.2);
                                                rmPlacePlayer(4, 0.4, 0.6);
                                                rmPlacePlayer(5, 0.6, 0.4);
                                                rmPlacePlayer(6, 0.6, 0.15);
                                                rmPlacePlayer(7, 0.85, 0.4);
                                                rmPlacePlayer(1, 0.15, 0.6);
                                                rmPlacePlayer(2, 0.4, 0.85);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(3, 0.8, 0.2);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(3, 0.2, 0.8);
                                                rmPlacePlayer(4, 0.6, 0.4);
                                                rmPlacePlayer(5, 0.4, 0.6);
                                                rmPlacePlayer(6, 0.15, 0.6);
                                                rmPlacePlayer(7, 0.4, 0.85);
                                                rmPlacePlayer(1, 0.6, 0.15);
                                                rmPlacePlayer(2, 0.85, 0.4);
                        }
            }
            else if (playerPlace7 == 4) // Player 4 in either side
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(4, 0.2, 0.8);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(4, 0.8, 0.2);
                                                rmPlacePlayer(5, 0.4, 0.6);
                                                rmPlacePlayer(6, 0.6, 0.4);
                                                rmPlacePlayer(7, 0.6, 0.15);
                                                rmPlacePlayer(1, 0.85, 0.4);
                                                rmPlacePlayer(2, 0.15, 0.6);
                                                rmPlacePlayer(3, 0.4, 0.85);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(4, 0.8, 0.2);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(4, 0.2, 0.8);
                                                rmPlacePlayer(5, 0.6, 0.4);
                                                rmPlacePlayer(6, 0.4, 0.6);
                                                rmPlacePlayer(7, 0.15, 0.6);
                                                rmPlacePlayer(1, 0.4, 0.85);
                                                rmPlacePlayer(2, 0.6, 0.15);
                                                rmPlacePlayer(3, 0.85, 0.4);
                        }
            }
            else if (playerPlace7 == 5) // Player 5 in either side
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(5, 0.2, 0.8);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(5, 0.8, 0.2);
                                                rmPlacePlayer(6, 0.4, 0.6);
                                                rmPlacePlayer(7, 0.6, 0.4);
                                                rmPlacePlayer(1, 0.6, 0.15);
                                                rmPlacePlayer(2, 0.85, 0.4);
                                                rmPlacePlayer(3, 0.15, 0.6);
                                                rmPlacePlayer(4, 0.4, 0.85);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(5, 0.8, 0.2);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(5, 0.2, 0.8);
                                                rmPlacePlayer(6, 0.6, 0.4);
                                                rmPlacePlayer(7, 0.4, 0.6);
                                                rmPlacePlayer(1, 0.15, 0.6);
                                                rmPlacePlayer(2, 0.4, 0.85);
                                                rmPlacePlayer(3, 0.6, 0.15);
                                                rmPlacePlayer(4, 0.85, 0.4);
                        }
            }
            else if (playerPlace7 == 6) // Player 6 in either side
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(6, 0.2, 0.8);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(6, 0.8, 0.2);
                                                rmPlacePlayer(7, 0.4, 0.6);
                                                rmPlacePlayer(1, 0.6, 0.4);
                                                rmPlacePlayer(2, 0.6, 0.15);
                                                rmPlacePlayer(3, 0.85, 0.4);
                                                rmPlacePlayer(4, 0.15, 0.6);
                                                rmPlacePlayer(5, 0.4, 0.85);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(6, 0.8, 0.2);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(6, 0.2, 0.8);
                                                rmPlacePlayer(7, 0.6, 0.4);
                                                rmPlacePlayer(1, 0.4, 0.6);
                                                rmPlacePlayer(2, 0.15, 0.6);
                                                rmPlacePlayer(3, 0.4, 0.85);
                                                rmPlacePlayer(4, 0.6, 0.15);
                                                rmPlacePlayer(5, 0.85, 0.4);
                        }
            }
            else if (playerPlace7 == 7) // Player 7 in either side
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(7, 0.2, 0.8);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(7, 0.8, 0.2);
                                                rmPlacePlayer(1, 0.4, 0.6);
                                                rmPlacePlayer(2, 0.6, 0.4);
                                                rmPlacePlayer(3, 0.6, 0.15);
                                                rmPlacePlayer(4, 0.85, 0.4);
                                                rmPlacePlayer(5, 0.15, 0.6);
                                                rmPlacePlayer(6, 0.4, 0.85);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(7, 0.8, 0.2);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(7, 0.2, 0.8);
                                                rmPlacePlayer(1, 0.6, 0.4);
                                                rmPlacePlayer(2, 0.4, 0.6);
                                                rmPlacePlayer(3, 0.15, 0.6);
                                                rmPlacePlayer(4, 0.4, 0.85);
                                                rmPlacePlayer(5, 0.6, 0.15);
                                                rmPlacePlayer(6, 0.85, 0.4);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 8) // Normal balanced placement
      {
            if (playerPlace8 == 1)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.6, 0.15);
                                                rmPlacePlayer(2, 0.85, 0.4);
                                                rmPlacePlayer(3, 0.15, 0.6);
                                                rmPlacePlayer(4, 0.4, 0.85);
                                                rmPlacePlayer(5, 0.4, 0.6);
                                                rmPlacePlayer(6, 0.6, 0.4);
                                                rmPlacePlayer(7, 0.2, 0.8);
                                                rmPlacePlayer(8, 0.8, 0.2);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.15, 0.6);
                                                rmPlacePlayer(2, 0.4, 0.85);
                                                rmPlacePlayer(3, 0.6, 0.15);
                                                rmPlacePlayer(4, 0.85, 0.4);
                                                rmPlacePlayer(5, 0.6, 0.4);
                                                rmPlacePlayer(6, 0.4, 0.6);
                                                rmPlacePlayer(7, 0.8, 0.2);
                                                rmPlacePlayer(8, 0.2, 0.8);
                        }
            }
            else if (playerPlace8 == 2)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.2, 0.8);
                                                rmPlacePlayer(2, 0.8, 0.2);
                                                rmPlacePlayer(3, 0.6, 0.15);
                                                rmPlacePlayer(4, 0.85, 0.4);
                                                rmPlacePlayer(5, 0.15, 0.6);
                                                rmPlacePlayer(6, 0.4, 0.85);
                                                rmPlacePlayer(7, 0.4, 0.6);
                                                rmPlacePlayer(8, 0.6, 0.4);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.8, 0.2);
                                                rmPlacePlayer(2, 0.2, 0.8);
                                                rmPlacePlayer(3, 0.15, 0.6);
                                                rmPlacePlayer(4, 0.4, 0.85);
                                                rmPlacePlayer(5, 0.6, 0.15);
                                                rmPlacePlayer(6, 0.85, 0.4);
                                                rmPlacePlayer(7, 0.6, 0.4);
                                                rmPlacePlayer(8, 0.4, 0.6);
                        }
            }
            else if (playerPlace8 == 3)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.4, 0.6);
                                                rmPlacePlayer(2, 0.6, 0.4);
                                                rmPlacePlayer(3, 0.2, 0.8);
                                                rmPlacePlayer(4, 0.8, 0.2);
                                                rmPlacePlayer(5, 0.6, 0.15);
                                                rmPlacePlayer(6, 0.85, 0.4);
                                                rmPlacePlayer(7, 0.15, 0.6);
                                                rmPlacePlayer(8, 0.4, 0.85);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.6, 0.4);
                                                rmPlacePlayer(2, 0.4, 0.6);
                                                rmPlacePlayer(3, 0.8, 0.2);
                                                rmPlacePlayer(4, 0.2, 0.8);
                                                rmPlacePlayer(5, 0.15, 0.6);
                                                rmPlacePlayer(6, 0.4, 0.85);
                                                rmPlacePlayer(7, 0.6, 0.15);
                                                rmPlacePlayer(8, 0.85, 0.4);
                        }
            }
            else if (playerPlace8 == 4)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.15, 0.6);
                                                rmPlacePlayer(2, 0.4, 0.85);
                                                rmPlacePlayer(3, 0.4, 0.6);
                                                rmPlacePlayer(4, 0.6, 0.4);
                                                rmPlacePlayer(5, 0.2, 0.8);
                                                rmPlacePlayer(6, 0.8, 0.2);
                                                rmPlacePlayer(7, 0.6, 0.15);
                                                rmPlacePlayer(8, 0.85, 0.4);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.6, 0.15);
                                                rmPlacePlayer(2, 0.85, 0.4);
                                                rmPlacePlayer(3, 0.6, 0.4);
                                                rmPlacePlayer(4, 0.4, 0.6);
                                                rmPlacePlayer(5, 0.8, 0.2);
                                                rmPlacePlayer(6, 0.2, 0.8);
                                                rmPlacePlayer(7, 0.15, 0.6);
                                                rmPlacePlayer(8, 0.4, 0.85);
                        }
            }
      }
   }


   // Player placement if teams - place teams in circle to apart from each other
   if(cNumberTeams == 2 && cNumberNonGaiaPlayers >= 3)
   {
      rmSetPlacementTeam(0);
      if (teamSide == 1)
            rmSetPlacementSection(0.30, 0.45);
      else if (teamSide == 2)
            rmSetPlacementSection(0.80, 0.95);
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.39, 0.39, 0.0);

      rmSetPlacementTeam(1);
      if (teamSide == 1)
            rmSetPlacementSection(0.80, 0.95);
      else if (teamSide == 2)
            rmSetPlacementSection(0.30, 0.45);
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.39, 0.39, 0.0);
   }


   // Text
   rmSetStatusText("",0.30);


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaBaseHeight(id, 1);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand); 
      rmSetAreaCoherence(id, 0.9);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }


   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.40);


   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startTreeID, "TreeGreatPlains", 8, 4.0);
   rmSetObjectDefMinDistance(startTreeID, 18);
   rmSetObjectDefMaxDistance(startTreeID, 25);
   rmAddObjectDefConstraint(startTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startTreeID, shortAvoidStartingUnits);

   int startDeerID=rmCreateObjectDef("starting deer");
   rmAddObjectDefItem(startDeerID, "deer", 6, 4.0);
   rmSetObjectDefCreateHerd(startDeerID, true);
   rmSetObjectDefMinDistance(startDeerID, 18);
   rmSetObjectDefMaxDistance(startDeerID, 25);
   rmAddObjectDefConstraint(startDeerID, avoidStartResource);
   rmAddObjectDefConstraint(startDeerID, avoidImpassableLand);
   rmAddObjectDefConstraint(startDeerID, shortAvoidStartingUnits);

   int startCopperID=rmCreateObjectDef("player copper");
   rmAddObjectDefItem(startCopperID, "MineCopper", 1, 5.0);
   rmSetObjectDefMinDistance(startCopperID, 18);
   rmSetObjectDefMaxDistance(startCopperID, 22);
   rmAddObjectDefConstraint(startCopperID, avoidStartResource);
   rmAddObjectDefConstraint(startCopperID, avoidImpassableLand);
   rmAddObjectDefConstraint(startCopperID, shortAvoidStartingUnits);

   // Place starting objects and resources

   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startCopperID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startDeerID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
      rmAddClosestPointConstraint(flagVsFlag);
      rmAddClosestPointConstraint(flagLand);		
      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Text
   rmSetStatusText("",0.50);


   // Define and place forests

   numTries=20*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(350), rmAreaTilesToFraction(400));
      rmSetAreaForestType(forest, "great plains forest");
      rmSetAreaForestDensity(forest, 0.8);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.5);
      rmSetAreaForestUnderbrush(forest, 0.2);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, avoidTradeRoute);
      rmAddAreaConstraint(forest, avoidTradeSockets);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row 
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }


   // Text
   rmSetStatusText("",0.60);


   // Define and place huntables
   
   // Define and place red fox herds
   int redFoxHerdID=rmCreateObjectDef("red fox herd");
   rmAddObjectDefItem(redFoxHerdID, "elk", rmRandInt(7,8), 8.0);
   rmSetObjectDefMinDistance(redFoxHerdID, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(redFoxHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(redFoxHerdID, classAnimals);
   rmAddObjectDefConstraint(redFoxHerdID, animalConstraint);
   rmAddObjectDefConstraint(redFoxHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(redFoxHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(redFoxHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(redFoxHerdID, avoidTradeSockets);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(redFoxHerdID, 0, 0.5, 0.5);

   // Define and place brown bear herds
   int bearHerdID=rmCreateObjectDef("bear herd");
   rmAddObjectDefItem(bearHerdID, "moose", rmRandInt(7,8), 8.0);
   rmSetObjectDefMinDistance(bearHerdID, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(bearHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(bearHerdID, classAnimals);
   rmAddObjectDefConstraint(bearHerdID, animalConstraint);
   rmAddObjectDefConstraint(bearHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(bearHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(bearHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(bearHerdID, avoidTradeSockets);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(bearHerdID, 0, 0.5, 0.5);

   // Define and place fallow deer herds
   int fallowDeerHerdID=rmCreateObjectDef("fallow deer herd");
   rmAddObjectDefItem(fallowDeerHerdID, "deer", rmRandInt(7,8), 8.0);
   rmSetObjectDefCreateHerd(fallowDeerHerdID, true);
   rmSetObjectDefMinDistance(fallowDeerHerdID, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(fallowDeerHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(fallowDeerHerdID, classAnimals);
   rmAddObjectDefConstraint(fallowDeerHerdID, animalConstraint);
   rmAddObjectDefConstraint(fallowDeerHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(fallowDeerHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(fallowDeerHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(fallowDeerHerdID, avoidTradeSockets);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(fallowDeerHerdID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.70);


   // Define and place copper mines

   int copperCount = 3*cNumberNonGaiaPlayers;

   for(i=0; < copperCount)
   {
      int copperID=rmCreateObjectDef("copper mine"+i);
      rmAddObjectDefItem(copperID, "MineCopper", 1, 0.0);
      rmSetObjectDefMinDistance(copperID, rmXFractionToMeters(0.00));
      rmSetObjectDefMaxDistance(copperID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(copperID, classMines);
      rmAddObjectDefConstraint(copperID, playerConstraint);
      rmAddObjectDefConstraint(copperID, avoidMines);
      rmAddObjectDefConstraint(copperID, avoidImpassableLand);
      rmAddObjectDefConstraint(copperID, avoidTradeRoute);
      rmAddObjectDefConstraint(copperID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(copperID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 23.0);
   rmSetObjectDefMaxDistance(nugget1, 25.0);
   rmAddObjectDefConstraint(nugget1, shortAvoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidAll);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, shortAvoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidAll);
   rmAddObjectDefConstraint(nugget2, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget2, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, shortAvoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidAll);
   rmAddObjectDefConstraint(nugget3, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget3, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, shortAvoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidAll);
   rmAddObjectDefConstraint(nugget4, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget4, avoidTradeSockets);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Text
   rmSetStatusText("",0.90);


   // Define and place sheep herds 

   int sheepID=rmCreateObjectDef("sheep");
   rmAddObjectDefItem(sheepID, "sheep", 2, 4.0);
   rmSetObjectDefMinDistance(sheepID, 0.0);
   rmSetObjectDefMaxDistance(sheepID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(sheepID, avoidSheep);
   rmAddObjectDefConstraint(sheepID, playerConstraint);
   rmAddObjectDefConstraint(sheepID, avoidImpassableLand);
   rmAddObjectDefConstraint(sheepID, avoidTradeRoute);
   rmAddObjectDefConstraint(sheepID, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(sheepID, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);


   // Define and place fish to sea

   int fishID=rmCreateObjectDef("fish");
   rmAddObjectDefItem(fishID, "ypFishTuna", 1, 4.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.5));
   rmAddObjectDefConstraint(fishID, fishVsFish);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 10*cNumberNonGaiaPlayers);


   // Text
   rmSetStatusText("",0.95);


   // Define and place water trade route - splits the map in middle

   // Create the water trade route
   int tradeRouteID = rmCreateTradeRoute();
   int socketID=rmCreateObjectDef("sockets to dock trade posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

   // Define the sockets
   rmAddObjectDefItem(socketID, "SPCDrydock", 1, 5.0);
   rmSetObjectDefAllowOverlap(socketID, true);
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 12.0);
   rmAddObjectDefConstraint(socketID, avoidImpassableLand);
   rmAddObjectDefConstraint(socketID, avoidAll);

   // Set the water trade route waypoints
   rmAddTradeRouteWaypoint(tradeRouteID, 1.0, 0.9);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.5, 0.5, 0, 0);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.0, 0.1, 0, 0);
   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route");

   // Place sockets to trade route
   if (socketSide == 1)
   {
      rmPlaceObjectDefAtLoc(socketID, 0, 0.45, 0.55);
      rmPlaceObjectDefAtLoc(socketID, 0, 0.4, 0.3);
      rmPlaceObjectDefAtLoc(socketID, 0, 0.7, 0.55);
   }
   else
   {
      rmPlaceObjectDefAtLoc(socketID, 0, 0.55, 0.45);
      rmPlaceObjectDefAtLoc(socketID, 0, 0.3, 0.4);
      rmPlaceObjectDefAtLoc(socketID, 0, 0.55, 0.7);
   }


   // Text
   rmSetStatusText("",1.0);
}  
