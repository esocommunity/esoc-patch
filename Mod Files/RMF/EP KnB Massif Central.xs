// RM script of Massif Central
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

   // Set up void to shorten creation of hills

   int number=0;
   void Hill(float Size=0.0, float Height=0.0, string Terrain="", float Coherence=0.0)
   {
      int areaID=rmCreateArea("area"+number);
      rmSetAreaLocation(areaID, 0.5, 0.5);
      rmSetAreaSize(areaID, Size, Size);
      rmSetAreaBaseHeight(areaID, Height);
      rmSetAreaMix(areaID, Terrain);
      rmSetAreaCoherence(areaID, Coherence);
      rmSetAreaMinBlobs(areaID, 10);
      rmSetAreaMaxBlobs(areaID, 20);
      rmSetAreaHeightBlend(areaID, 2);
      rmBuildArea(areaID);
      number=number+1;
   }

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Chooses the 3 natives, always Franks

   // Native 1
   rmSetSubCiv(0, "Comanche");

   // Native 2
   rmSetSubCiv(1, "Comanche");

   // Native 3
   rmSetSubCiv(2, "Comanche");


   // Picks the map size
   int playerTiles = 20000;
   if (cNumberNonGaiaPlayers == 8)
	playerTiles = 14000;
   else if (cNumberNonGaiaPlayers == 7)
	playerTiles = 15000;
   else if (cNumberNonGaiaPlayers == 6)
	playerTiles = 16000;
   else if (cNumberNonGaiaPlayers == 5)
	playerTiles = 17000;
   else if (cNumberNonGaiaPlayers == 4)
	playerTiles = 18000;
   else if (cNumberNonGaiaPlayers == 3)
	playerTiles = 19000;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses the side where teams start in team games
   int teamSide = rmRandInt(1,4);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("rockies");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.10, 4, 0.4, 7.0);
   rmSetBaseTerrainMix("great plains grass");
   rmTerrainInitialize("patagonia\ground_grass1_pat", 1.0);
   rmSetMapType("grass");
   rmSetMapType("land");
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   int classLake=rmDefineClass("lake");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("nuggets");
   rmDefineClass("natives");
   rmDefineClass("classTreeStraggler");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));
   int centreConstraint=rmCreatePieConstraint("centre Constraint", 0.5, 0.5, rmZFractionToMeters(0.15), rmZFractionToMeters(0.30), rmDegreesToRadians(0), rmDegreesToRadians(360));
   int edgeConstraint=rmCreatePieConstraint("edge Constraint", 0.5, 0.5, rmZFractionToMeters(0.30), rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 45.0);
   int shortForestConstraint=rmCreateClassDistanceConstraint("forest vs. forest short", rmClassID("classForest"), 20.0);
   int shortestForestConstraint=rmCreateClassDistanceConstraint("forest vs. forest shortest", rmClassID("classForest"), 10.0);
   int stragglerTreeConstraint=rmCreateClassDistanceConstraint("straggler tree constraint", rmClassID("classTreeStraggler"), 20.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 60.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 70.0);
   int avoidGoat=rmCreateTypeDistanceConstraint("herds avoids goat", "sheep", 50.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 5.0);

   // Native avoidance
   int avoidNatives=rmCreateClassDistanceConstraint("things avoids natives", rmClassID("natives"), 5.0);
   int nativesAvoidNatives=rmCreateClassDistanceConstraint("natives avoids natives", rmClassID("natives"), 100.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 40.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);

   // Trade route avoidance
   int avoidTradeRoute=rmCreateTradeRouteDistanceConstraint("trade route", 5.0);
   int avoidTradeSockets=rmCreateTypeDistanceConstraint("avoid trade sockets", "SocketTradeRoute", 5.0);


   // Text
   rmSetStatusText("",0.10);


   // Define and place hills in middle

   Hill(0.50, 1.0, "great plains grass", 0.5);
   Hill(0.48, 1.5, "great plains grass", 0.5);
   Hill(0.46, 2.0, "great plains grass", 0.5);
   Hill(0.44, 2.5, "great plains grass", 0.5);
   Hill(0.42, 3.0, "great plains grass", 0.5);
   Hill(0.40, 3.5, "great plains grass", 0.5);
   Hill(0.38, 4.0, "great plains grass", 0.5);
   Hill(0.36, 4.5, "great plains grass", 0.5);
   Hill(0.34, 5.0, "great plains grass", 0.5);
   Hill(0.32, 5.5, "great plains grass", 0.5);
   Hill(0.30, 6.0, "great plains drygrass", 0.5);
   Hill(0.28, 6.5, "great plains drygrass", 0.5);
   Hill(0.26, 7.0, "great plains drygrass", 0.5);
   Hill(0.24, 7.5, "great plains drygrass", 0.5);
   Hill(0.22, 8.0, "great plains drygrass", 0.5);
   Hill(0.20, 8.5, "great plains drygrass", 0.5);
   Hill(0.18, 9.0, "great plains drygrass", 0.5);
   Hill(0.16, 9.5, "great plains drygrass", 0.5);
   Hill(0.14, 10.0, "greatlakes_snow", 0.5);
   Hill(0.12, 10.5, "greatlakes_snow", 0.5);
   Hill(0.10, 11.0, "greatlakes_snow", 0.5);
   Hill(0.08, 11.5, "greatlakes_snow", 0.5);
   Hill(0.06, 12.0, "greatlakes_snow", 0.5);
   Hill(0.04, 12.5, "greatlakes_snow", 0.5);
   Hill(0.02, 13.0, "greatlakes_snow", 0.5);


   // Place players - in team and FFA games

   // Player placement if FFA (Free For All) - place players in circle
   if(cNumberTeams > 2)
   {
      rmSetTeamSpacingModifier(0.7);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);
   }

   // Player placement if teams - place teams in circle to apart from each other
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      if (rmGetNumberPlayersOnTeam(0) == 2)
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.05, 0.20);
         else if (teamSide == 2)
            rmSetPlacementSection(0.30, 0.45);
         else if (teamSide == 3)
            rmSetPlacementSection(0.55, 0.70);
         else if (teamSide == 4)
            rmSetPlacementSection(0.80, 0.95);
      }
      else
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.00, 0.25);
         else if (teamSide == 2)
            rmSetPlacementSection(0.25, 0.50);
         else if (teamSide == 3)
            rmSetPlacementSection(0.50, 0.75);
         else if (teamSide == 4)
            rmSetPlacementSection(0.75, 1.00);
      }
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);

      rmSetPlacementTeam(1);
      if (rmGetNumberPlayersOnTeam(1) == 2)
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.55, 0.70);
         else if (teamSide == 2)
            rmSetPlacementSection(0.80, 0.95);
         else if (teamSide == 3)
            rmSetPlacementSection(0.05, 0.20);
         else if (teamSide == 4)
            rmSetPlacementSection(0.30, 0.45);
      }
      else
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.50, 0.75);
         else if (teamSide == 2)
            rmSetPlacementSection(0.75, 1.00);
         else if (teamSide == 3)
            rmSetPlacementSection(0.00, 0.25);
         else if (teamSide == 4)
            rmSetPlacementSection(0.25, 0.50);
      }
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.50);


   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "TreeGreatPlains", 6, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 12);
   rmSetObjectDefMaxDistance(startAreaTreeID, 16);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startDeerID=rmCreateObjectDef("starting deer");
   rmAddObjectDefItem(startDeerID, "deer", 5, 4.0);
   rmSetObjectDefCreateHerd(startDeerID, true);
   rmSetObjectDefMinDistance(startDeerID, 14);
   rmSetObjectDefMaxDistance(startDeerID, 18);
   rmAddObjectDefConstraint(startDeerID, avoidStartResource);
   rmAddObjectDefConstraint(startDeerID, avoidImpassableLand);
   rmAddObjectDefConstraint(startDeerID, shortAvoidStartingUnits);

   int startCoalID=rmCreateObjectDef("player coal");
   rmAddObjectDefItem(startCoalID, "mine", 1, 5.0);
   rmSetObjectDefMinDistance(startCoalID, 14);
   rmSetObjectDefMaxDistance(startCoalID, 18);
   rmAddObjectDefConstraint(startCoalID, avoidStartResource);
   rmAddObjectDefConstraint(startCoalID, avoidImpassableLand);
   rmAddObjectDefConstraint(startCoalID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startDeerID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startCoalID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
   }


   // Text
   rmSetStatusText("",0.70);


   // Place the 3 natives, always Franks

   // Native 1
   int nativeVillageID = -1;
   int nativeVillageType = rmRandInt(1,5);
   nativeVillageID = rmCreateGrouping("franks village 1", "native comanche village "+nativeVillageType);
   rmSetGroupingMinDistance(nativeVillageID, 0.0);
   rmSetGroupingMaxDistance(nativeVillageID, rmXFractionToMeters(0.47));
   rmAddGroupingConstraint(nativeVillageID, veryLongPlayerConstraint);
   rmAddGroupingConstraint(nativeVillageID, nativesAvoidNatives);
   rmAddGroupingToClass(nativeVillageID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillageID, 0, 0.5, 0.5);

   // Native 2
   int nativeVillage2ID = -1;
   int nativeVillage2Type = rmRandInt(1,5);
   nativeVillage2ID = rmCreateGrouping("franks village 2", "native comanche village "+nativeVillage2Type);
   rmSetGroupingMinDistance(nativeVillage2ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage2ID, rmXFractionToMeters(0.47));
   rmAddGroupingConstraint(nativeVillage2ID, veryLongPlayerConstraint);
   rmAddGroupingConstraint(nativeVillage2ID, nativesAvoidNatives);
   rmAddGroupingToClass(nativeVillage2ID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillage2ID, 0, 0.5, 0.5);

   // Native 3
   int nativeVillage3ID = -1;
   int nativeVillage3Type = rmRandInt(1,5);
   nativeVillage3ID = rmCreateGrouping("franks village 3", "native comanche village "+nativeVillage3Type);
   rmSetGroupingMinDistance(nativeVillage3ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage3ID, rmXFractionToMeters(0.47));
   rmAddGroupingConstraint(nativeVillage3ID, veryLongPlayerConstraint);
   rmAddGroupingConstraint(nativeVillage3ID, nativesAvoidNatives);
   rmAddGroupingToClass(nativeVillage3ID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillage3ID, 0, 0.5, 0.5);


   // Define and place trade route - a big circle around the edge

   // Create the trade route
   int tradeRouteID = rmCreateTradeRoute();
   int socketID=rmCreateObjectDef("sockets to dock trade posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

   // Define the sockets
   rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socketID, true);
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 12.0);

   // Set the trade route waypoints
   rmAddTradeRouteWaypoint(tradeRouteID, 0.01, 0.51);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.06, 0.6);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.10, 0.68);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.18, 0.82);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.32, 0.90);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.95);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.68, 0.90);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.82, 0.82);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.9, 0.7);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.95, 0.5);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.9, 0.3);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.82, 0.18);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.68, 0.10);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.05);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.32, 0.10);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.17, 0.17);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.10, 0.32);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.06, 0.4);
   rmAddTradeRouteWaypoint(tradeRouteID, 0.01, 0.49);
   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route");
  
   // Place sockets to trade route
   vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.07);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.17);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.38);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.58);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.68);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.88);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);


   // Define and place forests

   // Define and place high forests - spruce, fir and pine
   int numTries=20*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest high"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(700), rmAreaTilesToFraction(800));
      rmSetAreaForestType(forest, "himalayas forest");
      rmSetAreaForestDensity(forest, 0.8);
      rmSetAreaForestClumpiness(forest, 0.5);
      rmSetAreaForestUnderbrush(forest, 0.0);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaCoherence(forest, 0.5);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, shortForestConstraint);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, avoidNatives);
      rmAddAreaConstraint(forest, centreConstraint);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 10 times in a row.  
            failCount++;
            if(failCount == 10)
                        break;
      }
      else
                        failCount=0; 
   }

   // Define and place low forests - birch, beech and oak
   numTries=20*cNumberNonGaiaPlayers;
   failCount=0;
   for (i=0; <numTries)
   {   
      int forest2=rmCreateArea("forest low"+i);
      rmSetAreaWarnFailure(forest2, false);
      rmSetAreaSize(forest2, rmAreaTilesToFraction(400), rmAreaTilesToFraction(500));
      rmSetAreaForestType(forest2, "great plains forest");
      rmSetAreaForestDensity(forest2, 0.8);
      rmSetAreaForestClumpiness(forest2, 0.5);
      rmSetAreaForestUnderbrush(forest2, 0.0);
      rmAddAreaToClass(forest2, rmClassID("classForest"));
      rmSetAreaCoherence(forest2, 0.5);
      rmAddAreaConstraint(forest2, somePlayerConstraint);
      rmAddAreaConstraint(forest2, forestConstraint);
      rmAddAreaConstraint(forest2, avoidImpassableLand);
      rmAddAreaConstraint(forest2, avoidNatives);
      rmAddAreaConstraint(forest2, edgeConstraint);
      rmAddAreaConstraint(forest2, avoidTradeRoute);
      rmAddAreaConstraint(forest2, avoidTradeSockets);
      if(rmBuildArea(forest2)==false)
      {
            // Stop trying once we fail 10 times in a row.  
            failCount++;
            if(failCount == 10)
                        break;
      }
      else
                        failCount=0; 
   }


   // Define and place straggler trees
   for (i=0; < 70)
   { 
      int stragglerTreeID=rmCreateObjectDef("straggler tree massif low"+i);
      rmAddObjectDefItem(stragglerTreeID, "TreeGreatPlains", rmRandInt(1,5), 10.0);
      rmSetObjectDefMinDistance(stragglerTreeID, rmZFractionToMeters(0.25));
      rmSetObjectDefMaxDistance(stragglerTreeID, rmZFractionToMeters(0.47));
      rmAddObjectDefToClass(stragglerTreeID, rmClassID("classForest"));
      rmAddObjectDefToClass(stragglerTreeID, rmClassID("classTreeStraggler"));
      rmAddObjectDefConstraint(stragglerTreeID, somePlayerConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, avoidImpassableLand);
      rmAddObjectDefConstraint(stragglerTreeID, shortestForestConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, stragglerTreeConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, avoidNatives);
      rmAddObjectDefConstraint(stragglerTreeID, avoidTradeRoute);
      rmAddObjectDefConstraint(stragglerTreeID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(stragglerTreeID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place animals
   
   // Define and place deer herds
   int deerHerdID=rmCreateObjectDef("deer herd");
   rmAddObjectDefItem(deerHerdID, "deer", rmRandInt(7,8), 8.0);
   rmSetObjectDefCreateHerd(deerHerdID, true);
   rmSetObjectDefMinDistance(deerHerdID, rmXFractionToMeters(0.10));
   rmSetObjectDefMaxDistance(deerHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(deerHerdID, classAnimals);
   rmAddObjectDefConstraint(deerHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(deerHerdID, animalConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidNatives);
   rmAddObjectDefConstraint(deerHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(deerHerdID, avoidTradeSockets);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(deerHerdID, 0, 0.5, 0.5);

   // Define and place wolf herds
   int wolfHerdID=rmCreateObjectDef("wolf herd");
   rmAddObjectDefItem(wolfHerdID, "moose", rmRandInt(5,6), 8.0);
   rmSetObjectDefMinDistance(wolfHerdID, rmXFractionToMeters(0.25));
   rmSetObjectDefMaxDistance(wolfHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(wolfHerdID, classAnimals);
   rmAddObjectDefConstraint(wolfHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(wolfHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(wolfHerdID, animalConstraint);
   rmAddObjectDefConstraint(wolfHerdID, avoidNatives);
   rmAddObjectDefConstraint(wolfHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(wolfHerdID, avoidTradeSockets);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(wolfHerdID, 0, 0.5, 0.5);


   // Define and place coal mines

   int coalCount = 3*cNumberNonGaiaPlayers+2;

   for(i=0; < coalCount)
   {
      int coalID=rmCreateObjectDef("coal mine"+i);
      rmAddObjectDefItem(coalID, "mine", 1, 0.0);
      rmSetObjectDefMinDistance(coalID, 0.0);
      rmSetObjectDefMaxDistance(coalID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(coalID, classMines);
      rmAddObjectDefConstraint(coalID, avoidImpassableLand);
      rmAddObjectDefConstraint(coalID, playerConstraint);
      rmAddObjectDefConstraint(coalID, avoidMines);
      rmAddObjectDefConstraint(coalID, avoidNatives);
      rmAddObjectDefConstraint(coalID, avoidTradeRoute);
      rmAddObjectDefConstraint(coalID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(coalID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.90);


   // Text
   rmSetStatusText("",0.99);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidNatives);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidNatives);
   rmAddObjectDefConstraint(nugget2, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget2, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidNatives);
   rmAddObjectDefConstraint(nugget3, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget3, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidNatives);
   rmAddObjectDefConstraint(nugget4, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget4, avoidTradeSockets);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Define and place goat herds 

   int goatID=rmCreateObjectDef("goat");
   rmAddObjectDefItem(goatID, "sheep", 3, 4.0);
   rmSetObjectDefMinDistance(goatID, 0.0);
   rmSetObjectDefMaxDistance(goatID, rmXFractionToMeters(0.10));
   rmAddObjectDefConstraint(goatID, avoidGoat);
   rmAddObjectDefConstraint(goatID, playerConstraint);
   rmAddObjectDefConstraint(goatID, avoidImpassableLand);
   rmPlaceObjectDefAtLoc(goatID, 0, 0.5, 0.5, cNumberNonGaiaPlayers);


   // Place King's Hill if KOTH game to middle

   if(rmGetIsKOTH()) 
   {
      float xLoc = 0.5;
      float yLoc = 0.5;
      float walk = 0.05;

      ypKingsHillPlacer(xLoc, yLoc, walk, 0);
   }
   

   // Text
   rmSetStatusText("",1.0); 
}  
