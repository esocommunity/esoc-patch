// RM script of Rhodopes
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Chooses the 2 natives, always Avars

   // Native 1
   rmSetSubCiv(0, "Comanche");

   // Native 2
   rmSetSubCiv(1, "Comanche");


   // Picks the map size
   int playerTiles = 20000;
   if (cNumberNonGaiaPlayers == 8)
	playerTiles = 14000;
   else if (cNumberNonGaiaPlayers == 7)
	playerTiles = 15000;
   else if (cNumberNonGaiaPlayers == 6)
	playerTiles = 16000;
   else if (cNumberNonGaiaPlayers == 5)
	playerTiles = 17000;
   else if (cNumberNonGaiaPlayers == 4)
	playerTiles = 18000;
   else if (cNumberNonGaiaPlayers == 3)
	playerTiles = 19000;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses the side where teams start in team games
   int teamSide = rmRandInt(1,2);


   // Picks a default water height
   rmSetSeaLevel(-5.0);


   // Lighting
   rmSetLightingSet("Himalayas");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.08, 4, 0.5, 10.0);
   rmTerrainInitialize("himalayas\ground_dirt5_himal", 1.0);
   rmSetMapType("grass");
   rmSetMapType("land");
   rmSetMapType("greatPlains");
   

   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("nuggets");
   rmDefineClass("natives");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int forestPlayerConstraint=rmCreateClassDistanceConstraint("forests stay away from players", classPlayer, 38.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 15.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 70.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 65.0);
   int avoidSheep=rmCreateTypeDistanceConstraint("sheep avoids sheep", "sheep", 70.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 5.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 40.0);

   // Native avoidance
   int avoidNatives=rmCreateClassDistanceConstraint("things avoids natives", rmClassID("natives"), 7.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);


   // Text
   rmSetStatusText("",0.10);


   // Define and place water areas

   int lakeID=rmCreateArea("lake");
   rmSetAreaWaterType(lakeID, "great lakes");
   rmSetAreaSize(lakeID, 0.04, 0.04);
   rmSetAreaLocation(lakeID, 0.5, 0.5);
   rmAddAreaInfluenceSegment(lakeID, 0.45, 0.45, 0.55, 0.55);
   rmSetAreaBaseHeight(lakeID, -5.0);
   rmSetAreaCoherence(lakeID, 0.7);
   rmSetAreaSmoothDistance(lakeID, 20);
   rmBuildArea(lakeID);

   int lake2ID=rmCreateArea("lake 2");
   rmSetAreaWaterType(lake2ID, "great lakes");
   rmSetAreaSize(lake2ID, 0.035, 0.035);
   rmSetAreaLocation(lake2ID, 0.5, 0.5);
   rmAddAreaInfluenceSegment(lake2ID, 0.4, 0.4, 0.6, 0.6);
   rmSetAreaBaseHeight(lake2ID, -5.0);
   rmSetAreaCoherence(lake2ID, 0.7);
   rmSetAreaSmoothDistance(lake2ID, 20);
   rmBuildArea(lake2ID);

   int lake3ID=rmCreateArea("lake 3");
   rmSetAreaWaterType(lake3ID, "great lakes");
   rmSetAreaSize(lake3ID, 0.03, 0.03);
   rmSetAreaLocation(lake3ID, 0.5, 0.5);
   rmAddAreaInfluenceSegment(lake3ID, 0.35, 0.35, 0.65, 0.65);
   rmSetAreaBaseHeight(lake3ID, -5.0);
   rmSetAreaCoherence(lake3ID, 0.7);
   rmSetAreaSmoothDistance(lake3ID, 20);
   rmBuildArea(lake3ID);

   int lake4ID=rmCreateArea("lake 4");
   rmSetAreaWaterType(lake4ID, "great lakes");
   rmSetAreaSize(lake4ID, 0.025, 0.025);
   rmSetAreaLocation(lake4ID, 0.5, 0.5);
   rmAddAreaInfluenceSegment(lake4ID, 0.3, 0.3, 0.7, 0.7);
   rmSetAreaBaseHeight(lake4ID, -5.0);
   rmSetAreaCoherence(lake4ID, 0.7);
   rmSetAreaSmoothDistance(lake4ID, 20);
   rmBuildArea(lake4ID);

   int lake5ID=rmCreateArea("lake 5");
   rmSetAreaWaterType(lake5ID, "great lakes");
   rmSetAreaSize(lake5ID, 0.02, 0.02);
   rmSetAreaLocation(lake5ID, 0.5, 0.5);
   rmAddAreaInfluenceSegment(lake5ID, 0.25, 0.25, 0.75, 0.75);
   rmSetAreaBaseHeight(lake5ID, -5.0);
   rmSetAreaCoherence(lake5ID, 0.7);
   rmSetAreaSmoothDistance(lake5ID, 20);
   rmBuildArea(lake5ID);


   // Text
   rmSetStatusText("",0.20);


   // Place players - in team and FFA games

   // Player placement if FFA (Free For All) - place players in circle
   if(cNumberTeams > 2 || cNumberNonGaiaPlayers == 2)
   {
      if (cNumberNonGaiaPlayers == 2)
            rmSetPlacementSection(0.38, 0.88);
      if (cNumberNonGaiaPlayers == 3 || cNumberNonGaiaPlayers == 4 || cNumberNonGaiaPlayers == 5)
            rmSetPlacementSection(0.0, 0.75);
      if (cNumberNonGaiaPlayers == 6 || cNumberNonGaiaPlayers == 7)
            rmSetPlacementSection(0.38, 0.37);
      if (cNumberNonGaiaPlayers == 8)
            rmSetPlacementSection(0.06, 0.045);
      rmPlacePlayersCircular(0.38, 0.38, 0.0);
   }

   // Player placement if teams - place teams in circle to east or west
   if(cNumberTeams == 2 && cNumberNonGaiaPlayers >= 3)
   {
      rmSetPlacementTeam(0);
      if (rmGetNumberPlayersOnTeam(0) == 2)
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.80, 0.95);
         else if (teamSide == 2)
            rmSetPlacementSection(0.30, 0.45);
      }
      else
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.75, 1.0);
         else if (teamSide == 2)
            rmSetPlacementSection(0.25, 0.50);
      }
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.38, 0.38, 0.0);

      rmSetPlacementTeam(1);
      if (rmGetNumberPlayersOnTeam(1) == 2)
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.30, 0.45);
         else if (teamSide == 2)
            rmSetPlacementSection(0.80, 0.95);
      }
      else
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.25, 0.50);
         else if (teamSide == 2)
            rmSetPlacementSection(0.75, 1.0);
      }
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.38, 0.38, 0.0);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(1000);

   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmSetAreaBaseHeight(id, 7.0);
      rmSetAreaCoherence(id, 0.5);
      rmSetAreaHeightBlend(id, 2);
      rmSetAreaLocPlayer(id, i);
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.30);
		

   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "ypTreeHimalayas", 6, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 12);
   rmSetObjectDefMaxDistance(startAreaTreeID, 16);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startBearID=rmCreateObjectDef("starting bear");
   rmAddObjectDefItem(startBearID, "deer", 6, 4.0);
   rmSetObjectDefCreateHerd(startBearID, true);
   rmSetObjectDefMinDistance(startBearID, 14);
   rmSetObjectDefMaxDistance(startBearID, 18);
   rmAddObjectDefConstraint(startBearID, avoidStartResource);
   rmAddObjectDefConstraint(startBearID, avoidImpassableLand);
   rmAddObjectDefConstraint(startBearID, shortAvoidStartingUnits);

   int startCoalID=rmCreateObjectDef("player coal");
   rmAddObjectDefItem(startCoalID, "mine", 1, 5.0);
   rmSetObjectDefMinDistance(startCoalID, 14);
   rmSetObjectDefMaxDistance(startCoalID, 18);
   rmAddObjectDefConstraint(startCoalID, avoidStartResource);
   rmAddObjectDefConstraint(startCoalID, avoidImpassableLand);
   rmAddObjectDefConstraint(startCoalID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startBearID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startCoalID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
   }


   // Text
   rmSetStatusText("",0.40);


   // Define and place the 2 natives, always Avars

   // Native 1, to western side
   int nativeVillageType = rmRandInt(1,5);
   int nativeVillageID = rmCreateGrouping("avars village", "native comanche village "+nativeVillageType);
   rmSetGroupingMinDistance(nativeVillageID, 0.0);
   rmSetGroupingMaxDistance(nativeVillageID, 10.0);
   rmAddGroupingToClass(nativeVillageID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillageID, 0, 0.4, 0.6);

   // Native 2, to eastern side
   int nativeVillage2Type = rmRandInt(1,5);
   int nativeVillage2ID = rmCreateGrouping("avars village 2", "native comanche village "+nativeVillage2Type);
   rmSetGroupingMinDistance(nativeVillage2ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage2ID, 10.0);
   rmAddGroupingToClass(nativeVillage2ID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillage2ID, 0, 0.6, 0.4);


   // Text
   rmSetStatusText("",0.50);


   // Define and place forests
	
   int numTries=500*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(1200), rmAreaTilesToFraction(1300));
      rmSetAreaForestType(forest, "Himalayas Forest");
      rmSetAreaForestDensity(forest, 0.9);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.4);
      rmSetAreaForestUnderbrush(forest, 0.0);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, forestPlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, avoidNatives);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 10 times in a row.  
            failCount++;
            if(failCount==10)
                        break;
      }
      else
                        failCount=0; 
   }


   // Text
   rmSetStatusText("",0.60);


   // Define and place animals
   
   // Define and place brown bear herds
   int bearHerdID=rmCreateObjectDef("bear herd");
   rmAddObjectDefItem(bearHerdID, "deer", rmRandInt(7,8), 8.0);
   rmSetObjectDefMinDistance(bearHerdID, 0.0);
   rmSetObjectDefMaxDistance(bearHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(bearHerdID, classAnimals);
   rmAddObjectDefConstraint(bearHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(bearHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(bearHerdID, animalConstraint);
   rmAddObjectDefConstraint(bearHerdID, avoidNatives);
   numTries=cNumberNonGaiaPlayers+3;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(bearHerdID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.70);


   // Define and place coal mines

   int coalCount = 2*cNumberNonGaiaPlayers+2;

   for(i=0; < coalCount)
   {
      int coalID=rmCreateObjectDef("coal mine"+i);
      rmAddObjectDefItem(coalID, "mine", 1, 0.0);
      rmSetObjectDefMinDistance(coalID, 0.0);
      rmSetObjectDefMaxDistance(coalID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(coalID, classMines);
      rmAddObjectDefConstraint(coalID, avoidImpassableLand);
      rmAddObjectDefConstraint(coalID, playerConstraint);
      rmAddObjectDefConstraint(coalID, avoidMines);
      rmAddObjectDefConstraint(coalID, avoidNatives);
      rmPlaceObjectDefAtLoc(coalID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 15.0);
   rmSetObjectDefMaxDistance(nugget1, 20.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidNatives);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Define and place sheep herds

   int sheepID=rmCreateObjectDef("sheep");
   rmAddObjectDefItem(sheepID, "sheep", 3, 5.0);
   rmSetObjectDefMinDistance(sheepID, 0.0);
   rmSetObjectDefMaxDistance(sheepID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(sheepID, avoidSheep);
   rmAddObjectDefConstraint(sheepID, playerConstraint);
   rmAddObjectDefConstraint(sheepID, avoidImpassableLand);
   rmAddObjectDefConstraint(sheepID, avoidNatives);
   rmPlaceObjectDefAtLoc(sheepID, 0, 0.5, 0.5, 4*cNumberNonGaiaPlayers);


   // Text
   rmSetStatusText("",0.90);


   // Create a trigger that disables dock

   for(i=1; <cNumberPlayers)
   {
      rmCreateTrigger("DisableDock"+i);
      rmSwitchToTrigger(rmTriggerID("DisableDock"+i));
      rmAddTriggerCondition("Always");
      rmAddTriggerEffect("Forbid and Disable Unit");
      rmSetTriggerEffectParamInt("PlayerID", i);
      rmSetTriggerEffectParam("ProtoUnit", "dock");
      rmSetTriggerActive(true);
      rmSetTriggerRunImmediately(true);
   }


   // Text
   rmSetStatusText("",1.0); 
}  
