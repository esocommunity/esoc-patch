// RM script of Crete
// For K&B mod
// By AOE_Fan

// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);

   // Picks the map size
   int playerTiles = 11000;
   if (cNumberNonGaiaPlayers > 4)
      playerTiles = 10000;
   if (cNumberNonGaiaPlayers > 6)
      playerTiles = 8500;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);
	

   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("pampas");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.1, 4, 0.4, 6.0);
   rmSetBaseTerrainMix("great plains drygrass");
   rmTerrainInitialize("california\ground8_cal", 1.0);
   rmSetMapType("grass");
   rmSetMapType("water");
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);


   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classMines=rmDefineClass("mines");
   int classGrass=rmDefineClass("grass");
   int classAnimals=rmDefineClass("animals");
   int classCliff=rmDefineClass("cliff");
   rmDefineClass("starting settlement");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("classTreeStraggler");
   rmDefineClass("nuggets");


   // Define constraints
   
   // Map edge avoidance
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player avoidance
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 40.0);
   int shortForestConstraint=rmCreateClassDistanceConstraint("forest vs. forest short", rmClassID("classForest"), 10.0);
   int stragglerTreeConstraint=rmCreateClassDistanceConstraint("straggler tree constraint", rmClassID("classTreeStraggler"), 20.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 60.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 55.0);
   int avoidSheep=rmCreateTypeDistanceConstraint("sheep avoids sheep", "sheep", 55.0);

   // Impassable land avoidance
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 6.0);

   // Cliff avoidance
   int cliffConstraint=rmCreateClassDistanceConstraint("avoid cliff", classCliff, 10.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 40.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 6.0);
   int grassConstraint=rmCreateClassDistanceConstraint("grass vs. grass", classGrass, 60.0);

   // Trade route avoidance
   int avoidTradeRoute=rmCreateTradeRouteDistanceConstraint("trade route", 5.0);
   int avoidTradeSockets=rmCreateTypeDistanceConstraint("avoid trade sockets", "SocketTradeRoute", 5.0);

   // Water objects avoidance	
   int fishVsFish=rmCreateTypeDistanceConstraint("fish vs fish", "ypFishTuna", 20.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish land", "land", true, 6.0);
   int whaleVsWhale=rmCreateTypeDistanceConstraint("whale vs whale", "HumpbackWhale", 25.0);
   int whaleLand = rmCreateTerrainDistanceConstraint("whale land", "land", true, 10.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid same", "HomeCityWaterSpawnFlag", 25);
   int flagLand = rmCreateTerrainDistanceConstraint("flag vs land", "land", true, 15.0);


   // Text
   rmSetStatusText("",0.10);


   // Define and place a thin line of cliffs, seperating the map a little

   if (rmGetIsFFA() == false)
   {
      int cliffID=rmCreateArea("cliff");
      rmSetAreaSize(cliffID, 0.02, 0.02);
      rmSetAreaWarnFailure(cliffID, false);
      rmSetAreaSmoothDistance(cliffID, 10);
      rmSetAreaCoherence(cliffID, 1.0);
      rmSetAreaLocation(cliffID, 0.15, 0.15);
      rmAddAreaInfluenceSegment(cliffID, 0.0, 0.0, 0.38, 0.38);
      rmSetAreaCliffType(cliffID, "New England Inland Grass");
      rmSetAreaCliffEdge(cliffID, 1, 1.0, 0.0, 0.0, 0);
      rmSetAreaCliffHeight(cliffID, 5, 0.0, 0.3);
      rmSetAreaCliffPainting(cliffID, true, false, true);
      rmSetAreaObeyWorldCircleConstraint(cliffID, false);
      rmAddAreaToClass(cliffID, classCliff);
      rmBuildArea(cliffID);
   }


   // Define and place green grass areas around the map

   int numTries=20;
   for (i=0; <numTries)
   { 
      int grass=rmCreateArea("grass"+i);
      rmSetAreaSize(grass, 0.05, 0.05);
      rmSetAreaForestType(grass, "great plains grass green");
      rmSetAreaForestDensity(grass, 0.2);
      rmSetAreaForestClumpiness(grass, 0.0);
      rmSetAreaForestUnderbrush(grass, 0.3);
      rmSetAreaBaseHeight(grass, 1.0);
      rmSetAreaMinBlobs(grass, 7);
      rmSetAreaMaxBlobs(grass, 10);
      rmSetAreaMinBlobDistance(grass, 7);
      rmSetAreaMaxBlobDistance(grass, 10);
      rmSetAreaCoherence(grass, 0.4);
      rmAddAreaToClass(grass, classGrass);
      rmAddAreaConstraint(grass, avoidImpassableLand);
      rmAddAreaConstraint(grass, grassConstraint);
      rmAddAreaConstraint(grass, cliffConstraint);
      rmAddAreaConstraint(grass, shortPlayerConstraint);
      rmAddAreaConstraint(grass, avoidTradeSockets);
   }


   // Text
   rmSetStatusText("",0.20);


   // Define and place water areas

   // The major sea area
   int seaID=rmCreateArea("Sea");
   rmSetAreaWaterType(seaID, "great lakes");
   rmSetAreaSize(seaID, 0.2, 0.2);
   rmSetAreaCoherence(seaID, 0.3);
   rmSetAreaLocation(seaID, 0.9, 0.9);
   rmSetAreaObeyWorldCircleConstraint(seaID, false);
   rmSetAreaBaseHeight(seaID, 0.0);
   rmSetAreaMinBlobs(seaID, 35);
   rmSetAreaMaxBlobs(seaID, 40);
   rmSetAreaMinBlobDistance(seaID, 35);
   rmSetAreaMaxBlobDistance(seaID, 40);
   rmSetAreaSmoothDistance(seaID, 50);
   rmBuildArea(seaID);

   // Sea area in east, that takes land off from edges
   int sea2ID=rmCreateArea("Sea 2");
   rmSetAreaWaterType(sea2ID, "great lakes");
   rmSetAreaSize(sea2ID, 0.05, 0.05);
   rmSetAreaCoherence(sea2ID, 0.8);
   rmSetAreaLocation(sea2ID, 1.0, 0.65);
   rmSetAreaObeyWorldCircleConstraint(sea2ID, false);
   rmSetAreaBaseHeight(sea2ID, 0.0);
   rmSetAreaMinBlobs(sea2ID, 3);
   rmSetAreaMaxBlobs(sea2ID, 4);
   rmSetAreaMinBlobDistance(sea2ID, 3);
   rmSetAreaMaxBlobDistance(sea2ID, 4);
   rmSetAreaSmoothDistance(sea2ID, 50);
   rmBuildArea(sea2ID);

   // Sea area in west, that takes land off from edges
   int sea3ID=rmCreateArea("Sea 3");
   rmSetAreaWaterType(sea3ID, "great lakes");
   rmSetAreaSize(sea3ID, 0.05, 0.05);
   rmSetAreaCoherence(sea3ID, 0.8);
   rmSetAreaLocation(sea3ID, 0.65, 1.0);
   rmSetAreaObeyWorldCircleConstraint(sea3ID, false);
   rmSetAreaBaseHeight(sea3ID, 0.0);
   rmSetAreaMinBlobs(sea3ID, 3);
   rmSetAreaMaxBlobs(sea3ID, 4);
   rmSetAreaMinBlobDistance(sea3ID, 3);
   rmSetAreaMaxBlobDistance(sea3ID, 4);
   rmSetAreaSmoothDistance(sea3ID, 50);
   rmBuildArea(sea3ID);


   // Text
   rmSetStatusText("",0.30);


   // Place players - in team and FFA games

   // Player placement if FFA (Free For All) - place players in circle
   if(cNumberTeams > 2)
   {
      rmSetTeamSpacingModifier(0.7);
      rmSetPlacementSection(0.35, 0.90);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);
   }

   // Player placement if teams - place teams in lines
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      rmPlacePlayersLine(0.2, 0.8, 0.2, 0.4, 5, 15);

      rmSetPlacementTeam(1);
      rmPlacePlayersLine(0.8, 0.2, 0.4, 0.2, 5, 15);
   }


   // Text
   rmSetStatusText("",0.40);


   // Create the player's areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.50);
		

   // Define starting objects and resources

   // Define Town Center
   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefConstraint(startingTCID, avoidTradeRoute);
   rmAddObjectDefConstraint(startingTCID, avoidTradeSockets);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   // Define starting units
   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   // Define starting trees
   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "TreeMadrone", 10, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 12);
   rmSetObjectDefMaxDistance(startAreaTreeID, 16);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);
   rmAddObjectDefConstraint(startAreaTreeID, avoidTradeRoute);
   rmAddObjectDefConstraint(startAreaTreeID, avoidTradeSockets);

   // Define starting berries
   int startBerryID=rmCreateObjectDef("starting berry");
   rmAddObjectDefItem(startBerryID, "berrybush", 4, 4.0);
   rmSetObjectDefCreateHerd(startBerryID, true);
   rmSetObjectDefMinDistance(startBerryID, 14);
   rmSetObjectDefMaxDistance(startBerryID, 18);
   rmAddObjectDefConstraint(startBerryID, avoidStartResource);
   rmAddObjectDefConstraint(startBerryID, avoidImpassableLand);
   rmAddObjectDefConstraint(startBerryID, shortAvoidStartingUnits);
   rmAddObjectDefConstraint(startBerryID, avoidTradeRoute);
   rmAddObjectDefConstraint(startBerryID, avoidTradeSockets);

   // Define starting mine
   int startSilverID=rmCreateObjectDef("player silver");
   rmAddObjectDefItem(startSilverID, "Mine", 1, 5.0);
   rmSetObjectDefMinDistance(startSilverID, 14);
   rmSetObjectDefMaxDistance(startSilverID, 18);
   rmAddObjectDefConstraint(startSilverID, avoidStartResource);
   rmAddObjectDefConstraint(startSilverID, avoidImpassableLand);
   rmAddObjectDefConstraint(startSilverID, shortAvoidStartingUnits);
   rmAddObjectDefConstraint(startSilverID, avoidTradeRoute);
   rmAddObjectDefConstraint(startSilverID, avoidTradeSockets);


   // Place starting objects and resources

   for(i=1; <cNumberPlayers)
   {					
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startBerryID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startSilverID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
      rmAddClosestPointConstraint(flagVsFlag);
      rmAddClosestPointConstraint(flagLand);		
      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Text
   rmSetStatusText("",0.60);


   // Define and place trade route - goes from east to west

   // Create the trade route
   int tradeRouteID = rmCreateTradeRoute();
   int socketID=rmCreateObjectDef("sockets to dock trade posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

   // Define the sockets
   rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socketID, true);
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 12.0);

   // Set the trade route waypoints
   rmAddTradeRouteWaypoint(tradeRouteID, 1.0, 0.3);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.4, 0.4, 12, 12);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.3, 1.0, 12, 12);
   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route"); 
  
   // Place sockets to trade route
   vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.1);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.3);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.7);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.9);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);


   // Text
   rmSetStatusText("",0.70);


   // Define and place forests
	
   numTries=20*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(250), rmAreaTilesToFraction(300));
      rmSetAreaForestType(forest, "california madrone forest");
      rmSetAreaForestDensity(forest, 0.8);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.5);
      rmSetAreaForestUnderbrush(forest, 0.3);
      rmSetAreaBaseHeight(forest, 1.0);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, cliffConstraint);
      rmAddAreaConstraint(forest, avoidTradeRoute);
      rmAddAreaConstraint(forest, avoidTradeSockets);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.  
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }


   // Define and place straggler trees
   for (i=0; < 40)
   { 
      int stragglerTreeID=rmCreateObjectDef("straggler tree olive"+i);
      rmAddObjectDefItem(stragglerTreeID, "TreeMadrone", rmRandInt(1,5), 10.0);
      rmSetObjectDefMinDistance(stragglerTreeID, 0.0);
      rmSetObjectDefMaxDistance(stragglerTreeID, rmZFractionToMeters(0.47));
      rmAddObjectDefToClass(stragglerTreeID, rmClassID("classForest"));
      rmAddObjectDefToClass(stragglerTreeID, rmClassID("classTreeStraggler"));
      rmAddObjectDefConstraint(stragglerTreeID, somePlayerConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, avoidImpassableLand);
      rmAddObjectDefConstraint(stragglerTreeID, shortForestConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, stragglerTreeConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, avoidTradeRoute);
      rmAddObjectDefConstraint(stragglerTreeID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(stragglerTreeID, 0, 0.5, 0.5);
   }



   // Text
   rmSetStatusText("",0.80);

   
   // Define and place land animals

   // Define and place deer herds
   int deerHerdID=rmCreateObjectDef("deer herd");
   rmAddObjectDefItem(deerHerdID, "deer", rmRandInt(6,8), 6.0);
   rmSetObjectDefCreateHerd(deerHerdID, true);
   rmSetObjectDefMinDistance(deerHerdID, 0.0);
   rmSetObjectDefMaxDistance(deerHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(deerHerdID, classAnimals);
   rmAddObjectDefConstraint(deerHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(deerHerdID, animalConstraint);
   rmAddObjectDefConstraint(deerHerdID, cliffConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(deerHerdID, avoidTradeSockets);
   numTries=2*cNumberNonGaiaPlayers+1;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(deerHerdID, 0, 0.5, 0.5);

   // Define and place kri-kri herds
   int krikriHerdID=rmCreateObjectDef("krikri herd");
   rmAddObjectDefItem(krikriHerdID, "BighornSheep", rmRandInt(6,8), 6.0);
   rmSetObjectDefCreateHerd(krikriHerdID, true);
   rmSetObjectDefMinDistance(krikriHerdID, 0.0);
   rmSetObjectDefMaxDistance(krikriHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(krikriHerdID, classAnimals);
   rmAddObjectDefConstraint(krikriHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(krikriHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(krikriHerdID, animalConstraint);
   rmAddObjectDefConstraint(krikriHerdID, cliffConstraint);
   rmAddObjectDefConstraint(krikriHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(krikriHerdID, avoidTradeSockets);
   numTries=2*cNumberNonGaiaPlayers+1;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(krikriHerdID, 0, 0.5, 0.5);


   // Define and place silver mines

   int silverCount = 3*cNumberNonGaiaPlayers;

   for(i=0; < silverCount)
   {
      int silverID=rmCreateObjectDef("silver mine"+i);
      rmAddObjectDefItem(silverID, "mine", 1, 0.0);
      rmSetObjectDefMinDistance(silverID, 0.0);
      rmSetObjectDefMaxDistance(silverID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(silverID, classMines);
      rmAddObjectDefConstraint(silverID, avoidImpassableLand);
      rmAddObjectDefConstraint(silverID, playerConstraint);
      rmAddObjectDefConstraint(silverID, cliffConstraint);
      rmAddObjectDefConstraint(silverID, avoidMines);
      rmAddObjectDefConstraint(silverID, avoidTradeRoute);
      rmAddObjectDefConstraint(silverID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(silverID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.90);
	

   // Define and place water animals

   // Define and place fish to sea
   int fishID=rmCreateObjectDef("fish");
   rmAddObjectDefItem(fishID, "ypFishTuna", 1, 4.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(fishID, fishVsFish);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 10*cNumberNonGaiaPlayers);

   // Define and place whales to sea
   int whaleID=rmCreateObjectDef("whale");
   rmAddObjectDefItem(whaleID, "HumpbackWhale", 1, 4.0);
   rmSetObjectDefMinDistance(whaleID, 0.0);
   rmSetObjectDefMaxDistance(whaleID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(whaleID, whaleVsWhale);
   rmAddObjectDefConstraint(whaleID, whaleLand);
   rmPlaceObjectDefAtLoc(whaleID, 0, 0.5, 0.5, 2.5*cNumberNonGaiaPlayers);


   // Text
   rmSetStatusText("",0.99);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget2, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget3, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget4, avoidTradeSockets);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Define and place goat herds 

   int sheepID=rmCreateObjectDef("sheep");
   rmAddObjectDefItem(sheepID, "sheep", 2, 4.0);
   rmSetObjectDefMinDistance(sheepID, 0.0);
   rmSetObjectDefMaxDistance(sheepID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(sheepID, avoidSheep);
   rmAddObjectDefConstraint(sheepID, avoidTradeRoute);
   rmAddObjectDefConstraint(sheepID, avoidTradeSockets);
   rmAddObjectDefConstraint(sheepID, playerConstraint);
   rmAddObjectDefConstraint(sheepID, cliffConstraint);
   rmAddObjectDefConstraint(sheepID, avoidImpassableLand);
   rmPlaceObjectDefAtLoc(sheepID, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);


   // Place King's Hill if KOTH game to middle - automatically avoids the trade route

   if(rmGetIsKOTH()) 
   {
      float xLoc = 0.5;
      float yLoc = 0.5;
      float walk = 0.05;

      ypKingsHillPlacer(xLoc, yLoc, walk, 0);
   }


   // Text
   rmSetStatusText("",1.0); 
}  
