// RM script of Ireland
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Chooses the 6 natives, Celts and Britons

   // Native 1
   rmSetSubCiv(0, "Comanche");

   // Native 2
   rmSetSubCiv(1, "Comanche");

   // Native 3
   rmSetSubCiv(2, "Comanche");

   // Native 4
   rmSetSubCiv(3, "Comanche");

   // Native 5
   rmSetSubCiv(4, "Comanche");

   // Native 6
   rmSetSubCiv(5, "Comanche");


   // Picks the map size
   int playerTiles = 11000;
   if (cNumberNonGaiaPlayers > 4)
      playerTiles = 10000;
   if (cNumberNonGaiaPlayers > 6)
      playerTiles = 8500;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses the side where teams start in team games
   int teamSide = rmRandInt(1,4);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("Great Lakes");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.10, 4, 0.4, 7.0);
   rmSetBaseTerrainMix("great plains grass");
   rmTerrainInitialize("great_plains\ground6_gp", 1.0);
   rmSetMapType("grass");
   rmSetMapType("land");
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   int classLake=rmDefineClass("lake");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("nuggets");
   rmDefineClass("natives");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 50.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 60.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 55.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 5.0);

   // Lakes avoidance
   int lakeConstraint=rmCreateClassDistanceConstraint("lakes avoid lakes", classLake, 80.0);

   // Native avoidance
   int avoidNatives=rmCreateClassDistanceConstraint("things avoids natives", rmClassID("natives"), 5.0);
   int nativesAvoidNatives=rmCreateClassDistanceConstraint("natives avoids natives", rmClassID("natives"), 60.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 40.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);

   // Water objects avoidance
   int fishVsFish=rmCreateTypeDistanceConstraint("fish vs fish", "FishSalmon", 20.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish land", "land", true, 5.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid same", "HomeCityWaterSpawnFlag", 25.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag vs land", "land", true, 15.0);


   // Text
   rmSetStatusText("",0.10);


   // Place players - in team and FFA games

   // Player placement if FFA (Free For All) - place players in circle
   if(cNumberTeams > 2)
   {
      rmSetTeamSpacingModifier(0.7);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);
   }

   // Player placement if teams - place teams in circle to apart from each other
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      if (rmGetNumberPlayersOnTeam(0) == 2)
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.05, 0.20);
         else if (teamSide == 2)
            rmSetPlacementSection(0.30, 0.45);
         else if (teamSide == 3)
            rmSetPlacementSection(0.55, 0.70);
         else if (teamSide == 4)
            rmSetPlacementSection(0.80, 0.95);
      }
      else
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.00, 0.25);
         else if (teamSide == 2)
            rmSetPlacementSection(0.25, 0.50);
         else if (teamSide == 3)
            rmSetPlacementSection(0.50, 0.75);
         else if (teamSide == 4)
            rmSetPlacementSection(0.75, 1.00);
      }
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);

      rmSetPlacementTeam(1);
      if (rmGetNumberPlayersOnTeam(1) == 2)
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.55, 0.70);
         else if (teamSide == 2)
            rmSetPlacementSection(0.80, 0.95);
         else if (teamSide == 3)
            rmSetPlacementSection(0.05, 0.20);
         else if (teamSide == 4)
            rmSetPlacementSection(0.30, 0.45);
      }
      else
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.50, 0.75);
         else if (teamSide == 2)
            rmSetPlacementSection(0.75, 1.00);
         else if (teamSide == 3)
            rmSetPlacementSection(0.00, 0.25);
         else if (teamSide == 4)
            rmSetPlacementSection(0.25, 0.50);
      }
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.50);


   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "TreeGreatLakes", 6, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 12);
   rmSetObjectDefMaxDistance(startAreaTreeID, 16);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startDeerID=rmCreateObjectDef("starting deer");
   rmAddObjectDefItem(startDeerID, "deer", 5, 4.0);
   rmSetObjectDefCreateHerd(startDeerID, true);
   rmSetObjectDefMinDistance(startDeerID, 14);
   rmSetObjectDefMaxDistance(startDeerID, 18);
   rmAddObjectDefConstraint(startDeerID, avoidStartResource);
   rmAddObjectDefConstraint(startDeerID, avoidImpassableLand);
   rmAddObjectDefConstraint(startDeerID, shortAvoidStartingUnits);

   int startSilverID=rmCreateObjectDef("player silver");
   rmAddObjectDefItem(startSilverID, "Mine", 1, 5.0);
   rmSetObjectDefMinDistance(startSilverID, 14);
   rmSetObjectDefMaxDistance(startSilverID, 18);
   rmAddObjectDefConstraint(startSilverID, avoidStartResource);
   rmAddObjectDefConstraint(startSilverID, avoidImpassableLand);
   rmAddObjectDefConstraint(startSilverID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startDeerID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startSilverID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
      rmAddClosestPointConstraint(flagVsFlag);
      rmAddClosestPointConstraint(flagLand);		
      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Text
   rmSetStatusText("",0.70);


   // Define and place lakes
 
   for(i=1; < 7)
   {
      int lakes=rmCreateArea("lakes"+i);
      rmSetAreaSize(lakes, 0.008, 0.015);
      rmAddAreaToClass(lakes, classLake);
      rmSetAreaWaterType(lakes, "great lakes");
      rmSetAreaBaseHeight(lakes, 0.0);
      rmSetAreaCoherence(lakes, 0.5);
      rmSetAreaMinBlobs(lakes, 10);
      rmSetAreaMaxBlobs(lakes, 20);
      rmSetAreaMinBlobDistance(lakes, 10);
      rmSetAreaMaxBlobDistance(lakes, 20);
      rmSetAreaSmoothDistance(lakes, 10);
      rmAddAreaConstraint(lakes, lakeConstraint);
      rmAddAreaConstraint(lakes, somePlayerConstraint);
      rmBuildArea(lakes);
   }


   // Place the 6 natives, Celts and Britons

   // Native 1
   int nativeVillageID = -1;
   int nativeVillageType = rmRandInt(1,5);
   nativeVillageID = rmCreateGrouping("celts village 1", "native comanche village "+nativeVillageType);
   rmSetGroupingMinDistance(nativeVillageID, 0.0);
   rmSetGroupingMaxDistance(nativeVillageID, rmXFractionToMeters(0.47));
   rmAddGroupingConstraint(nativeVillageID, playerConstraint);
   rmAddGroupingConstraint(nativeVillageID, nativesAvoidNatives);
   rmAddGroupingToClass(nativeVillageID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillageID, 0, 0.5, 0.5);

   // Native 2
   int nativeVillage2ID = -1;
   int nativeVillage2Type = rmRandInt(1,5);
   nativeVillage2ID = rmCreateGrouping("celts village 2", "native comanche village "+nativeVillage2Type);
   rmSetGroupingMinDistance(nativeVillage2ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage2ID, rmXFractionToMeters(0.47));
   rmAddGroupingConstraint(nativeVillage2ID, playerConstraint);
   rmAddGroupingConstraint(nativeVillage2ID, nativesAvoidNatives);
   rmAddGroupingToClass(nativeVillage2ID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillage2ID, 0, 0.5, 0.5);

   // Native 3
   int nativeVillage3ID = -1;
   int nativeVillage3Type = rmRandInt(1,5);
   nativeVillage3ID = rmCreateGrouping("celts village 3", "native comanche village "+nativeVillage3Type);
   rmSetGroupingMinDistance(nativeVillage3ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage3ID, rmXFractionToMeters(0.47));
   rmAddGroupingConstraint(nativeVillage3ID, playerConstraint);
   rmAddGroupingConstraint(nativeVillage3ID, nativesAvoidNatives);
   rmAddGroupingToClass(nativeVillage3ID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillage3ID, 0, 0.5, 0.5);

   // Native 4
   int nativeVillage4ID = -1;
   int nativeVillage4Type = rmRandInt(1,5);
   nativeVillage4ID = rmCreateGrouping("britons village 1", "native comanche village "+nativeVillage4Type);
   rmSetGroupingMinDistance(nativeVillage4ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage4ID, rmXFractionToMeters(0.47));
   rmAddGroupingConstraint(nativeVillage4ID, playerConstraint);
   rmAddGroupingConstraint(nativeVillage4ID, nativesAvoidNatives);
   rmAddGroupingToClass(nativeVillage4ID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillage4ID, 0, 0.5, 0.5);

   // Native 5
   int nativeVillage5ID = -1;
   int nativeVillage5Type = rmRandInt(1,5);
   nativeVillage5ID = rmCreateGrouping("britons village 2", "native comanche village "+nativeVillage5Type);
   rmSetGroupingMinDistance(nativeVillage5ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage5ID, rmXFractionToMeters(0.47));
   rmAddGroupingConstraint(nativeVillage5ID, playerConstraint);
   rmAddGroupingConstraint(nativeVillage5ID, nativesAvoidNatives);
   rmAddGroupingToClass(nativeVillage5ID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillage5ID, 0, 0.5, 0.5);

   // Native 6
   int nativeVillage6ID = -1;
   int nativeVillage6Type = rmRandInt(1,5);
   nativeVillage6ID = rmCreateGrouping("britons village 3", "native comanche village "+nativeVillage6Type);
   rmSetGroupingMinDistance(nativeVillage6ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage6ID, rmXFractionToMeters(0.47));
   rmAddGroupingConstraint(nativeVillage6ID, playerConstraint);
   rmAddGroupingConstraint(nativeVillage6ID, nativesAvoidNatives);
   rmAddGroupingToClass(nativeVillage6ID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillage6ID, 0, 0.5, 0.5);


   // Define and place forests
	
   int numTries=10*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(200), rmAreaTilesToFraction(250));
      rmSetAreaForestType(forest, "great lakes forest");
      rmSetAreaForestDensity(forest, 0.7);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.4);
      rmSetAreaForestUnderbrush(forest, 0.3);
      rmSetAreaBaseHeight(forest, 1.0);
      rmSetAreaMinBlobs(forest, 1);
      rmSetAreaMaxBlobs(forest, 3);
      rmSetAreaMinBlobDistance(forest, 10.0);
      rmSetAreaMaxBlobDistance(forest, 15.0);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, avoidNatives);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.  
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place animals
   
   // Define and place deer herds
   int deerHerdID=rmCreateObjectDef("deer herd");
   rmAddObjectDefItem(deerHerdID, "deer", rmRandInt(7,8), 8.0);
   rmSetObjectDefCreateHerd(deerHerdID, true);
   rmSetObjectDefMinDistance(deerHerdID, 0.0);
   rmSetObjectDefMaxDistance(deerHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(deerHerdID, classAnimals);
   rmAddObjectDefConstraint(deerHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(deerHerdID, animalConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidNatives);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(deerHerdID, 0, 0.5, 0.5);

   // Define and place fox herds
   int foxHerdID=rmCreateObjectDef("fox herd");
   rmAddObjectDefItem(foxHerdID, "moose", rmRandInt(7,8), 8.0);
   rmSetObjectDefMinDistance(foxHerdID, 0.0);
   rmSetObjectDefMaxDistance(foxHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(foxHerdID, classAnimals);
   rmAddObjectDefConstraint(foxHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(foxHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(foxHerdID, animalConstraint);
   rmAddObjectDefConstraint(foxHerdID, avoidNatives);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(foxHerdID, 0, 0.5, 0.5);


   // Define and place silver mines

   if (cNumberNonGaiaPlayers <= 5)
      int silverCount = 3*cNumberNonGaiaPlayers;
   else
      silverCount = 2*cNumberNonGaiaPlayers;

   for(i=0; < silverCount)
   {
      int silverID=rmCreateObjectDef("silver mine"+i);
      rmAddObjectDefItem(silverID, "mine", 1, 0.0);
      rmSetObjectDefMinDistance(silverID, 0.0);
      rmSetObjectDefMaxDistance(silverID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(silverID, classMines);
      rmAddObjectDefConstraint(silverID, avoidImpassableLand);
      rmAddObjectDefConstraint(silverID, playerConstraint);
      rmAddObjectDefConstraint(silverID, avoidMines);
      rmAddObjectDefConstraint(silverID, avoidNatives);
      rmPlaceObjectDefAtLoc(silverID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.90);


   // Define and place fish

   int fishID=rmCreateObjectDef("fish");
   rmAddObjectDefItem(fishID, "FishSalmon", 1, 4.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(fishID, fishVsFish);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 5*cNumberNonGaiaPlayers);


   // Text
   rmSetStatusText("",0.99);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidNatives);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidNatives);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Place King's Hill if KOTH game to middle

   if(rmGetIsKOTH()) 
   {
      float xLoc = 0.5;
      float yLoc = 0.5;
      float walk = 0.05;

      ypKingsHillPlacer(xLoc, yLoc, walk, 0);
   }


   // Text
   rmSetStatusText("",1.0); 
}  
