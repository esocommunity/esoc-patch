// RM script of Greek Archipelago
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Picks the map size
   int testTiles = 100000;		

   int size=2.0*sqrt(testTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);
	

   // Set map the map smoothness
   rmSetMapElevationHeightBlend(1);


   // Picks a default water height
   rmSetSeaLevel(-1.0);


   // Pick lighting
   rmSetLightingSet("nwterritory");


   // Picks default terrain and map type
   rmSetMapElevationParameters(cElevTurbulence, 0.10, 4, 0.4, 8.0);
   rmSetSeaType("great lakes");
   rmTerrainInitialize("water");
   rmSetMapType("grass");
   rmSetMapType("water");
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint
   rmSetWorldCircleConstraint(true);


   // Define classes, these are used for constraints
   int classPlayer=rmDefineClass("player");
   int classIsland=rmDefineClass("island");
   int classNeutralIsland=rmDefineClass("neutral island");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   rmDefineClass("starting settlement");
   rmDefineClass("startingUnit");
   rmDefineClass("nuggets");


   // Text
   rmSetStatusText("",0.10);


   // Define constraints - used for things to avoid certain things
   
   // Map edge and centre constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));
   int islandEdgeConstraint=rmCreatePieConstraint("island avoid edges", 0.5, 0.5, 0, rmZFractionToMeters(0.45), rmDegreesToRadians(0), rmDegreesToRadians(360));
   int flagEdgeConstraint = rmCreatePieConstraint("flags away from edge of map", 0.5, 0.5, rmGetMapXSize()-200, rmGetMapXSize()-100, 0, 0, 0);

   // All player things avoidance
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);

   // All resource avoidance
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 120.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 60.0);
   int avoidGoat=rmCreateTypeDistanceConstraint("herds avoids goat", "sheep", 60.0);
   int avoidTrees=rmCreateTypeDistanceConstraint("trees avoids trees", "TreeMadrone", 15.0);

   // Terrain avoidance
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 5.0);
   int resourceAvoidImpassableLand=rmCreateTerrainDistanceConstraint("resource avoid impassable land", "Land", false, 3.0);
   int shortAvoidImpassableLand=rmCreateTerrainDistanceConstraint("short avoid impassable land", "Land", false, 2.0);

   // Area constraints
   int islandConstraint=rmCreateClassDistanceConstraint("stay away from islands", classIsland, 35.0);
   int playerIslandConstraint=rmCreateClassDistanceConstraint("stay away from player's islands", classPlayer, 50.0);

   // Nuggets avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 60.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 4.0);

   // Water objects avoidance
   int avoidFish=rmCreateTypeDistanceConstraint("fish avoid fish", "FishSardine", 20.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish avoid land", "land", true, 2.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag avoid land", "land", true, 20.0);
   int avoidFlag = rmCreateTypeDistanceConstraint("flags avoid flags", "HomeCityWaterSpawnFlag", 25.0);


   // Text
   rmSetStatusText("",0.20);


   // Place players

   rmSetTeamSpacingModifier(0.7);
   rmPlacePlayersCircular(0.42, 0.42, 0.0);


   // Define and place player areas (or "islands" in this map!)

   float playerFraction=rmAreaTilesToFraction(1500);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmAddAreaToClass(id, classIsland);
      rmSetAreaMix(id, "nwt_grass1");
      rmSetAreaBaseHeight(id, 0);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 2);
      rmAddAreaConstraint(id, playerConstraint);
      rmSetAreaCoherence(id, 0.7);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
      rmSetAreaElevationType(id, cElevTurbulence);
      rmSetAreaElevationVariation(id, 5.0);
      rmSetAreaElevationMinFrequency(id, 0.10);
      rmSetAreaElevationOctaves(id, 3);
      rmSetAreaElevationPersistence(id, 0.2);
      rmSetAreaElevationNoiseBias(id, 1);
   }


   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.30);


   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTree1ID=rmCreateObjectDef("starting trees 1");
   rmAddObjectDefItem(startAreaTree1ID, "TreeMadrone", 10, 6.0);
   rmSetObjectDefMinDistance(startAreaTree1ID, 20);
   rmSetObjectDefMaxDistance(startAreaTree1ID, 25);
   rmAddObjectDefConstraint(startAreaTree1ID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTree1ID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTree1ID, shortAvoidStartingUnits);

   int startAreaTree2ID=rmCreateObjectDef("starting trees 2");
   rmAddObjectDefItem(startAreaTree2ID, "TreeMadrone", 10, 6.0);
   rmSetObjectDefMinDistance(startAreaTree2ID, 20);
   rmSetObjectDefMaxDistance(startAreaTree2ID, 25);
   rmAddObjectDefConstraint(startAreaTree2ID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTree2ID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTree2ID, shortAvoidStartingUnits);

   int startAreaTree3ID=rmCreateObjectDef("starting trees 3");
   rmAddObjectDefItem(startAreaTree3ID, "TreeMadrone", 10, 6.0);
   rmSetObjectDefMinDistance(startAreaTree3ID, 20);
   rmSetObjectDefMaxDistance(startAreaTree3ID, 25);
   rmAddObjectDefConstraint(startAreaTree3ID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTree3ID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTree3ID, shortAvoidStartingUnits);

   int startBerryID=rmCreateObjectDef("starting berries");
   rmAddObjectDefItem(startBerryID, "Berrybush", 4, 4.0);
   rmSetObjectDefCreateHerd(startBerryID, true);
   rmSetObjectDefMinDistance(startBerryID, 14);
   rmSetObjectDefMaxDistance(startBerryID, 20);
   rmAddObjectDefConstraint(startBerryID, avoidStartResource);
   rmAddObjectDefConstraint(startBerryID, avoidImpassableLand);
   rmAddObjectDefConstraint(startBerryID, shortAvoidStartingUnits);

   int startGoldID=rmCreateObjectDef("player gold");
   rmAddObjectDefItem(startGoldID, "MineGold", 1, 5.0);
   rmSetObjectDefMinDistance(startGoldID, 14);
   rmSetObjectDefMaxDistance(startGoldID, 20);
   rmAddObjectDefConstraint(startGoldID, avoidStartResource);
   rmAddObjectDefConstraint(startGoldID, avoidImpassableLand);
   rmAddObjectDefConstraint(startGoldID, shortAvoidStartingUnits);

   // Place starting objects and resources

   for(i=1; <cNumberPlayers)
   {		
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTree1ID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTree2ID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTree3ID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startGoldID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startBerryID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define water flag and starting ship
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 1.0);
      if(rmGetPlayerCiv(i) == rmGetCivID("Ottomans"))
         rmAddObjectDefItem(waterFlagID, "Galley", 1, 10.0);
      else
         rmAddObjectDefItem(waterFlagID, "Caravel", 1, 10.0);
      rmAddClosestPointConstraint(flagEdgeConstraint);
      rmAddClosestPointConstraint(avoidFlag);
      rmAddClosestPointConstraint(flagLand);


      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Text
   rmSetStatusText("",0.40);


   // Define and place random islands

   float islandFraction=rmAreaTilesToFraction(800);
   for(i=1; <40)
   {
      int islands=rmCreateArea("random island"+i);
      rmSetAreaSize(islands, islandFraction, islandFraction);
      rmAddAreaToClass(islands, classIsland);
      rmAddAreaToClass(islands, classNeutralIsland);
      rmSetAreaMix(islands, "nwt_grass1");
      rmSetAreaBaseHeight(islands, 0);
      rmSetAreaMinBlobs(islands, 3);
      rmSetAreaMaxBlobs(islands, 4);
      rmAddAreaConstraint(islands, islandConstraint); 
      rmAddAreaConstraint(islands, playerIslandConstraint); 
      rmAddAreaConstraint(islands, islandEdgeConstraint);
      rmSetAreaCoherence(islands, 0.9);
      rmSetAreaElevationType(islands, cElevTurbulence);
      rmSetAreaElevationVariation(islands, 5.0);
      rmSetAreaElevationMinFrequency(islands, 0.10);
      rmSetAreaElevationOctaves(islands, 3);
      rmSetAreaElevationPersistence(islands, 0.2);
      rmSetAreaElevationNoiseBias(islands, 1);
   }


   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.50);


   // Define and place trees to random islands   
   int treesID=rmCreateObjectDef("trees");
   rmAddObjectDefItem(treesID, "TreeMadrone", rmRandInt(8,9), 7.0);
   rmSetObjectDefMinDistance(treesID, 0);
   rmSetObjectDefMaxDistance(treesID, 1);
   rmAddObjectDefConstraint(treesID, resourceAvoidImpassableLand);
   rmAddObjectDefConstraint(treesID, avoidTrees);
   int numTries = 70;
   for (j=0; <numTries) 
      rmPlaceObjectDefInRandomAreaOfClass(treesID, 0, classNeutralIsland);


   // Text
   rmSetStatusText("",0.60);


   // Define and place huntables to islands - only few deer
   
   // Define and place deer herds
   int deerHerdID=rmCreateObjectDef("deer herd");
   rmAddObjectDefItem(deerHerdID, "deer", rmRandInt(7,8), 4.0);
   rmSetObjectDefCreateHerd(deerHerdID, true);
   rmSetObjectDefMinDistance(deerHerdID, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(deerHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(deerHerdID, classAnimals);
   rmAddObjectDefConstraint(deerHerdID, animalConstraint);
   rmAddObjectDefConstraint(deerHerdID, shortPlayerConstraint);
   rmAddObjectDefConstraint(deerHerdID, resourceAvoidImpassableLand);
   numTries=3*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(deerHerdID, 0, 0.5, 0.5);

   // Text
   rmSetStatusText("",0.70);


   // Define and place silver mines

   int silverCount = 18;

   for(i=0; < silverCount)
   {
      int silverID=rmCreateObjectDef("silver mine"+i);
      rmAddObjectDefItem(silverID, "mine", 1, 0.0);
      rmAddObjectDefToClass(silverID, classMines);
      rmSetObjectDefMinDistance(silverID, 0.0);
      rmSetObjectDefMaxDistance(silverID, rmXFractionToMeters(0.47));
      rmAddObjectDefConstraint(silverID, resourceAvoidImpassableLand);
      rmAddObjectDefConstraint(silverID, playerConstraint);
      rmAddObjectDefConstraint(silverID, avoidMines);
      rmPlaceObjectDefAtLoc(silverID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 15.0);
   rmSetObjectDefMaxDistance(nugget1, 25.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Text
   rmSetStatusText("",0.90);


   // Define and place goat herds
   int goatID=rmCreateObjectDef("goat");
   rmAddObjectDefItem(goatID, "sheep", 2, 4.0);
   rmSetObjectDefMinDistance(goatID, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(goatID, rmXFractionToMeters(0.50));
   rmAddObjectDefConstraint(goatID, avoidGoat);
   rmAddObjectDefConstraint(goatID, resourceAvoidImpassableLand);
   rmAddObjectDefConstraint(goatID, shortPlayerConstraint);
   rmPlaceObjectDefAtLoc(goatID, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);


   // Define and place many fish
   int fishID=rmCreateObjectDef("fish");
   rmAddObjectDefItem(fishID, "FishSardine", 1, 4.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.50));
   rmAddObjectDefConstraint(fishID, avoidFish);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 20*cNumberNonGaiaPlayers);


   // Place King's Hill if KOTH game to middle - automatically stays on some island

   if(rmGetIsKOTH()) 
   {
      float xLoc = 0.5;
      float yLoc = 0.5;
      float walk = 0.05;

      ypKingsHillPlacer(xLoc, yLoc, walk, 0);
   }
   

   // Text
   rmSetStatusText("",1.0);

}