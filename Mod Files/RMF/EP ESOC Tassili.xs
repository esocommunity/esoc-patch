// --------------------------------------------------------------------------------------------------------------------
// --------------------------------------------- T A S S I L I --------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------- Commentaries ----------------------------------------------------
// I took some ideas from Oasis from AOM and NE Sahara from Napoleonic Era mod. 
// I love the mood of this map.
// Map done by Rikikipu (January 2016)


// --------------------------------------------- Introduction ------------------------------------------------------


include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{

   rmSetStatusText("",0.01);


	int playerTiles = 6500;
	if (cNumberNonGaiaPlayers == 2)
		playerTiles = 7000;
	if (cNumberNonGaiaPlayers >2)
		playerTiles = 7500;
	if (cNumberNonGaiaPlayers >4)
		playerTiles = 8000;		

	int size=1.7*sqrt(cNumberNonGaiaPlayers*playerTiles);
	rmSetMapSize(size, size*1.4);
     rmSetSeaLevel(0.0);
	rmSetBaseTerrainMix("sonora_dirt");
	rmTerrainInitialize("sonora\ground2_son", 4.0);
	rmSetMapElevationParameters(cElevTurbulence, 0.06, 2, 0.1, 5.0);
	rmSetMapType("sonora");
	rmSetMapType("grass");
	rmSetMapType("land");
    rmSetLightingSet("constantinople");
	rmSetSeaLevel(2.0);

	chooseMercs();

	// Corner constraint.
	rmSetWorldCircleConstraint(true);

   // Define some classes. These are used later for constraints.
	int classPlayer=rmDefineClass("player");
	rmDefineClass("classHill");
	rmDefineClass("classPatch");
	rmDefineClass("starting settlement");
	rmDefineClass("startingUnit");
	rmDefineClass("classForest");
	rmDefineClass("importantItem");
	rmDefineClass("natives");
	rmDefineClass("classCliff");
	rmDefineClass("secrets");
	rmDefineClass("nuggets");
	rmDefineClass("center");
	rmDefineClass("tradeIslands");
    rmDefineClass("socketClass");
	rmDefineClass("GoldMineR");
	rmDefineClass("GoldMineL");
	int classGreatLake=rmDefineClass("great lake");

// ------------------------------------------------------------- Constraints --------------------------------------------------
   

	// Useful random constraints
	int avoidTradeIslands=rmCreateClassDistanceConstraint("stay away from trade islands", rmClassID("tradeIslands"), 40.0);
    int avoidTownCenterFar=rmCreateTypeDistanceConstraint("avoid Town Center Far", "townCenter", 60.0);
    int avoidTownCenterFar1=rmCreateTypeDistanceConstraint("avoid Town Center Far 1", "townCenter", 40.0);
	int avoidTownCenterFar2=rmCreateTypeDistanceConstraint("avoid Town Center Far 2", "townCenter", 45.0);
	int avoidCoin1=rmCreateTypeDistanceConstraint("avoid coin starts", "Mine", 30.0);
	int shortAvoidCoin=rmCreateTypeDistanceConstraint("short avoid coin", "gold", 10.0);
	int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);

	// Avoid impassable land
	int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 6.0);
	int shortAvoidImpassableLand=rmCreateTerrainDistanceConstraint("short avoid impassable land", "Land", false, 2.0);
	int longAvoidImpassableLand=rmCreateTerrainDistanceConstraint("long avoid impassable land", "Land", false, 10.0);
    int avoidCliffshort=rmCreateClassDistanceConstraint("cliff vs. cliff short", rmClassID("classCliff"), 8.0);


	// Ressource avoidance
	int avoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units", rmClassID("startingUnit"), 45.0);
	int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);
	int avoidImportantItem=rmCreateClassDistanceConstraint("secrets etc avoid each other", rmClassID("importantItem"), 10.0);
    int avoidNugget=rmCreateTypeDistanceConstraint("nugget avoid nugget", "AbstractNugget", 50.0);
	int avoidNugget1=rmCreateTypeDistanceConstraint("nugget avoid nugget1", "AbstractNugget", 20.0);
	int ypIbexConstraint=rmCreateTypeDistanceConstraint("avoid the ypIbex", "ypIbex", 35.0);
	int ypIbexConstraint1=rmCreateTypeDistanceConstraint("avoid the ypIbex1", "pronghorn", 35.0);
	int ypIbexConstraintM=rmCreateTypeDistanceConstraint("avoid the ypIbexM", "ypIbex", 55.0);
	int ypIbexConstraint1M=rmCreateTypeDistanceConstraint("avoid the ypIbex1M", "pronghorn", 55.0);		
	int shortypIbexConstraint=rmCreateTypeDistanceConstraint("short avoid the ypIbex", "ypIbex", 20.0);

	// Decoration avoidance
	int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 6.0);

	// Trade route avoidance.
	int avoidTradeRoute = rmCreateTradeRouteDistanceConstraint("trade route", 5.0);
	int shortAvoidTradeRoute = rmCreateTradeRouteDistanceConstraint("short trade route", 2.0);
	int avoidTradeRouteFar = rmCreateTradeRouteDistanceConstraint("trade route far", 18.0);
	int avoidTradeSockets = rmCreateTypeDistanceConstraint("avoid trade sockets", "sockettraderoute", 10.0);
	int avoidSocket=rmCreateClassDistanceConstraint("socket avoidance", rmClassID("socketClass"), 8.0);
	   
	// Forest constraints
	if (cNumberNonGaiaPlayers<4)
	{
    int stayNE1 = rmCreatePieConstraint("stay NE1",0.5,0.5, rmXFractionToMeters(0.59),rmXFractionToMeters(0.68), rmDegreesToRadians(130),rmDegreesToRadians(250));
    int staySW1 = rmCreatePieConstraint("stay SW1",0.5,0.5, rmXFractionToMeters(0.59),rmXFractionToMeters(0.68), rmDegreesToRadians(310),rmDegreesToRadians(70));
	}
	else if (cNumberNonGaiaPlayers<6)
	{
    stayNE1 = rmCreatePieConstraint("stay NE1",0.5,0.5, rmXFractionToMeters(0.59),rmXFractionToMeters(0.68), rmDegreesToRadians(130),rmDegreesToRadians(250));
    staySW1 = rmCreatePieConstraint("stay SW1",0.5,0.5, rmXFractionToMeters(0.59),rmXFractionToMeters(0.68), rmDegreesToRadians(310),rmDegreesToRadians(70));
	}
	else
    {
    stayNE1 = rmCreatePieConstraint("stay NE1",0.5,0.5, rmXFractionToMeters(0.6),rmXFractionToMeters(0.69), rmDegreesToRadians(130),rmDegreesToRadians(250));
    staySW1 = rmCreatePieConstraint("stay SW1",0.5,0.5, rmXFractionToMeters(0.6),rmXFractionToMeters(0.69), rmDegreesToRadians(310),rmDegreesToRadians(70));
	}





	rmSetStatusText("",0.10);

// ---------------------------------------------- TradeRoute ------------------------------------------------

	int socketID=rmCreateObjectDef("sockets to dock Trade Posts");
	rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
	rmSetObjectDefAllowOverlap(socketID, true);
	rmAddObjectDefToClass(socketID, rmClassID("socketClass"));
	rmSetObjectDefMinDistance(socketID, 0.0);
	rmSetObjectDefMaxDistance(socketID, 6.0);
	rmAddObjectDefConstraint(socketID, shortAvoidTradeRoute);
	
	int socketID1=rmCreateObjectDef("sockets to dock Trade Posts1");
	rmAddObjectDefItem(socketID1, "SocketTradeRoute", 1, 0.0);
	rmSetObjectDefAllowOverlap(socketID1, true);
	rmAddObjectDefToClass(socketID1, rmClassID("socketClass"));
	rmSetObjectDefMinDistance(socketID1, 0.0);
	rmSetObjectDefMaxDistance(socketID1, 6.0);
	rmAddObjectDefConstraint(socketID1, shortAvoidTradeRoute);

	int tradeRouteID = rmCreateTradeRoute();
	rmSetObjectDefTradeRouteID(socketID, tradeRouteID);
	rmAddTradeRouteWaypoint(tradeRouteID, 0.78, 0.99); // -1
	rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.78, 0.01, 0, 0); // -6
	
    int tradeRouteID1 = rmCreateTradeRoute();
	rmSetObjectDefTradeRouteID(socketID1, tradeRouteID1);
	rmAddTradeRouteWaypoint(tradeRouteID1, 0.22, 0.01); // -1	
	rmAddRandomTradeRouteWaypoints(tradeRouteID1, 0.22, 0.99, 0, 0); // -10
	
	bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
	bool placedTradeRoute1 = rmBuildTradeRoute(tradeRouteID1, "dirt");

		vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.25);
	    rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

	    socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.75);
	    rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

	    socketLoc = rmGetTradeRouteWayPoint(tradeRouteID1, 0.25);
	    rmPlaceObjectDefAtPoint(socketID1, 0, socketLoc);

	    socketLoc = rmGetTradeRouteWayPoint(tradeRouteID1, 0.75);
	    rmPlaceObjectDefAtPoint(socketID1, 0, socketLoc);
	if (cNumberNonGaiaPlayers > 4)
		{ socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
	    rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

	    socketLoc = rmGetTradeRouteWayPoint(tradeRouteID1, 0.5);
	    rmPlaceObjectDefAtPoint(socketID1, 0, socketLoc);
		}

		int failCont=0;

// ------------------------------------ Design : cliffs and forests ------------------------------------------
		
	   int cliffID = rmCreateArea("cliff top");
   rmAddAreaInfluenceSegment(cliffID, 1.0, 0.0, 1.0, 1.0);
   rmSetAreaLocation(cliffID, 0.97, 0.5);
   rmSetAreaSize(cliffID, rmAreaTilesToFraction(300), rmAreaTilesToFraction(300));
   rmSetAreaCoherence(cliffID, 0.75);
   rmSetAreaEdgeFilling(cliffID, 1.0);
   rmSetAreaSmoothDistance(cliffID, 4.0);
   rmSetAreaHeightBlend(cliffID, 6.0);
   rmSetAreaCliffHeight(cliffID, 8.0, 2.0, 0.0);
   rmSetAreaCliffType(cliffID, "spc rocky cliffs");
   rmSetAreaCliffEdge(cliffID, 1.0, 1.0, 0.0, 1.0, 0);
   rmAddAreaToClass(cliffID, rmClassID("classCliff"));
   rmSetAreaCliffPainting(cliffID, true, true, true, 1.0, true);
   rmSetAreaWarnFailure(cliffID, false);
   rmBuildArea(cliffID);	
		
		
   int cliffID1 = rmCreateArea("cliff bottom");
   rmAddAreaInfluenceSegment(cliffID1, 0.0, 0.0, 0.0, 1.0);
   rmSetAreaLocation(cliffID1, 0.03, 0.5);
   rmSetAreaSize(cliffID1, rmAreaTilesToFraction(300), rmAreaTilesToFraction(300));
   rmSetAreaCoherence(cliffID1, 0.75);
   rmSetAreaEdgeFilling(cliffID1, 1.0);
   rmSetAreaSmoothDistance(cliffID1, 4.0);
   rmSetAreaHeightBlend(cliffID1, 6.0);
   rmSetAreaCliffHeight(cliffID1, 8.0, 2.0, 0.0);
   rmSetAreaCliffType(cliffID1, "spc rocky cliffs");
   rmSetAreaCliffEdge(cliffID1, 1.0, 1.0, 0.0, 1.0, 0);
   rmAddAreaToClass(cliffID1, rmClassID("classCliff"));
   rmSetAreaCliffPainting(cliffID1, true, true, true, 1.0, true);
   rmSetAreaWarnFailure(cliffID1, false);
   rmBuildArea(cliffID1);
  
  
  if (cNumberNonGaiaPlayers==2)
  {
	    int forestID=rmCreateArea("forest along trade route bottom");
      rmSetAreaSize(forestID, 0.02, 0.02);
      rmSetAreaLocation(forestID, 0.15, 0.5);
      rmSetAreaForestType(forestID, "caribbean palm forest");
	  rmAddAreaInfluenceSegment(forestID, 0.17, 0.24, 0.17, 0.76);
      rmSetAreaMinBlobs(forestID, 1);
      rmSetAreaMaxBlobs(forestID, 1);
	  rmSetAreaForestDensity(forestID, 0.80);
	  rmSetAreaForestClumpiness(forestID, 0.5);
	  rmSetAreaForestUnderbrush(forestID, 0.9);
      rmSetAreaSmoothDistance(forestID, 3);
      rmSetAreaCoherence(forestID, 0.95);
	  rmAddAreaConstraint(forestID, avoidSocket);
	  rmAddAreaConstraint(forestID, shortAvoidTradeRoute);
      rmAddAreaToClass(forestID, rmClassID("classForest"));
	  rmBuildArea(forestID);
     
	  
	  	int forestIDA=rmCreateArea("forest along trade route bottom A");
      rmSetAreaSize(forestIDA, 0.02, 0.02);
      rmSetAreaLocation(forestIDA, 0.22, 0.5);
      rmSetAreaForestType(forestIDA, "caribbean palm forest");
	  rmAddAreaInfluenceSegment(forestIDA, 0.22, 0.24, 0.22, 0.76);
      rmSetAreaMinBlobs(forestIDA, 1);
      rmSetAreaMaxBlobs(forestIDA, 1);
	  rmSetAreaForestDensity(forestIDA, 0.80);
	  rmSetAreaForestClumpiness(forestIDA, 0.5);
	  rmSetAreaForestUnderbrush(forestIDA, 0.9);
      rmSetAreaSmoothDistance(forestIDA, 3);
      rmSetAreaCoherence(forestIDA, 0.95);
	  rmAddAreaConstraint(forestIDA, avoidSocket);
	  rmAddAreaConstraint(forestIDA, shortAvoidTradeRoute);
      rmAddAreaToClass(forestIDA, rmClassID("classForest"));
      rmBuildArea(forestIDA);
	  
	  int  forestID1=rmCreateArea("forest along trade route top");
      rmSetAreaSize(forestID1, 0.02, 0.02);
      rmSetAreaLocation(forestID1, 0.72, 0.5);
      rmSetAreaForestType(forestID1, "caribbean palm forest");
	  rmAddAreaInfluenceSegment(forestID1, 0.72, 0.24, 0.72, 0.76);
      rmSetAreaMinBlobs(forestID1, 1);
      rmSetAreaMaxBlobs(forestID1, 1);
	  rmSetAreaForestDensity(forestID1, 0.80);
	  rmSetAreaForestClumpiness(forestID1, 0.5);
	  rmSetAreaForestUnderbrush(forestID1, 0.9);
      rmSetAreaSmoothDistance(forestID1, 3);
      rmSetAreaCoherence(forestID1, 0.95);
	  rmAddAreaConstraint(forestID1, avoidSocket);
	  rmAddAreaConstraint(forestID1, shortAvoidTradeRoute);
      rmAddAreaToClass(forestID1, rmClassID("classForest"));
      rmBuildArea(forestID1);
      
	  
	  	  int  forestID1A=rmCreateArea("forest along trade route top A");
      rmSetAreaSize(forestID1A, 0.02, 0.02);
      rmSetAreaLocation(forestID1A, 0.8, 0.5);
      rmSetAreaForestType(forestID1A, "caribbean palm forest");
	  rmAddAreaInfluenceSegment(forestID1A, 0.8, 0.24, 0.8, 0.76);
      rmSetAreaMinBlobs(forestID1A, 1);
      rmSetAreaMaxBlobs(forestID1A, 1);
	  rmSetAreaForestDensity(forestID1A, 0.80);
	  rmSetAreaForestClumpiness(forestID1A, 0.5);
	  rmSetAreaForestUnderbrush(forestID1A, 0.9);
      rmSetAreaSmoothDistance(forestID1A, 3);
      rmSetAreaCoherence(forestID1A, 0.95);
	  rmAddAreaConstraint(forestID1A, avoidSocket);
	  rmAddAreaConstraint(forestID1A, shortAvoidTradeRoute);
      rmAddAreaToClass(forestID1A, rmClassID("classForest"));
      rmBuildArea(forestID1A);
  }
  else
  {
	  	  int  forestIDT=rmCreateArea("forest IDT for bottom traderoute team");
      rmSetAreaSize(forestIDT, 0.04, 0.04);
      rmSetAreaLocation(forestIDT, 0.22, 0.4);
      rmSetAreaForestType(forestIDT, "caribbean palm forest");
	  rmAddAreaInfluenceSegment(forestIDT, 0.22, 0.24, 0.22, 0.76);
      rmSetAreaMinBlobs(forestIDT, 1);
      rmSetAreaMaxBlobs(forestIDT, 1);
	  rmSetAreaForestDensity(forestIDT, 0.6);
	  rmSetAreaForestClumpiness(forestIDT, 0.5);
	  rmSetAreaForestUnderbrush(forestIDT, 0.9);
      rmSetAreaSmoothDistance(forestIDT, 5);
      rmSetAreaCoherence(forestIDT, 0.95);
	  rmAddAreaConstraint(forestIDT, avoidSocket);
      rmAddAreaToClass(forestIDT, rmClassID("classForest"));
      rmBuildArea(forestIDT);
	  
	  int  forestID1T=rmCreateArea("forest ID1 for top traderoute team ");
      rmSetAreaSize(forestID1T, 0.04, 0.04);
      rmSetAreaLocation(forestID1T, 0.78, 0.6);
      rmSetAreaForestType(forestID1T, "caribbean palm forest");
	  rmAddAreaInfluenceSegment(forestID1T, 0.78, 0.24, 0.78, 0.76);
      rmSetAreaMinBlobs(forestID1T, 1);
      rmSetAreaMaxBlobs(forestID1T, 1);
	  rmSetAreaForestDensity(forestID1T, 0.6);
	  rmSetAreaForestClumpiness(forestID1T, 0.5);
	  rmSetAreaForestUnderbrush(forestID1T, 0.9);
      rmSetAreaSmoothDistance(forestID1T, 5);
      rmSetAreaCoherence(forestID1T, 0.95);
	  rmAddAreaConstraint(forestID1T, avoidSocket);
      rmAddAreaToClass(forestID1T, rmClassID("classForest"));
      rmBuildArea(forestID1T); 
  }

	
	// Text
	rmSetStatusText("",0.50);
	int numTries = -1;
	int failCount = -1;
	
	
// -------------------------------------------------------------- Let's place some design (oasis and forest ) ---------------------------------------------
// OMG these fucking oasis were hard to code

   int Square = rmCreateBoxConstraint("the big swimming pool",0.37,0.32,0.62,0.68,00.1);

   if (cNumberNonGaiaPlayers <4)
   {numTries = 170;}
   else if (cNumberNonGaiaPlayers <6)
   {numTries =210;}
   else
   {numTries =215;}   

   int coreOneID=rmCreateArea("core one");
   rmSetAreaSize(coreOneID, 0.2, 0.2);
   rmSetAreaLocation(coreOneID, 0.5, 0.5);
   rmSetAreaWaterType(coreOneID, "Constantinople");
   rmAddAreaToClass(coreOneID, rmClassID("center"));
   rmSetAreaBaseHeight(coreOneID, 4.2);
   rmAddAreaConstraint(coreOneID, Square);
   rmBuildArea(coreOneID);  
	 
  for (i=0; < numTries)
  {
	int IslandB=rmCreateArea("trade island B"+i);
	rmSetAreaSize(IslandB, 0.0015, 0.0015);
	rmSetAreaLocation(IslandB, rmRandFloat(0.25,0.75), rmRandFloat (0.25,0.75));
	rmAddAreaToClass(IslandB, classGreatLake);
	rmSetAreaBaseHeight(IslandB, 4.0);
		rmSetAreaTerrainType(IslandB, "sonora\ground2_son");
	rmSetAreaMinBlobs(IslandB, 3);
	rmSetAreaMaxBlobs(IslandB, 5);
	rmSetAreaMinBlobDistance(IslandB, 4);
	rmSetAreaMaxBlobDistance(IslandB, 7);
	rmSetAreaSmoothDistance(IslandB, 1);
	rmSetAreaCoherence(IslandB, 0.01);
	rmBuildArea(IslandB); 
  }
 
    int nearShore1 = rmCreateTerrainMaxDistanceConstraint("stay near water1", "water", true, 6.0);

      // Create a forest
	  for (i=0; <15)
	  {	  
	  int forestOneID=rmCreateArea("forest one"+i);
      rmSetAreaSize(forestOneID, 0.5, 0.5);
      rmSetAreaForestType(forestOneID, "caribbean palm forest");
      rmAddAreaToClass(forestOneID, rmClassID("center"));
      rmSetAreaMinBlobs(forestOneID, 5);
      rmSetAreaMaxBlobs(forestOneID, 10);
	  rmSetAreaForestDensity(forestOneID, 0.15);
	  rmSetAreaForestClumpiness(forestOneID, 0.01);	  
	  rmSetAreaForestUnderbrush(forestOneID, 0.0);
      rmSetAreaCoherence(forestOneID, 0.01);
      rmAddAreaToClass(forestOneID, rmClassID("classForest"));
	  rmAddAreaConstraint(forestOneID, nearShore1);
      rmBuildArea(forestOneID);
	  }

	    int forestPlayer=rmCreateArea("forest Player");
      rmSetAreaSize(forestPlayer, 0.08, 0.08);
      rmSetAreaForestType(forestPlayer, "caribbean palm forest");
      rmSetAreaMinBlobs(forestPlayer, 1);
      rmSetAreaMaxBlobs(forestPlayer, 1);
	  rmSetAreaForestDensity(forestPlayer, 0.6);
	  rmSetAreaForestClumpiness(forestPlayer, 0.1);
	  rmSetAreaForestUnderbrush(forestPlayer, 0.9);
      rmSetAreaSmoothDistance(forestPlayer, 5);
      rmSetAreaCoherence(forestPlayer, 0.95);
	  rmAddAreaConstraint(forestPlayer, staySW1);
	  rmAddAreaConstraint(forestPlayer, avoidCliffshort);
      rmAddAreaToClass(forestPlayer, rmClassID("classForest"));
      rmBuildArea(forestPlayer);	  
	  
	 	  
	    int forestPlayer1=rmCreateArea("forest Player1");
      rmSetAreaSize(forestPlayer1, 0.08, 0.08);
      rmSetAreaForestType(forestPlayer1, "caribbean palm forest");
      rmSetAreaMinBlobs(forestPlayer1, 1);
      rmSetAreaMaxBlobs(forestPlayer1, 1);
	  rmSetAreaForestDensity(forestPlayer1, 0.6);
	  rmSetAreaForestClumpiness(forestPlayer1, 0.1);
	  rmSetAreaForestUnderbrush(forestPlayer1, 0.9);
      rmSetAreaSmoothDistance(forestPlayer1, 5);
      rmSetAreaCoherence(forestPlayer1, 0.95);
	  rmAddAreaConstraint(forestPlayer1, stayNE1);
	  rmAddAreaConstraint(forestPlayer1, avoidCliffshort);
      rmAddAreaToClass(forestPlayer1, rmClassID("classForest"));
      rmBuildArea(forestPlayer1);
	  
	  	  int forestDef=rmCreateArea("forest Def");
      rmSetAreaSize(forestDef, 0.015, 0.015);
      rmSetAreaLocation(forestDef, 0.5, 0.76);
      rmSetAreaForestType(forestDef, "caribbean palm forest");
      rmSetAreaMinBlobs(forestDef, 1);
      rmSetAreaMaxBlobs(forestDef, 1);
	  rmSetAreaForestDensity(forestDef, 0.5);
	  rmSetAreaForestClumpiness(forestDef, 0.5);
	  rmSetAreaForestUnderbrush(forestDef, 0.9);
      rmSetAreaSmoothDistance(forestDef, 5);
      rmSetAreaCoherence(forestDef, 0.95);
	  rmAddAreaConstraint(forestDef, avoidSocket);
      rmAddAreaToClass(forestDef, rmClassID("classForest"));
	  rmAddAreaToClass(forestDef, rmClassID("GoldMineL"));
	  rmAddAreaToClass(forestDef, rmClassID("GoldMineR"));
      rmBuildArea(forestDef);
	  
	  int forestDef1=rmCreateArea("forest Def1");
      rmSetAreaSize(forestDef1, 0.015, 0.015);
      rmSetAreaLocation(forestDef1, 0.5, 0.24);
      rmSetAreaForestType(forestDef1, "caribbean palm forest");
      rmSetAreaMinBlobs(forestDef1, 1);
      rmSetAreaMaxBlobs(forestDef1, 1);
	  rmSetAreaForestDensity(forestDef1, 0.5);
	  rmSetAreaForestClumpiness(forestDef1, 0.5);
	  rmSetAreaForestUnderbrush(forestDef1, 0.9);
      rmSetAreaSmoothDistance(forestDef1, 5);
      rmSetAreaCoherence(forestDef1, 0.95);
	  rmAddAreaConstraint(forestDef1, avoidSocket);
	  rmAddAreaToClass(forestDef1, rmClassID("GoldMineL"));
	  rmAddAreaToClass(forestDef1, rmClassID("GoldMineR"));
      rmAddAreaToClass(forestDef1, rmClassID("classForest"));
      rmBuildArea(forestDef1);
	  

	int avoidForestPlayer = rmCreateAreaDistanceConstraint("avoid the edge forest", forestPlayer, 5.0);
	int avoidForestPlayer1 = rmCreateAreaDistanceConstraint("avoid the edge forest1", forestPlayer1, 5.0);
	  
	  
	  
// Define and place forests - north and south
	int forestTreeID = 0;

   int desertID = rmCreateArea("color patch");
   rmSetAreaLocation(desertID, 0.5, 0.5); 
   rmSetAreaWarnFailure(desertID, false);
   rmSetAreaSize(desertID, 0.99, 0.99);
   rmSetAreaCoherence(desertID, 0.99);
   rmSetAreaElevationType(desertID, cElevTurbulence);
   rmSetAreaMix(desertID, "sonora_dirt");     
   rmBuildArea(desertID);
   
   
   if (cNumberNonGaiaPlayers==2)
   {
   int newGrass = rmCreateArea("new grass");
   rmSetAreaLocation(newGrass, 0.35, 0.15); 
   rmSetAreaWarnFailure(newGrass, false);
   rmSetAreaSize(newGrass,rmXMetersToFraction(3), rmXMetersToFraction(3));
   rmSetAreaCoherence(newGrass, 0.3);
   rmSetAreaSmoothDistance(newGrass, 15);
   rmSetAreaObeyWorldCircleConstraint(newGrass, false);
   rmSetAreaTerrainType(newGrass, "borneo\shoreline1_borneo");
   rmBuildArea(newGrass);
   
   int newGrass1 = rmCreateArea("new grass1");
   rmSetAreaLocation(newGrass1, 0.65, 0.85); 
   rmSetAreaWarnFailure(newGrass1, false);
   rmSetAreaSize(newGrass1,rmXMetersToFraction(3), rmXMetersToFraction(3));
   rmSetAreaCoherence(newGrass1, 0.3);
   rmSetAreaSmoothDistance(newGrass1, 15);
   rmSetAreaObeyWorldCircleConstraint(newGrass1, false);
   rmSetAreaTerrainType(newGrass1, "borneo\shoreline1_borneo");
   rmBuildArea(newGrass1);
   
   int stayGrass = rmCreateAreaConstraint("stay grass", newGrass);
   int stayGrass1 = rmCreateAreaConstraint("stay grass1", newGrass1);
   
   for (i=0; <5)
   {
    int gunFloor = rmCreateObjectDef("gunFloorA 1"+i);
	rmAddObjectDefItem(gunFloor, "PropsColony", 1, 1.0);
	rmSetObjectDefMinDistance(gunFloor, avoidAll);
	rmSetObjectDefMinDistance(gunFloor, 0.0);
	rmSetObjectDefMaxDistance(gunFloor, 15);
	rmPlaceObjectDefAtLoc(gunFloor, 0, 0.35, 0.15);
   }
      for (i=0; <2)
   {
    int gunFloor2 = rmCreateObjectDef("gunFloorB 2"+i);
	rmAddObjectDefItem(gunFloor2, "Skulls", 1, 1.0);
	rmSetObjectDefMinDistance(gunFloor2, avoidAll);
		rmSetObjectDefMinDistance(gunFloor, 0.0);
	rmSetObjectDefMaxDistance(gunFloor2, 15);
	rmPlaceObjectDefAtLoc(gunFloor2, 0, 0.35, 0.15);
   }
      for (i=0; <5)
   {
    int gunFloor3 = rmCreateObjectDef("gunFloorC 3"+i);
	rmAddObjectDefItem(gunFloor3, "PropsColony", 1, 1.0);
	rmSetObjectDefMinDistance(gunFloor3, avoidAll);
		rmSetObjectDefMinDistance(gunFloor3, 0.0);
	rmSetObjectDefMaxDistance(gunFloor3, 15);
	rmPlaceObjectDefAtLoc(gunFloor3, 0, 0.65, 0.85);
   }
      for (i=0; <2)
   {
    int gunFloor4 = rmCreateObjectDef("gunFloorD 4"+i);
	rmAddObjectDefItem(gunFloor4, "Skulls", 1, 1.0);
	rmSetObjectDefMinDistance(gunFloor4, avoidAll);
		rmSetObjectDefMinDistance(gunFloor4, 0.0);
	rmSetObjectDefMaxDistance(gunFloor4, 15);
	rmPlaceObjectDefAtLoc(gunFloor4, 0, 0.65, 0.85);
   }
   }
 // ----------------------------------------------------- Players and blabla starting stuff -------------------------------------------------
 
	int avoidForestDef=rmCreateClassDistanceConstraint("avoid forest defensive", rmClassID("GoldMineL"), 5.0); // this is here for fixing the second hunt location
	int mineOK=rmCreatePieConstraint("mine OK", 0.5, 0.5, rmZFractionToMeters(0.36), rmZFractionToMeters(0.4), rmDegreesToRadians(0), rmDegreesToRadians(360));

	float bonusTeam =0;
if (cNumberNonGaiaPlayers == 4)
	bonusTeam = 0.02;
else if (cNumberNonGaiaPlayers == 6)
	bonusTeam = 0.03;
else if (cNumberNonGaiaPlayers == 8)
	bonusTeam = 0.04;

if (cNumberTeams ==2)
{
    if (rmRandFloat(0,1)<0.5)
    {
		rmSetPlacementTeam(0);
		rmSetPlacementSection(0.45 - bonusTeam, 0.55 + bonusTeam); // 0.5
		rmSetTeamSpacingModifier(0.25);
		rmPlacePlayersCircular(0.36, 0.37, 0);
			
		rmSetPlacementTeam(1);
		rmSetPlacementSection(0.94 - bonusTeam, 0.05 + bonusTeam); // 0.5
		rmSetTeamSpacingModifier(0.25);
		rmPlacePlayersCircular(0.36, 0.37, 0);
    }
	else
	{	
		rmSetPlacementTeam(1);
		rmSetPlacementSection(0.45 - bonusTeam, 0.55 + bonusTeam); // 0.5
		rmSetTeamSpacingModifier(0.25);
		rmPlacePlayersCircular(0.36, 0.37, 0);
			
		rmSetPlacementTeam(0);
		rmSetPlacementSection(0.94 - bonusTeam, 0.05 + bonusTeam); // 0.5
		rmSetTeamSpacingModifier(0.25);
		rmPlacePlayersCircular(0.36, 0.37, 0);
	}
}
else
{
	rmPlacePlayer(1, 0.67, 0.18);
	rmPlacePlayer(4, 0.33, 0.18);		
	rmPlacePlayer(5, 0.33, 0.82);
	rmPlacePlayer(2, 0.67, 0.82);
	rmPlacePlayer(3, 0.1, 0.35);
	rmPlacePlayer(6, 0.9, 0.65);
	rmPlacePlayer(7, 0.1, 0.65);
	rmPlacePlayer(8, 0.9, 0.35);	
}	
	
		
	int startingTCID= rmCreateObjectDef("startingTC");
	if (rmGetNomadStart())
		{
			rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 0.0);
		}
		else
		{
            rmAddObjectDefItem(startingTCID, "townCenter", 1, 0.0);
		}
	rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
	rmAddObjectDefConstraint(startingTCID, avoidTradeRoute);
	rmAddObjectDefToClass(startingTCID, rmClassID("player"));


	int startingUnits = rmCreateStartingUnitsObjectDef(5.0);
	rmSetObjectDefMinDistance(startingUnits, 10.0);
	rmSetObjectDefMaxDistance(startingUnits, 15.0);
	rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

	int StartAreaTreeID=rmCreateObjectDef("starting trees");
	rmAddObjectDefItem(StartAreaTreeID, "TreeCaribbean", 2, 4.0);
	rmSetObjectDefMinDistance(StartAreaTreeID, 10);
	rmSetObjectDefMaxDistance(StartAreaTreeID, 18);
	rmAddObjectDefConstraint(StartAreaTreeID, avoidStartResource);
	rmAddObjectDefConstraint(StartAreaTreeID, shortAvoidImpassableLand);
	rmAddObjectDefConstraint(StartAreaTreeID, avoidTradeRoute);
	rmAddObjectDefConstraint(StartAreaTreeID, shortAvoidCoin);
	rmAddObjectDefConstraint(StartAreaTreeID, shortAvoidStartingUnits);


	int StartElkID=rmCreateObjectDef("starting moose");
	rmAddObjectDefItem(StartElkID, "pronghorn", 6, 4.0);
	rmSetObjectDefCreateHerd(StartElkID, true);
	rmSetObjectDefCreateHerd(StartElkID, true);
	rmSetObjectDefMinDistance(StartElkID, 13);
	rmSetObjectDefMaxDistance(StartElkID, 15);
	rmAddObjectDefConstraint(StartElkID, avoidStartResource);
	rmAddObjectDefConstraint(StartElkID, shortAvoidCoin);
	rmAddObjectDefConstraint(StartElkID, shortAvoidImpassableLand);
	
	int StartElkID1=rmCreateObjectDef("starting moose1");
	rmAddObjectDefItem(StartElkID1, "ypIbex", 12, 9.0);
	rmSetObjectDefCreateHerd(StartElkID1, true);
	rmSetObjectDefCreateHerd(StartElkID1, true);
	if (cNumberNonGaiaPlayers<6)
	{
	rmSetObjectDefMinDistance(StartElkID1, 36);
	rmSetObjectDefMaxDistance(StartElkID1, 38);
	}
	else{
	rmSetObjectDefMinDistance(StartElkID1, 20);
	rmSetObjectDefMaxDistance(StartElkID1, 24);		
	}
	if(cNumberNonGaiaPlayers==2)
	{
	rmAddObjectDefConstraint(StartElkID1, avoidTradeRouteFar);
	}
	rmAddObjectDefConstraint(StartElkID1, ypIbexConstraint);
	rmAddObjectDefConstraint(StartElkID1, shortAvoidImpassableLand);
	rmAddObjectDefConstraint(StartElkID1, avoidForestPlayer);
	rmAddObjectDefConstraint(StartElkID1, avoidForestPlayer1);
	rmAddObjectDefConstraint(StartElkID1, avoidAll);

	int startSilverID = rmCreateObjectDef("player silver");
	rmAddObjectDefItem(startSilverID, "mine", 1, 5.0);
	rmSetObjectDefMinDistance(startSilverID, 15.0);
	rmSetObjectDefMaxDistance(startSilverID, 17.0);
	rmAddObjectDefConstraint(startSilverID, avoidStartResource);
	rmAddObjectDefConstraint(startSilverID, avoidSocket);
	rmAddObjectDefConstraint(startSilverID, shortAvoidTradeRoute);
	rmAddObjectDefConstraint(startSilverID, shortAvoidImpassableLand);
	rmAddObjectDefConstraint(startSilverID, avoidCoin1);
	
		int startSilverID1 = rmCreateObjectDef("player silver1");
	rmAddObjectDefItem(startSilverID1, "mine", 1, 0.0);
	rmSetObjectDefMinDistance(startSilverID1, 34.0);
	rmSetObjectDefMaxDistance(startSilverID1, 36.0);
	rmAddObjectDefConstraint(startSilverID1, avoidTradeRouteFar);
	rmAddObjectDefConstraint(startSilverID1, mineOK);
	 
	 	int playerNuggetID=rmCreateObjectDef("player nugget");
	rmAddObjectDefItem(playerNuggetID, "nugget", 1, 0.0);
	rmAddObjectDefToClass(playerNuggetID, rmClassID("startingUnit"));
    rmSetObjectDefMinDistance(playerNuggetID, 23.0);
    rmSetObjectDefMaxDistance(playerNuggetID, 32.0);
	rmAddObjectDefConstraint(playerNuggetID, avoidNugget1);
	rmAddObjectDefConstraint(playerNuggetID, avoidTradeRoute);
	rmAddObjectDefConstraint(playerNuggetID, avoidSocket);
	rmAddObjectDefConstraint(playerNuggetID, avoidAll);

	for(i=1; <cNumberPlayers)
	{
		rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i),);
    
    if(ypIsAsian(i) && rmGetNomadStart() == false)
	{
      rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
    }
		rmSetNuggetDifficulty(1, 1);
		if (cNumberNonGaiaPlayers ==2)
    	{rmPlaceObjectDefAtLoc(startSilverID1, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	    rmPlaceObjectDefAtLoc(startSilverID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		}
		rmPlaceObjectDefAtLoc(StartElkID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartElkID1, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(playerNuggetID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(playerNuggetID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
        rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));	

	}

if (cNumberNonGaiaPlayers>2) // Fair mines for team
{
int mineb=rmAddFairLoc("TownCenter", false, false, 15, 17, 12, 5); 
	if(rmPlaceFairLocs())
	{
	mineb=rmCreateObjectDef("mine behind");
	rmAddObjectDefItem(mineb, "mine", 1, 0.0);
	for(i=1; <cNumberNonGaiaPlayers+1)
		{
	for(j=0; <rmGetNumberFairLocs(i))
			{
	rmPlaceObjectDefAtLoc(mineb, i, rmFairLocXFraction(i, j), rmFairLocZFraction(i, j), 1);
			}
		}
	}

int minef=rmAddFairLoc("TownCenter", true, true, 24, 27, 12, 5);
	if(rmPlaceFairLocs())
	{
	minef=rmCreateObjectDef("forward mine");
	rmAddObjectDefItem(minef, "mine", 1, 0.0);
	for(i=1; <cNumberNonGaiaPlayers+1)
	{
	for(j=1; <rmGetNumberFairLocs(i))
	{
	rmPlaceObjectDefAtLoc(minef, i, rmFairLocXFraction(i, j), rmFairLocZFraction(i, j), 1);
	}
	}
	}
}  

// --------------------------------------------- Ressources  -----------------------------------------------------------

// ********************************************* mines *****************************************************************

// This is all the area in order to get symetrical mines on the map. 3 mines on the left side, 3 mines on the right side.
   int CircleLeft = rmCreatePieConstraint("circle Constraint left", 0.4, 0.5, rmZFractionToMeters(0.1), rmZFractionToMeters(0.5), rmDegreesToRadians(0), rmDegreesToRadians(360));
   int CircleRight = rmCreatePieConstraint("circle Constraint right", 0.6, 0.5, rmZFractionToMeters(0.1), rmZFractionToMeters(0.5), rmDegreesToRadians(0), rmDegreesToRadians(360));

   int stayLeft = rmCreateBoxConstraint("left mines",0.0,0.0,0.4,1.0,0.01);
   int stayRight = rmCreateBoxConstraint("right mines",0.6,0.0,1.0,1.0,0.01);
   int SquareLeftSide = rmCreateBoxConstraint("left middle mine",0.0,0.32,0.4,0.68,0.01);
   int SquareRightSide = rmCreateBoxConstraint("right middle mine",1.0,0.32,0.6,0.68,0.01);
   int avoidCoinR=rmCreateClassDistanceConstraint("avoid gold coinR", rmClassID("GoldMineR"), 60.0);
   int avoidCoinL=rmCreateClassDistanceConstraint("avoid gold coinL", rmClassID("GoldMineL"), 60.0);

   int LeftSide = rmCreateArea("left side");
   rmSetAreaLocation(LeftSide, 0.1, 0.47);
   rmSetAreaSize(LeftSide, 0.2, 0.2);
   rmSetAreaCoherence(LeftSide, 0.95);
   rmAddAreaConstraint(LeftSide, SquareLeftSide);
   rmAddAreaConstraint(LeftSide, CircleLeft);
   //rmSetAreaForestType(LeftSide, "caribbean palm forest"); // just to check how look the area that mines need to avoid
   rmBuildArea(LeftSide);
   
      int RightSide = rmCreateArea("right side");
   rmSetAreaLocation(RightSide, 0.9, 0.53);
   rmSetAreaSize(RightSide, 0.15, 0.15);
   rmSetAreaCoherence(RightSide, 0.95);
   rmAddAreaConstraint(RightSide, SquareRightSide);
   rmAddAreaConstraint(RightSide,CircleRight);
  //  rmSetAreaForestType(RightSide, "caribbean palm forest"); // just to check how look the area that mines need to avoid
   rmBuildArea(RightSide);
   
   int avoidLeftSide = rmCreateAreaDistanceConstraint("avoid square left", LeftSide, 0.01); // Left mines avoid this area
   int avoidRightSide = rmCreateAreaDistanceConstraint("avoid square right", RightSide, 0.01); // Right mines avoid this area


// here we go   
   if (cNumberNonGaiaPlayers==2)
   {
   for(i=0; < 3)
	{	
	int silverID = rmCreateObjectDef("silver left"+i);
	rmAddObjectDefItem(silverID, "mine", 1, 0);
	rmSetObjectDefMinDistance(silverID, 0.0);
	rmSetObjectDefMaxDistance(silverID, rmXFractionToMeters(0.45));
	rmAddObjectDefToClass(silverID, rmClassID("GoldMineL"));
	rmAddObjectDefConstraint(silverID, avoidAll);
    rmAddObjectDefConstraint(silverID, avoidCoinL);
	rmAddObjectDefConstraint(silverID, avoidSocket);
	rmAddObjectDefConstraint(silverID, avoidTradeRoute);
	rmAddObjectDefConstraint(silverID, avoidCliffshort);
	rmAddObjectDefConstraint(silverID, avoidTownCenterFar);
	rmAddObjectDefConstraint(silverID, avoidImpassableLand);
	rmAddObjectDefConstraint(silverID, avoidLeftSide);
	rmAddObjectDefConstraint(silverID, stayLeft);
	rmPlaceObjectDefAtLoc(silverID, 0, 0.5, 0.5);
	}
	
	   for(i=0; < 3)
	{	
	int silverID1 = rmCreateObjectDef("silver right"+i);
	rmAddObjectDefItem(silverID1, "mine", 1, 0);
	rmSetObjectDefMinDistance(silverID1, 0.0);
	rmSetObjectDefMaxDistance(silverID1, rmXFractionToMeters(0.45));
	rmAddObjectDefToClass(silverID1, rmClassID("GoldMineR"));
	rmAddObjectDefConstraint(silverID1, avoidAll);
	rmAddObjectDefConstraint(silverID1, avoidSocket);
	rmAddObjectDefConstraint(silverID1, avoidCoinR);
	rmAddObjectDefConstraint(silverID1, avoidTradeRoute);
	rmAddObjectDefConstraint(silverID1, avoidCliffshort);
	rmAddObjectDefConstraint(silverID1, avoidTownCenterFar);
	rmAddObjectDefConstraint(silverID1, avoidImpassableLand);
	rmAddObjectDefConstraint(silverID1, avoidRightSide);
	rmAddObjectDefConstraint(silverID1, stayRight);
	rmPlaceObjectDefAtLoc(silverID1, 0, 0.5, 0.5);
	}
   }
   else
   {
	for(i=0; < cNumberNonGaiaPlayers*3)
		{
	int silverIDT = rmCreateObjectDef("silver for team"+i);
	rmAddObjectDefItem(silverIDT, "mine", 1, 0);
	rmSetObjectDefMinDistance(silverIDT, 0.0);
	rmSetObjectDefMaxDistance(silverIDT, rmXFractionToMeters(0.45));
	rmAddObjectDefToClass(silverIDT, rmClassID("GoldMineL"));
	rmAddObjectDefConstraint(silverIDT, avoidAll);
    rmAddObjectDefConstraint(silverIDT, avoidCoinL);
	rmAddObjectDefConstraint(silverIDT, avoidSocket);
	rmAddObjectDefConstraint(silverIDT, avoidTradeRoute);
	rmAddObjectDefConstraint(silverIDT, avoidCliffshort);
	rmAddObjectDefConstraint(silverIDT, avoidTownCenterFar);
	rmAddObjectDefConstraint(silverIDT, avoidImpassableLand);
	rmPlaceObjectDefAtLoc(silverIDT, 0, 0.5, 0.5);
		}
   }
   
//   ******************************************* hunts *****************************************
   if (cNumberNonGaiaPlayers == 2)
   {
    int pronghornIDLT=rmCreateObjectDef("pronghorn left top herd");
    rmAddObjectDefItem(pronghornIDLT, "pronghorn", rmRandInt(8,9), 6.0);
    rmSetObjectDefMinDistance(pronghornIDLT, 0.0);
    rmSetObjectDefMaxDistance(pronghornIDLT, 10.0);
	rmAddObjectDefConstraint(pronghornIDLT, avoidAll);
	rmAddObjectDefConstraint(pronghornIDLT, avoidCliffshort);
    rmSetObjectDefCreateHerd(pronghornIDLT, true);
	rmPlaceObjectDefAtLoc(pronghornIDLT, 0, 0.1, 0.7, 1);
	
    int pronghornIDLB=rmCreateObjectDef("ibex bottom herd");
    rmAddObjectDefItem(pronghornIDLB, "ypIbex", rmRandInt(8,9), 6.0);
    rmSetObjectDefMinDistance(pronghornIDLB, 0.0);
    rmSetObjectDefMaxDistance(pronghornIDLB, 10.0);
	rmAddObjectDefConstraint(pronghornIDLB, avoidAll);
    rmAddObjectDefConstraint(pronghornIDLB, avoidCliffshort);	
    rmSetObjectDefCreateHerd(pronghornIDLB, true);
	rmPlaceObjectDefAtLoc(pronghornIDLB, 0, 0.1, 0.2, 1);
	
    int pronghornIDLM=rmCreateObjectDef("pronghorn left middle");
    rmAddObjectDefItem(pronghornIDLM, "pronghorn", rmRandInt(8,9), 6.0);
    rmSetObjectDefMinDistance(pronghornIDLM, 0.0);
    rmSetObjectDefMaxDistance(pronghornIDLM, 7.0);
	rmAddObjectDefConstraint(pronghornIDLM, avoidAll);
    rmSetObjectDefCreateHerd(pronghornIDLM, true);
    rmAddObjectDefConstraint(pronghornIDLM, avoidCliffshort);
	rmPlaceObjectDefAtLoc(pronghornIDLM, 0, 0.1, 0.45, 1);

	int pronghornIDRT=rmCreateObjectDef("pronghorn right top");
    rmAddObjectDefItem(pronghornIDRT, "pronghorn", rmRandInt(8,9), 6.0);
    rmSetObjectDefMinDistance(pronghornIDRT, 0.0);
    rmSetObjectDefMaxDistance(pronghornIDRT, 10.0);
	rmAddObjectDefConstraint(pronghornIDRT, avoidAll);
	rmAddObjectDefConstraint(pronghornIDRT, avoidCliffshort);
    rmSetObjectDefCreateHerd(pronghornIDRT, true);
	rmPlaceObjectDefAtLoc(pronghornIDRT, 0, 0.9, 0.8, 1);
	
    int pronghornIDRB=rmCreateObjectDef("ibex right bottom");
    rmAddObjectDefItem(pronghornIDRB, "ypIbex", rmRandInt(8,9), 6.0);
    rmSetObjectDefMinDistance(pronghornIDRB, 0.0);
    rmSetObjectDefMaxDistance(pronghornIDRB, 10.0);
	rmAddObjectDefConstraint(pronghornIDRB, avoidAll);
	rmAddObjectDefConstraint(pronghornIDRB, avoidCliffshort);	
    rmSetObjectDefCreateHerd(pronghornIDRB, true);
	rmPlaceObjectDefAtLoc(pronghornIDRB, 0, 0.9, 0.3, 1);
	
    int pronghornIDRM=rmCreateObjectDef("pronghorn right middle");
    rmAddObjectDefItem(pronghornIDRM, "pronghorn", rmRandInt(8,9), 6.0);
    rmSetObjectDefMinDistance(pronghornIDRM, 0.0);
    rmSetObjectDefMaxDistance(pronghornIDRM, 7.0);
	rmAddObjectDefConstraint(pronghornIDRM, avoidAll);
	rmAddObjectDefConstraint(pronghornIDRM, avoidCliffshort);
    rmSetObjectDefCreateHerd(pronghornIDRM, true);
	rmPlaceObjectDefAtLoc(pronghornIDRM, 0, 0.9, 0.55, 1);
	
	int pronghornID2=rmCreateObjectDef("pronghorn herd2");
    rmAddObjectDefItem(pronghornID2, "ypIbex", rmRandInt(9,10), 6.0);
    rmSetObjectDefMinDistance(pronghornID2, 0.0);
    rmSetObjectDefMaxDistance(pronghornID2, 4.0);
	rmAddObjectDefConstraint(pronghornID2, avoidAll);  
    rmSetObjectDefCreateHerd(pronghornID2, true);
	rmPlaceObjectDefAtLoc(pronghornID2, 0, 0.5, 0.5, 1);
   }
   else
   {
	    int  pronghornID=rmCreateObjectDef("pronghorn for team");
   rmAddObjectDefItem(pronghornID, "pronghorn", rmRandInt(8,9), 6.0);
   rmSetObjectDefMinDistance(pronghornID, 0.0);
   rmSetObjectDefMaxDistance(pronghornID, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(pronghornID, avoidAll);
   rmAddObjectDefConstraint(pronghornID, ypIbexConstraint);
    rmAddObjectDefConstraint(pronghornID, ypIbexConstraint1);  
	rmAddObjectDefConstraint(pronghornID, avoidCliffshort);
	rmAddObjectDefConstraint(pronghornID, avoidTownCenterFar);
   rmSetObjectDefCreateHerd(pronghornID, true);
	rmPlaceObjectDefAtLoc(pronghornID, 0, 0.5, 0.5, cNumberNonGaiaPlayers*2);

  
  
   int pronghornID1=rmCreateObjectDef("ibex for team");
   rmAddObjectDefItem(pronghornID1, "ypIbex", rmRandInt(8,9), 6.0);
   rmSetObjectDefMinDistance(pronghornID1, 0.0);
   rmSetObjectDefMaxDistance(pronghornID1, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(pronghornID1, avoidAll);
   rmAddObjectDefConstraint(pronghornID1, ypIbexConstraint);
   rmAddObjectDefConstraint(pronghornID1, ypIbexConstraint1);   
	rmAddObjectDefConstraint(pronghornID1, avoidCliffshort);
	rmAddObjectDefConstraint(pronghornID1, avoidTownCenterFar);		
   rmSetObjectDefCreateHerd(pronghornID1, true);
	rmPlaceObjectDefAtLoc(pronghornID1, 0, 0.5, 0.5, cNumberNonGaiaPlayers*3);  
	   
   }
// -------------------------------------------------- Treasure time ---------------------------------------------------  
 
   	int nuggetID1= rmCreateObjectDef("nugget1"); 
	rmAddObjectDefItem(nuggetID1, "Nugget", 1, 0.0);
	rmSetObjectDefMinDistance(nuggetID1, 0.0);
	rmSetObjectDefMaxDistance(nuggetID1, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(nuggetID1, shortAvoidImpassableLand);
  	rmAddObjectDefConstraint(nuggetID1, avoidTradeRoute);
	rmAddObjectDefConstraint(nuggetID1, avoidSocket);
    rmAddObjectDefConstraint(nuggetID1, avoidTownCenterFar1);
	rmAddObjectDefConstraint(nuggetID1, avoidCliffshort);
  	rmAddObjectDefConstraint(nuggetID1, avoidAll);
  	rmAddObjectDefConstraint(nuggetID1, avoidNugget);	
	rmSetNuggetDifficulty(1, 2);
	rmPlaceObjectDefAtLoc(nuggetID1, 0, 0.5, 0.5, cNumberNonGaiaPlayers*3);
	
	   	int nuggetID= rmCreateObjectDef("nugget"); 
	rmAddObjectDefItem(nuggetID, "Nugget", 1, 0.0);
	rmSetObjectDefMinDistance(nuggetID, 0.0);
	rmSetObjectDefMaxDistance(nuggetID, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(nuggetID, shortAvoidImpassableLand);
  	rmAddObjectDefConstraint(nuggetID, avoidTradeRoute);
	rmAddObjectDefConstraint(nuggetID, avoidSocket);
    rmAddObjectDefConstraint(nuggetID, avoidTownCenterFar1);
	rmAddObjectDefConstraint(nuggetID, avoidCliffshort);
  	rmAddObjectDefConstraint(nuggetID, avoidAll);
	 rmAddObjectDefConstraint(nuggetID, avoidNugget);
	rmSetNuggetDifficulty(2, 2);
	rmPlaceObjectDefAtLoc(nuggetID, 0, 0.5, 0.5, cNumberNonGaiaPlayers);
	

// --------------------------------------------------------------- E N D ----------------------------------------------
	
	// Text
	rmSetStatusText("",1.0);

  
	}  

  
