// RM script of Tuscany
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Chooses the 1 native, always Goths

   // Native 1
   rmSetSubCiv(0, "Comanche");


   // Picks the map size
   int playerTiles = 11000;
   if (cNumberNonGaiaPlayers > 4)
      playerTiles = 10000;
   if (cNumberNonGaiaPlayers > 6)
      playerTiles = 8500;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses the side where teams start in team games
   int teamSide = rmRandInt(1,2);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("mongolia");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.08, 6, 0.4, 7.0);
   rmSetBaseTerrainMix("great plains grass");
   rmTerrainInitialize("nwterritory\ground_grass3_nwt", 1.0);
   rmSetMapType("grass");
   rmSetMapType("land");
   rmSetMapType("greatPlains");
   

   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   int classHillMiddle=rmDefineClass("hill middle");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("nuggets");
   rmDefineClass("natives");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));
   int edgeConstraint=rmCreatePieConstraint("hills stay between players", 0.5, 0.5, rmZFractionToMeters(0.30), rmZFractionToMeters(0.42), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int hillPlayerConstraint=rmCreateClassDistanceConstraint("hills stay away from players", classPlayer, 70.0);
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 50.0);
   int shortForestConstraint=rmCreateClassDistanceConstraint("forest vs. forest short", rmClassID("classForest"), 10.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 90.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 60.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 5.0);

   // Native avoidance
   int avoidNatives=rmCreateClassDistanceConstraint("things avoids natives", rmClassID("natives"), 5.0);

   // Area avoidance
   int middleHillConstraint=rmCreateClassDistanceConstraint("avoid middle hill", classHillMiddle, 10.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 40.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);

   // Trade route avoidance
   int avoidTradeRoute=rmCreateTradeRouteDistanceConstraint("trade route", 5.0);
   int avoidTradeSockets=rmCreateTypeDistanceConstraint("avoid trade sockets", "SocketTradeRoute", 5.0);


   // Text
   rmSetStatusText("",0.10);


   // Place players - in team and FFA games

   // Player placement if FFA (Free For All) - place players in circle
   if(cNumberTeams > 2)
   {
      rmSetTeamSpacingModifier(0.7);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);
   }

   // Player placement if teams - place teams in circle to apart from each other
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      if (rmGetNumberPlayersOnTeam(0) == 2)
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.05, 0.20);
         else if (teamSide == 2)
            rmSetPlacementSection(0.55, 0.70);
      }
      else
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.00, 0.25);
         else if (teamSide == 2)
            rmSetPlacementSection(0.50, 0.75);
      }
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);

      rmSetPlacementTeam(1);
      if (rmGetNumberPlayersOnTeam(1) == 2)
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.55, 0.70);
         else if (teamSide == 2)
            rmSetPlacementSection(0.05, 0.20);
      }
      else
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.50, 0.75);
         else if (teamSide == 2)
            rmSetPlacementSection(0.00, 0.25);
      }
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(900);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmSetAreaBaseHeight(id, 7.0);
      rmSetAreaCoherence(id, 0.5);
      rmSetAreaHeightBlend(id, 2);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }


   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.20);


   // Text
   rmSetStatusText("",0.30);


   // Define and place the middle hill

   int hillID=rmCreateArea("Hill");
   rmSetAreaSize(hillID, 0.02, 0.02);
   rmSetAreaCoherence(hillID, 0.6);
   rmSetAreaLocation(hillID, 0.5, 0.5);
   rmAddAreaToClass(hillID, classHillMiddle);
   rmSetAreaBaseHeight(hillID, 6.0);
   rmSetAreaMinBlobs(hillID, 1);
   rmSetAreaMaxBlobs(hillID, 5);
   rmSetAreaHeightBlend(hillID, 2);
   rmSetAreaMinBlobDistance(hillID, 1);
   rmSetAreaMaxBlobDistance(hillID, 5);
   rmSetAreaSmoothDistance(hillID, 5);
   rmBuildArea(hillID);


   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmSetObjectDefMinDistance(startingTCID, 5.0);
   rmSetObjectDefMaxDistance(startingTCID, 10.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "TreeMadrone", 6, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 12);
   rmSetObjectDefMaxDistance(startAreaTreeID, 16);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startDeerID=rmCreateObjectDef("starting deer");
   rmAddObjectDefItem(startDeerID, "deer", 6, 4.0);
   rmSetObjectDefCreateHerd(startDeerID, true);
   rmSetObjectDefMinDistance(startDeerID, 14);
   rmSetObjectDefMaxDistance(startDeerID, 18);
   rmAddObjectDefConstraint(startDeerID, avoidStartResource);
   rmAddObjectDefConstraint(startDeerID, avoidImpassableLand);
   rmAddObjectDefConstraint(startDeerID, shortAvoidStartingUnits);

   int startCopperID=rmCreateObjectDef("player copper");
   rmAddObjectDefItem(startCopperID, "MineCopper", 1, 5.0);
   rmSetObjectDefMinDistance(startCopperID, 14);
   rmSetObjectDefMaxDistance(startCopperID, 18);
   rmAddObjectDefConstraint(startCopperID, avoidStartResource);
   rmAddObjectDefConstraint(startCopperID, avoidImpassableLand);
   rmAddObjectDefConstraint(startCopperID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startDeerID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startCopperID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
   }


   // Text
   rmSetStatusText("",0.40);


   // Define and place the 1 native to land, always Goths

   int nativeVillageType = rmRandInt(1,5);
   int nativeVillageID = rmCreateGrouping("goths village", "native comanche village "+nativeVillageType);
   rmSetGroupingMinDistance(nativeVillageID, 0.0);
   rmSetGroupingMaxDistance(nativeVillageID, 10.0);
   rmAddGroupingToClass(nativeVillageID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillageID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.50);


   // Define and place trade routes

   // Create the trade routes
   int tradeRouteID = rmCreateTradeRoute();
   int socketID=rmCreateObjectDef("sockets to dock trade posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

   int tradeRoute2ID = rmCreateTradeRoute();
   int socket2ID=rmCreateObjectDef("sockets to dock trade posts 2");
   rmSetObjectDefTradeRouteID(socket2ID, tradeRoute2ID);

   // Define the sockets
   rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socketID, true);
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 12.0);

   rmAddObjectDefItem(socket2ID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socket2ID, true);
   rmSetObjectDefMinDistance(socket2ID, 0.0);
   rmSetObjectDefMaxDistance(socket2ID, 12.0);

   // Set the trade route waypoints
   rmAddTradeRouteWaypoint(tradeRouteID, 1.0, 0.3);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.6, 0.6, 8, 8);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.3, 1.0, 8, 8);
   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route");

   rmAddTradeRouteWaypoint(tradeRoute2ID, 0.65, 0.0);
   rmAddRandomTradeRouteWaypoints(tradeRoute2ID, 0.4, 0.4, 8, 8);
   rmAddRandomTradeRouteWaypoints(tradeRoute2ID, 0.0, 0.65, 8, 8);
   placedTradeRoute = rmBuildTradeRoute(tradeRoute2ID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route 2"); 
  
   // Place sockets to trade routes
   vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.35);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.65);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

   socketLoc = rmGetTradeRouteWayPoint(tradeRoute2ID, 0.35);
   rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRoute2ID, 0.65);
   rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc);


   // Define and place forests
	
   int numTries=10*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(150), rmAreaTilesToFraction(200));
      rmSetAreaForestType(forest, "california madrone forest");
      rmSetAreaForestDensity(forest, 0.9);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.5);
      rmSetAreaForestUnderbrush(forest, 0.3);
      rmSetAreaBaseHeight(forest, 1.0);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, avoidNatives);
      rmAddAreaConstraint(forest, middleHillConstraint);
      rmAddAreaConstraint(forest, avoidTradeRoute);
      rmAddAreaConstraint(forest, avoidTradeSockets);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.  
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }


   // Define and place straggler trees
   for (i=0; < 50)
   { 
      int stragglerTreeID=rmCreateObjectDef("straggler tree olive"+i);
      rmAddObjectDefItem(stragglerTreeID, "TreeMadrone", rmRandInt(1,5), 10.0);
      rmSetObjectDefMinDistance(stragglerTreeID, 0.0);
      rmSetObjectDefMaxDistance(stragglerTreeID, rmZFractionToMeters(0.47));
      rmAddObjectDefToClass(stragglerTreeID, rmClassID("classForest"));
      rmAddObjectDefConstraint(stragglerTreeID, somePlayerConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, avoidImpassableLand);
      rmAddObjectDefConstraint(stragglerTreeID, shortForestConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, avoidNatives);
      rmAddObjectDefConstraint(stragglerTreeID, avoidTradeRoute);
      rmAddObjectDefConstraint(stragglerTreeID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(stragglerTreeID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.70);


   // Define and place animals
   
   // Define and place deer herds to middle only
   int deerHerdID=rmCreateObjectDef("deer herd");
   rmAddObjectDefItem(deerHerdID, "deer", rmRandInt(7,8), 6.0);
   rmSetObjectDefCreateHerd(deerHerdID, true);
   rmSetObjectDefMinDistance(deerHerdID, 0.0);
   rmSetObjectDefMaxDistance(deerHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(deerHerdID, classAnimals);
   rmAddObjectDefConstraint(deerHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(deerHerdID, animalConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidNatives);
   rmAddObjectDefConstraint(deerHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(deerHerdID, avoidTradeSockets);
   numTries=1.5*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(deerHerdID, 0, 0.5, 0.5);

   // Define and place wolf herds everywhere
   int wolfHerdID=rmCreateObjectDef("wolf herd");
   rmAddObjectDefItem(wolfHerdID, "BighornSheep", rmRandInt(7,8), 6.0);
   rmSetObjectDefMinDistance(wolfHerdID, 0.0);
   rmSetObjectDefMaxDistance(wolfHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(wolfHerdID, classAnimals);
   rmAddObjectDefConstraint(wolfHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(wolfHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(wolfHerdID, animalConstraint);
   rmAddObjectDefConstraint(wolfHerdID, avoidNatives);
   rmAddObjectDefConstraint(wolfHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(wolfHerdID, avoidTradeSockets);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(wolfHerdID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.80);


   // Define and place copper mines

   int copperCount = 3*cNumberNonGaiaPlayers+2;

   for(i=0; < copperCount)
   {
      int copperID=rmCreateObjectDef("copper mine"+i);
      rmAddObjectDefItem(copperID, "MineCopper", 1, 0.0);
      rmSetObjectDefMinDistance(copperID, 0.0);
      rmSetObjectDefMaxDistance(copperID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(copperID, classMines);
      rmAddObjectDefConstraint(copperID, avoidImpassableLand);
      rmAddObjectDefConstraint(copperID, playerConstraint);
      rmAddObjectDefConstraint(copperID, avoidMines);
      rmAddObjectDefConstraint(copperID, avoidNatives);
      rmAddObjectDefConstraint(copperID, avoidTradeRoute);
      rmAddObjectDefConstraint(copperID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(copperID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.90);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidNatives);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidNatives);
   rmAddObjectDefConstraint(nugget2, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget2, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidNatives);
   rmAddObjectDefConstraint(nugget3, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget3, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, 1.5*cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidNatives);
   rmAddObjectDefConstraint(nugget4, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget4, avoidTradeSockets);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Text
   rmSetStatusText("",1.0);
}
