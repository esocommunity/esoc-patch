// RM script of Low Countries
// For K&B mod
// By AOE_Fan

// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);

   // Chooses the 3 natives - Franks, Frisians or Saxons randomly

   int nativeInt1 = rmRandInt(1,3);
   int nativeInt2 = rmRandInt(1,3);
   int nativeInt3 = rmRandInt(1,3);

   // Native 1
   if (nativeInt1 == 1)
      rmSetSubCiv(0, "Comanche");
   else if (nativeInt1 == 2)
      rmSetSubCiv(0, "Comanche");
   else
      rmSetSubCiv(0, "Comanche");

   // Native 2
   if (nativeInt2 == 1)
      rmSetSubCiv(1, "Comanche");
   else if (nativeInt2 == 2)
      rmSetSubCiv(1, "Comanche");
   else
      rmSetSubCiv(1, "Comanche");

   // Native 3
   if (nativeInt3 == 1)
      rmSetSubCiv(2, "Comanche");
   else if (nativeInt3 == 2)
      rmSetSubCiv(2, "Comanche");
   else
      rmSetSubCiv(2, "Comanche");


   // Picks the map size
   int playerTiles = 11000;
   if (cNumberNonGaiaPlayers > 4)
      playerTiles = 10000;
   if (cNumberNonGaiaPlayers > 6)
      playerTiles = 8500;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses the side where teams start in team games
   int teamSide = rmRandInt(1,2);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("bayou");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.10, 4, 0.4, 5.0);
   rmTerrainInitialize("carolinas\ground_grass3_car", 1.0);
   rmSetMapType("grass");
   rmSetMapType("water");
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   int classSwamp=rmDefineClass("swamp");
   int classLake=rmDefineClass("lake");
   int classPond=rmDefineClass("pond");
   int classIsland=rmDefineClass("island");
   rmDefineClass("startingUnit");
   rmDefineClass("nuggets");
   rmDefineClass("natives");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 75.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 65.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 5.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 40.0);

   // Area avoidance
   int lakeConstraint=rmCreateClassDistanceConstraint("avoid middle swamp", classLake, 10.0);
   int swampConstraint=rmCreateClassDistanceConstraint("avoid random swamps", classSwamp, 30.0);
   int pondConstraint=rmCreateClassDistanceConstraint("avoid ponds in middle", classPond, 30.0);
   int shortPondConstraint=rmCreateClassDistanceConstraint("avoid ponds in middle short", classPond, 10.0);
   int islandConstraint=rmCreateClassDistanceConstraint("avoid islands", classIsland, 30.0);
   int shortIslandConstraint=rmCreateClassDistanceConstraint("avoid islands short", classIsland, 10.0);
   int avoidLand = rmCreateTerrainDistanceConstraint("things avoid land", "land", true, 10.0);

   // Trade route avoidance
   int avoidTradeRoute=rmCreateTradeRouteDistanceConstraint("trade route", 5.0);
   int avoidTradeSockets=rmCreateTypeDistanceConstraint("avoid trade sockets", "SocketTradeRoute", 5.0);

   // Native avoidance
   int avoidNatives=rmCreateClassDistanceConstraint("things avoids natives", rmClassID("natives"), 5.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);


   // Text
   rmSetStatusText("",0.10);


   // Define and place the middle swamp

   int lakeID=rmCreateArea("Lake");
   rmSetAreaWaterType(lakeID, "bayou");
   rmAddAreaToClass(lakeID, classLake);
   rmSetAreaSize(lakeID, 0.2, 0.2);
   rmSetAreaCoherence(lakeID, 0.3);
   rmSetAreaLocation(lakeID, 0.5, 0.5);
   rmSetAreaBaseHeight(lakeID, 0.0);
   rmSetAreaMinBlobs(lakeID, 1);
   rmSetAreaMaxBlobs(lakeID, 1);
   rmSetAreaMinBlobDistance(lakeID, 1);
   rmSetAreaMaxBlobDistance(lakeID, 1);
   rmSetAreaSmoothDistance(lakeID, 20);
   rmAddAreaConstraint(lakeID, avoidImpassableLand);
   rmBuildArea(lakeID);


   // Define and place trade route - splits the map in middle, passing the middle swamp

   // Create the trade route
   int tradeRouteID = rmCreateTradeRoute();
   int socketID=rmCreateObjectDef("sockets to dock trade posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

   // Define the sockets
   rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socketID, true);
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 12.0);

   // Set the trade route waypoints
   rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.5);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 1.0, 0.5, 18, 18);
   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route"); 
  
   // Place sockets to trade route - to inland only!
   vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.1);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.9);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);


   // Define and place small random ponds inside the middle swamp

   float pondMin=rmAreaTilesToFraction(400);
   float pondMax=rmAreaTilesToFraction(450);

   for(i=1; <15)
   {
      int ponds=rmCreateArea("random pond inside swamp"+i, rmAreaID("Lake"));
      rmSetAreaSize(ponds, pondMin, pondMax);
      rmSetAreaWaterType(ponds, "bayou");
      rmAddAreaToClass(ponds, classPond);
      rmSetAreaBaseHeight(ponds, 0.0);
      rmSetAreaCoherence(ponds, 0.7);
      rmSetAreaSmoothDistance(ponds, 10);
      rmSetAreaMinBlobs(ponds, 5);
      rmSetAreaMaxBlobs(ponds, 10);
      rmSetAreaMinBlobDistance(ponds, 5);
      rmSetAreaMaxBlobDistance(ponds, 10);
      rmAddAreaConstraint(ponds, pondConstraint);
      rmAddAreaConstraint(ponds, avoidTradeRoute);
      rmAddAreaConstraint(ponds, avoidLand);
      rmBuildArea(ponds);
   }


   // Define and place random islands to middle swamp for natives

   float islandMin=rmAreaTilesToFraction(500);
   float islandMax=rmAreaTilesToFraction(550);

   for(i=1; <10)
   {
      int islands=rmCreateArea("islands"+i, rmAreaID("Lake"));
      rmSetAreaMix(islands, "carolina_grass"); 
      rmSetAreaSize(islands, islandMin, islandMax);
      rmAddAreaToClass(islands, classIsland);
      rmSetAreaBaseHeight(islands, 0.0);
      rmSetAreaCoherence(islands, 0.6);
      rmSetAreaMinBlobs(islands, 10);
      rmSetAreaMaxBlobs(islands, 15);
      rmSetAreaMinBlobDistance(islands, 10);
      rmSetAreaMaxBlobDistance(islands, 15);
      rmSetAreaSmoothDistance(islands, 5);
      rmAddAreaConstraint(islands, avoidTradeRoute);
      rmAddAreaConstraint(islands, islandConstraint);
      rmAddAreaConstraint(islands, avoidLand);
      rmBuildArea(islands);
   }


   // Text
   rmSetStatusText("",0.20);


   // Place players in FFA and team games

   // Player placement if FFA (Free For All) - place players in circle
   if(cNumberTeams > 2)
   {
      rmSetPlacementSection(0.10, 0.90);
      rmSetTeamSpacingModifier(0.7);
      rmPlacePlayersCircular(0.38, 0.38, 0);
   }

   // Player placement if teams - place teams in circle to apart from each other
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      if (teamSide == 1)
            rmSetPlacementSection(0.10, 0.40);
      else
            rmSetPlacementSection(0.60, 0.90);
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.38, 0.38, 0.0);

      rmSetPlacementTeam(1);
      if (teamSide == 1)
            rmSetPlacementSection(0.60, 0.90);
      else
            rmSetPlacementSection(0.10, 0.40);
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.38, 0.38, 0.0);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.30);
		

   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "TreeBayou", 10, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 14);
   rmSetObjectDefMaxDistance(startAreaTreeID, 18);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startDeerID=rmCreateObjectDef("starting deer");
   rmAddObjectDefItem(startDeerID, "deer", 6, 6.0);
   rmSetObjectDefCreateHerd(startDeerID, true);
   rmSetObjectDefMinDistance(startDeerID, 14);
   rmSetObjectDefMaxDistance(startDeerID, 18);
   rmAddObjectDefConstraint(startDeerID, avoidStartResource);
   rmAddObjectDefConstraint(startDeerID, avoidImpassableLand);
   rmAddObjectDefConstraint(startDeerID, shortAvoidStartingUnits);

   int startCoalID=rmCreateObjectDef("player coal");
   rmAddObjectDefItem(startCoalID, "mine", 1, 5.0);
   rmSetObjectDefMinDistance(startCoalID, 14);
   rmSetObjectDefMaxDistance(startCoalID, 18);
   rmAddObjectDefConstraint(startCoalID, avoidStartResource);
   rmAddObjectDefConstraint(startCoalID, avoidImpassableLand);
   rmAddObjectDefConstraint(startCoalID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startDeerID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startCoalID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
   }


   // Text
   rmSetStatusText("",0.40);


   // Define and place the 3 natives to island, either Franks, Frisians or Saxons

   // Native 1
   int nativeVillageID = -1;
   int nativeVillageType = rmRandInt(1,5);
   if (nativeInt1 == 1)
      nativeVillageID = rmCreateGrouping("franks village", "native comanche village "+nativeVillageType);
   else if (nativeInt1 == 2)
      nativeVillageID = rmCreateGrouping("frisians village", "native comanche village "+nativeVillageType);
   else
      nativeVillageID = rmCreateGrouping("saxons village", "native comanche village "+nativeVillageType);
   rmSetGroupingMinDistance(nativeVillageID, 0.0);
   rmSetGroupingMaxDistance(nativeVillageID, 5.0);
   rmAddGroupingToClass(nativeVillageID, rmClassID("natives"));
   //rmAddGroupingConstraint(nativeVillageID, avoidImpassableLand);
   rmPlaceGroupingInArea(nativeVillageID, 0, rmAreaID("islands1"));

   // Native 2
   int nativeVillage2ID = -1;
   int nativeVillage2Type = rmRandInt(1,5);
   if (nativeInt2 == 1)
      nativeVillage2ID = rmCreateGrouping("franks village 2", "native comanche village "+nativeVillageType);
   else if (nativeInt2 == 2)
      nativeVillage2ID = rmCreateGrouping("frisians village 2", "native comanche village "+nativeVillageType);
   else
      nativeVillage2ID = rmCreateGrouping("saxons village 2", "native comanche village "+nativeVillageType);
   rmSetGroupingMinDistance(nativeVillage2ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage2ID, 5.0);
   rmAddGroupingToClass(nativeVillage2ID, rmClassID("natives"));
   //rmAddGroupingConstraint(nativeVillage2ID, avoidImpassableLand);
   rmPlaceGroupingInArea(nativeVillage2ID, 0, rmAreaID("islands2"));

   // Native 3
   int nativeVillage3ID = -1;
   int nativeVillage3Type = rmRandInt(1,5);
   if (nativeInt3 == 1)
      nativeVillage3ID = rmCreateGrouping("franks village 3", "native comanche village "+nativeVillageType);
   else if (nativeInt3 == 2)
      nativeVillage3ID = rmCreateGrouping("frisians village 3", "native comanche village "+nativeVillageType);
   else
      nativeVillage3ID = rmCreateGrouping("saxons village 3", "native comanche village "+nativeVillageType);
   rmSetGroupingMinDistance(nativeVillage3ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage3ID, 5.0);
   rmAddGroupingToClass(nativeVillage3ID, rmClassID("natives"));
   //rmAddGroupingConstraint(nativeVillage3ID, avoidImpassableLand);
   rmPlaceGroupingInArea(nativeVillage3ID, 0, rmAreaID("islands3"));


   // Define and place small random swamps

   float swampMin=rmAreaTilesToFraction(350);
   float swampMax=rmAreaTilesToFraction(400);

   for(i=1; <40)
   {
      int swamps=rmCreateArea("random swamp"+i);
      rmSetAreaSize(swamps, swampMin, swampMax);
      rmSetAreaWaterType(swamps, "bayou");
      rmAddAreaToClass(swamps, classSwamp);
      rmSetAreaBaseHeight(swamps, 0.0);
      rmSetAreaCoherence(swamps, 0.6);
      rmSetAreaSmoothDistance(swamps, 5);
      rmSetAreaMinBlobs(swamps, 10);
      rmSetAreaMaxBlobs(swamps, 15);
      rmSetAreaMinBlobDistance(swamps, 10);
      rmSetAreaMaxBlobDistance(swamps, 15);
      rmAddAreaConstraint(swamps, lakeConstraint);
      rmAddAreaConstraint(swamps, swampConstraint);
      rmAddAreaConstraint(swamps, avoidImpassableLand);
      rmAddAreaConstraint(swamps, somePlayerConstraint);
      rmAddAreaConstraint(swamps, avoidTradeRoute);
      rmAddAreaConstraint(swamps, avoidTradeSockets);
      rmBuildArea(swamps);
   }


   // Text
   rmSetStatusText("",0.50);


   // Define and place animals
   
   // Define and place deer herds
   int deerHerdID=rmCreateObjectDef("deer herd");
   rmAddObjectDefItem(deerHerdID, "deer", rmRandInt(5,6), 6.0);
   rmSetObjectDefCreateHerd(deerHerdID, true);
   rmSetObjectDefMinDistance(deerHerdID, 0.0);
   rmSetObjectDefMaxDistance(deerHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(deerHerdID, classAnimals);
   rmAddObjectDefConstraint(deerHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(deerHerdID, animalConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(deerHerdID, avoidTradeSockets);
   int numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(deerHerdID, 0, 0.5, 0.5);

   // Define and place red deer herds
   int normalDeerHerdID=rmCreateObjectDef("normal deer herd");
   rmAddObjectDefItem(normalDeerHerdID, "moose", rmRandInt(5,6), 6.0);
   rmSetObjectDefCreateHerd(normalDeerHerdID, true);
   rmSetObjectDefMinDistance(normalDeerHerdID, 0.0);
   rmSetObjectDefMaxDistance(normalDeerHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(normalDeerHerdID, classAnimals);
   rmAddObjectDefConstraint(normalDeerHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(normalDeerHerdID, animalConstraint);
   rmAddObjectDefConstraint(normalDeerHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(normalDeerHerdID, avoidTradeSockets);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(normalDeerHerdID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.60);


   // Define and place coal mines

   int coalCount = 3*cNumberNonGaiaPlayers;

   for(i=0; < coalCount)
   {
      int coalID=rmCreateObjectDef("coal mine"+i);
      rmAddObjectDefItem(coalID, "mine", 1, 0.0);
      rmSetObjectDefMinDistance(coalID, 0.0);
      rmSetObjectDefMaxDistance(coalID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(coalID, classMines);
      rmAddObjectDefConstraint(coalID, somePlayerConstraint);
      rmAddObjectDefConstraint(coalID, avoidMines);
      rmAddObjectDefConstraint(coalID, avoidTradeRoute);
      rmAddObjectDefConstraint(coalID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(coalID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidAll);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget2, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget3, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget4, avoidTradeSockets);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Place King's Hill if KOTH game to middle on a small island

   if(rmGetIsKOTH()) 
   {
      ypKingsHillLandfill(0.5, 0.5, 0.01, 1.0, "carolina_grass", 0);
      ypKingsHillPlacer(0.5, 0.5, 0.05, 0);
   }
   

   // Text
   rmSetStatusText("",1.0); 
}