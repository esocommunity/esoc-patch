// RM script of Hedjaz
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Picks the map size
   int playerTiles = 20000;
   if (cNumberNonGaiaPlayers == 8)
	playerTiles = 14000;
   else if (cNumberNonGaiaPlayers == 7)
	playerTiles = 15000;
   else if (cNumberNonGaiaPlayers == 6)
	playerTiles = 16000;
   else if (cNumberNonGaiaPlayers == 5)
	playerTiles = 17000;
   else if (cNumberNonGaiaPlayers == 4)
	playerTiles = 18000;
   else if (cNumberNonGaiaPlayers == 3)
	playerTiles = 19000;


   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   int longSide=1.5*size; 
   rmSetMapSize(size, longSide);

   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses the side the players start in FFA - this is also complex! :)
   int playerSide = rmRandInt(1,2);


   // If 5 or 7 player FFA game, changes the starting place of the last (5th or 7th) player. Bad explanation, isn't it? :D
   int oddPlayer = rmRandInt(1,2);


   // Very VERY complex FFA player placement random integers, only I can really understand these! :P
   int playerPlace3 = rmRandInt(1,3);
   int playerPlace4 = rmRandInt(1,2);
   int playerPlace5 = rmRandInt(1,5);
   int playerPlace6 = rmRandInt(1,3);
   int playerPlace7 = rmRandInt(1,7);
   int playerPlace8 = rmRandInt(1,4);


   // Chooses the side where teams start in team games
   int teamSide = rmRandInt(1,2);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Pick lighting
   rmSetLightingSet("3x01a_newengland");


   // Picks default terrain and map type
   rmSetMapElevationParameters(cElevTurbulence, 0.08, 4, 0.5, 12.0);
   rmTerrainInitialize("borneo\shoreline1_borneo", 1.0);
   rmSetMapType("land");
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   int classWadi=rmDefineClass("wadi");
   int classPass=rmDefineClass("pass");
   int classGreen=rmDefineClass("green");
   int classDate=rmDefineClass("date");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("nuggets");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 50.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 100.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int veryLongAnimalConstraint=rmCreateClassDistanceConstraint("avoid all animals very long", classAnimals, 120.0);
   int longAnimalConstraint=rmCreateClassDistanceConstraint("avoid all animals long", classAnimals, 100.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 60.0);
   int dateConstraint=rmCreateClassDistanceConstraint("avoid date trees", classDate, 45.0);

   // Impassable land avoidance
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 6.0);
   int longAvoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land long", "Land", false, 25.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 60.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 6.0);

   // Wadi avoidance
   int wadiConstraint=rmCreateClassDistanceConstraint("avoid wadis", classWadi, 10.0);
   int passConstraint=rmCreateClassDistanceConstraint("avoid wadi passes", classPass, 20.0);

   // Green area avoidance
   int greenConstraint=rmCreateClassDistanceConstraint("avoid green area", classGreen, 25.0);

   // Trade route avoidance
   int avoidTradeRoute=rmCreateTradeRouteDistanceConstraint("trade route", 5.0);
   int avoidTradeSockets=rmCreateTypeDistanceConstraint("avoid trade sockets", "SocketTradeRoute", 5.0);

   // Water objects avoidance
   int fishVsFish=rmCreateTypeDistanceConstraint("fish v fish", "ypFishTuna", 20.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish land", "land", true, 5.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag vs land", "land", true, 20.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid same", "HomeCityWaterSpawnFlag", 75);


   // Text
   rmSetStatusText("",0.10);


   // Define and place greener area near the sea

   int greenAreaID=rmCreateArea("green area");
   rmSetAreaLocation(greenAreaID, 0.2, 0.5);
   rmAddAreaInfluenceSegment(greenAreaID, 0.2, 0.0, 0.2, 1.0);
   rmSetAreaSize(greenAreaID, rmAreaTilesToFraction(cNumberNonGaiaPlayers*800+20000), rmAreaTilesToFraction(cNumberNonGaiaPlayers*800+20000));
   rmSetAreaTerrainType(greenAreaID, "borneo\ground_sand3_borneo");
   rmSetAreaBaseHeight(greenAreaID, -2.0);
   rmSetAreaCoherence(greenAreaID, 0.6);
   rmSetAreaSmoothDistance(greenAreaID, 10);
   rmAddAreaToClass(greenAreaID, classGreen);
   rmSetAreaObeyWorldCircleConstraint(greenAreaID, false);
   rmSetAreaElevationType(greenAreaID, cElevTurbulence);
   rmSetAreaElevationVariation(greenAreaID, 6.0);
   rmSetAreaElevationMinFrequency(greenAreaID, 0.02);
   rmSetAreaElevationOctaves(greenAreaID, 4);
   rmSetAreaElevationPersistence(greenAreaID, 0.7);
   rmSetAreaElevationNoiseBias(greenAreaID, 1);
   rmBuildArea(greenAreaID);


   // Define and place water areas

   // Main sea to west
   int seaID=rmCreateArea("Sea");
   rmSetAreaWaterType(seaID, "caribbean coast");
   rmSetAreaSize(seaID, rmAreaTilesToFraction(cNumberNonGaiaPlayers*1500+6000), rmAreaTilesToFraction(cNumberNonGaiaPlayers*1500+6000));
   rmSetAreaCoherence(seaID, 0.5);
   rmSetAreaLocation(seaID, 0.0, 0.5);
   rmAddAreaInfluenceSegment(seaID, 0.0, 0.0, 0.0, 1.0);
   rmSetAreaBaseHeight(seaID, 0.0);
   rmSetAreaMinBlobs(seaID, 4);
   rmSetAreaMaxBlobs(seaID, 5);
   rmSetAreaMinBlobDistance(seaID, 4);
   rmSetAreaMaxBlobDistance(seaID, 5);
   rmSetAreaSmoothDistance(seaID, 50);
   rmBuildArea(seaID);

   // Take a small land piece from south-west corner
   int sea2ID=rmCreateArea("Sea2");
   rmSetAreaWaterType(sea2ID, "caribbean coast");
   rmSetAreaSize(sea2ID, rmAreaTilesToFraction(1500), rmAreaTilesToFraction(1500));
   rmSetAreaCoherence(sea2ID, 0.5);
   rmSetAreaLocation(sea2ID, 0.0, 0.12);
   rmSetAreaBaseHeight(sea2ID, 0.0);
   rmSetAreaObeyWorldCircleConstraint(sea2ID, false);
   rmSetAreaSmoothDistance(sea2ID, 50);
   rmBuildArea(sea2ID);

   // Take a small land piece from north-west corner
   int sea3ID=rmCreateArea("Sea3");
   rmSetAreaWaterType(sea3ID, "caribbean coast");
   rmSetAreaSize(sea3ID, rmAreaTilesToFraction(1500), rmAreaTilesToFraction(1500));
   rmSetAreaCoherence(sea3ID, 0.5);
   rmSetAreaLocation(sea3ID, 0.0, 0.88);
   rmSetAreaBaseHeight(sea3ID, 0.0);
   rmSetAreaObeyWorldCircleConstraint(sea3ID, false);
   rmSetAreaSmoothDistance(sea3ID, 50);
   rmBuildArea(sea3ID);


   // Text
   rmSetStatusText("",0.20);


   // Define and place 2 wadis

   // **WADI 1**

   // Define and place wadi 1  - closer to sea
   int wadiID=rmCreateArea("Wadi");
   rmSetAreaTerrainType(wadiID, "sonora\ground2_son");
   rmSetAreaCliffType(wadiID, "sonora");
   rmSetAreaCliffEdge(wadiID, 1, 1.0, 0.0, 0.0, 0);
   rmSetAreaCliffHeight(wadiID, -5, 0.0, 0.3);
   rmSetAreaCliffPainting(wadiID, true, false, true);
   rmSetAreaSize(wadiID, rmAreaTilesToFraction(3000), rmAreaTilesToFraction(3000));
   if (cNumberNonGaiaPlayers >= 4)
      rmSetAreaSize(wadiID, rmAreaTilesToFraction(6000), rmAreaTilesToFraction(6000));
   if (cNumberNonGaiaPlayers >= 6)
      rmSetAreaSize(wadiID, rmAreaTilesToFraction(8000), rmAreaTilesToFraction(8000));
   rmSetAreaLocation(wadiID, 0.35, 0.5);
   rmAddAreaInfluenceSegment(wadiID, 0.35, 0.0, 0.35, 1.0);
   rmSetAreaCoherence(wadiID, 0.8);
   rmSetAreaSmoothDistance(wadiID, 20);
   rmAddAreaToClass(wadiID, classWadi);
   rmBuildArea(wadiID);

   // Define and place wadi 1's passes

   // Pass to north
   int wadiPassNorthID=rmCreateArea("wadi pass north");
   rmSetAreaLocation(wadiPassNorthID, 0.35, 0.8);
   rmSetAreaSize(wadiPassNorthID, rmAreaTilesToFraction(400), rmAreaTilesToFraction(400));
   rmSetAreaTerrainType(wadiPassNorthID, "sonora\ground2_son");
   rmSetAreaBaseHeight(wadiPassNorthID, -2.0);
   rmSetAreaCoherence(wadiPassNorthID, 0.8);
   rmSetAreaSmoothDistance(wadiPassNorthID, 10);
   rmSetAreaHeightBlend(wadiPassNorthID, 10);
   rmAddAreaToClass(wadiPassNorthID, classWadi);
   rmAddAreaToClass(wadiPassNorthID, classPass);
   rmBuildArea(wadiPassNorthID);

   // Pass to middle
   int wadiPassMiddleID=rmCreateArea("wadi pass middle");
   rmSetAreaLocation(wadiPassMiddleID, 0.35, 0.5);
   rmSetAreaSize(wadiPassMiddleID, rmAreaTilesToFraction(400), rmAreaTilesToFraction(400));
   rmSetAreaTerrainType(wadiPassMiddleID, "sonora\ground2_son");
   rmSetAreaBaseHeight(wadiPassMiddleID, -2.0);
   rmSetAreaCoherence(wadiPassMiddleID, 0.8);
   rmSetAreaSmoothDistance(wadiPassMiddleID, 10);
   rmSetAreaHeightBlend(wadiPassMiddleID, 10);
   rmAddAreaToClass(wadiPassMiddleID, classWadi);
   rmAddAreaToClass(wadiPassMiddleID, classPass);
   rmBuildArea(wadiPassMiddleID);

   // Pass to south
   int wadiPassSouthID=rmCreateArea("wadi pass south");
   rmSetAreaLocation(wadiPassSouthID, 0.35, 0.2);
   rmSetAreaSize(wadiPassSouthID, rmAreaTilesToFraction(400), rmAreaTilesToFraction(400));
   rmSetAreaTerrainType(wadiPassSouthID, "sonora\ground2_son");
   rmSetAreaBaseHeight(wadiPassSouthID, -2.0);
   rmSetAreaCoherence(wadiPassSouthID, 0.8);
   rmSetAreaSmoothDistance(wadiPassSouthID, 10);
   rmSetAreaHeightBlend(wadiPassSouthID, 10);
   rmAddAreaToClass(wadiPassSouthID, classWadi);
   rmAddAreaToClass(wadiPassSouthID, classPass);
   rmBuildArea(wadiPassSouthID);

   // **WADI 2**

   // Define and place wadi 2  - farther to sea
   int wadi2ID=rmCreateArea("Wadi 2");
   rmSetAreaTerrainType(wadi2ID, "sonora\ground2_son");
   rmSetAreaCliffType(wadi2ID, "sonora");
   rmSetAreaCliffEdge(wadi2ID, 1, 1.0, 0.0, 0.0, 0);
   rmSetAreaCliffHeight(wadi2ID, -5, 0.0, 0.3);
   rmSetAreaCliffPainting(wadi2ID, true, false, true);
   rmSetAreaSize(wadi2ID, rmAreaTilesToFraction(3000), rmAreaTilesToFraction(3000));
   if (cNumberNonGaiaPlayers >= 4)
      rmSetAreaSize(wadi2ID, rmAreaTilesToFraction(6000), rmAreaTilesToFraction(6000));
   if (cNumberNonGaiaPlayers >= 6)
      rmSetAreaSize(wadi2ID, rmAreaTilesToFraction(8000), rmAreaTilesToFraction(8000));
   rmSetAreaLocation(wadi2ID, 0.86, 0.5);
   rmAddAreaInfluenceSegment(wadi2ID, 0.86, 0.0, 0.86, 1.0);
   rmSetAreaCoherence(wadi2ID, 0.8);
   rmSetAreaSmoothDistance(wadi2ID, 20);
   rmAddAreaToClass(wadi2ID, classWadi);
   rmBuildArea(wadi2ID);

   // Define and place wadi 2's passes

   // Pass to north
   int wadi2PassNorthID=rmCreateArea("wadi 2 pass north");
   rmSetAreaLocation(wadi2PassNorthID, 0.86, 0.8);
   rmSetAreaSize(wadi2PassNorthID, rmAreaTilesToFraction(400), rmAreaTilesToFraction(400));
   rmSetAreaTerrainType(wadi2PassNorthID, "sonora\ground2_son");
   rmSetAreaBaseHeight(wadi2PassNorthID, -2.0);
   rmSetAreaCoherence(wadi2PassNorthID, 0.8);
   rmSetAreaSmoothDistance(wadi2PassNorthID, 10);
   rmSetAreaHeightBlend(wadi2PassNorthID, 10);
   rmAddAreaToClass(wadi2PassNorthID, classWadi);
   rmAddAreaToClass(wadi2PassNorthID, classPass);
   rmBuildArea(wadi2PassNorthID);

   // Pass to middle
   int wadi2PassMiddleID=rmCreateArea("wadi 2 pass middle");
   rmSetAreaLocation(wadi2PassMiddleID, 0.86, 0.5);
   rmSetAreaSize(wadi2PassMiddleID, rmAreaTilesToFraction(400), rmAreaTilesToFraction(400));
   rmSetAreaTerrainType(wadi2PassMiddleID, "sonora\ground2_son");
   rmSetAreaBaseHeight(wadi2PassMiddleID, -2.0);
   rmSetAreaCoherence(wadi2PassMiddleID, 0.8);
   rmSetAreaSmoothDistance(wadi2PassMiddleID, 10);
   rmSetAreaHeightBlend(wadi2PassMiddleID, 10);
   rmAddAreaToClass(wadi2PassMiddleID, classWadi);
   rmAddAreaToClass(wadi2PassMiddleID, classPass);
   rmBuildArea(wadi2PassMiddleID);

   // Pass to south
   int wadi2PassSouthID=rmCreateArea("wadi 2 pass south");
   rmSetAreaLocation(wadi2PassSouthID, 0.86, 0.2);
   rmSetAreaSize(wadi2PassSouthID, rmAreaTilesToFraction(400), rmAreaTilesToFraction(400));
   rmSetAreaTerrainType(wadi2PassSouthID, "sonora\ground2_son");
   rmSetAreaBaseHeight(wadi2PassSouthID, -2.0);
   rmSetAreaCoherence(wadi2PassSouthID, 0.8);
   rmSetAreaSmoothDistance(wadi2PassSouthID, 10);
   rmSetAreaHeightBlend(wadi2PassSouthID, 10);
   rmAddAreaToClass(wadi2PassSouthID, classWadi);
   rmAddAreaToClass(wadi2PassSouthID, classPass);
   rmBuildArea(wadi2PassSouthID);


   // Place players - in team and FFA games

   // Player placement if FFA (Free For All) - place players in lines
   if(cNumberTeams > 2 || cNumberNonGaiaPlayers == 2)
   {
      if (cNumberNonGaiaPlayers == 2) // Place far from the sea to different sides (in opposite direction of wadis)
      {
            if (teamSide == 1)
            {
                        rmPlacePlayer(1, 0.76, 0.2);
                        rmPlacePlayer(2, 0.76, 0.8);
            }
            else if (teamSide == 2)
            {
                        rmPlacePlayer(1, 0.76, 0.8);
                        rmPlacePlayer(2, 0.76, 0.2);
            }
      }
      if (cNumberNonGaiaPlayers == 3) // One player is near the sea in other side (in opposite direction of wadis)
      {
            if (playerPlace3 == 1) // Player 1 in other side
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.36, 0.2);
                                                rmPlacePlayer(2, 0.36, 0.8);
                                                rmPlacePlayer(3, 0.76, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.36, 0.8);
                                                rmPlacePlayer(2, 0.36, 0.2);
                                                rmPlacePlayer(3, 0.76, 0.2);
                        }
            }
            else if (playerPlace3 == 2) // Player 2 in other side
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.76, 0.8);
                                                rmPlacePlayer(2, 0.36, 0.2);
                                                rmPlacePlayer(3, 0.36, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.76, 0.2);
                                                rmPlacePlayer(2, 0.36, 0.8);
                                                rmPlacePlayer(3, 0.36, 0.2);
                        }
            }
            else if (playerPlace3 == 3) // Player 3 in other side
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.36, 0.8);
                                                rmPlacePlayer(2, 0.76, 0.8);
                                                rmPlacePlayer(3, 0.36, 0.2);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.36, 0.2);
                                                rmPlacePlayer(2, 0.76, 0.2);
                                                rmPlacePlayer(3, 0.36, 0.8);
                        }
            }
      }

      if (cNumberNonGaiaPlayers == 4) // Normal balanced placement
      {
            if (playerPlace4 == 1)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.36, 0.2);
                                                rmPlacePlayer(2, 0.76, 0.2);
                                                rmPlacePlayer(3, 0.36, 0.8);
                                                rmPlacePlayer(4, 0.76, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.36, 0.8);
                                                rmPlacePlayer(2, 0.76, 0.8);
                                                rmPlacePlayer(3, 0.36, 0.2);
                                                rmPlacePlayer(4, 0.76, 0.2);
                        }
            }
            else if (playerPlace4 == 2)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.76, 0.2);
                                                rmPlacePlayer(2, 0.36, 0.8);
                                                rmPlacePlayer(3, 0.76, 0.2);
                                                rmPlacePlayer(4, 0.36, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.76, 0.8);
                                                rmPlacePlayer(2, 0.36, 0.2);
                                                rmPlacePlayer(3, 0.76, 0.8);
                                                rmPlacePlayer(4, 0.36, 0.2);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 5) // Two players near sea, two players between 2. wadi and trade route, one player is in middle (in direction of wadis)
      {
            if (playerPlace5 == 1) // Player 1 in middle, players 2-3 near the sea, players 4-5 between second wadi and trade route
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(1, 0.2, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(1, 0.75, 0.5);
                                                rmPlacePlayer(2, 0.2, 0.2);
                                                rmPlacePlayer(3, 0.2, 0.8);
                                                rmPlacePlayer(4, 0.75, 0.2);
                                                rmPlacePlayer(5, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(1, 0.75, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(1, 0.2, 0.5);
                                                rmPlacePlayer(2, 0.2, 0.8);
                                                rmPlacePlayer(3, 0.2, 0.2);
                                                rmPlacePlayer(4, 0.75, 0.8);
                                                rmPlacePlayer(5, 0.75, 0.2);
                        }
            }
            else if (playerPlace5 == 2) // Player 2 in middle, players 3-4 near the sea, players 5-1 between second wadi and trade route
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(2, 0.2, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(2, 0.75, 0.5);
                                                rmPlacePlayer(3, 0.2, 0.2);
                                                rmPlacePlayer(4, 0.2, 0.8);
                                                rmPlacePlayer(5, 0.75, 0.2);
                                                rmPlacePlayer(1, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(2, 0.75, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(2, 0.2, 0.5);
                                                rmPlacePlayer(3, 0.2, 0.8);
                                                rmPlacePlayer(4, 0.2, 0.2);
                                                rmPlacePlayer(5, 0.75, 0.8);
                                                rmPlacePlayer(1, 0.75, 0.2);
                        }
            }
            else if (playerPlace5 == 3) // Player 3 in middle, players 4-5 near the sea, players 1-2 between second wadi and trade route
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(3, 0.2, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(3, 0.75, 0.5);
                                                rmPlacePlayer(4, 0.2, 0.2);
                                                rmPlacePlayer(5, 0.2, 0.8);
                                                rmPlacePlayer(1, 0.75, 0.2);
                                                rmPlacePlayer(2, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(3, 0.75, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(3, 0.2, 0.5);
                                                rmPlacePlayer(4, 0.2, 0.8);
                                                rmPlacePlayer(5, 0.2, 0.2);
                                                rmPlacePlayer(1, 0.75, 0.8);
                                                rmPlacePlayer(2, 0.75, 0.2);
                        }
            }
            else if (playerPlace5 == 4) // Player 4 in middle, players 5-1 near the sea, players 2-3 between second wadi and trade route
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(4, 0.2, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(4, 0.75, 0.5);
                                                rmPlacePlayer(5, 0.2, 0.2);
                                                rmPlacePlayer(1, 0.2, 0.8);
                                                rmPlacePlayer(2, 0.75, 0.2);
                                                rmPlacePlayer(3, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(4, 0.75, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(4, 0.2, 0.5);
                                                rmPlacePlayer(5, 0.2, 0.8);
                                                rmPlacePlayer(1, 0.2, 0.2);
                                                rmPlacePlayer(2, 0.75, 0.8);
                                                rmPlacePlayer(3, 0.75, 0.2);
                        }
            }
            else if (playerPlace5 == 5) // Player 5 in middle, players 1-2 near the sea, players 3-4 between second wadi and trade route
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(5, 0.2, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(5, 0.75, 0.5);
                                                rmPlacePlayer(1, 0.2, 0.2);
                                                rmPlacePlayer(2, 0.2, 0.8);
                                                rmPlacePlayer(3, 0.75, 0.2);
                                                rmPlacePlayer(4, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                                                                rmPlacePlayer(5, 0.75, 0.5);
                                                else if (oddPlayer == 2)
                                                                                                rmPlacePlayer(5, 0.2, 0.5);
                                                rmPlacePlayer(1, 0.2, 0.8);
                                                rmPlacePlayer(2, 0.2, 0.2);
                                                rmPlacePlayer(3, 0.75, 0.8);
                                                rmPlacePlayer(4, 0.75, 0.2);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 6) // Normal balanced placement
      {
            if (playerPlace6 == 1) // Players 1-2 in middle, players 3-4 near the sea, players 5-6 between second wadi and trade route
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.2, 0.5);
                                                rmPlacePlayer(2, 0.75, 0.5);
                                                rmPlacePlayer(3, 0.2, 0.2);
                                                rmPlacePlayer(4, 0.2, 0.8);
                                                rmPlacePlayer(5, 0.75, 0.2);
                                                rmPlacePlayer(6, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.75, 0.5);
                                                rmPlacePlayer(2, 0.2, 0.5);
                                                rmPlacePlayer(3, 0.2, 0.8);
                                                rmPlacePlayer(4, 0.2, 0.2);
                                                rmPlacePlayer(5, 0.75, 0.8);
                                                rmPlacePlayer(6, 0.75, 0.2);
                        }
            }
            else if (playerPlace6 == 2) // Players 3-4 in middle, players 5-6 near the sea, players 1-2 between second wadi and trade route
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(3, 0.2, 0.5);
                                                rmPlacePlayer(4, 0.75, 0.5);
                                                rmPlacePlayer(5, 0.2, 0.2);
                                                rmPlacePlayer(6, 0.2, 0.8);
                                                rmPlacePlayer(1, 0.75, 0.2);
                                                rmPlacePlayer(2, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(3, 0.75, 0.5);
                                                rmPlacePlayer(4, 0.2, 0.5);
                                                rmPlacePlayer(5, 0.2, 0.8);
                                                rmPlacePlayer(6, 0.2, 0.2);
                                                rmPlacePlayer(1, 0.75, 0.8);
                                                rmPlacePlayer(2, 0.75, 0.2);
                        }
            }
            else if (playerPlace6 == 3) // Players 5-6 in middle, players 1-2 near the sea, players 3-4 between second wadi and trade route
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(5, 0.2, 0.5);
                                                rmPlacePlayer(6, 0.75, 0.5);
                                                rmPlacePlayer(1, 0.2, 0.2);
                                                rmPlacePlayer(2, 0.2, 0.8);
                                                rmPlacePlayer(3, 0.75, 0.2);
                                                rmPlacePlayer(4, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(5, 0.75, 0.5);
                                                rmPlacePlayer(6, 0.2, 0.5);
                                                rmPlacePlayer(1, 0.2, 0.8);
                                                rmPlacePlayer(2, 0.2, 0.2);
                                                rmPlacePlayer(3, 0.75, 0.8);
                                                rmPlacePlayer(4, 0.75, 0.2);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 7) // One player is in middle in other side
      {
            if (playerPlace7 == 1) // Player 2 in middle
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(1, 0.2, 0.4);
                                                                                                rmPlacePlayer(2, 0.75, 0.5);
                                                                                                rmPlacePlayer(3, 0.2, 0.6);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(1, 0.75, 0.4);
                                                                                                rmPlacePlayer(2, 0.2, 0.5);
                                                                                                rmPlacePlayer(3, 0.75, 0.6);
                                                }
                                                rmPlacePlayer(4, 0.2, 0.2);
                                                rmPlacePlayer(5, 0.2, 0.8);
                                                rmPlacePlayer(6, 0.75, 0.2);
                                                rmPlacePlayer(7, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(1, 0.2, 0.6);
                                                                                                rmPlacePlayer(2, 0.75, 0.5);
                                                                                                rmPlacePlayer(3, 0.2, 0.4);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(1, 0.75, 0.6);
                                                                                                rmPlacePlayer(2, 0.2, 0.5);
                                                                                                rmPlacePlayer(3, 0.75, 0.4);
                                                }
                                                rmPlacePlayer(4, 0.2, 0.8);
                                                rmPlacePlayer(5, 0.2, 0.2);
                                                rmPlacePlayer(6, 0.75, 0.8);
                                                rmPlacePlayer(7, 0.75, 0.2);
                        }
            }
            else if (playerPlace7 == 2) // Player 3 in middle
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(2, 0.2, 0.4);
                                                                                                rmPlacePlayer(3, 0.75, 0.5);
                                                                                                rmPlacePlayer(4, 0.2, 0.6);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(2, 0.75, 0.4);
                                                                                                rmPlacePlayer(3, 0.2, 0.5);
                                                                                                rmPlacePlayer(4, 0.75, 0.6);
                                                }
                                                rmPlacePlayer(5, 0.2, 0.2);
                                                rmPlacePlayer(6, 0.2, 0.8);
                                                rmPlacePlayer(7, 0.75, 0.2);
                                                rmPlacePlayer(1, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(2, 0.2, 0.6);
                                                                                                rmPlacePlayer(3, 0.75, 0.5);
                                                                                                rmPlacePlayer(4, 0.2, 0.4);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(2, 0.75, 0.6);
                                                                                                rmPlacePlayer(3, 0.2, 0.5);
                                                                                                rmPlacePlayer(4, 0.75, 0.4);
                                                }
                                                rmPlacePlayer(5, 0.2, 0.8);
                                                rmPlacePlayer(6, 0.2, 0.2);
                                                rmPlacePlayer(7, 0.75, 0.8);
                                                rmPlacePlayer(1, 0.75, 0.2);
                        }
            }
            else if (playerPlace7 == 3) // Player 4 in middle
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(3, 0.2, 0.4);
                                                                                                rmPlacePlayer(4, 0.75, 0.5);
                                                                                                rmPlacePlayer(5, 0.2, 0.6);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(3, 0.75, 0.4);
                                                                                                rmPlacePlayer(4, 0.2, 0.5);
                                                                                                rmPlacePlayer(5, 0.75, 0.6);
                                                }
                                                rmPlacePlayer(6, 0.2, 0.2);
                                                rmPlacePlayer(7, 0.2, 0.8);
                                                rmPlacePlayer(1, 0.75, 0.2);
                                                rmPlacePlayer(2, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(3, 0.2, 0.6);
                                                                                                rmPlacePlayer(4, 0.75, 0.5);
                                                                                                rmPlacePlayer(5, 0.2, 0.4);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(3, 0.75, 0.6);
                                                                                                rmPlacePlayer(4, 0.2, 0.5);
                                                                                                rmPlacePlayer(5, 0.75, 0.4);
                                                }
                                                rmPlacePlayer(6, 0.2, 0.8);
                                                rmPlacePlayer(7, 0.2, 0.2);
                                                rmPlacePlayer(1, 0.75, 0.8);
                                                rmPlacePlayer(2, 0.75, 0.2);
                        }
            }
            else if (playerPlace7 == 4) // Player 5 in middle
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(4, 0.2, 0.4);
                                                                                                rmPlacePlayer(5, 0.75, 0.5);
                                                                                                rmPlacePlayer(6, 0.2, 0.6);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(4, 0.75, 0.4);
                                                                                                rmPlacePlayer(5, 0.2, 0.5);
                                                                                                rmPlacePlayer(6, 0.75, 0.6);
                                                }
                                                rmPlacePlayer(7, 0.2, 0.2);
                                                rmPlacePlayer(1, 0.2, 0.8);
                                                rmPlacePlayer(2, 0.75, 0.2);
                                                rmPlacePlayer(3, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(4, 0.2, 0.6);
                                                                                                rmPlacePlayer(5, 0.75, 0.5);
                                                                                                rmPlacePlayer(6, 0.2, 0.4);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(4, 0.75, 0.6);
                                                                                                rmPlacePlayer(5, 0.2, 0.5);
                                                                                                rmPlacePlayer(6, 0.75, 0.4);
                                                }
                                                rmPlacePlayer(7, 0.2, 0.8);
                                                rmPlacePlayer(1, 0.2, 0.2);
                                                rmPlacePlayer(2, 0.75, 0.8);
                                                rmPlacePlayer(3, 0.75, 0.2);
                        }
            }
            else if (playerPlace7 == 5) // Player 6 in middle
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(5, 0.2, 0.4);
                                                                                                rmPlacePlayer(6, 0.75, 0.5);
                                                                                                rmPlacePlayer(7, 0.2, 0.6);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(5, 0.75, 0.4);
                                                                                                rmPlacePlayer(6, 0.2, 0.5);
                                                                                                rmPlacePlayer(7, 0.75, 0.6);
                                                }
                                                rmPlacePlayer(1, 0.2, 0.2);
                                                rmPlacePlayer(2, 0.2, 0.8);
                                                rmPlacePlayer(3, 0.75, 0.2);
                                                rmPlacePlayer(4, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(5, 0.2, 0.6);
                                                                                                rmPlacePlayer(6, 0.75, 0.5);
                                                                                                rmPlacePlayer(7, 0.2, 0.4);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(5, 0.75, 0.6);
                                                                                                rmPlacePlayer(6, 0.2, 0.5);
                                                                                                rmPlacePlayer(7, 0.75, 0.4);
                                                }
                                                rmPlacePlayer(1, 0.2, 0.8);
                                                rmPlacePlayer(2, 0.2, 0.2);
                                                rmPlacePlayer(3, 0.75, 0.8);
                                                rmPlacePlayer(4, 0.75, 0.2);
                        }
            }
            else if (playerPlace7 == 6) // Player 7 in middle
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(6, 0.2, 0.4);
                                                                                                rmPlacePlayer(7, 0.75, 0.5);
                                                                                                rmPlacePlayer(1, 0.2, 0.6);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(6, 0.75, 0.4);
                                                                                                rmPlacePlayer(7, 0.2, 0.5);
                                                                                                rmPlacePlayer(1, 0.75, 0.6);
                                                }
                                                rmPlacePlayer(2, 0.2, 0.2);
                                                rmPlacePlayer(3, 0.2, 0.8);
                                                rmPlacePlayer(4, 0.75, 0.2);
                                                rmPlacePlayer(5, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(6, 0.2, 0.6);
                                                                                                rmPlacePlayer(7, 0.75, 0.5);
                                                                                                rmPlacePlayer(1, 0.2, 0.4);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(6, 0.75, 0.6);
                                                                                                rmPlacePlayer(7, 0.2, 0.5);
                                                                                                rmPlacePlayer(1, 0.75, 0.4);
                                                }
                                                rmPlacePlayer(2, 0.2, 0.8);
                                                rmPlacePlayer(3, 0.2, 0.2);
                                                rmPlacePlayer(4, 0.75, 0.8);
                                                rmPlacePlayer(5, 0.75, 0.2);
                        }
            }
            else if (playerPlace7 == 7) // Player 1 in middle
            {
                        if (playerSide == 1)
                        {
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(7, 0.2, 0.4);
                                                                                                rmPlacePlayer(1, 0.75, 0.5);
                                                                                                rmPlacePlayer(2, 0.2, 0.6);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(7, 0.75, 0.4);
                                                                                                rmPlacePlayer(1, 0.2, 0.5);
                                                                                                rmPlacePlayer(2, 0.75, 0.6);
                                                }
                                                rmPlacePlayer(3, 0.2, 0.2);
                                                rmPlacePlayer(4, 0.2, 0.8);
                                                rmPlacePlayer(5, 0.75, 0.2);
                                                rmPlacePlayer(6, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                if (oddPlayer == 1)
                                                {
                                                                                                rmPlacePlayer(7, 0.2, 0.6);
                                                                                                rmPlacePlayer(1, 0.75, 0.5);
                                                                                                rmPlacePlayer(2, 0.2, 0.4);
                                                }
                                                else if (oddPlayer == 2)
                                                {
                                                                                                rmPlacePlayer(7, 0.75, 0.6);
                                                                                                rmPlacePlayer(1, 0.2, 0.5);
                                                                                                rmPlacePlayer(2, 0.75, 0.4);
                                                }
                                                rmPlacePlayer(3, 0.2, 0.8);
                                                rmPlacePlayer(4, 0.2, 0.2);
                                                rmPlacePlayer(5, 0.75, 0.8);
                                                rmPlacePlayer(6, 0.75, 0.2);
                        }
            }
      }
      if (cNumberNonGaiaPlayers == 8) // Normal balanced placement
      {
            if (playerPlace8 == 1)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.2, 0.2);
                                                rmPlacePlayer(2, 0.2, 0.4);
                                                rmPlacePlayer(3, 0.2, 0.6);
                                                rmPlacePlayer(4, 0.2, 0.8);
                                                rmPlacePlayer(5, 0.75, 0.2);
                                                rmPlacePlayer(6, 0.75, 0.4);
                                                rmPlacePlayer(7, 0.75, 0.6);
                                                rmPlacePlayer(8, 0.75, 0.8);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.75, 0.2);
                                                rmPlacePlayer(2, 0.75, 0.4);
                                                rmPlacePlayer(3, 0.75, 0.6);
                                                rmPlacePlayer(4, 0.75, 0.8);
                                                rmPlacePlayer(5, 0.2, 0.2);
                                                rmPlacePlayer(6, 0.2, 0.4);
                                                rmPlacePlayer(7, 0.2, 0.6);
                                                rmPlacePlayer(8, 0.2, 0.8);
                        }
            }
            else if (playerPlace8 == 2)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.2, 0.4);
                                                rmPlacePlayer(2, 0.2, 0.6);
                                                rmPlacePlayer(3, 0.2, 0.8);
                                                rmPlacePlayer(4, 0.2, 0.2);
                                                rmPlacePlayer(5, 0.75, 0.4);
                                                rmPlacePlayer(6, 0.75, 0.6);
                                                rmPlacePlayer(7, 0.75, 0.8);
                                                rmPlacePlayer(8, 0.75, 0.2);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.75, 0.4);
                                                rmPlacePlayer(2, 0.75, 0.6);
                                                rmPlacePlayer(3, 0.75, 0.8);
                                                rmPlacePlayer(4, 0.75, 0.2);
                                                rmPlacePlayer(5, 0.2, 0.4);
                                                rmPlacePlayer(6, 0.2, 0.6);
                                                rmPlacePlayer(7, 0.2, 0.8);
                                                rmPlacePlayer(8, 0.2, 0.2);
                        }
            }
            else if (playerPlace8 == 3)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.2, 0.6);
                                                rmPlacePlayer(2, 0.2, 0.8);
                                                rmPlacePlayer(3, 0.2, 0.2);
                                                rmPlacePlayer(4, 0.2, 0.4);
                                                rmPlacePlayer(5, 0.75, 0.4);
                                                rmPlacePlayer(6, 0.75, 0.6);
                                                rmPlacePlayer(7, 0.75, 0.8);
                                                rmPlacePlayer(8, 0.75, 0.2);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.75, 0.6);
                                                rmPlacePlayer(2, 0.75, 0.8);
                                                rmPlacePlayer(3, 0.75, 0.2);
                                                rmPlacePlayer(4, 0.75, 0.4);
                                                rmPlacePlayer(5, 0.2, 0.6);
                                                rmPlacePlayer(6, 0.2, 0.8);
                                                rmPlacePlayer(7, 0.2, 0.2);
                                                rmPlacePlayer(8, 0.2, 0.4);
                        }
            }
            else if (playerPlace8 == 4)
            {
                        if (playerSide == 1)
                        {
                                                rmPlacePlayer(1, 0.2, 0.8);
                                                rmPlacePlayer(2, 0.2, 0.2);
                                                rmPlacePlayer(3, 0.2, 0.4);
                                                rmPlacePlayer(4, 0.2, 0.6);
                                                rmPlacePlayer(5, 0.75, 0.8);
                                                rmPlacePlayer(6, 0.75, 0.2);
                                                rmPlacePlayer(7, 0.75, 0.4);
                                                rmPlacePlayer(8, 0.75, 0.6);
                        }
                        else if (playerSide == 2)
                        {
                                                rmPlacePlayer(1, 0.75, 0.8);
                                                rmPlacePlayer(2, 0.75, 0.2);
                                                rmPlacePlayer(3, 0.75, 0.4);
                                                rmPlacePlayer(4, 0.75, 0.6);
                                                rmPlacePlayer(5, 0.2, 0.8);
                                                rmPlacePlayer(6, 0.2, 0.2);
                                                rmPlacePlayer(7, 0.2, 0.4);
                                                rmPlacePlayer(8, 0.2, 0.6);
                        }
            }
      }
   }

   // Player placement if teams - place teams in circle to apart from each other
   if(cNumberTeams == 2 && cNumberNonGaiaPlayers >= 3)
   {
      rmSetPlacementTeam(0);
      if (teamSide == 1)
            rmPlacePlayersLine(0.4, 0.2, 0.76, 0.2, 0, 15);
      else if (teamSide == 2)
            rmPlacePlayersLine(0.4, 0.8, 0.76, 0.8, 0, 15);

      rmSetPlacementTeam(1);
      if (teamSide == 1)
            rmPlacePlayersLine(0.4, 0.8, 0.76, 0.8, 0, 15);
      else if (teamSide == 2)
            rmPlacePlayersLine(0.4, 0.2, 0.76, 0.2, 0, 15);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.30);


   // Define and place trade route - splits the map in middle

   // Create the trade route
   int tradeRouteID = rmCreateTradeRoute();
   int socketID=rmCreateObjectDef("sockets to dock trade posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

   // Define the sockets
   rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socketID, true);
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 12.0);

   // Set the trade route waypoints
   rmAddTradeRouteWaypoint(tradeRouteID, 0.56, 0.0);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.56, 1.0, 18, 18);
   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route"); 
  
   // Place sockets to trade route
   vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.2);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.4);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.6);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.8);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);


   // Text
   rmSetStatusText("",0.40);


   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "ypTreeBorneoPalm", 12, 7.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 16);
   rmSetObjectDefMaxDistance(startAreaTreeID, 20);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startGazelleID=rmCreateObjectDef("starting gazelle");
   rmAddObjectDefItem(startGazelleID, "deer", 5, 4.0);
   rmSetObjectDefCreateHerd(startGazelleID, true);
   rmSetObjectDefMinDistance(startGazelleID, 14);
   rmSetObjectDefMaxDistance(startGazelleID, 18);
   rmAddObjectDefConstraint(startGazelleID, avoidStartResource);
   rmAddObjectDefConstraint(startGazelleID, avoidImpassableLand);
   rmAddObjectDefConstraint(startGazelleID, shortAvoidStartingUnits);

   int startOilID=rmCreateObjectDef("player oil");
   rmAddObjectDefItem(startOilID, "mine", 1, 5.0);
   rmSetObjectDefMinDistance(startOilID, 14);
   rmSetObjectDefMaxDistance(startOilID, 18);
   rmAddObjectDefConstraint(startOilID, avoidStartResource);
   rmAddObjectDefConstraint(startOilID, avoidImpassableLand);
   rmAddObjectDefConstraint(startOilID, shortAvoidStartingUnits);

   // Place starting objects and resources

   for(i=1; <cNumberPlayers)
   {					
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startGazelleID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startOilID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
      rmAddClosestPointConstraint(flagVsFlag);
      rmAddClosestPointConstraint(flagLand);		
      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Text
   rmSetStatusText("",0.50);


   // Define and place forests
	
   int numTries=10*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i, rmAreaID("green area"));
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(100), rmAreaTilesToFraction(150));
      rmSetAreaForestType(forest, "Borneo Palm Forest");
      rmSetAreaForestDensity(forest, 0.7);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.4);
      rmSetAreaForestUnderbrush(forest, 0.3);
      rmSetAreaBaseHeight(forest, 1.0);
      rmSetAreaMinBlobs(forest, 1);
      rmSetAreaMaxBlobs(forest, 3);
      rmSetAreaMinBlobDistance(forest, 10);
      rmSetAreaMaxBlobDistance(forest, 15);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, longAvoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, avoidTradeRoute);
      rmAddAreaConstraint(forest, avoidTradeSockets);
      rmAddAreaConstraint(forest, wadiConstraint);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.  
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }


   // Text
   rmSetStatusText("",0.60);


   // Define and place animals
   
   // Define and place arabian leopard herds inside wadis
   int arabianLeopardHerdID=rmCreateObjectDef("arabian leopard herd");
   rmAddObjectDefItem(arabianLeopardHerdID, "BighornSheep", rmRandInt(6,7), 8.0);
   rmSetObjectDefMinDistance(arabianLeopardHerdID, 0.0);
   rmSetObjectDefMaxDistance(arabianLeopardHerdID, rmZFractionToMeters(0.47));
   rmAddObjectDefToClass(arabianLeopardHerdID, classAnimals);
   rmAddObjectDefConstraint(arabianLeopardHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(arabianLeopardHerdID, veryLongAnimalConstraint);
   rmAddObjectDefConstraint(arabianLeopardHerdID, passConstraint);
   if (cNumberNonGaiaPlayers <= 4)
      numTries=3;
   else if (cNumberNonGaiaPlayers >= 5)
      numTries=4;
   for (i=0; <numTries)
      rmPlaceObjectDefInRandomAreaOfClass(arabianLeopardHerdID, 0, classWadi, 1);

   // Define and place gazelle herds inside wadis
   int gazelleHerdWadisID=rmCreateObjectDef("gazelle herd wadis");
   rmAddObjectDefItem(gazelleHerdWadisID, "deer", rmRandInt(6,7), 8.0);
   rmSetObjectDefCreateHerd(gazelleHerdWadisID, true);
   rmSetObjectDefMinDistance(gazelleHerdWadisID, 0.0);
   rmSetObjectDefMaxDistance(gazelleHerdWadisID, rmZFractionToMeters(0.47));
   rmAddObjectDefToClass(gazelleHerdWadisID, classAnimals);
   rmAddObjectDefConstraint(gazelleHerdWadisID, somePlayerConstraint);
   rmAddObjectDefConstraint(gazelleHerdWadisID, veryLongAnimalConstraint);
   rmAddObjectDefConstraint(gazelleHerdWadisID, passConstraint);
   if (cNumberNonGaiaPlayers <= 4)
      numTries=3;
   else if (cNumberNonGaiaPlayers >= 5)
      numTries=4;
   for (i=0; <numTries)
      rmPlaceObjectDefInRandomAreaOfClass(gazelleHerdWadisID, 0, classWadi, 1);

   // Define and place gazelle herds to the green area
   int gazelleHerdGreenID=rmCreateObjectDef("gazelle herd green area");
   rmAddObjectDefItem(gazelleHerdGreenID, "deer", rmRandInt(5,6), 8.0);
   rmSetObjectDefCreateHerd(gazelleHerdGreenID, true);
   rmSetObjectDefMinDistance(gazelleHerdGreenID, 0.0);
   rmSetObjectDefMaxDistance(gazelleHerdGreenID, rmZFractionToMeters(0.47));
   rmAddObjectDefToClass(gazelleHerdGreenID, classAnimals);
   rmAddObjectDefConstraint(gazelleHerdGreenID, somePlayerConstraint);
   rmAddObjectDefConstraint(gazelleHerdGreenID, avoidImpassableLand);
   rmAddObjectDefConstraint(gazelleHerdGreenID, animalConstraint);
   rmAddObjectDefConstraint(gazelleHerdGreenID, avoidTradeRoute);
   rmAddObjectDefConstraint(gazelleHerdGreenID, avoidTradeSockets);
   rmAddObjectDefConstraint(gazelleHerdGreenID, wadiConstraint);
   numTries=cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefInArea(gazelleHerdGreenID, 0, rmAreaID("green area"), 1);

   // Define and place camel herds to the green area
   int camelHerdGreenID=rmCreateObjectDef("camel herd green area");
   rmAddObjectDefItem(camelHerdGreenID, "Pronghorn", rmRandInt(6,7), 8.0);
   rmSetObjectDefCreateHerd(camelHerdGreenID, true);
   rmSetObjectDefMinDistance(camelHerdGreenID, 0.0);
   rmSetObjectDefMaxDistance(camelHerdGreenID, rmZFractionToMeters(0.47));
   rmAddObjectDefToClass(camelHerdGreenID, classAnimals);
   rmAddObjectDefConstraint(camelHerdGreenID, somePlayerConstraint);
   rmAddObjectDefConstraint(camelHerdGreenID, avoidImpassableLand);
   rmAddObjectDefConstraint(camelHerdGreenID, animalConstraint);
   rmAddObjectDefConstraint(camelHerdGreenID, avoidTradeRoute);
   rmAddObjectDefConstraint(camelHerdGreenID, avoidTradeSockets);
   rmAddObjectDefConstraint(camelHerdGreenID, wadiConstraint);
   if (cNumberNonGaiaPlayers <= 3)
      numTries=2;
   else
      numTries=cNumberNonGaiaPlayers/2;
   for (i=0; <numTries)
      rmPlaceObjectDefInArea(camelHerdGreenID, 0, rmAreaID("green area"), 1);

   // Define and place camel herds everywhere else than in green area or wadis
   int camelHerdNormalID=rmCreateObjectDef("camel herd normal");
   rmAddObjectDefItem(camelHerdNormalID, "Pronghorn", rmRandInt(4,5), 8.0);
   rmSetObjectDefCreateHerd(camelHerdNormalID, true);
   rmSetObjectDefMinDistance(camelHerdNormalID, 0.0);
   rmSetObjectDefMaxDistance(camelHerdNormalID, rmZFractionToMeters(0.47));
   rmAddObjectDefToClass(camelHerdNormalID, classAnimals);
   rmAddObjectDefConstraint(camelHerdNormalID, somePlayerConstraint);
   rmAddObjectDefConstraint(camelHerdNormalID, avoidImpassableLand);
   rmAddObjectDefConstraint(camelHerdNormalID, longAnimalConstraint);
   rmAddObjectDefConstraint(camelHerdNormalID, avoidTradeRoute);
   rmAddObjectDefConstraint(camelHerdNormalID, avoidTradeSockets);
   rmAddObjectDefConstraint(camelHerdNormalID, wadiConstraint);
   rmAddObjectDefConstraint(camelHerdNormalID, greenConstraint);
   numTries=1.5*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(camelHerdNormalID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.70);


   // Define and place oil wells

   int oilCount = 3.5*cNumberNonGaiaPlayers;

   for(i=0; < oilCount)
   {
      int oilID=rmCreateObjectDef("oil well"+i);
      rmAddObjectDefItem(oilID, "mine", 1, 0.0);
      rmSetObjectDefMinDistance(oilID, 0.0);
      rmSetObjectDefMaxDistance(oilID, rmZFractionToMeters(0.47));
      rmAddObjectDefToClass(oilID, classMines);
      rmAddObjectDefConstraint(oilID, avoidImpassableLand);
      rmAddObjectDefConstraint(oilID, playerConstraint);
      rmAddObjectDefConstraint(oilID, avoidMines);
      rmAddObjectDefConstraint(oilID, avoidTradeRoute);
      rmAddObjectDefConstraint(oilID, avoidTradeSockets);
      rmAddObjectDefConstraint(oilID, wadiConstraint);
      rmPlaceObjectDefAtLoc(oilID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place small batches of date trees all over the map

   numTries=20+3*cNumberNonGaiaPlayers;
   for (i=0; < numTries)
   {
      int dateTreesID=rmCreateObjectDef("date trees"+i);
      rmAddObjectDefItem(dateTreesID, "ypTreeBorneoPalm", rmRandInt(4,6), 7.0);
      rmSetObjectDefMinDistance(dateTreesID, 0.0);
      rmSetObjectDefMaxDistance(dateTreesID, rmZFractionToMeters(0.47));
      rmAddObjectDefToClass(dateTreesID, classDate);
      rmAddObjectDefConstraint(dateTreesID, somePlayerConstraint);
      rmAddObjectDefConstraint(dateTreesID, longAvoidImpassableLand);
      rmAddObjectDefConstraint(dateTreesID, dateConstraint);
      rmAddObjectDefConstraint(dateTreesID, avoidTradeRoute);
      rmAddObjectDefConstraint(dateTreesID, avoidTradeSockets);
      rmAddObjectDefConstraint(dateTreesID, wadiConstraint);
      rmAddObjectDefConstraint(dateTreesID, forestConstraint);
      rmPlaceObjectDefAtLoc(dateTreesID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.90);
	

   // Define and place fish

   int fishID=rmCreateObjectDef("fish");
   rmAddObjectDefItem(fishID, "ypFishTuna", 1, 4.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmZFractionToMeters(0.5));
   rmAddObjectDefConstraint(fishID, fishVsFish);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 5*cNumberNonGaiaPlayers);


   // Text
   rmSetStatusText("",0.99);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget1, avoidTradeSockets);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmZFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget2, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmZFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget3, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, 1.5*cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmZFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget4, avoidTradeSockets);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Text
   rmSetStatusText("",1.0); 
}  
