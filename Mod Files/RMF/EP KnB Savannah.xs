// RM script of Savannah
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Chooses the 3 natives, Sudanese

   // Native 1
   rmSetSubCiv(0, "Comanche");

   // Native 2
   rmSetSubCiv(1, "Comanche");

   // Native 3
   rmSetSubCiv(2, "Comanche");


   // Picks the map size
   int playerTiles = 20000;
   if (cNumberNonGaiaPlayers == 8)
	playerTiles = 14000;
   else if (cNumberNonGaiaPlayers == 7)
	playerTiles = 15000;
   else if (cNumberNonGaiaPlayers == 6)
	playerTiles = 16000;
   else if (cNumberNonGaiaPlayers == 5)
	playerTiles = 17000;
   else if (cNumberNonGaiaPlayers == 4)
	playerTiles = 18000;
   else if (cNumberNonGaiaPlayers == 3)
	playerTiles = 19000;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses the side where teams start in team games
   int teamSide = rmRandInt(1,4);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("Texas");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.10, 4, 0.4, 5.0);
   rmTerrainInitialize("pampas\groundforest_pam", 1.0);
   rmSetMapType("grass");
   rmSetMapType("water");
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("nuggets");
   rmDefineClass("natives");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int longestPlayerConstraint=rmCreateClassDistanceConstraint("natives stay away from players", classPlayer, 150.0);
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 70.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 80.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 60.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 5.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 50.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);

   // Native avoidance
   int avoidNatives=rmCreateClassDistanceConstraint("things avoids natives", rmClassID("natives"), 5.0);
   int avoidNativesLong=rmCreateClassDistanceConstraint("natives avoids natives", rmClassID("natives"), 180.0);

   // Water objects avoidance	
   int fishVsFish=rmCreateTypeDistanceConstraint("fish avoid fish", "ypFishTuna", 25.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish avoid land", "land", true, 5.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag avoid land", "land", true, 15.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid flags", "HomeCityWaterSpawnFlag", 25);


   // Text
   rmSetStatusText("",0.10);


   // Define and place lake in middle

   int lakeID=rmCreateArea("Lake");
   rmSetAreaWaterType(lakeID, "bayou spc");
   rmSetAreaSize(lakeID, 0.02, 0.07);
   rmSetAreaCoherence(lakeID, 0.7);
   rmSetAreaLocation(lakeID, 0.5, 0.5);
   rmSetAreaBaseHeight(lakeID, 0.0);
   rmSetAreaObeyWorldCircleConstraint(lakeID, false);
   rmSetAreaMinBlobs(lakeID, 3);
   rmSetAreaMaxBlobs(lakeID, 3);
   rmSetAreaMinBlobDistance(lakeID, 3);
   rmSetAreaMaxBlobDistance(lakeID, 3);
   rmSetAreaSmoothDistance(lakeID, 10);
   rmBuildArea(lakeID);


   // Text
   rmSetStatusText("",0.20);


   // Place players in FFA and team games

   // Player placement if FFA (Free For All) - place players in circle
   if(cNumberTeams > 2)
   {
      rmSetTeamSpacingModifier(0.7);
      rmPlacePlayersCircular(0.4, 0.4, 0.0);
   }

   // Player placement if teams - place teams in circle to apart from each other
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      if (rmGetNumberPlayersOnTeam(0) == 2)
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.05, 0.20);
         else if (teamSide == 2)
            rmSetPlacementSection(0.30, 0.45);
         else if (teamSide == 3)
            rmSetPlacementSection(0.55, 0.70);
         else if (teamSide == 4)
            rmSetPlacementSection(0.80, 0.95);
      }
      else
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.00, 0.25);
         else if (teamSide == 2)
            rmSetPlacementSection(0.25, 0.50);
         else if (teamSide == 3)
            rmSetPlacementSection(0.50, 0.75);
         else if (teamSide == 4)
            rmSetPlacementSection(0.75, 1.00);
      }
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.4, 0.4, 0.0);

      rmSetPlacementTeam(1);
      if (rmGetNumberPlayersOnTeam(1) == 2)
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.55, 0.70);
         else if (teamSide == 2)
            rmSetPlacementSection(0.80, 0.95);
         else if (teamSide == 3)
            rmSetPlacementSection(0.05, 0.20);
         else if (teamSide == 4)
            rmSetPlacementSection(0.30, 0.45);
      }
      else
      {
         if (teamSide == 1)
            rmSetPlacementSection(0.50, 0.75);
         else if (teamSide == 2)
            rmSetPlacementSection(0.75, 1.00);
         else if (teamSide == 3)
            rmSetPlacementSection(0.00, 0.25);
         else if (teamSide == 4)
            rmSetPlacementSection(0.25, 0.50);
      }
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.4, 0.4, 0.0);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.30);
		

   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "TreeTexasDirt", 6, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 14);
   rmSetObjectDefMaxDistance(startAreaTreeID, 18);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startGazelleID=rmCreateObjectDef("starting gazelle");
   rmAddObjectDefItem(startGazelleID, "rhea", 6, 6.0);
   rmSetObjectDefCreateHerd(startGazelleID, true);
   rmSetObjectDefMinDistance(startGazelleID, 14);
   rmSetObjectDefMaxDistance(startGazelleID, 18);
   rmAddObjectDefConstraint(startGazelleID, avoidStartResource);
   rmAddObjectDefConstraint(startGazelleID, avoidImpassableLand);
   rmAddObjectDefConstraint(startGazelleID, shortAvoidStartingUnits);

   int startCopperID=rmCreateObjectDef("player copper");
   rmAddObjectDefItem(startCopperID, "MineCopper", 1, 5.0);
   rmSetObjectDefMinDistance(startCopperID, 14);
   rmSetObjectDefMaxDistance(startCopperID, 18);
   rmAddObjectDefConstraint(startCopperID, avoidStartResource);
   rmAddObjectDefConstraint(startCopperID, avoidImpassableLand);
   rmAddObjectDefConstraint(startCopperID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startGazelleID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startCopperID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
      rmAddClosestPointConstraint(flagVsFlag);
      rmAddClosestPointConstraint(flagLand);		
      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Text
   rmSetStatusText("",0.40);


   // Define and place the 3 natives: always Sudanese

   // Native 1
   int nativeVillageType = rmRandInt(1,5);
   int nativeVillageID = rmCreateGrouping("sudanese village", "native comanche village "+nativeVillageType);
   rmSetGroupingMinDistance(nativeVillageID, 0.0);
   rmSetGroupingMaxDistance(nativeVillageID, rmXFractionToMeters(0.47));
   rmAddGroupingToClass(nativeVillageID, rmClassID("natives"));
   rmAddGroupingConstraint(nativeVillageID, avoidImpassableLand);
   rmAddGroupingConstraint(nativeVillageID, longestPlayerConstraint);
   rmAddGroupingConstraint(nativeVillageID, avoidNativesLong);
   rmPlaceGroupingAtLoc(nativeVillageID, 0, 0.5, 0.5);

   // Native 2
   int nativeVillage2Type = rmRandInt(1,5);
   int nativeVillage2ID = rmCreateGrouping("sudanese village 2", "native comanche village "+nativeVillage2Type);
   rmSetGroupingMinDistance(nativeVillage2ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage2ID, rmXFractionToMeters(0.47));
   rmAddGroupingToClass(nativeVillage2ID, rmClassID("natives"));
   rmAddGroupingConstraint(nativeVillage2ID, avoidImpassableLand);
   rmAddGroupingConstraint(nativeVillage2ID, longestPlayerConstraint);
   rmAddGroupingConstraint(nativeVillage2ID, avoidNativesLong);
   rmPlaceGroupingAtLoc(nativeVillage2ID, 0, 0.5, 0.5);

   // Native 3
   int nativeVillage3Type = rmRandInt(1,5);
   int nativeVillage3ID = rmCreateGrouping("sudanese village 3", "native comanche village "+nativeVillage2Type);
   rmSetGroupingMinDistance(nativeVillage3ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage3ID, rmXFractionToMeters(0.47));
   rmAddGroupingToClass(nativeVillage3ID, rmClassID("natives"));
   rmAddGroupingConstraint(nativeVillage3ID, avoidImpassableLand);
   rmAddGroupingConstraint(nativeVillage3ID, longestPlayerConstraint);
   rmAddGroupingConstraint(nativeVillage3ID, avoidNativesLong);
   rmPlaceGroupingAtLoc(nativeVillage3ID, 0, 0.5, 0.5);


   // Define and place forests
	
   int numTries=20*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(400), rmAreaTilesToFraction(500));
      rmSetAreaForestType(forest, "texas forest");
      rmSetAreaForestDensity(forest, 0.8);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.5);
      rmSetAreaForestUnderbrush(forest, 0.2);
      rmSetAreaBaseHeight(forest, 1.0);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, avoidNatives);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.  
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }


   // Text
   rmSetStatusText("",0.50);


   // Define and place animals
   
   // Define and place ostrich herds
   int ostrichHerdID=rmCreateObjectDef("ostrich herd");
   rmAddObjectDefItem(ostrichHerdID, "rhea", rmRandInt(7,9), 9.0);
   rmSetObjectDefCreateHerd(ostrichHerdID, true);
   rmSetObjectDefMinDistance(ostrichHerdID, 0.0);
   rmSetObjectDefMaxDistance(ostrichHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(ostrichHerdID, classAnimals);
   rmAddObjectDefConstraint(ostrichHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(ostrichHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(ostrichHerdID, animalConstraint);
   rmAddObjectDefConstraint(ostrichHerdID, avoidNatives);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(ostrichHerdID, 0, 0.5, 0.5);

   // Define and place elephant herds
   int elephantHerdID=rmCreateObjectDef("elephant herd");
   rmAddObjectDefItem(elephantHerdID, "ypWildElephant", rmRandInt(4,5), 9.0);
   rmSetObjectDefCreateHerd(elephantHerdID, true);
   rmSetObjectDefMinDistance(elephantHerdID, 0.0);
   rmSetObjectDefMaxDistance(elephantHerdID, rmXFractionToMeters(0.30));
   rmAddObjectDefToClass(elephantHerdID, classAnimals);
   rmAddObjectDefConstraint(elephantHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(elephantHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(elephantHerdID, animalConstraint);
   rmAddObjectDefConstraint(elephantHerdID, avoidNatives);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(elephantHerdID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.60);


   // Define and place copper mines

   int copperCount = 3*cNumberNonGaiaPlayers+3;

   for(i=0; < copperCount)
   {
      int copperID=rmCreateObjectDef("copper mine"+i);
      rmAddObjectDefItem(copperID, "MineCopper", 1, 0.0);
      rmSetObjectDefMinDistance(copperID, 0.0);
      rmSetObjectDefMaxDistance(copperID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(copperID, classMines);
      rmAddObjectDefConstraint(copperID, avoidImpassableLand);
      rmAddObjectDefConstraint(copperID, somePlayerConstraint);
      rmAddObjectDefConstraint(copperID, avoidMines);
      rmAddObjectDefConstraint(copperID, avoidNatives);
      rmPlaceObjectDefAtLoc(copperID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.70);
	

   // Define and place fish

   int fishID=rmCreateObjectDef("fish");
   rmAddObjectDefItem(fishID, "ypFishTuna", 1, 4.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(fishID, fishVsFish);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 20);


   // Text
   rmSetStatusText("",0.80);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidAll);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, 1.5*cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidNatives);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Text
   rmSetStatusText("",1.0); 
}