// RM script of Alps
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Chooses the 1 native, always Huns

   // Native 1
   rmSetSubCiv(0, "Comanche");


   // Picks the map size
   int playerTiles = 20000;
   if (cNumberNonGaiaPlayers == 8)
	playerTiles = 14000;
   else if (cNumberNonGaiaPlayers == 7)
	playerTiles = 15000;
   else if (cNumberNonGaiaPlayers == 6)
	playerTiles = 16000;
   else if (cNumberNonGaiaPlayers == 5)
	playerTiles = 17000;
   else if (cNumberNonGaiaPlayers == 4)
	playerTiles = 18000;
   else if (cNumberNonGaiaPlayers == 3)
	playerTiles = 19000;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);
	

   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses if the middle is open (0) or cliff (1)
   int middleArea = rmRandInt(0,1);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("Paris");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.1, 4, 0.4, 6.0);
   rmTerrainInitialize("terrain\himalayas\ground_dirt5_himal", 1.0);
   rmSetMapType("snow");
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);


   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classMines=rmDefineClass("mines");
   int classAnimals=rmDefineClass("animals");
   int classCliff=rmDefineClass("cliff");
   int classLand=rmDefineClass("land");
   rmDefineClass("starting settlement");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("classTreeStraggler");
   rmDefineClass("nuggets");
   rmDefineClass("natives");


   // Define constraints
   
   // Map edge avoidance
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player avoidance
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 45.0);
   int shortForestConstraint=rmCreateClassDistanceConstraint("forest vs. forest short", rmClassID("classForest"), 10.0);
   int stragglerTreeConstraint=rmCreateClassDistanceConstraint("straggler tree constraint", rmClassID("classTreeStraggler"), 20.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 90.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 55.0);

   // Impassable land avoidance
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 6.0);

   // Area avoidance
   int cliffConstraint=rmCreateClassDistanceConstraint("avoid cliff", classCliff, 10.0);
   int landConstraint=rmCreateClassDistanceConstraint("land avoid land", classLand, 40.0);
   int landConstraintShort=rmCreateClassDistanceConstraint("land avoid land short", classLand, 5.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 40.0);

   // Native avoidance
   int avoidNatives=rmCreateClassDistanceConstraint("things avoids natives", rmClassID("natives"), 5.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);

   // Trade route avoidance
   int avoidTradeRoute=rmCreateTradeRouteDistanceConstraint("trade route", 5.0);
   int avoidTradeSockets=rmCreateTypeDistanceConstraint("avoid trade sockets", "SocketTradeRoute", 5.0);


   // Text
   rmSetStatusText("",0.10);


   // Define and place land areas

   // South land
   int landID=rmCreateArea("Land");
   rmSetAreaTerrainType(landID, "terrain\himalayas\ground_dirt5_himal");
   rmSetAreaSize(landID, 0.2, 0.2);
   rmSetAreaCoherence(landID, 0.6);
   rmSetAreaLocation(landID, 0.2, 0.2);
   rmSetAreaObeyWorldCircleConstraint(landID, false);
   rmAddAreaToClass(landID, classLand);
   rmAddAreaConstraint(landID, landConstraint); 
   rmBuildArea(landID);

   // Northwest land
   int land2ID=rmCreateArea("Land 2");
   rmSetAreaTerrainType(land2ID, "terrain\himalayas\ground_dirt5_himal");
   rmSetAreaSize(land2ID, 0.2, 0.2);
   rmSetAreaCoherence(land2ID, 0.6);
   rmSetAreaLocation(land2ID, 0.35, 0.8);
   rmSetAreaObeyWorldCircleConstraint(land2ID, false);
   rmAddAreaToClass(land2ID, classLand);
   rmAddAreaConstraint(land2ID, landConstraint); 
   rmBuildArea(land2ID);

   // Northeast land
   int land3ID=rmCreateArea("Land 3");
   rmSetAreaTerrainType(land3ID, "terrain\himalayas\ground_dirt5_himal");
   rmSetAreaSize(land3ID, 0.25, 0.25);
   rmSetAreaCoherence(land3ID, 0.6);
   rmSetAreaLocation(land3ID, 0.8, 0.45);
   rmSetAreaObeyWorldCircleConstraint(land3ID, false);
   rmAddAreaToClass(land3ID, classLand);
   rmAddAreaConstraint(land3ID, landConstraint); 
   rmBuildArea(land3ID);

   // Define and place cliffs between land areas

   int cliffID=rmCreateArea("cliff");
   rmSetAreaSize(cliffID, 0.8, 0.8);
   rmSetAreaCoherence(cliffID, 0.2);
   rmSetAreaLocation(cliffID, 1.05, 1.05);
   rmSetAreaBaseHeight(cliffID, 15);
   rmSetAreaTerrainType(cliffID, "terrain\rockies\clifftop_roc");
   rmAddAreaTerrainLayer(cliffID, "terrain\rockies\clifftop_roc", 4, 12);
   rmAddAreaTerrainLayer(cliffID, "terrain\rockies\clifftop_roc", 0, 4);
   rmSetAreaHeightBlend(cliffID, 2);
   rmSetAreaElevationType(cliffID, cElevTurbulence);
   rmSetAreaElevationVariation(cliffID, 12.0);
   rmSetAreaObeyWorldCircleConstraint(cliffID, false);
   rmAddAreaToClass(cliffID, classCliff);
   rmAddAreaConstraint(cliffID, landConstraintShort); 
   rmBuildArea(cliffID);


   // Define and place land/cliff in middle

   if(middleArea == 0)
   {
      int land4ID=rmCreateArea("Land4");
      rmSetAreaTerrainType(land4ID, "terrain\himalayas\ground_dirt5_himal");
      rmSetAreaSize(land4ID, 0.06, 0.06);
      rmSetAreaCoherence(land4ID, 0.9);
      rmSetAreaLocation(land4ID, 0.5, 0.5);
      rmSetAreaBaseHeight(land4ID, 0);
      rmSetAreaHeightBlend(land4ID, 2);
      rmSetAreaObeyWorldCircleConstraint(land4ID, false);
      rmSetAreaElevationType(land4ID, cElevTurbulence);
      rmSetAreaElevationVariation(land4ID, 8.0);
      rmSetAreaElevationMinFrequency(land4ID, 0.10);
      rmSetAreaElevationOctaves(land4ID, 4);
      rmSetAreaElevationPersistence(land4ID, 0.4);
      rmBuildArea(land4ID);
   }
   else
   {
      land4ID=rmCreateArea("Land4");
      rmSetAreaTerrainType(land4ID, "terrain\rockies\clifftop_roc");
      rmAddAreaTerrainLayer(land4ID, "terrain\rockies\clifftop_roc", 5, 16);
      rmAddAreaTerrainLayer(land4ID, "terrain\rockies\clifftop_roc", 0, 5);
      rmSetAreaSize(land4ID, 0.03, 0.03);
      rmSetAreaCoherence(land4ID, 0.4);
      rmSetAreaLocation(land4ID, 0.5, 0.5);
      rmSetAreaBaseHeight(land4ID, 20);
      rmSetAreaHeightBlend(land4ID, 2);
      rmSetAreaElevationType(land4ID, cElevTurbulence);
      rmSetAreaElevationVariation(land4ID, 12.0);
      rmSetAreaObeyWorldCircleConstraint(land4ID, false);
      rmAddAreaToClass(land4ID, classCliff);
      rmBuildArea(land4ID);


      // Define and place fake areas to make connections place properly

      int fakeID=rmCreateArea("Fake");
      rmSetAreaSize(fakeID, 0.01, 0.01);
      rmSetAreaCoherence(fakeID, 1.0);
      rmSetAreaLocation(fakeID, 0.25, 0.5);
      rmBuildArea(fakeID);

      int fake2ID=rmCreateArea("Fake 2");
      rmSetAreaSize(fake2ID, 0.01, 0.01);
      rmSetAreaCoherence(fake2ID, 1.0);
      rmSetAreaLocation(fake2ID, 0.25, 0.7);
      rmBuildArea(fake2ID);

      int fake3ID=rmCreateArea("Fake 3");
      rmSetAreaSize(fake3ID, 0.01, 0.01);
      rmSetAreaCoherence(fake3ID, 1.0);
      rmSetAreaLocation(fake3ID, 0.45, 0.2);
      rmBuildArea(fake3ID);

      int fake4ID=rmCreateArea("Fake 4");
      rmSetAreaSize(fake4ID, 0.01, 0.01);
      rmSetAreaCoherence(fake4ID, 1.0);
      rmSetAreaLocation(fake4ID, 0.7, 0.2);
      rmBuildArea(fake4ID);

      int fake5ID=rmCreateArea("Fake 5");
      rmSetAreaSize(fake5ID, 0.01, 0.01);
      rmSetAreaCoherence(fake5ID, 1.0);
      rmSetAreaLocation(fake5ID, 0.65, 0.75);
      rmBuildArea(fake5ID);

      int fake6ID=rmCreateArea("Fake 6");
      rmSetAreaSize(fake6ID, 0.01, 0.01);
      rmSetAreaCoherence(fake6ID, 1.0);
      rmSetAreaLocation(fake6ID, 0.8, 0.65);
      rmBuildArea(fake6ID);


      // Define and place connections when the middle is cliff

      int connectionID=rmCreateConnection("Pass 1");
      rmSetConnectionType(connectionID, cConnectAreas, false, 1.0);
      rmAddConnectionTerrainReplacement(connectionID, "terrain\rockies\clifftop_roc", "terrain\himalayas\ground_dirt5_himal");
      rmAddConnectionTerrainReplacement(connectionID, "terrain\rockies\clifftop_roc", "terrain\himalayas\ground_dirt5_himal");
      rmAddConnectionTerrainReplacement(connectionID, "terrain\rockies\clifftop_roc", "terrain\himalayas\ground_dirt5_himal");
      rmSetConnectionWidth(connectionID, 20, 0);  
      rmSetConnectionHeightBlend(connectionID, 2);
      rmSetConnectionCoherence(connectionID, 0.6);
      rmSetConnectionBaseHeight(connectionID, 2.0);
      rmAddConnectionArea(connectionID, fakeID);
      rmAddConnectionArea(connectionID, fake2ID);
      rmBuildConnection(connectionID);

      int connection2ID=rmCreateConnection("Pass 2");
      rmSetConnectionType(connection2ID, cConnectAreas, false, 1.0);
      rmAddConnectionTerrainReplacement(connection2ID, "terrain\rockies\clifftop_roc", "terrain\himalayas\ground_dirt5_himal");
      rmAddConnectionTerrainReplacement(connection2ID, "terrain\rockies\clifftop_roc", "terrain\himalayas\ground_dirt5_himal");
      rmAddConnectionTerrainReplacement(connection2ID, "terrain\rockies\clifftop_roc", "terrain\himalayas\ground_dirt5_himal");
      rmSetConnectionWidth(connection2ID, 20, 0);  
      rmSetConnectionHeightBlend(connection2ID, 2);
      rmSetConnectionCoherence(connection2ID, 0.6);
      rmSetConnectionBaseHeight(connection2ID, 2.0);
      rmAddConnectionArea(connection2ID, fake3ID);
      rmAddConnectionArea(connection2ID, fake4ID);
      rmBuildConnection(connection2ID);

      int connection3ID=rmCreateConnection("Pass 3");
      rmSetConnectionType(connection3ID, cConnectAreas, false, 1.0);
      rmAddConnectionTerrainReplacement(connection3ID, "terrain\rockies\clifftop_roc", "terrain\himalayas\ground_dirt5_himal");
      rmAddConnectionTerrainReplacement(connection3ID, "terrain\rockies\clifftop_roc", "terrain\himalayas\ground_dirt5_himal");
      rmAddConnectionTerrainReplacement(connection3ID, "terrain\rockies\clifftop_roc", "terrain\himalayas\ground_dirt5_himal");
      rmSetConnectionWidth(connection3ID, 20, 0);  
      rmSetConnectionHeightBlend(connection3ID, 2);
      rmSetConnectionCoherence(connection3ID, 0.6);
      rmSetConnectionBaseHeight(connection3ID, 2.0);
      rmAddConnectionArea(connection3ID, fake5ID);
      rmAddConnectionArea(connection3ID, fake6ID);
      rmBuildConnection(connection3ID);
   }


   // Text
   rmSetStatusText("",0.20);


   // Text
   rmSetStatusText("",0.30);


   // Place players - in team and FFA games

   // Player placement if FFA (Free For All) - place players in circle
   if(cNumberTeams > 2)
   {
      rmSetTeamSpacingModifier(0.7);
      rmSetPlacementSection(0.20, 0.73);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);
   }

   // Player placement if teams - place teams in lines
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      if (rmGetNumberPlayersOnTeam(0) == 2)
         rmSetPlacementSection(0.24, 0.36);
      else
         rmSetPlacementSection(0.20, 0.40);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);

      rmSetPlacementTeam(1);
      if (rmGetNumberPlayersOnTeam(1) == 2)
         rmSetPlacementSection(0.57, 0.69);
      else
         rmSetPlacementSection(0.53, 0.73);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);
   }


   // Text
   rmSetStatusText("",0.40);


   // Create the player's areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.50);
		

   // Define starting objects and resources

   // Define Town Center
   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefConstraint(startingTCID, avoidTradeRoute);
   rmAddObjectDefConstraint(startingTCID, avoidTradeSockets);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   // Define starting units
   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   // Define starting trees
   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "ypTreeHimalayas", 10, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 12);
   rmSetObjectDefMaxDistance(startAreaTreeID, 16);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);
   rmAddObjectDefConstraint(startAreaTreeID, avoidTradeRoute);
   rmAddObjectDefConstraint(startAreaTreeID, avoidTradeSockets);

   // Define starting berries
   int startBerryID=rmCreateObjectDef("starting berry");
   rmAddObjectDefItem(startBerryID, "berrybush", 4, 4.0);
   rmSetObjectDefCreateHerd(startBerryID, true);
   rmSetObjectDefMinDistance(startBerryID, 14);
   rmSetObjectDefMaxDistance(startBerryID, 18);
   rmAddObjectDefConstraint(startBerryID, avoidStartResource);
   rmAddObjectDefConstraint(startBerryID, avoidImpassableLand);
   rmAddObjectDefConstraint(startBerryID, shortAvoidStartingUnits);
   rmAddObjectDefConstraint(startBerryID, avoidTradeRoute);
   rmAddObjectDefConstraint(startBerryID, avoidTradeSockets);

   // Define starting mine
   int startSilverID=rmCreateObjectDef("player silver");
   rmAddObjectDefItem(startSilverID, "Mine", 1, 5.0);
   rmSetObjectDefMinDistance(startSilverID, 14);
   rmSetObjectDefMaxDistance(startSilverID, 18);
   rmAddObjectDefConstraint(startSilverID, avoidStartResource);
   rmAddObjectDefConstraint(startSilverID, avoidImpassableLand);
   rmAddObjectDefConstraint(startSilverID, shortAvoidStartingUnits);
   rmAddObjectDefConstraint(startSilverID, avoidTradeRoute);
   rmAddObjectDefConstraint(startSilverID, avoidTradeSockets);


   // Place starting objects and resources

   for(i=1; <cNumberPlayers)
   {					
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startBerryID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startSilverID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
   }


   // Text
   rmSetStatusText("",0.60);


   // Define and place the 1 native to middle, always Huns, if middle is open

   if(middleArea == 0)
   {
      int nativeVillageType = rmRandInt(1,5);
      int nativeVillageID = rmCreateGrouping("huns village", "native comanche village "+nativeVillageType);
      rmSetGroupingMinDistance(nativeVillageID, 0.0);
      rmSetGroupingMaxDistance(nativeVillageID, 10.0);
      rmAddGroupingToClass(nativeVillageID, rmClassID("natives"));
      rmPlaceGroupingAtLoc(nativeVillageID, 0, 0.5, 0.5);
   }


   // Define and place trade route - goes around the map

   // Create the trade route
   int tradeRouteID = rmCreateTradeRoute();
   int socketID=rmCreateObjectDef("sockets to dock trade posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

   // Define the sockets
   rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socketID, true);
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 12.0);

   // Set the trade route waypoints
   rmAddTradeRouteWaypoint(tradeRouteID, 0.65, 0.9);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.54, 0.64, 0, 0);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.47, 0.66, 0, 0);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.42, 0.63, 0, 0);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.2, 0.7, 0, 0);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.2, 0.8, 10, 10);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.3, 0.88, 10, 10);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.37, 0.93, 10, 10);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.5, 0.93, 10, 10);
   rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.65, 0.9, 10, 10);
   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route"); 
  
   // Place sockets to trade route
   vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.0);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.25);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.75);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);


   // Text
   rmSetStatusText("",0.70);


   // Define and place forests
	
   int numTries=10*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(450), rmAreaTilesToFraction(500));
      rmSetAreaForestType(forest, "Himalayas Forest");
      rmSetAreaForestDensity(forest, 1.0);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.5);
      rmSetAreaForestUnderbrush(forest, 0.3);
      rmSetAreaBaseHeight(forest, 1.0);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, cliffConstraint);
      rmAddAreaConstraint(forest, avoidNatives);
      rmAddAreaConstraint(forest, avoidTradeRoute);
      rmAddAreaConstraint(forest, avoidTradeSockets);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.  
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }


   // Define and place straggler trees
   for (i=0; < 40)
   { 
      int stragglerTreeID=rmCreateObjectDef("straggler tree"+i);
      rmAddObjectDefItem(stragglerTreeID, "ypTreeHimalayas", rmRandInt(1,5), 10.0);
      rmSetObjectDefMinDistance(stragglerTreeID, 0.0);
      rmSetObjectDefMaxDistance(stragglerTreeID, rmZFractionToMeters(0.47));
      rmAddObjectDefToClass(stragglerTreeID, rmClassID("classForest"));
      rmAddObjectDefToClass(stragglerTreeID, rmClassID("classTreeStraggler"));
      rmAddObjectDefConstraint(stragglerTreeID, somePlayerConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, avoidImpassableLand);
      rmAddObjectDefConstraint(stragglerTreeID, shortForestConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, stragglerTreeConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, avoidNatives);
      rmAddObjectDefConstraint(stragglerTreeID, avoidTradeRoute);
      rmAddObjectDefConstraint(stragglerTreeID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(stragglerTreeID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place land animals

   // Define and place ibex herds
   int ibexHerdID=rmCreateObjectDef("ibex herd");
   rmAddObjectDefItem(ibexHerdID, "ypIbex", rmRandInt(7,8), 6.0);
   rmSetObjectDefCreateHerd(ibexHerdID, true);
   rmSetObjectDefMinDistance(ibexHerdID, 0.0);
   rmSetObjectDefMaxDistance(ibexHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(ibexHerdID, classAnimals);
   rmAddObjectDefConstraint(ibexHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(ibexHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(ibexHerdID, animalConstraint);
   rmAddObjectDefConstraint(ibexHerdID, avoidNatives);
   rmAddObjectDefConstraint(ibexHerdID, cliffConstraint);
   rmAddObjectDefConstraint(ibexHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(ibexHerdID, avoidTradeSockets);
   numTries=3*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(ibexHerdID, 0, 0.5, 0.5);


   // Define and place silver mines

   int silverCount = 4*cNumberNonGaiaPlayers;

   for(i=0; < silverCount)
   {
      int silverID=rmCreateObjectDef("silver mine"+i);
      rmAddObjectDefItem(silverID, "mine", 1, 0.0);
      rmSetObjectDefMinDistance(silverID, 0.0);
      rmSetObjectDefMaxDistance(silverID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(silverID, classMines);
      rmAddObjectDefConstraint(silverID, avoidImpassableLand);
      rmAddObjectDefConstraint(silverID, playerConstraint);
      rmAddObjectDefConstraint(silverID, cliffConstraint);
      rmAddObjectDefConstraint(silverID, avoidMines);
      rmAddObjectDefConstraint(silverID, avoidNatives);
      rmAddObjectDefConstraint(silverID, avoidTradeRoute);
      rmAddObjectDefConstraint(silverID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(silverID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.90);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidNatives);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, cliffConstraint);
   rmAddObjectDefConstraint(nugget2, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget2, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget2, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, cliffConstraint);
   rmAddObjectDefConstraint(nugget3, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget3, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget3, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, cliffConstraint);
   rmAddObjectDefConstraint(nugget4, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget4, avoidTradeSockets);
   rmAddObjectDefConstraint(nugget4, avoidNatives);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);
	  

   // Text
   rmSetStatusText("",1.0); 
}  
