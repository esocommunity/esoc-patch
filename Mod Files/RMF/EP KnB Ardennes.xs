// RM script of Ardennes
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Picks the map size
   int playerTiles = 11000;
   if (cNumberNonGaiaPlayers > 4)
      playerTiles = 10000;
   if (cNumberNonGaiaPlayers > 6)
      playerTiles = 8500;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses the side where teams start in team games
   int teamSide = rmRandInt(1,4);


   // Chooses the type of trade route
   int routeType = rmRandInt(1,40);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("constantinople");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.10, 4, 0.4, 10.0);
   rmTerrainInitialize("borneo\ground_grass1_borneo", 1.0);
   rmSetMapType("grass");
   rmSetMapType("land");
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);


   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("nuggets");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 40.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 80.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 55.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 5.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 40.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);

   // Trade route avoidance
   int avoidTradeRoute=rmCreateTradeRouteDistanceConstraint("trade route", 5.0);
   int avoidTradeSockets=rmCreateTypeDistanceConstraint("avoid trade sockets", "SocketTradeRoute", 5.0);


   // Text
   rmSetStatusText("",0.10);


   // Place players - in team and FFA games

   // Player placement if FFA (Free For All) - place players in circle
   if(cNumberTeams > 2)
   {
      rmSetTeamSpacingModifier(0.7);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);
   }

   // Player placement if teams - place teams in circle to apart from each other
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      if (teamSide == 1)
            rmSetPlacementSection(0.00, 0.25);
      else if (teamSide == 2)
            rmSetPlacementSection(0.25, 0.50);
      else if (teamSide == 3)
            rmSetPlacementSection(0.50, 0.75);
      else if (teamSide == 4)
            rmSetPlacementSection(0.75, 1.00);
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);

      rmSetPlacementTeam(1);
      if (teamSide == 1)
            rmSetPlacementSection(0.50, 0.75);
      else if (teamSide == 2)
            rmSetPlacementSection(0.75, 1.00);
      else if (teamSide == 3)
            rmSetPlacementSection(0.00, 0.25);
      else if (teamSide == 4)
            rmSetPlacementSection(0.25, 0.50);
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.36, 0.36, 0.0);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.20);


   // Define and place trade routes - 40 different variations!

   // Create the trade routes

   // Trade route 1
   int tradeRouteID = rmCreateTradeRoute();
   int socketID=rmCreateObjectDef("sockets to dock trade posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

   // Trade route 2
   int tradeRoute2ID = rmCreateTradeRoute();
   int socket2ID=rmCreateObjectDef("sockets to dock trade posts 2");
   rmSetObjectDefTradeRouteID(socket2ID, tradeRoute2ID);

   // Trade route 3
   int tradeRoute3ID = rmCreateTradeRoute();
   int socket3ID=rmCreateObjectDef("sockets to dock trade posts 3");
   rmSetObjectDefTradeRouteID(socket3ID, tradeRoute3ID);

   // Trade route 4
   int tradeRoute4ID = rmCreateTradeRoute();
   int socket4ID=rmCreateObjectDef("sockets to dock trade posts 4");
   rmSetObjectDefTradeRouteID(socket4ID, tradeRoute4ID);

   // Define the sockets

   // Sockets trade route 1
   rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socketID, true);
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 12.0);

   // Sockets trade route 2
   rmAddObjectDefItem(socket2ID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socket2ID, true);
   rmSetObjectDefMinDistance(socket2ID, 0.0);
   rmSetObjectDefMaxDistance(socket2ID, 12.0);

   // Sockets trade route 3
   rmAddObjectDefItem(socket3ID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socket3ID, true);
   rmSetObjectDefMinDistance(socket3ID, 0.0);
   rmSetObjectDefMaxDistance(socket3ID, 12.0);

   // Sockets trade route 4
   rmAddObjectDefItem(socket4ID, "SocketTradeRoute", 1, 0.0);
   rmSetObjectDefAllowOverlap(socket4ID, true);
   rmSetObjectDefMinDistance(socket4ID, 0.0);
   rmSetObjectDefMaxDistance(socket4ID, 12.0);

   // Set the trade route waypoints

   if (routeType == 1) // Long straight S-N - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.0);
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 1.0, 1.0, 0, 0);
   }
   else if (routeType == 2) // Long straight W-E - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 1.0);
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 1.0, 0.0, 0, 0);
   }
   else if (routeType == 3) // Long straight SW-NE - 1 
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.5);
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 1.0, 0.5, 0, 0);
   }
   else if (routeType == 4) // Long straight SE-NW - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.0);
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.5, 1.0, 0, 0);
   }
   else if (routeType == 5) // Short straight S-N - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.35, 0.35);
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.65, 0.65, 0, 0);
   }
   else if (routeType == 6) // Short straight W-E - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.35, 0.65);
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.65, 0.35, 0, 0);
   }
   else if (routeType == 7) // Short straight SW-NE - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.5);
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.7, 0.5, 0, 0);
   }
   else if (routeType == 8) // Short straight SE-NW - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.3);
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.5, 0.7, 0, 0);
   }
   else if (routeType == 9) // Short straight SE-NW (1) + short straight SW-NE (2) - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.3);
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.5, 0.7, 0, 0);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.3, 0.5);
      rmAddRandomTradeRouteWaypoints(tradeRoute2ID, 0.7, 0.5, 0, 0);
   }
   else if (routeType == 10) // Short straight S-N (1) + short straight W-E (2) - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.35, 0.35);
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.65, 0.65, 0, 0);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.35, 0.65);
      rmAddRandomTradeRouteWaypoints(tradeRoute2ID, 0.65, 0.35, 0, 0);
   }
   else if (routeType == 11) // Long straight SE-NW (1) + long straight SW-NE (2) - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.0);
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.5, 1.0, 0, 0);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.0, 0.5);
      rmAddRandomTradeRouteWaypoints(tradeRoute2ID, 1.0, 0.5, 0, 0);
   }
   else if (routeType == 12) // Long straight S-N (1) + long straight W-E (2) - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.0);
      rmAddRandomTradeRouteWaypoints(tradeRouteID, 1.0, 1.0, 0, 0);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.0, 1.0);
      rmAddRandomTradeRouteWaypoints(tradeRoute2ID, 1.0, 0.0, 0, 0);
   }
   else if (routeType == 13) // Circle outer - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.05, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.8);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.95);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.8, 0.8);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.95, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.8, 0.2);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.05);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.2);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.05, 0.5);
   }
   else if (routeType == 14) // Circle inner - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.7);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.8);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.7, 0.7);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.8, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.7, 0.3);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.2);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.3);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.5);
   }
   else if (routeType == 15) // Circle outer (1) + circle inner (2) - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.05, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.8);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.95);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.8, 0.8);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.95, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.8, 0.2);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.05);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.2);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.05, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.2, 0.5);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.3, 0.7);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.8);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.7, 0.7);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.8, 0.5);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.7, 0.3);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.2);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.3, 0.3);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.2, 0.5);
   }
   else if (routeType == 16) // Four-pointed star (1) - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.1, 0.9);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.8);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.9, 0.9);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.8, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.9, 0.1);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.2);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.1, 0.1);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.5);
   }
   else if (routeType == 17) // Four-pointed star (1) + circle inner (2) - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.1, 0.9);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.8);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.9, 0.9);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.8, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.9, 0.1);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.2);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.1, 0.1);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.2, 0.5);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.3, 0.7);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.8);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.7, 0.7);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.8, 0.5);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.7, 0.3);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.2);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.3, 0.3);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.2, 0.5);
   }
   else if (routeType == 18) // Small four-pointed star (1) + circle inner (2) - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.8);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.7);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.8, 0.8);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.7, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.8, 0.2);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.3);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.2);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.8);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.2, 0.5);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.3, 0.7);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.8);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.7, 0.7);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.8, 0.5);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.7, 0.3);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.2);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.3, 0.3);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.2, 0.5);
   }
   else if (routeType == 19) // "Eye" horizontal - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.12, 0.88);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.8);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.7, 0.7);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.8, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.88, 0.12);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.2);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.3);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.5);
   }
   else if (routeType == 20) // "Eye" vertical - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.12, 0.12);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.2, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.7);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.8);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.88, 0.88);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.8, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.7, 0.3);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.2);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.12, 0.12);
   }
   else if (routeType == 21) // Diamond horizontal - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.12, 0.88);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.7, 0.7);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.88, 0.12);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.3);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.12, 0.88);
   }
   else if (routeType == 22) // Diamond vertical - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.12, 0.12);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.7);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.88, 0.88);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.7, 0.3);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.12, 0.12);
   }
   else if (routeType == 23) // Square small normal - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.7);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.7, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.3);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.5);
   }
   else if (routeType == 24) // Square small inverted - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.3);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.7);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.7, 0.7);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.7, 0.3);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.3);
   }
   else if (routeType == 25) // Square large normal - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.99);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.99, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.0);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.5);
   }
   else if (routeType == 26) // Square large inverted - 1
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.15, 0.15);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.15, 0.85);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.85, 0.85);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.85, 0.15);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.15, 0.15);
   }
   else if (routeType == 27) // Four routes to middle long normal - 4
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.15, 0.15);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.15, 0.85);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute3ID, 0.85, 0.85);
      rmAddTradeRouteWaypoint(tradeRoute3ID, 0.5, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute4ID, 0.85, 0.15);
      rmAddTradeRouteWaypoint(tradeRoute4ID, 0.5, 0.5);
   }
   else if (routeType == 28) // Four routes to middle long inverted - 4
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 1.0);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute3ID, 1.0, 0.5);
      rmAddTradeRouteWaypoint(tradeRoute3ID, 0.5, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute4ID, 0.5, 0.0);
      rmAddTradeRouteWaypoint(tradeRoute4ID, 0.5, 0.5);
   }
   else if (routeType == 29) // Four routes to middle short normal - 4
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.3);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.3, 0.7);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute3ID, 0.7, 0.7);
      rmAddTradeRouteWaypoint(tradeRoute3ID, 0.5, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute4ID, 0.7, 0.3);
      rmAddTradeRouteWaypoint(tradeRoute4ID, 0.5, 0.5);
   }
   else if (routeType == 30) // Four routes to middle short inverted - 4
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.7);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute3ID, 0.7, 0.5);
      rmAddTradeRouteWaypoint(tradeRoute3ID, 0.5, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute4ID, 0.5, 0.3);
      rmAddTradeRouteWaypoint(tradeRoute4ID, 0.5, 0.5);
   }
   else if (routeType == 31) // Dual routes S-N - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.12);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.9, 1.0);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.12, 0.0);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 1.0, 0.9);
   }
   else if (routeType == 32) // Dual routes W-E - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.1, 1.0);
      rmAddTradeRouteWaypoint(tradeRouteID, 1.0, 0.1);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.0, 0.9);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.9, 0.0);
   }
   else if (routeType == 33) // Dual routes SW-NE - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.4);
      rmAddTradeRouteWaypoint(tradeRouteID, 1.0, 0.4);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.0, 0.6);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 1.0, 0.6);
   }
   else if (routeType == 34) // Dual routes SE-NW - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.4, 0.0);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.4, 1.0);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.6, 0.0);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.6, 1.0);
   }
   else if (routeType == 35) // Triangle, base in south - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.85, 0.85);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.0);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.0);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.0, 0.5);
   }
   else if (routeType == 36) // Triangle, base in north - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 1.0, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.15, 0.15);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 1.0);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 1.0);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 1.0, 0.5);
   }
   else if (routeType == 37) // Triangle, base in east - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.0);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.15, 0.85);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.99, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.99, 0.5);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.0);
   }
   else if (routeType == 38) // Triangle, base in west - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.99);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.85, 0.15);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.0, 0.5);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.99);
   }
   else if (routeType == 39) // Square small normal (1) + square large normal (2) - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.7);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.7, 0.5);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.5, 0.3);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.5);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.0, 0.5);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.99);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.99, 0.5);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.5, 0.0);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.0, 0.5);
   }
   else if (routeType == 40) // Square small inverted (1) + square large inverted (2) - 2
   {
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.3);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.7);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.7, 0.7);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.7, 0.3);
      rmAddTradeRouteWaypoint(tradeRouteID, 0.3, 0.3);

      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.15, 0.15);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.15, 0.85);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.85, 0.85);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.85, 0.15);
      rmAddTradeRouteWaypoint(tradeRoute2ID, 0.15, 0.15);
   }
   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route");

   bool placedTradeRoute2 = rmBuildTradeRoute(tradeRoute2ID, "dirt");
   if(placedTradeRoute2 == false)
      rmEchoError("Failed to place trade route 2");

   bool placedTradeRoute3 = rmBuildTradeRoute(tradeRoute3ID, "dirt");
   if(placedTradeRoute3 == false)
      rmEchoError("Failed to place trade route 3");

   bool placedTradeRoute4 = rmBuildTradeRoute(tradeRoute4ID, "dirt");
   if(placedTradeRoute4 == false)
      rmEchoError("Failed to place trade route 4");


   // Text
   rmSetStatusText("",0.30);


   // Place sockets to trade routes

   if (routeType <= 4) // Routes 1-4
   {
      vector socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.2);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.8);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   }
   if (routeType == 5 || routeType == 6 || routeType == 7 || routeType == 8) // Routes 5-8
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.1);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.9);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   }
   if (routeType == 9 || routeType == 10) // Routes 9-10
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.0);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 1.0);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

      vector socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.0);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 1.0);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
   }
   if (routeType == 11) // Route 11
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.1);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.9);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.1);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.9);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
   }
   if (routeType == 12) // Route 12
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.2);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.8);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.2);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.8);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
   }
   if (routeType == 13 || routeType == 14) // Routes 13-14
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.13);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.37);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.63);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.87);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   }
   if (routeType == 15) // Route 15
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.13);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.63);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.37);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.87);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
   }
   if (routeType == 16) // Route 16
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.0);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.25);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.75);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   }
   if (routeType == 17) // Route 17
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.0);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.25);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.75);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.13);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.63);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
   }
   if (routeType == 18) // Route 18
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.0);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.37);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.87);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
   }
   if (routeType == 19) // Route 19
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.0);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.25);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.75);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   }
   if (routeType == 20 || routeType == 21 || routeType == 22) // Routes 20-22
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.125);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.375);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.625);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.875);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   }
   if (routeType == 23 || routeType == 24 || routeType == 25 || routeType == 26) // Routes 23-26
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.0);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.25);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.75);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
   }
   if (routeType == 27 || routeType == 28) // Routes 27-28
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.2);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.2);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);

      vector socketLoc3=rmGetTradeRouteWayPoint(tradeRoute3ID, 0.2);
      rmPlaceObjectDefAtPoint(socket3ID, 0, socketLoc3);

      vector socketLoc4=rmGetTradeRouteWayPoint(tradeRoute4ID, 0.2);
      rmPlaceObjectDefAtPoint(socket4ID, 0, socketLoc4);
   }
   if (routeType == 29 || routeType == 30) // Routes 29-30
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.0);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.0);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);

      socketLoc3=rmGetTradeRouteWayPoint(tradeRoute3ID, 0.0);
      rmPlaceObjectDefAtPoint(socket3ID, 0, socketLoc3);

      socketLoc4=rmGetTradeRouteWayPoint(tradeRoute4ID, 0.0);
      rmPlaceObjectDefAtPoint(socket4ID, 0, socketLoc4);
   }
   if (routeType == 31 || routeType == 32 || routeType == 33 || routeType == 34) // Routes 31-34
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.33);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.66);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.33);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.66);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
   }

   if (routeType == 35 || routeType == 36 || routeType == 37 || routeType == 38) // Routes 35-38
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.25);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.75);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.5);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
   }
   if (routeType == 39) // Route 39
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.375);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.875);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.125);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.375);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.625);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.875);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
   }
   if (routeType == 40) // Route 40
   {
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.0);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
      socketLoc=rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
      rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.25);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
      socketLoc2=rmGetTradeRouteWayPoint(tradeRoute2ID, 0.75);
      rmPlaceObjectDefAtPoint(socket2ID, 0, socketLoc2);
   }


   // Text
   rmSetStatusText("",0.40);


   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmSetObjectDefMinDistance(startingTCID, 5.0);
   rmSetObjectDefMaxDistance(startingTCID, 10.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefConstraint(startingTCID, avoidTradeRoute);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "TreeGreatLakes", 6, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 12);
   rmSetObjectDefMaxDistance(startAreaTreeID, 16);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startDeerID=rmCreateObjectDef("starting deer");
   rmAddObjectDefItem(startDeerID, "deer", 5, 4.0);
   rmSetObjectDefCreateHerd(startDeerID, true);
   rmSetObjectDefMinDistance(startDeerID, 14);
   rmSetObjectDefMaxDistance(startDeerID, 18);
   rmAddObjectDefConstraint(startDeerID, avoidStartResource);
   rmAddObjectDefConstraint(startDeerID, avoidImpassableLand);
   rmAddObjectDefConstraint(startDeerID, shortAvoidStartingUnits);

   int startCoalID=rmCreateObjectDef("player coal");
   rmAddObjectDefItem(startCoalID, "mine", 1, 5.0);
   rmSetObjectDefMinDistance(startCoalID, 14);
   rmSetObjectDefMaxDistance(startCoalID, 18);
   rmAddObjectDefConstraint(startCoalID, avoidStartResource);
   rmAddObjectDefConstraint(startCoalID, avoidImpassableLand);
   rmAddObjectDefConstraint(startCoalID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startDeerID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startCoalID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
   }


   // Text
   rmSetStatusText("",0.50);


   // Define and place forests
	
   int numTries=30*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(350), rmAreaTilesToFraction(400));
      rmSetAreaForestType(forest, "great lakes forest");
      rmSetAreaForestDensity(forest, 1.0);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.4);
      rmSetAreaForestUnderbrush(forest, 0.3);
      rmSetAreaBaseHeight(forest, 1.0);
      rmSetAreaMinBlobs(forest, 1);
      rmSetAreaMaxBlobs(forest, 3);
      rmSetAreaMinBlobDistance(forest, 10.0);
      rmSetAreaMaxBlobDistance(forest, 15.0);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, avoidTradeRoute);
      rmAddAreaConstraint(forest, avoidTradeSockets);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.  
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }


   // Text
   rmSetStatusText("",0.60);


   // Define and place animals
   
   // Define and place deer herds
   int deerHerdID=rmCreateObjectDef("deer herd");
   rmAddObjectDefItem(deerHerdID, "deer", rmRandInt(5,6), 8.0);
   rmSetObjectDefCreateHerd(deerHerdID, true);
   rmSetObjectDefMinDistance(deerHerdID, 0.0);
   rmSetObjectDefMaxDistance(deerHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(deerHerdID, classAnimals);
   rmAddObjectDefConstraint(deerHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(deerHerdID, animalConstraint);
   rmAddObjectDefConstraint(deerHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(deerHerdID, avoidTradeSockets);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(deerHerdID, 0, 0.5, 0.5);

   // Define and place fox herds
   int foxHerdID=rmCreateObjectDef("fox herd");
   rmAddObjectDefItem(foxHerdID, "moose", rmRandInt(5,6), 8.0);
   rmSetObjectDefMinDistance(foxHerdID, 0.0);
   rmSetObjectDefMaxDistance(foxHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(foxHerdID, classAnimals);
   rmAddObjectDefConstraint(foxHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(foxHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(foxHerdID, animalConstraint);
   rmAddObjectDefConstraint(foxHerdID, avoidTradeRoute);
   rmAddObjectDefConstraint(foxHerdID, avoidTradeSockets);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(foxHerdID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.70);


   // Define and place coal mines

   int coalCount = 3*cNumberNonGaiaPlayers;

   for(i=0; < coalCount)
   {
      int coalID=rmCreateObjectDef("coal mine"+i);
      rmAddObjectDefItem(coalID, "mine", 1, 0.0);
      rmSetObjectDefMinDistance(coalID, 0.0);
      rmSetObjectDefMaxDistance(coalID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(coalID, classMines);
      rmAddObjectDefConstraint(coalID, avoidImpassableLand);
      rmAddObjectDefConstraint(coalID, playerConstraint);
      rmAddObjectDefConstraint(coalID, avoidMines);
      rmAddObjectDefConstraint(coalID, avoidTradeRoute);
      rmAddObjectDefConstraint(coalID, avoidTradeSockets);
      rmPlaceObjectDefAtLoc(coalID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget1, avoidTradeSockets);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget2, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, 1.5*cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget3, avoidTradeSockets);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidTradeRoute);
   rmAddObjectDefConstraint(nugget4, avoidTradeSockets);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Text
   rmSetStatusText("",0.90);


   // Place King's Hill if KOTH game to middle

   if(rmGetIsKOTH()) 
   {
      float xLoc = 0.5;
      float yLoc = 0.5;
      float walk = 0.05;

      ypKingsHillPlacer(xLoc, yLoc, walk, 0);
   }


   // Text
   rmSetStatusText("",1.0); 
}
