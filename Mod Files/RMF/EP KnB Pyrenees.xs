// RM script of Pyrenees
// For K&B mod
// By AOE_Fan

// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Chooses the 2 natives, Visigoths and Franks

   // Native 1
   rmSetSubCiv(0, "Comanche");

   // Native 2
   rmSetSubCiv(1, "Comanche");


   // Picks the map size
   int playerTiles = 20000;
   if (cNumberNonGaiaPlayers == 8)
	playerTiles = 14000;
   else if (cNumberNonGaiaPlayers == 7)
	playerTiles = 15000;
   else if (cNumberNonGaiaPlayers == 6)
	playerTiles = 16000;
   else if (cNumberNonGaiaPlayers == 5)
	playerTiles = 17000;
   else if (cNumberNonGaiaPlayers == 4)
	playerTiles = 18000;
   else if (cNumberNonGaiaPlayers == 3)
	playerTiles = 19000;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmSetMapSize(size, size);


   // Set map the map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses where the mountain pass is - 1 = NW, 2 = Middle, 3 = SE
   int passLocation = rmRandInt(1,3);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Pick lighting
   rmSetLightingSet("Rockies");


   // Picks default terrain and map type
   rmSetMapElevationParameters(cElevTurbulence, 0.2, 4, 0.3, 7.0);
   rmTerrainInitialize("rockies\ground4_roc", 0.0);
   rmSetMapType("grass");
   rmSetMapType("land");
   rmSetMapType("greatPlains");
   

   chooseMercs();


   // Corner constraint
   rmSetWorldCircleConstraint(true);


   // Define classes, these are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   rmDefineClass("startingUnit");
   rmDefineClass("nuggets");
   rmDefineClass("classMountain");
   rmDefineClass("classTerrain");
   rmDefineClass("classForest");
   rmDefineClass("classTreeStraggler");
   rmDefineClass("natives");


   // Define constraints - used for things to avoid certain things
   
   // Map edge and centre constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // All player things avoidance
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // All resource avoidance
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 80.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 65.0);
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 40.0);
   int shortForestConstraint=rmCreateClassDistanceConstraint("forest vs. forest short", rmClassID("classForest"), 10.0);
   int stragglerTreeConstraint=rmCreateClassDistanceConstraint("straggler tree constraint", rmClassID("classTreeStraggler"), 20.0);

   // Terrain avoidance
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 8.0);
   int avoidImpassableLandLong=rmCreateTerrainDistanceConstraint("avoid impassable land long", "Land", false, 15.0);

   // Area avoidance
   int avoidMountain=rmCreateClassDistanceConstraint("avoid mountain peaks", rmClassID("classMountain"), 25.0);
   int avoidTerrain=rmCreateClassDistanceConstraint("avoid mountain terrain", rmClassID("classTerrain"), 5.0);

   // Nuggets avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 60.0);

   // Native avoidance
   int avoidNatives=rmCreateClassDistanceConstraint("things avoids natives", rmClassID("natives"), 5.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);


   // Text
   rmSetStatusText("",0.10);


   // Place players - in team and FFA games

   // Player placement if FFA (Free For All) OR two player game - manual placement
   if(cNumberTeams > 2 || cNumberNonGaiaPlayers == 2)
   {
      if (cNumberNonGaiaPlayers == 2)
      {
            rmPlacePlayer(1, 0.85, 0.5);
            rmPlacePlayer(2, 0.15, 0.5);
      }
      if (cNumberNonGaiaPlayers == 3)
      {
            rmPlacePlayer(1, 0.8, 0.8);
            rmPlacePlayer(2, 0.8, 0.2);
            rmPlacePlayer(3, 0.15, 0.5);
      }
      if (cNumberNonGaiaPlayers == 4)
      {
            rmPlacePlayer(1, 0.8, 0.8);
            rmPlacePlayer(2, 0.8, 0.2);
            rmPlacePlayer(3, 0.2, 0.8);
            rmPlacePlayer(4, 0.2, 0.2);
      }
      if (cNumberNonGaiaPlayers == 5)
      {
            rmPlacePlayer(1, 0.8, 0.8);
            rmPlacePlayer(2, 0.8, 0.2);
            rmPlacePlayer(3, 0.2, 0.8);
            rmPlacePlayer(4, 0.2, 0.2);
            rmPlacePlayer(5, 0.9, 0.5);
      }
      if (cNumberNonGaiaPlayers == 6)
      {
            rmPlacePlayer(1, 0.8, 0.8);
            rmPlacePlayer(2, 0.8, 0.2);
            rmPlacePlayer(3, 0.2, 0.8);
            rmPlacePlayer(4, 0.2, 0.2);
            rmPlacePlayer(5, 0.9, 0.5);
            rmPlacePlayer(6, 0.1, 0.5);
      }
      if (cNumberNonGaiaPlayers == 7)
      {
            rmPlacePlayer(1, 0.7, 0.85);
            rmPlacePlayer(2, 0.7, 0.15);
            rmPlacePlayer(3, 0.3, 0.85);
            rmPlacePlayer(4, 0.3, 0.15);
            rmPlacePlayer(5, 0.85, 0.35);
            rmPlacePlayer(6, 0.15, 0.65);
            rmPlacePlayer(7, 0.85, 0.65);
      }
      if (cNumberNonGaiaPlayers == 8)
      {
            rmPlacePlayer(1, 0.7, 0.85);
            rmPlacePlayer(2, 0.7, 0.15);
            rmPlacePlayer(3, 0.3, 0.85);
            rmPlacePlayer(4, 0.3, 0.15);
            rmPlacePlayer(5, 0.85, 0.35);
            rmPlacePlayer(6, 0.15, 0.65);
            rmPlacePlayer(7, 0.85, 0.65);
            rmPlacePlayer(8, 0.15, 0.35);
      }
   }

   // Player placement if teams - place both teams in their own "sides"
   if(cNumberTeams == 2 && cNumberNonGaiaPlayers >= 3)
   {
      rmSetPlacementTeam(0);
      rmSetPlacementSection(0.15, 0.35);
      rmPlacePlayersCircular(0.39, 0.40, 0.0);

      rmSetPlacementTeam(1);
      rmSetPlacementSection(0.65, 0.85);
      rmPlacePlayersCircular(0.39, 0.40, 0.0);
   }


   // Text
   rmSetStatusText("",0.20);

   // Define and place player area
   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaBaseHeight(id, 1);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand); 
      rmSetAreaCoherence(id, 0.9);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }


   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.30);


   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startTreeID, "TreeMadrone", 6, 4.0);
   rmSetObjectDefMinDistance(startTreeID, 16);
   rmSetObjectDefMaxDistance(startTreeID, 20);
   rmAddObjectDefConstraint(startTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startTreeID, shortAvoidStartingUnits);

   int startIbexID=rmCreateObjectDef("starting ibex");
   rmAddObjectDefItem(startIbexID, "ypIbex", 6, 4.0);
   rmSetObjectDefCreateHerd(startIbexID, true);
   rmSetObjectDefMinDistance(startIbexID, 14);
   rmSetObjectDefMaxDistance(startIbexID, 20);
   rmAddObjectDefConstraint(startIbexID, avoidStartResource);
   rmAddObjectDefConstraint(startIbexID, avoidImpassableLand);
   rmAddObjectDefConstraint(startIbexID, shortAvoidStartingUnits);

   int startCopperID=rmCreateObjectDef("player copper");
   rmAddObjectDefItem(startCopperID, "MineCopper", 1, 5.0);
   rmSetObjectDefMinDistance(startCopperID, 14);
   rmSetObjectDefMaxDistance(startCopperID, 20);
   rmAddObjectDefConstraint(startCopperID, avoidStartResource);
   rmAddObjectDefConstraint(startCopperID, avoidImpassableLand);
   rmAddObjectDefConstraint(startCopperID, shortAvoidStartingUnits);

   // Place starting objects and resources

   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startCopperID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startIbexID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
   }


   // Text
   rmSetStatusText("",0.40);


   // Define and place mountains dividing the map

   // Define different sizes of mountains - scales with the size of map
   int radius = size * 0.5;
   int mountainMixSize = radius * 90.0;
   int mountainTerrainSize = radius * 60.0;
   int mountainPeaksSizeShort = radius * 3.0;
   int mountainPeaksSize = radius * 5.0;
   int mountainPeaksSizeLong = radius * 7.0;
   int mountainTreesSize = radius * 10.0;

   // Define and place mountain mix - softenes the area between the snowy mountain terrain and green base terrain
   int mountainMixID = rmCreateArea("mountain Mix"); 
   rmSetAreaSize(mountainMixID, rmAreaTilesToFraction(mountainMixSize), rmAreaTilesToFraction(mountainMixSize));
   rmSetAreaLocation(mountainMixID, 0.5, 0.5); 
   rmAddAreaInfluenceSegment(mountainMixID, 0.5, 0.0, 0.5, 1.0); 
   rmSetAreaCoherence(mountainMixID, 0.8);
   rmSetAreaSmoothDistance(mountainMixID, 10);
   rmSetAreaBaseHeight(mountainMixID, 2.0);
   rmSetAreaHeightBlend(mountainMixID, 2);
   rmSetAreaWarnFailure(mountainMixID, false);
   rmSetAreaMix(mountainMixID, "rockies_snow");
   rmBuildArea(mountainMixID);

   // Define and place mountain terrain - snowy in middle, more green in edges
   int mountainTerrainID = rmCreateArea("mountain terrain"); 
   rmSetAreaSize(mountainTerrainID, rmAreaTilesToFraction(mountainTerrainSize), rmAreaTilesToFraction(mountainTerrainSize));
   rmAddAreaToClass(mountainTerrainID, rmClassID("classTerrain"));
   rmSetAreaLocation(mountainTerrainID, 0.5, 0.5); 
   rmAddAreaInfluenceSegment(mountainTerrainID, 0.5, 0.0, 0.5, 1.0); 
   rmSetAreaCoherence(mountainTerrainID, 0.8);
   rmSetAreaSmoothDistance(mountainTerrainID, 10);
   rmSetAreaBaseHeight(mountainTerrainID, 5.0);
   rmSetAreaElevationType(mountainTerrainID, cElevTurbulence);
   rmSetAreaElevationVariation(mountainTerrainID, 3);
   rmSetAreaHeightBlend(mountainTerrainID, 1);
   rmSetAreaWarnFailure(mountainTerrainID, false);
   rmSetAreaTerrainType(mountainTerrainID, "patagonia\ground_snow1_pat");
   rmAddAreaTerrainLayer(mountainTerrainID, "patagonia\ground_snow1_pat", 11, 15);
   rmAddAreaTerrainLayer(mountainTerrainID, "rockies\groundsnow6_roc", 7, 11);
   rmAddAreaTerrainLayer(mountainTerrainID, "rockies\groundsnow7_roc", 3, 7);
   rmAddAreaTerrainLayer(mountainTerrainID, "rockies\groundsnow8_roc", 0, 3);
   rmBuildArea(mountainTerrainID);


   // Text
   rmSetStatusText("",0.50);


   // Define and place south mountain peaks
   int mountainPeaksSID = rmCreateArea("mountain peaks south");
   if (passLocation == 1)
      rmSetAreaSize(mountainPeaksSID, rmAreaTilesToFraction(mountainPeaksSizeLong), rmAreaTilesToFraction(mountainPeaksSizeLong));
   else if (passLocation == 2)
      rmSetAreaSize(mountainPeaksSID, rmAreaTilesToFraction(mountainPeaksSize), rmAreaTilesToFraction(mountainPeaksSize));
   else if (passLocation == 3)
      rmSetAreaSize(mountainPeaksSID, rmAreaTilesToFraction(mountainPeaksSizeShort), rmAreaTilesToFraction(mountainPeaksSizeShort));
   rmAddAreaToClass(mountainPeaksSID, rmClassID("classMountain"));
   rmSetAreaLocation(mountainPeaksSID, 0.5, 0.25);
   if (passLocation == 1)
      rmAddAreaInfluenceSegment(mountainPeaksSID, 0.5, 0.0, 0.5, 0.5);
   else if (passLocation == 2)
      rmAddAreaInfluenceSegment(mountainPeaksSID, 0.5, 0.0, 0.5, 0.4);
   else if (passLocation == 3)
      rmAddAreaInfluenceSegment(mountainPeaksSID, 0.5, 0.0, 0.5, 0.3);
   rmSetAreaCoherence(mountainPeaksSID, 0.8); 
   rmSetAreaSmoothDistance(mountainPeaksSID, 5);
   rmSetAreaBaseHeight(mountainPeaksSID, 17);
   rmSetAreaElevationType(mountainPeaksSID, cElevTurbulence);
   rmSetAreaElevationVariation(mountainPeaksSID, 9.0);
   rmSetAreaHeightBlend(mountainPeaksSID, 1);
   rmSetAreaWarnFailure(mountainPeaksSID, false);
   rmSetAreaTerrainType(mountainPeaksSID, "patagonia\ground_snow1_pat");
   rmBuildArea(mountainPeaksSID);


   // Define and place north mountain peaks
   int mountainPeaksNID = rmCreateArea("mountain peaks north");
   if (passLocation == 1)
      rmSetAreaSize(mountainPeaksNID, rmAreaTilesToFraction(mountainPeaksSizeShort), rmAreaTilesToFraction(mountainPeaksSizeShort));
   else if (passLocation == 2)
      rmSetAreaSize(mountainPeaksNID, rmAreaTilesToFraction(mountainPeaksSize), rmAreaTilesToFraction(mountainPeaksSize));
   else if (passLocation == 3)
      rmSetAreaSize(mountainPeaksNID, rmAreaTilesToFraction(mountainPeaksSizeLong), rmAreaTilesToFraction(mountainPeaksSizeLong));
   rmAddAreaToClass(mountainPeaksNID, rmClassID("classMountain"));
   rmAddAreaConstraint(mountainPeaksNID, avoidMountain);
   rmSetAreaLocation(mountainPeaksNID, 0.5, 0.75);  
   if (passLocation == 1)    
      rmAddAreaInfluenceSegment(mountainPeaksNID, 0.5, 1.0, 0.5, 0.7); 
   else if (passLocation == 2)
      rmAddAreaInfluenceSegment(mountainPeaksNID, 0.5, 1.0, 0.5, 0.6); 
   else if (passLocation == 3)
      rmAddAreaInfluenceSegment(mountainPeaksNID, 0.5, 1.0, 0.5, 0.5); 
   rmSetAreaCoherence(mountainPeaksNID, 0.8); 
   rmSetAreaSmoothDistance(mountainPeaksNID, 5);
   rmSetAreaBaseHeight(mountainPeaksNID, 17);
   rmSetAreaElevationType(mountainPeaksNID, cElevTurbulence);
   rmSetAreaElevationVariation(mountainPeaksNID, 9.0);
   rmSetAreaHeightBlend(mountainPeaksNID, 1);
   rmSetAreaWarnFailure(mountainPeaksNID, false);
   rmSetAreaTerrainType(mountainPeaksNID, "patagonia\ground_snow1_pat");
   rmBuildArea(mountainPeaksNID);


   // Text
   rmSetStatusText("",0.60);


   // Define and place southern fir forests near mountains
   int mountainTreesSID = rmCreateArea("mountain trees S"); 
   rmSetAreaSize(mountainTreesSID, rmAreaTilesToFraction(mountainTreesSize), rmAreaTilesToFraction(mountainTreesSize));
   if (cNumberNonGaiaPlayers < 6)
   {
      rmSetAreaLocation(mountainTreesSID, 0.4, 0.5); 
      rmAddAreaInfluenceSegment(mountainTreesSID, 0.4, 0.0, 0.4, 1.0); 
   }
   else if (cNumberNonGaiaPlayers >= 6)
   {
      rmSetAreaLocation(mountainTreesSID, 0.42, 0.5); 
      rmAddAreaInfluenceSegment(mountainTreesSID, 0.42, 0.0, 0.42, 1.0); 
   }
   else if (cNumberNonGaiaPlayers == 8)
   {
      rmSetAreaLocation(mountainTreesSID, 0.45, 0.5); 
      rmAddAreaInfluenceSegment(mountainTreesSID, 0.45, 0.0, 0.45, 1.0); 
   }
   rmSetAreaCoherence(mountainTreesSID, 0.8);
   rmSetAreaSmoothDistance(mountainTreesSID, 10);
   rmSetAreaBaseHeight(mountainTreesSID, 3.0);
   rmSetAreaHeightBlend(mountainTreesSID, 2);
   rmSetAreaWarnFailure(mountainTreesSID, false);
   rmSetAreaForestType(mountainTreesSID, "Himalayas Forest");
   rmAddAreaToClass(mountainTreesSID, rmClassID("classForest"));
   rmSetAreaForestDensity(mountainTreesSID, 0.9);
   rmSetAreaForestClumpiness(mountainTreesSID, 0.9);
   rmBuildArea(mountainTreesSID);

   // Define and place northern fir forests near mountains
   int mountainTreesNID = rmCreateArea("mountain trees N"); 
   rmSetAreaSize(mountainTreesNID, rmAreaTilesToFraction(mountainTreesSize), rmAreaTilesToFraction(mountainTreesSize));
   if (cNumberNonGaiaPlayers < 6)
   {
      rmSetAreaLocation(mountainTreesNID, 0.6, 0.5); 
      rmAddAreaInfluenceSegment(mountainTreesNID, 0.6, 0.0, 0.6, 1.0);
   }
   else if (cNumberNonGaiaPlayers >= 6)
   { 
      rmSetAreaLocation(mountainTreesNID, 0.58, 0.5); 
      rmAddAreaInfluenceSegment(mountainTreesNID, 0.58, 0.0, 0.58, 1.0);
   }
   else if (cNumberNonGaiaPlayers == 8)
   { 
      rmSetAreaLocation(mountainTreesNID, 0.55, 0.5); 
      rmAddAreaInfluenceSegment(mountainTreesNID, 0.55, 0.0, 0.55, 1.0);
   }
   rmSetAreaCoherence(mountainTreesNID, 0.8);
   rmSetAreaSmoothDistance(mountainTreesNID, 10);
   rmSetAreaBaseHeight(mountainTreesNID, 2.0);
   rmSetAreaHeightBlend(mountainTreesNID, 2);
   rmSetAreaWarnFailure(mountainTreesNID, false);
   rmSetAreaForestType(mountainTreesNID, "Himalayas Forest");
   rmAddAreaToClass(mountainTreesNID, rmClassID("classForest"));
   rmSetAreaForestDensity(mountainTreesNID, 0.9);
   rmSetAreaForestClumpiness(mountainTreesNID, 0.9);
   rmBuildArea(mountainTreesNID);


   // Text
   rmSetStatusText("",0.70);


   // Define and place the 2 natives, Visigoths and Franks

   // Visigoths, to south-western side
   int nativeVillageType = rmRandInt(1,5);
   int nativeVillageID = rmCreateGrouping("visigoths village", "native comanche village "+nativeVillageType);
   rmSetGroupingMinDistance(nativeVillageID, 0.0);
   rmSetGroupingMaxDistance(nativeVillageID, 10.0);
   rmAddGroupingToClass(nativeVillageID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillageID, 0, 0.3, 0.5);

   // Franks, to north-eastern side
   int nativeVillage2Type = rmRandInt(1,5);
   int nativeVillage2ID = rmCreateGrouping("franks village", "native comanche village "+nativeVillage2Type);
   rmSetGroupingMinDistance(nativeVillage2ID, 0.0);
   rmSetGroupingMaxDistance(nativeVillage2ID, 10.0);
   rmAddGroupingToClass(nativeVillage2ID, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeVillage2ID, 0, 0.7, 0.5);


   // Define and place forests

   int numTries=10*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i);
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(150), rmAreaTilesToFraction(200));
      rmSetAreaForestType(forest, "california madrone forest");
      rmSetAreaForestDensity(forest, 0.9);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.8);
      rmSetAreaForestUnderbrush(forest, 0.2);
      rmSetAreaBaseHeight(forest, 1.0);
      rmSetAreaCoherence(forest, 0.4);
      rmSetAreaSmoothDistance(forest, 1);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, avoidTerrain);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      rmAddAreaConstraint(forest, avoidNatives);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.  
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }


   // Define and place straggler trees
   for (i=0; < 40)
   { 
      int stragglerTreeID=rmCreateObjectDef("straggler tree madrone"+i);
      rmAddObjectDefItem(stragglerTreeID, "TreeMadrone", rmRandInt(1,5), 10.0);
      rmSetObjectDefMinDistance(stragglerTreeID, 0.0);
      rmSetObjectDefMaxDistance(stragglerTreeID, rmZFractionToMeters(0.47));
      rmAddObjectDefToClass(stragglerTreeID, rmClassID("classForest"));
      rmAddObjectDefToClass(stragglerTreeID, rmClassID("classTreeStraggler"));
      rmAddObjectDefConstraint(stragglerTreeID, somePlayerConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, avoidImpassableLand);
      rmAddObjectDefConstraint(stragglerTreeID, shortForestConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, stragglerTreeConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, avoidNatives);
      rmAddObjectDefConstraint(stragglerTreeID, avoidTerrain);
      rmPlaceObjectDefAtLoc(stragglerTreeID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.80);


   // Define and place huntables
   
   // Define and place bear herds
   int bearHerdID=rmCreateObjectDef("bear herd");
   rmAddObjectDefItem(bearHerdID, "deer", rmRandInt(6,7), 4.0);
   rmSetObjectDefMinDistance(bearHerdID, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(bearHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(bearHerdID, classAnimals);
   rmAddObjectDefConstraint(bearHerdID, animalConstraint);
   rmAddObjectDefConstraint(bearHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(bearHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(bearHerdID, avoidTerrain);
   rmAddObjectDefConstraint(bearHerdID, avoidNatives);
   numTries=cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(bearHerdID, 0, 0.5, 0.5);

   // Define and place ibex herds
   int ibexHerdID=rmCreateObjectDef("ibex herd");
   rmAddObjectDefItem(ibexHerdID, "ypIbex", rmRandInt(6,7), 4.0);
   rmSetObjectDefCreateHerd(ibexHerdID, true);
   rmSetObjectDefMinDistance(ibexHerdID, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(ibexHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(ibexHerdID, classAnimals);
   rmAddObjectDefConstraint(ibexHerdID, animalConstraint);
   rmAddObjectDefConstraint(ibexHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(ibexHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(ibexHerdID, avoidTerrain);
   rmAddObjectDefConstraint(ibexHerdID, avoidNatives);
   numTries=2.5*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(ibexHerdID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.90);


   // Define and place copper mines

   int copperCount = 3*cNumberNonGaiaPlayers+2;

   for(i=0; < copperCount)
   {
      int copperID=rmCreateObjectDef("copper mine"+i);
      rmAddObjectDefItem(copperID, "MineCopper", 1, 0.0);
      rmSetObjectDefMinDistance(copperID, rmXFractionToMeters(0.00));
      rmSetObjectDefMaxDistance(copperID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(copperID, classMines);
      rmAddObjectDefConstraint(copperID, playerConstraint);
      rmAddObjectDefConstraint(copperID, avoidMines);
      rmAddObjectDefConstraint(copperID, avoidImpassableLand);
      rmAddObjectDefConstraint(copperID, avoidTerrain);
      rmAddObjectDefConstraint(copperID, avoidNatives);
      rmPlaceObjectDefAtLoc(copperID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.95);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidTerrain);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmAddObjectDefConstraint(nugget2, avoidTerrain);
   rmAddObjectDefConstraint(nugget2, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmAddObjectDefConstraint(nugget3, avoidTerrain);
   rmAddObjectDefConstraint(nugget3, avoidNatives);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, 1.5*cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, rmXFractionToMeters(0.00));
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   rmAddObjectDefConstraint(nugget4, avoidTerrain);
   rmAddObjectDefConstraint(nugget4, avoidNatives);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Text
   rmSetStatusText("",1.0);
}  
