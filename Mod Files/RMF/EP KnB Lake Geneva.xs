// RM script of Lake Geneva
// For K&B mod
// By AOE_Fan
// TAD-compatible version!

include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";


   // Set up void to shorten creation of lake

   int number=0;
   void Lake(float Size=0.0, float LocationX=0.0, float LocationZ=0.0, float Coherence=0.0)
   {
      int areaID=rmCreateArea("area"+number);
      rmSetAreaSize(areaID, Size, Size);
      rmSetAreaLocation(areaID, LocationX, LocationZ);
      rmSetAreaWaterType(areaID, "great lakes");
      rmSetAreaBaseHeight(areaID, 0.0);
      rmSetAreaCoherence(areaID, Coherence);
      rmSetAreaSmoothDistance(areaID, 10);
      rmSetAreaMinBlobs(areaID, 10);
      rmSetAreaMaxBlobs(areaID, 20);
      rmBuildArea(areaID);
      number=number+1;
   }

void main(void)
{
   // Text
   // Make the loading bar move
   rmSetStatusText("",0.01);


   // Picks the map size
   int playerTiles = 20000;
   if (cNumberNonGaiaPlayers == 8)
	playerTiles = 14000;
   else if (cNumberNonGaiaPlayers == 7)
	playerTiles = 15000;
   else if (cNumberNonGaiaPlayers == 6)
	playerTiles = 16000;
   else if (cNumberNonGaiaPlayers == 5)
	playerTiles = 17000;
   else if (cNumberNonGaiaPlayers == 4)
	playerTiles = 18000;
   else if (cNumberNonGaiaPlayers == 3)
	playerTiles = 19000;

   int size=2.0*sqrt(cNumberNonGaiaPlayers*playerTiles);
   rmEchoInfo("Map size="+size+"m x "+size+"m");
   rmSetMapSize(size, size);
	
   // Set map smoothness
   rmSetMapElevationHeightBlend(1);


   // Chooses the side where teams start in team games
   int teamSide = rmRandInt(1,2);


   // Picks a default water height
   rmSetSeaLevel(0.0);


   // Lighting
   rmSetLightingSet("borneo");


   // Picks default terrain
   rmSetMapElevationParameters(cElevTurbulence, 0.10, 4, 0.4, 5.0);
   rmTerrainInitialize("saguenay\groundforest_sag", 1.0);
   rmSetWindMagnitude(0.0);
   rmSetMapType("grass");
   rmSetMapType("water");
   rmSetMapType("greatPlains");


   chooseMercs();


   // Corner constraint.
   rmSetWorldCircleConstraint(true);

   // Define classes. These are used for constraints
   int classPlayer=rmDefineClass("player");
   int classAnimals=rmDefineClass("animals");
   int classMines=rmDefineClass("mines");
   int classCliff=rmDefineClass("cliff");
   rmDefineClass("startingUnit");
   rmDefineClass("classForest");
   rmDefineClass("classForestBeech");
   rmDefineClass("classTreeStraggler");
   rmDefineClass("nuggets");


   // Define constraints
   
   // Map edge constraints
   int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));
   int beechConstraint=rmCreatePieConstraint("beech Constraint", 0.5, 0.5, rmXFractionToMeters(0.37), rmXFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Player constraints
   int veryLongPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players very long", classPlayer, 70.0);
   int longPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players long", classPlayer, 55.0);
   int playerConstraint=rmCreateClassDistanceConstraint("stay away from players", classPlayer, 45.0);
   int mediumPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players medium", classPlayer, 40.0);
   int somePlayerConstraint=rmCreateClassDistanceConstraint("things stay away from players", classPlayer, 30.0);
   int shortPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players short", classPlayer, 20.0);
   int shortAvoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units short", rmClassID("startingUnit"), 10.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 60.0);
   int forestConstraintShort=rmCreateClassDistanceConstraint("beech vs. forest", rmClassID("classForest"), 30.0);
   int forestConstraintBeech=rmCreateClassDistanceConstraint("beech vs. beech", rmClassID("classForestBeech"), 50.0);
   int shortForestConstraint=rmCreateClassDistanceConstraint("forest vs. forest short", rmClassID("classForest"), 5.0);
   int stragglerTreeConstraint=rmCreateClassDistanceConstraint("straggler tree constraint", rmClassID("classTreeStraggler"), 20.0);
   int avoidMines=rmCreateClassDistanceConstraint("avoid mines", classMines, 70.0);
   int avoidStartResource=rmCreateTypeDistanceConstraint("start resource no overlap", "resource", 10.0);
   int animalConstraint=rmCreateClassDistanceConstraint("avoid all animals", classAnimals, 85.0);

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 5.0);

   // Nugget avoidance
   int avoidNuggets=rmCreateClassDistanceConstraint("stuff avoids nuggets", rmClassID("nuggets"), 40.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 5.0);

   // Water objects avoidance	
   int fishVsFish=rmCreateTypeDistanceConstraint("fish avoid fish", "FishSalmon", 25.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish avoid land", "land", true, 5.0);
   int flagLand = rmCreateTerrainDistanceConstraint("flag avoid land", "land", true, 15.0);
   int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid flags", "HomeCityWaterSpawnFlag", 25);


   // Text
   rmSetStatusText("",0.10);


   // Define and place cliffs to edges

   int cliffID=rmCreateArea("cliff");
   rmSetAreaSize(cliffID, 0.73, 0.73);
   rmSetAreaLocation(cliffID, 0.5, 0.5);
   rmSetAreaCoherence(cliffID, 1.0);
   rmAddAreaToClass(cliffID, classCliff);
   rmSetAreaCliffType(cliffID, "rocky mountain2");
   rmSetAreaCliffEdge(cliffID, 1, 1.0, 0.0, 0.0, 0);
   rmSetAreaCliffHeight(cliffID, -8, 1.0, 0.3);
   rmSetAreaCliffPainting(cliffID, true, false, true);
   rmSetAreaObeyWorldCircleConstraint(cliffID, false);
   rmBuildArea(cliffID);

   int cliff2ID=rmCreateArea("cliff 2");
   rmSetAreaSize(cliff2ID, 0.70, 0.70);
   rmSetAreaLocation(cliff2ID, 0.5, 0.5);
   rmSetAreaCoherence(cliff2ID, 1.0);
   rmAddAreaToClass(cliff2ID, classCliff);
   rmSetAreaCliffType(cliff2ID, "rocky mountain2");
   rmSetAreaCliffEdge(cliff2ID, 1, 1.0, 0.0, 0.0, 0);
   rmSetAreaCliffHeight(cliff2ID, -6, 1.0, 0.3);
   rmSetAreaCliffPainting(cliff2ID, true, false, true);
   rmSetAreaObeyWorldCircleConstraint(cliff2ID, false);
   rmBuildArea(cliff2ID);


   // Define and place normal terrain - overwrites cliffs

   int terrainID=rmCreateArea("terrain");
   rmSetAreaSize(terrainID, 1.0, 1.0);
   rmSetAreaLocation(terrainID, 0.5, 0.5);
   rmSetAreaTerrainType(terrainID, "saguenay\groundforest_sag");
   rmAddAreaTerrainLayer(terrainID, "rockies\groundsnow8_roc", 0, 18);
   rmSetAreaCoherence(terrainID, 1.0);
   rmSetAreaSmoothDistance(terrainID, 10);
   rmSetAreaCliffType(terrainID, "rocky mountain2");
   rmSetAreaCliffEdge(terrainID, 1, 1.0, 0.0, 0.0, 0);
   rmSetAreaCliffHeight(terrainID, 10, 1.0, 0.3);
   rmBuildArea(terrainID);


   int terrain2ID=rmCreateArea("terrain 2");
   rmSetAreaSize(terrain2ID, 0.35, 0.35);
   rmSetAreaLocation(terrain2ID, 0.5, 0.5);
   rmSetAreaTerrainType(terrain2ID, "new_england\ground3_ne");
   rmSetAreaCoherence(terrain2ID, 0.8);
   rmSetAreaSmoothDistance(terrain2ID, 10);
   rmSetAreaCliffType(terrain2ID, "New England");
   rmSetAreaCliffEdge(terrain2ID, 3, 0.2, 0.0, 1.0, 0);
   rmSetAreaCliffHeight(terrain2ID, 5, 1.0, 0.3);
   rmBuildArea(terrain2ID);


   // Define and place Lake Geneva in middle

   Lake(0.007, 0.52, 0.52, 0.5);
   Lake(0.007, 0.56, 0.56, 0.5);
   Lake(0.007, 0.59, 0.59, 0.5);
   Lake(0.007, 0.60, 0.43, 0.5);
   Lake(0.007, 0.65, 0.35, 0.5);
   Lake(0.007, 0.65, 0.4, 0.5);
   Lake(0.007, 0.65, 0.5, 0.5);
   Lake(0.007, 0.48, 0.55, 0.5);
   Lake(0.007, 0.43, 0.55, 0.5);
   Lake(0.007, 0.38, 0.58, 0.5);
   Lake(0.007, 0.30, 0.58, 0.5);
   Lake(0.005, 0.25, 0.57, 0.5);
   Lake(0.007, 0.63, 0.56, 0.5);
   Lake(0.007, 0.53, 0.64, 0.5);
   Lake(0.007, 0.46, 0.66, 0.5);
   Lake(0.007, 0.27, 0.6, 0.5);
   Lake(0.007, 0.35, 0.66, 0.5);
   Lake(0.007, 0.4, 0.64, 0.5);
   Lake(0.007, 0.3, 0.63, 0.5);
   Lake(0.007, 0.54, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.20);


   // Place players in FFA and team games

   // Player placement if FFA (Free For All) - place players in circle
   if(cNumberTeams > 2)
   {
      rmSetTeamSpacingModifier(0.7);
      rmPlacePlayersCircular(0.4, 0.4, 0.0);
   }

   // Player placement if teams - place teams in circle to apart from each other
   if(cNumberTeams == 2)
   {
      rmSetPlacementTeam(0);
      if (teamSide == 1)
      {
         if (rmGetNumberPlayersOnTeam(0) == 2)
            rmSetPlacementSection(0.925, 0.075);
         else
            rmSetPlacementSection(0.875, 0.125);
      }
      else
      {
         if (rmGetNumberPlayersOnTeam(0) == 2)
            rmSetPlacementSection(0.425, 0.575);
         else
            rmSetPlacementSection(0.375, 0.625);
      }
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.4, 0.4, 0.0);

      rmSetPlacementTeam(1);
      if (teamSide == 1)
      {
         if (rmGetNumberPlayersOnTeam(1) == 2)
            rmSetPlacementSection(0.425, 0.575);
         else
            rmSetPlacementSection(0.375, 0.625);
      }
      else
      {
         if (rmGetNumberPlayersOnTeam(1) == 2)
            rmSetPlacementSection(0.925, 0.075);
         else
            rmSetPlacementSection(0.875, 0.125);
      }
      rmSetTeamSpacingModifier(0.25);
      rmPlacePlayersCircular(0.4, 0.4, 0.0);
   }


   // Define and place players' areas

   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <cNumberPlayers)
   {
      int id=rmCreateArea("Player"+i);
      rmSetPlayerArea(i, id);
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, avoidImpassableLand);
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

   // Build the areas.
   rmBuildAllAreas();


   // Text
   rmSetStatusText("",0.30);
		

   // Define starting objects and resources

   int startingTCID=rmCreateObjectDef("startingTC");
   if (rmGetNomadStart())
      rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 5.0);
   else
      rmAddObjectDefItem(startingTCID, "townCenter", 1, 5.0);
   rmAddObjectDefConstraint(startingTCID, avoidImpassableLand);
   rmAddObjectDefToClass(startingTCID, classPlayer);

   int startingUnits=rmCreateStartingUnitsObjectDef(5.0);
   rmSetObjectDefMinDistance(startingUnits, 5.0);
   rmSetObjectDefMaxDistance(startingUnits, 10.0);
   rmAddObjectDefToClass(startingUnits, rmClassID("startingUnit"));

   int startAreaTreeID=rmCreateObjectDef("starting trees");
   rmAddObjectDefItem(startAreaTreeID, "TreeGreatPlains", 6, 4.0);
   rmSetObjectDefMinDistance(startAreaTreeID, 14);
   rmSetObjectDefMaxDistance(startAreaTreeID, 18);
   rmAddObjectDefConstraint(startAreaTreeID, avoidStartResource);
   rmAddObjectDefConstraint(startAreaTreeID, avoidImpassableLand);
   rmAddObjectDefConstraint(startAreaTreeID, shortAvoidStartingUnits);

   int startBerryID=rmCreateObjectDef("starting berry");
   rmAddObjectDefItem(startBerryID, "berrybush", 3, 6.0);
   rmSetObjectDefCreateHerd(startBerryID, true);
   rmSetObjectDefMinDistance(startBerryID, 14);
   rmSetObjectDefMaxDistance(startBerryID, 18);
   rmAddObjectDefConstraint(startBerryID, avoidStartResource);
   rmAddObjectDefConstraint(startBerryID, avoidImpassableLand);
   rmAddObjectDefConstraint(startBerryID, shortAvoidStartingUnits);

   int startSilverID=rmCreateObjectDef("player silver");
   rmAddObjectDefItem(startSilverID, "Mine", 1, 5.0);
   rmSetObjectDefMinDistance(startSilverID, 14);
   rmSetObjectDefMaxDistance(startSilverID, 18);
   rmAddObjectDefConstraint(startSilverID, avoidStartResource);
   rmAddObjectDefConstraint(startSilverID, avoidImpassableLand);
   rmAddObjectDefConstraint(startSilverID, shortAvoidStartingUnits);


   for(i=1; <cNumberPlayers)
   {					
      // Place starting objects and resources
      rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	  if(ypIsAsian(i) && rmGetNomadStart() == false)
         rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startBerryID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
      rmPlaceObjectDefAtLoc(startSilverID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

      // Define and place water flags
      int waterFlagID=rmCreateObjectDef("HC water flag"+i);
      rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
      rmAddClosestPointConstraint(flagVsFlag);
      rmAddClosestPointConstraint(flagLand);		
      vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingTCID, i));
      vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

      rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
      rmClearClosestPointConstraints();
   }


   // Text
   rmSetStatusText("",0.40);


   // Define and place forests
   // Oak forests to middle
   int numTries=10*cNumberNonGaiaPlayers;
   int failCount=0;
   for (i=0; <numTries)
   {   
      int forest=rmCreateArea("forest"+i, rmAreaID("terrain 2"));
      rmSetAreaWarnFailure(forest, false);
      rmSetAreaSize(forest, rmAreaTilesToFraction(300), rmAreaTilesToFraction(350));
      rmSetAreaForestType(forest, "great plains forest");
      rmSetAreaForestDensity(forest, 0.8);
      rmAddAreaToClass(forest, rmClassID("classForest"));
      rmSetAreaForestClumpiness(forest, 0.5);
      rmSetAreaForestUnderbrush(forest, 0.2);
      rmSetAreaCoherence(forest, 0.4);
      rmAddAreaConstraint(forest, avoidImpassableLand);
      rmAddAreaConstraint(forest, somePlayerConstraint);
      rmAddAreaConstraint(forest, forestConstraint);
      if(rmBuildArea(forest)==false)
      {
            // Stop trying once we fail 5 times in a row.
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }

   // Define and place straggler trees
   for (i=0; < 40)
   { 
      int stragglerTreeID=rmCreateObjectDef("straggler tree oak"+i);
      rmAddObjectDefItem(stragglerTreeID, "TreeGreatPlains", rmRandInt(1,5), 10.0);
      rmSetObjectDefMinDistance(stragglerTreeID, 0.0);
      rmSetObjectDefMaxDistance(stragglerTreeID, rmZFractionToMeters(0.35));
      rmAddObjectDefToClass(stragglerTreeID, rmClassID("classForest"));
      rmAddObjectDefToClass(stragglerTreeID, rmClassID("classTreeStraggler"));
      rmAddObjectDefConstraint(stragglerTreeID, somePlayerConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, avoidImpassableLand);
      rmAddObjectDefConstraint(stragglerTreeID, shortForestConstraint);
      rmAddObjectDefConstraint(stragglerTreeID, stragglerTreeConstraint);
      rmPlaceObjectDefAtLoc(stragglerTreeID, 0, 0.5, 0.5);
   }

   // Beech forests between middle and cliffs
   numTries=10*cNumberNonGaiaPlayers;
   failCount=0;
   for (i=0; <numTries)
   {   
      int forest2=rmCreateArea("forest 2"+i);
      rmSetAreaWarnFailure(forest2, false);
      rmSetAreaSize(forest2, rmAreaTilesToFraction(500), rmAreaTilesToFraction(550));
      rmSetAreaForestType(forest2, "great plains forest");
      rmSetAreaForestDensity(forest2, 0.8);
      rmAddAreaToClass(forest2, rmClassID("classForest"));
      rmAddAreaToClass(forest2, rmClassID("classForestBeech"));
      rmSetAreaForestClumpiness(forest2, 0.5);
      rmSetAreaForestUnderbrush(forest2, 0.2);
      rmSetAreaCoherence(forest2, 0.4);
      rmAddAreaConstraint(forest2, avoidImpassableLand);
      rmAddAreaConstraint(forest2, somePlayerConstraint);
      rmAddAreaConstraint(forest2, forestConstraintShort);
      rmAddAreaConstraint(forest2, forestConstraintBeech);
      rmAddAreaConstraint(forest2, beechConstraint);
      if(rmBuildArea(forest2)==false)
      {
            // Stop trying once we fail 5 times in a row.
            failCount++;
            if(failCount==5)
                        break;
      }
      else
                        failCount=0; 
   }

   // Define and place straggler trees
   for (i=0; < 40)
   { 
      int stragglerTree2ID=rmCreateObjectDef("straggler tree beech"+i);
      rmAddObjectDefItem(stragglerTree2ID, "TreeGreatPlains", rmRandInt(1,5), 10.0);
      rmSetObjectDefMinDistance(stragglerTree2ID, rmZFractionToMeters(0.37));
      rmSetObjectDefMaxDistance(stragglerTree2ID, rmZFractionToMeters(0.47));
      rmAddObjectDefToClass(stragglerTree2ID, rmClassID("classForest"));
      rmAddObjectDefToClass(stragglerTree2ID, rmClassID("classTreeStraggler"));
      rmAddObjectDefConstraint(stragglerTree2ID, somePlayerConstraint);
      rmAddObjectDefConstraint(stragglerTree2ID, avoidImpassableLand);
      rmAddObjectDefConstraint(stragglerTree2ID, shortForestConstraint);
      rmAddObjectDefConstraint(stragglerTree2ID, stragglerTreeConstraint);
      rmPlaceObjectDefAtLoc(stragglerTree2ID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.50);


   // Define and place animals and berries
   
   // Define and place ibex herds
   int ibexHerdID=rmCreateObjectDef("ibex herd");
   rmAddObjectDefItem(ibexHerdID, "ypIbex", rmRandInt(7,8), 9.0);
   rmSetObjectDefCreateHerd(ibexHerdID, true);
   rmSetObjectDefMinDistance(ibexHerdID, rmXFractionToMeters(0.35));
   rmSetObjectDefMaxDistance(ibexHerdID, rmXFractionToMeters(0.47));
   rmAddObjectDefToClass(ibexHerdID, classAnimals);
   rmAddObjectDefConstraint(ibexHerdID, somePlayerConstraint);
   rmAddObjectDefConstraint(ibexHerdID, avoidImpassableLand);
   rmAddObjectDefConstraint(ibexHerdID, animalConstraint);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(ibexHerdID, 0, 0.5, 0.5);

   // Define and place blueberry bushes
   int berryID=rmCreateObjectDef("berry");
   rmAddObjectDefItem(berryID, "berrybush", rmRandInt(3,4), 9.0);
   rmSetObjectDefCreateHerd(berryID, true);
   rmSetObjectDefMinDistance(berryID, 0.0);
   rmSetObjectDefMaxDistance(berryID, rmXFractionToMeters(0.35));
   rmAddObjectDefToClass(berryID, classAnimals);
   rmAddObjectDefConstraint(berryID, somePlayerConstraint);
   rmAddObjectDefConstraint(berryID, avoidImpassableLand);
   rmAddObjectDefConstraint(berryID, animalConstraint);
   numTries=2*cNumberNonGaiaPlayers;
   for (i=0; <numTries)
      rmPlaceObjectDefAtLoc(berryID, 0, 0.5, 0.5);


   // Text
   rmSetStatusText("",0.60);


   // Define and place silver mines

   int silverCount = 3*cNumberNonGaiaPlayers+2;

   for(i=0; < silverCount)
   {
      int silverID=rmCreateObjectDef("silver mine"+i);
      rmAddObjectDefItem(silverID, "Mine", 1, 0.0);
      rmSetObjectDefMinDistance(silverID, 0.0);
      rmSetObjectDefMaxDistance(silverID, rmXFractionToMeters(0.47));
      rmAddObjectDefToClass(silverID, classMines);
      rmAddObjectDefConstraint(silverID, avoidImpassableLand);
      rmAddObjectDefConstraint(silverID, somePlayerConstraint);
      rmAddObjectDefConstraint(silverID, avoidMines);
      rmPlaceObjectDefAtLoc(silverID, 0, 0.5, 0.5);
   }


   // Text
   rmSetStatusText("",0.70);
	

   // Define and place fish

   int fishID=rmCreateObjectDef("fish");
   rmAddObjectDefItem(fishID, "FishSalmon", 1, 4.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(fishID, fishVsFish);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 6*cNumberNonGaiaPlayers);


   // Text
   rmSetStatusText("",0.80);


   // Define and place treasures

   // Easy treasures
   int nugget1=rmCreateObjectDef("nugget easy"); 
   rmAddObjectDefItem(nugget1, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(1, 1);
   rmAddObjectDefToClass(nugget1, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget1, 20.0);
   rmSetObjectDefMaxDistance(nugget1, 30.0);
   rmAddObjectDefConstraint(nugget1, shortPlayerConstraint);
   rmAddObjectDefConstraint(nugget1, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget1, avoidNuggets);
   rmAddObjectDefConstraint(nugget1, avoidAll);
   rmPlaceObjectDefPerPlayer(nugget1, false, 1);

   // Medium treasures
   int nugget2=rmCreateObjectDef("nugget medium"); 
   rmAddObjectDefItem(nugget2, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(2, 2);
   rmAddObjectDefToClass(nugget2, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget2, 0.0);
   rmSetObjectDefMaxDistance(nugget2, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget2, mediumPlayerConstraint);
   rmAddObjectDefConstraint(nugget2, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget2, avoidNuggets);
   rmPlaceObjectDefAtLoc(nugget2, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Hard treasures
   int nugget3=rmCreateObjectDef("nugget hard"); 
   rmAddObjectDefItem(nugget3, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(3, 3);
   rmAddObjectDefToClass(nugget3, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget3, 0.0);
   rmSetObjectDefMaxDistance(nugget3, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget3, longPlayerConstraint);
   rmAddObjectDefConstraint(nugget3, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget3, avoidNuggets);
   rmPlaceObjectDefAtLoc(nugget3, 0, 0.5, 0.5, cNumberNonGaiaPlayers);

   // Very hard treasures
   int nugget4=rmCreateObjectDef("nugget nuts"); 
   rmAddObjectDefItem(nugget4, "Nugget", 1, 0.0);
   rmSetNuggetDifficulty(4, 4);
   rmAddObjectDefToClass(nugget4, rmClassID("nuggets"));
   rmSetObjectDefMinDistance(nugget4, 0.0);
   rmSetObjectDefMaxDistance(nugget4, rmXFractionToMeters(0.47));
   rmAddObjectDefConstraint(nugget4, veryLongPlayerConstraint);
   rmAddObjectDefConstraint(nugget4, avoidImpassableLand);
   rmAddObjectDefConstraint(nugget4, avoidNuggets);
   if (cNumberNonGaiaPlayers <= 4)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 2);
   else if (cNumberNonGaiaPlayers >= 5)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 3);
   else if (cNumberNonGaiaPlayers >= 7)
      rmPlaceObjectDefAtLoc(nugget4, 0, 0.5, 0.5, 4);


   // Text
   rmSetStatusText("",1.0); 
}