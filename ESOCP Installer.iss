; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

[Setup]
AppName=ESO Community Patch
AppId=ESO Community Patch
AppVersion=2.0.0.1
AppPublisher=ESO Community
AppPublisherURL=http://eso-community.net/
DefaultDirName={reg:HKLM\SOFTWARE\Wow6432Node\Microsoft\microsoft games\age of empires 3 expansion pack 2\1.0,setuppath|{reg:HKLM\SOFTWARE\Microsoft\microsoft games\age of empires 3 expansion pack 2\1.0,setuppath|{pf}\ESOCFP}}
Compression=lzma2/ultra64
LZMANumFastBytes=273
LZMAUseSeparateProcess=yes
CompressionThreads=4
SolidCompression=yes
DisableProgramGroupPage=yes
UninstallDisplayName=Age of Empires III: ESO-Community Patch
UninstallDisplayIcon="{app}\ESOCPatchLauncher.exe"
DirExistsWarning=no
;UsePreviousAppDir=no

ArchitecturesAllowed=x86 x64 ia64
ArchitecturesInstallIn64BitMode=x64 ia64

[Files]
Source: "build\RMF\*"; DestDir: "{app}\RMF\"; Flags: recursesubdirs ignoreversion
Source: "build\data\Data3.bar"; DestDir: "{app}\data\"; Flags: uninsneveruninstall ignoreversion
Source: "build\data\protof.xml"; DestDir: "{app}\data\"; Flags: ignoreversion
Source: "build\data\randommapstringsf.xml"; DestDir: "{app}\data\"; Flags: ignoreversion
Source: "build\data\techtreef.xml"; DestDir: "{app}\data\"; Flags: ignoreversion
Source: "build\splashf.bmp"; DestDir: "{app}\"; Flags: ignoreversion
Source: "build\DataPF.bar"; DestDir: "{app}\"; Flags: ignoreversion
Source: "build\ESOCP3.dll"; DestDir: "{app}\"; Flags: ignoreversion
Source: "build\ESOCP2.dll"; DestDir: "{app}\"; Flags: ignoreversion
Source: "build\ESOCP1.dll"; DestDir: "{app}\"; Flags: ignoreversion
Source: "build\age3f.exe"; DestDir: "{app}\"; Flags: ignoreversion
Source: "build\ESOCPatchLauncher.exe"; DestDir: "{app}\"; Flags: ignoreversion
Source: "build\dependencies\vcredist_x86.exe"; DestDir: {tmp}; Flags: ignoreversion; AfterInstall: InstallVisualC; Check: VisualCIsNotInstalled
Source: "build\dependencies\dotNetFx40_Client_setup.exe"; DestDir: {tmp}; Flags: ignoreversion; AfterInstall: InstallFramework; Check: FrameworkIsNotInstalled

[Icons]
Name: "{commondesktop}\ESOC Patch"; Filename: "{app}\ESOCPatchLauncher.exe"
Name: "{commonprograms}\ESO Community Patch\ESO Community Patch"; Filename: "{app}\ESOCPatchLauncher.exe"
Name: "{commonprograms}\ESO Community Patch\Uninstall ESO Community Patch"; Filename: "{uninstallexe}"

[Run]
Filename: "{app}\ESOCPatchLauncher.exe"; Parameters: "--CopyRM3"; Flags: shellexec runhidden runascurrentuser
Filename: "{app}\ESOCPatchLauncher.exe"; Description: Launch ESOC Patch; Flags: nowait postinstall skipifsilent shellexec runascurrentuser
 
[Code]
function InitializeSetup(): boolean;
var
  sUninstallPath:String;
  ResultCode:Integer;
begin
  if RegQueryStringValue(HKLM32, 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\ESO Community Patch_is1', 'UninstallString', sUninstallPath) then
  begin
    if MsgBox('The existing installation will be uninstalled before installing.', mbInformation, MB_OKCANCEL) = IDOK then begin
      Exec(RemoveQuotes(sUninstallPath), '/SILENT /NORESTART /SUPPRESSMSGBOXES', '', SW_SHOW, ewWaitUntilTerminated, ResultCode);
    end
    else
    begin
      Result := false;
      exit;
    end
  end
  else if IsWin64 then
  begin
    if RegQueryStringValue(HKLM64, 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\ESO Community Patch_is1', 'UninstallString', sUninstallPath) then
    begin
      if MsgBox('The existing installation will be uninstalled before installing.', mbInformation, MB_OKCANCEL) = IDOK  then begin
        Exec(RemoveQuotes(sUninstallPath), '/SILENT /NORESTART /SUPPRESSMSGBOXES', '', SW_SHOW, ewWaitUntilTerminated, ResultCode);
      end
      else
      begin
        Result := false;
        exit;
      end
    end
  end;

  Result := true;
end;

#IFDEF UNICODE
  #DEFINE AW "W"
#ELSE
  #DEFINE AW "A"
#ENDIF

type
  INSTALLSTATE = Longint;
const
  INSTALLSTATE_INVALIDARG = -2;  // An invalid parameter was passed to the function.
  INSTALLSTATE_UNKNOWN = -1;     // The product is neither advertised or installed.
  INSTALLSTATE_ADVERTISED = 1;   // The product is advertised but not installed.
  INSTALLSTATE_ABSENT = 2;       // The product is installed for a different user.
  INSTALLSTATE_DEFAULT = 5;      // The product is installed for the current user.

function MsiQueryProductState(szProduct: string): INSTALLSTATE; 
  external 'MsiQueryProductState{#AW}@msi.dll stdcall';

function VisualCIsNotInstalled: Boolean;
begin
  Result := not (MsiQueryProductState('{13A4EE12-23EA-3371-91EE-EFB36DDFFF3E}') = INSTALLSTATE_DEFAULT);
end;

procedure InstallVisualC;
var
  StatusText: string;
  ResultCode: Integer;
begin
  StatusText := WizardForm.StatusLabel.Caption;
  WizardForm.StatusLabel.Caption := 'Installing Microsoft Visual C++ 2013 (x86)...';
  WizardForm.ProgressGauge.Style := npbstMarquee;
  try
    if not Exec(ExpandConstant('{tmp}\vcredist_x86.exe'),'/q /norestart','', SW_HIDE, ewWaitUntilTerminated, ResultCode) then
  begin
    if MsgBox('The installation of Microsoft Visual C++ 2013 (x86) appears to have failed.' + #13#10 + 'Do you want to continue the installation?', mbInformation, MB_OKCANCEL) = IDCANCEL then begin
      exit;
    end
  end;
  finally
    WizardForm.StatusLabel.Caption := StatusText;
    WizardForm.ProgressGauge.Style := npbstNormal;
  end;
end;

function FrameworkIsNotInstalled: Boolean;
var
  fullFrameworkReg: cardinal;
  clientFrameworkReg: cardinal;
begin
  RegQueryDWordValue(HKLM, 'Software\Microsoft\NET Framework Setup\NDP\v4\Full', 'Install', fullFrameworkReg);
  RegQueryDWordValue(HKLM, 'Software\Microsoft\NET Framework Setup\NDP\v4\Client', 'Install', clientFrameworkReg);
  Result := not (fullFrameworkReg <> 0) and not (clientFrameworkReg <> 0);
end;

procedure InstallFramework;
var
  StatusText: string;
  ResultCode: Integer;
begin
  StatusText := WizardForm.StatusLabel.Caption;
  WizardForm.StatusLabel.Caption := 'Installing Microsoft .NET Framework 4...';
  WizardForm.ProgressGauge.Style := npbstMarquee;
  try
    if not Exec(ExpandConstant('{tmp}\dotNetFx40_Client_setup.exe'),'/norestart','', SW_SHOW, ewWaitUntilTerminated, ResultCode) then
  begin
    if MsgBox('The installation of Microsoft .NET Framework 4 appears to have failed.' + #13#10 + 'Do you want to continue the installation?', mbInformation, MB_OKCANCEL) = IDCANCEL then begin
      exit;
    end
  end;
  finally
    WizardForm.StatusLabel.Caption := StatusText;
    WizardForm.ProgressGauge.Style := npbstNormal;
  end;
end;