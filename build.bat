@echo off
echo The format of version must be: x.x.x.x
set /p version=Version number: 
@echo on

:: Create the build directory
rmdir "build" /s /q
rmdir "output" /s /q
xcopy "Mod Files" "build\" /s

:: Change the version number in the mercenaries.xs and launcher/anticheat source
python "scripts_build\versionnum_change.py" "%version%"
del "build\data\randommapstringsf.xml.bak" "build\RMF\mercenaries.xs.bak"

:: Run ASfilecrc to create the resources.xml
"scripts_build\tools\ASfilecrc.exe" "build\RMF" "build\DataPF\Data\resources.xml"

:: Compile all XMLs (etc) in build\DataPF to XMBs
python "scripts_build\build_xmbs.py"

:: Compile the DataPF folder into the BAR
mklink /j "C:\Users\%Username%\Desktop\__DataPF" "build\DataPF\"
python "scripts_build\bar_editor_auto.py" "%CD%\build\DataPF.bar"
rmdir "C:\Users\%Username%\Desktop\__DataPF"
rmdir "build\DataPF" /s /q

:: Hash the DataPF.bar and put the hash in the anticheat source code
python "scripts_build\bar_hash.py"

@echo off
:: Asks the user to open the Visual Studio - Good to not open multiplies times
set /P OpenVisual=Open Visual Studio? (Y/N)
if /I "%OpenVisual%"=="y" goto OpenVisual

:Continue1
echo.

set /P AddBytes = Add more random strings to the ESOCP1? (Y/N)
if /I "%AddBytes%"=="y" goto AddRandString

echo.
echo Press enter after you have compiled and modified agef.exe to have the correct filesizes..
echo After this, everything will be copied.
echo.
@echo on
pause

:: Delete the files that will not be used anymore
del "build\binkw32.dll" "build\deformerdlly.dll" "build\granny2.dll" "build\rockalldll.dll"

:: Copy the launcher into the build folder
copy "ESOC Patch Launcher\Release\ESOCPatchLauncher.exe" "build\ESOCPatchLauncher.exe"

:: Copy the the dlls into the build folder
copy "ESOC Patch Launcher\Release\ESOCP1.dll" "build\ESOCP1.dll"
copy "ESOC Patch Launcher\Release\ESOCP2.dll" "build\ESOCP2.dll"
copy "ESOC Patch Launcher\Release\ESOCP3.dll" "build\ESOCP3.dll"

:: Compile our installer
"scripts_build\tools\Inno Setup\Compil32.exe" /cc "ESOCP Installer.iss"
move "output\setup.exe" "output\ESOC Patch %version%.exe"

:: Create RARs for update and Mac Install
cd "build"
del "dependencies" /F /Q

:: "..\scripts_build\tools\Rar.exe" a -r "..\output\Install_%version%.rar" "*"

move "ESOCPatchLauncher.exe" "NEW_ESOCPatchLauncher.exe"
copy "..\ESOC Patch Launcher\Release\patch_update.exe" "patch_update.exe"

"..\scripts_build\tools\Rar.exe" a -r "..\output\Update_%version%.rar" "*"

cd ..

goto:eof

:: Functions at end of file for easier reading

:OpenVisual
:: Open Visual Studio to allow the user to compile it
for /f "tokens=2 delims==" %%a in ('assoc .sln') do (for /f "tokens=2 delims==" %%b in ('ftype %%a') do set VisualStudioPath=%%b )
set VisualStudioPath=%VisualStudioPath:"%1"=%
%VisualStudioPath%"ESOC Patch Launcher\ESOC Patch Launcher.sln"
goto Continue1

:AddRandString
python "scripts_build\add_randstring.py"
goto Continue1
