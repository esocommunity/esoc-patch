## File Structure

#### ESOC Patch Launcher\
The C# Project for the launcher and anticheat, to be opened with Visual Studio 2015.

#### Mod Files\
The files for the mod to Age of Empires itself. Any changes that are not in protof.xml, randommapstringsf.xml, or techtreef.xml, belong in the DataPF instead.

#### Mod Files\DataPF\
Most of the modded files which will get compiled into the DataPF.bar Some of these have not actually been modified, but are necessary to have because they were in the DataPY.bar already, which the DataPF.bar replaces. These files are automatically compressed to .XMB files and then added to the DataPF.bar in the build script.

#### Mod Files\RMF\
The maps folder. Contains all original maps and groupings as well as maps we (Garja) have modified, as the RMF folder completely replaces the RM3 folder. 

#### scripts\
Scripts included in the installer's build process. These require Inno Setup.

#### scripts_build\
Python scripts used in the build process. These require Python 3.4.


## Build Environment

#### Required Tools
- Requires Python 3.4 for build scripts
- Python packages (install via pip)
	- pywinauto (cmd: pip install pywinauto)


## Build Procedure

1. Make appropriate file changes for the update.
2. Start the build.bat file by double clicking on it. 
3. Enter the version number. Ensure this is in the format "x.x.x.x", where x is a digit 0-9.
4. Do not touch things while the script is running, as parts of it automate clicking on things and typing
5. The first time it pauses, hit enter to continue.
5. The second time it pauses, it will ask if you want to open the Visual Studio, type "y" to open or "n" to continue.
6. With Visual Studio opened, rebuild the project.
7. When the script pauses, the age3ep.exe should be modified to have the correct file sizes:
	a. Open a copy of the latest age3ep.exe while it is located in the Age of Empires 3 install directory with OllyDbg. (It won't load correctly if it is not located there.)
	b. The file sizes of ESOCP1.dll, ESOCP2.dll, and ESOCP3.dll, converted to hex, are the second argument passed to the CMP instruction at offsets 00B83854, 00B83899, 00B838DE, respectively. It is important that at least on of these file sizes has changed in each version so that a new age3ep.exe is always required.
	c. If all the sizes remain the same, you only need to type "y" in the console of build.bat and rebuild; This goes generate a random string. It will be stored in the "string randstr" variable in ESOCP1.dll; a significant change in number of characters is required to change the size. Repeat this step as many times as needed to change the size.
	d. To save your changes of age3ep.exe, right click in the main window, select "Copy to executable" -> "All modifications". In the resulting new window, right click and select "save". Save this .exe in the build\ folder as well as the Mod Files\ folder (as build\ is deleted at every run).
8. Allow the remainder of the script to run.
9. The final files will be in Output\. Install_x.xx.x.rar is for manual installations, Update_x.xx.x.rar is the update file in gets uploaded to the server, and ESOC Patch x.xx.x.exe is the installer for distribution. See the DEPLOY PROCEDURE section for more details.  


## Deploy Procedure

1. Test changes thoroughly to ensure things run correctly. This includes testing on multiple PCs and for multiple users, testing both via a new install with the installer and an auto-update install (which is explained as part of the following process) 
2. Upload the Update_x.xx.x.rar to /home/ec2-user/PatchFiles using FTP.
3. Update the table patch_VersionHistory in the SQL database.
4. Update the download in the public google drive of the .exe
5. Make appropriate forum posts.