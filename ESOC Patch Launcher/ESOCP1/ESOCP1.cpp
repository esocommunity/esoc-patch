// TestApp.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include "conio.h"
#include <iostream>
#include <windows.h>
#include "ESOCP2.h"

_declspec(dllexport) void ShowMessageBox();

// md5 stuff
#include <string.h>								
#include "MD5.h"
#include <fstream>
#include <string>
using std::cout; using std::endl; using std::ifstream;

bool MD5Hash(std::string filename, std::string hash)
{
	//Start opening your file
	ifstream inBigArrayfile;
	inBigArrayfile.open(filename.c_str(), std::ios::binary | std::ios::in);

	//Find length of file
	inBigArrayfile.seekg(0, std::ios::end);
	long Length = inBigArrayfile.tellg();
	inBigArrayfile.seekg(0, std::ios::beg);

	//read in the data from your file
	char * InFileData = new char[Length];
	inBigArrayfile.read(InFileData, Length);

	//Calculate MD5 hash
	std::string Temp = md5(InFileData, Length);

	//Clean up
	delete[] InFileData;

	return (Temp.c_str() == hash);
}

int MainFunction()
{		
	std::string randstr = "";

	if (!MD5Hash("ESOCP2.dll", "6b74a6ae97df7ee94b0c4cd7dd6b4372") || !MD5Hash("ESOCP3.dll", "7565f9336ac250fbb2bd616421e46c00")) {

		system("\"ESOCPatchLauncher.exe\" --WarningDll");

		ExitProcess(0);
	}

	ShowMessageBox();

	return 1;
}

BOOL APIENTRY DllMain(HMODULE hmodule,
	DWORD ul_reason_for_call,
	LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			{
				if (MainFunction())
					return true;

				system("\"ESOCPatchLauncher.exe\" --VisualCWarning");
				return false;
			}
		case DLL_THREAD_ATTACH: break;
		case DLL_THREAD_DETACH: break;
		case DLL_PROCESS_DETACH: break;
	}
	return FALSE;
}
