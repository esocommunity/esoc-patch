﻿using System.IO;
using System.IO.Compression;
using System.Xml.Linq;

namespace ESOCP3
{
    public class Xmb
    {
        private static string[] SerializeStringArray(BinaryReader reader)
        {
            int length = reader.ReadInt32();

            string[] strings = new string[length];

            for (int i = 0; i < length; ++i)
                strings[i] = SerializeU16(reader);

            return strings;
        }

        public static string SerializeU16(BinaryReader reader)
        {
            string data = "";

            int length = reader.ReadInt32();

            for (int i = 0; i < length; ++i)
                data += (char)reader.ReadInt16();

            return data;
        }

        private static void Serialize(BinaryReader reader, XElement root, string[] elements, string[] parameters)
        {
            reader.ReadByte();
            reader.ReadByte();

            reader.ReadInt32();

            string content = SerializeU16(reader);
            root.Value = content;

            int nameId = reader.ReadInt32();
            root.Name = elements[nameId];

            reader.ReadInt32();

            int length = reader.ReadInt32();

            for (int i = 0; i < length; i++)
            {
                nameId = reader.ReadInt32();
                string name = parameters[nameId];

                string value = SerializeU16(reader);
                root.Add(new XAttribute(name, value));
            }

            XNode node = root.FirstNode;

            length = reader.ReadInt32();

            for (int i = 0; i < length; i++)
            {
                while (node != null && !(node is XElement))
                    node = node.NextNode;

                if (node == null)
                    root.Add(node = new XElement("root"));

                Serialize(reader, node as XElement, elements, parameters);

                node = node.NextNode;
            }
        }

        public static XDocument Load(Stream stream)
        {
            Stream newStream;

            int header1 = stream.ReadByte();
            int header2 = stream.ReadByte();

            if (header1 == 'l' && header2 == '3')
            {
                using (BinaryReader binaryReader = new BinaryReader(stream))
                {
                    binaryReader.ReadBytes(2);
                    binaryReader.ReadInt32();
                    binaryReader.ReadBytes(2);

                    MemoryStream memoryStream = new MemoryStream();

                    using (DeflateStream decompStream = new DeflateStream(stream, CompressionMode.Decompress))
                        decompStream.CopyTo(memoryStream);

                    newStream = memoryStream;
                }
            }
            else
                newStream = stream;

            stream.Position = 0;

            using (newStream)
            {
                newStream.Position = 0;

                newStream.ReadByte();
                newStream.ReadByte();

                using (BinaryReader reader = new BinaryReader(newStream))
                {
                    XElement root = new XElement("root");

                    reader.ReadInt32();

                    reader.ReadByte();
                    reader.ReadByte();

                    reader.ReadInt32();
                    reader.ReadInt32();

                    string[] elements = SerializeStringArray(reader);
                    string[] parameters = SerializeStringArray(reader);

                    Serialize(reader, root, elements, parameters);

                    XDocument document = new XDocument(root);

                    return document;
                }
            }
        }
    }
}