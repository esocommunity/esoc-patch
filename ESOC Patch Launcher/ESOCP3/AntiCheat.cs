﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Linq;
using Microsoft.Win32;

namespace ESOCP3
{
    public partial class AntiCheat
    {
        [Flags]
        private enum ErrorType
        {
            Unknown = 1,
            Process = Unknown << 1,
            File = Process << 1,
            Watcher = File << 1,
            Forbidden = Watcher << 1,
            CannotRead = Forbidden << 1,
            Corrupted = CannotRead << 1,
            BarHash = Corrupted << 1,
            XmlLoad = BarHash << 1,
            XmbToXml = XmlLoad << 1,
        }

        private ErrorType _errorType;
        private string _errorDetails;
        private string _errorFilePath;

        private string[] _gamePaths;
        
        public AntiCheat()
        {
            _errorType = ErrorType.Unknown;
            _errorDetails = "Unknown";
            _errorFilePath = "Unknown";

            _gamePaths = GamePaths();

            Thread filesThread = new Thread(FilesWork)
            {
                IsBackground = true,
                Priority = ThreadPriority.Lowest
            };

            Thread processesThread = new Thread(ProcessesWork)
            {
                IsBackground = true,
                Priority = ThreadPriority.Lowest
            };

            filesThread.Start();
            processesThread.Start();
        }

        private void SuspiciousBehavior()
        {
            IntPtr hWnd = Process.GetCurrentProcess().MainWindowHandle;
            NativeMethods.ShowWindow(hWnd, 0);

            string fileName = $"Suspicious Behavior {DateTime.Now.ToString("dd-MM-yyyy HH.mm.ss")}.log";

            string fileContent = $"{_errorType}\n";
            fileContent += $"{_errorFilePath}\n";
            fileContent += $"{_errorDetails}";

            try
            {
                if(!Directory.Exists("ESOC Patch Log"))
                    Directory.CreateDirectory("ESOC Patch Log");

                File.WriteAllText($"ESOC Patch Log/{fileName}", fileContent);
            }
            catch { }

            Process process1 = new Process
            {
                StartInfo =
                {
                    UseShellExecute = false,
                    Arguments = $"--SuspiciousBehavior \"{fileName}\"",
                    FileName = "ESOCPatchLauncher.exe",
                    Verb = "runas"
                }
            };

            Process process2 = new Process
            {
                StartInfo =
                {
                    UseShellExecute = false,
                    Arguments = $"--SendReport SuspiciousBehavior \"{fileName}\"",
                    FileName = "ESOCPatchLauncher.exe",
                    Verb = "runas"
                }
            };

            try
            {
                process1.Start();
                process2.Start();
            }
            catch { }
            finally
            {
                Environment.Exit(0);
            }
        }

        public static string[] GamePaths()
        {
            RegistryKey localKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
            RegistryKey gameKey = localKey.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft Games\\Age of Empires 3 Expansion Pack 2\\1.0");

            string setupPath = gameKey?.GetValue("SetupPath", "").ToString();

            if (string.IsNullOrEmpty(setupPath))
                setupPath = AppDomain.CurrentDomain.BaseDirectory;

            string filePath = Path.Combine(setupPath, "data\\stringtabley.xml");

            XElement element = null;

            try
            {
                element =
                    XDocument.Load(filePath)
                        .Descendants("String")
                        .FirstOrDefault(element2 => (string)element2.Attribute("_locID") == "42265");
            }
            catch { }

            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

            if (element != null)
                documentsPath = Path.Combine(documentsPath, element.Value);

            string[] gamePatchs =
            {
                setupPath,
                documentsPath
            };

            return gamePatchs;
        }
    }
}