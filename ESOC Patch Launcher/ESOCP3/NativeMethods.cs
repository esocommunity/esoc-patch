﻿using System;
using System.Runtime.InteropServices;

namespace ESOCP3
{
    public static class NativeMethods
    {
        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
    }
}