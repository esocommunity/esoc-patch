﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace ESOCP3
{
    public static class Program
    {
        [STAThread]
        public static void Main()
        {
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                Exception exception = args.ExceptionObject as Exception;

                OnException(exception);
            };

            Application.ThreadException += (sender, args) => { OnException(args.Exception); };

            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += delegate
            {
                AntiCheat antiCheat = new AntiCheat();

                GC.KeepAlive(antiCheat);
            };
            backgroundWorker.RunWorkerAsync();
        }

        private static void OnException(Exception exception)
        {
            IntPtr hWnd = Process.GetCurrentProcess().MainWindowHandle;
            NativeMethods.ShowWindow(hWnd, 0);

            Process process = new Process
            {
                StartInfo =
                {
                    UseShellExecute = false,
                    Arguments = "--UnexpectedError ESOCP3",
                    FileName = "ESOCPatchLauncher.exe",
                    Verb = "runas"
                }
            };

            try
            {
                process.Start();
            }
            catch { }

            if (exception == null)
                return;

            string fileName = $"Patch Exception {DateTime.Now.ToString("dd-MM-yyyy HH.mm.ss")}.log";

            try
            {
                if (!Directory.Exists("ESOC Patch Log"))
                    Directory.CreateDirectory("ESOC Patch Log");

                File.WriteAllText($"ESOC Patch Log/{fileName}", exception.ToString());
            }
            catch
            {
                Environment.Exit(0);
            }

            process = new Process
            {
                StartInfo =
                {
                    UseShellExecute = false,
                    Arguments = $"--SendReport PatchError ESOCP3 \"{fileName}\"",
                    FileName = "ESOCPatchLauncher.exe",
                    Verb = "runas"
                }
            };
            
            try
            {
                process.Start();
            }
            catch { }
            finally
            {
                Environment.Exit(0);
            }
        }
    }
}
