﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml.Linq;

namespace ESOCP3
{
    public partial class AntiCheat
    {
        private string[] _fileExtensions =
        {
            /*
                File extensions
            */

            ".cfg",
            ".con",
            ".xml",
            ".xmb"
        };

        private string[] _mainDirectorys =
        {
            /*
                Subfolder to check in Main folder of game
                
                - Folder name
            */

            "data",
            "trigger3",
            "Startup"
        };

        private string[] _documentsDirectorys =
        {
            /*
                Subfolders to check in Documents folder of user
                
                - Folder name
            */

            "Startup"
        };

        private string[] _additionalFiles =
        {
            /*
                Additional files to check
                
                Add the target folder before file relative path
                    0\ = Main folder
                    1\ = Documents folder
                
                - File relative path
            */

            "0\\data\\Data3.bar",
            "0\\DataPF.bar"
        };

        /*
            Extracts the command and context of map()
        */

        private Regex _mapExtractRegex = new Regex(@"map\s*\(\s*"".*?""\s*\,\s*""(.*?)""\s*\,\s*""(.*)""\s*\)",
            RegexOptions.IgnoreCase | RegexOptions.Multiline);

        private string[] _forbiddenInCfg =
        {
            /*
                Forbidden commands in .cfg files

                - Pure string
            */
            
            "showconsole"
        };

        private TupleList<Regex, string, string> _forbiddenInXml = new TupleList<Regex, string, string>
        {
            /*
                Forbidden commands in .xml files
                
                First element: Command

                    - Must be added the following in Regex: RegexOptions.IgnoreCase | RegexOptions.Multiline
                
                Second element: Context

                    null = Forbidden in any context
                
                    - Pure string

                Third element: Argument
                    
                    null = Allows any argument in this context
                
                    Tokens*
                        =Argument = Equals Argument
                        .Argument = Contains Argument
                        ^Argument = Starts with Argument

                    - Pure string
                    - To compare the arguments it is necessary that the regex returns a value in group 1
                    * Needs to use one of the tokens if argument is not null
            */
            
            {
                new Regex(@"SpawnUnits\s*\(", RegexOptions.IgnoreCase | RegexOptions.Multiline),
                null,
                null
            },
            {
                new Regex(@"HomeCityResearch\s*\(", RegexOptions.IgnoreCase | RegexOptions.Multiline),
                null,
                null
            },
            {
                new Regex(@"UpgradeTradeRoute\s*\(", RegexOptions.IgnoreCase | RegexOptions.Multiline),
                null,
                null
            },
            {
                new Regex(@"SpecialPower\s*\(", RegexOptions.IgnoreCase | RegexOptions.Multiline),
                null,
                null
            },
            {
                new Regex(@"UiConsulateUiInSelected\s*\(", RegexOptions.IgnoreCase | RegexOptions.Multiline),
                "ConsulateAccel",
                null
            },
            {
                new Regex(@"ResearchTechInSelected\s*\(\s*""(.*?)""\s*\)", RegexOptions.IgnoreCase | RegexOptions.Multiline),
                "ConsulateAccel",
                "^ypBigSequester"
            },
            {
                new Regex(@"(?:tis|trainInSelected)\s*\(\s*"".*?""\s*\,\s*(\d{2,}|[6-9])\s*\)", RegexOptions.IgnoreCase | RegexOptions.Multiline),
                null,
                null
            },
            {
                new Regex(@"GadgetReal\s*\(\s*""console""\s*\)", RegexOptions.IgnoreCase | RegexOptions.Multiline),
                null,
                null
            },
            {
                new Regex(@"GadgetToggle\s*\(\s*""console""\s*\)", RegexOptions.IgnoreCase | RegexOptions.Multiline),
                null,
                null
            }
        };

        private DictionaryList<string, string> _filesHash = new DictionaryList<string, string>
        {
            /*
                Key = MD5 of file
                Value = File Relative Path*
                
                * Add the target folder before file relative path
                    0\ = Main folder
                    1\ = Documents folder
            */

            // ESOC
            {"0\\DataPF.bar", "B0-2B-D0-0D-0B-30-FA-9A-5B-61-A7-2E-36-1A-5C-EB"},

            // Original
            {"0\\data\\Data3.bar", "D7-10-F0-11-D3-C9-F8-FA-D1-FC-C8-FB-3A-8F-8B-55"},
            {"0\\trigger3\\typetest.xml", "8A-37-B0-03-89-B8-23-76-9E-12-93-BC-3E-EC-2D-0E"},
            {"0\\Startup\\gamex.con", "A7-08-4D-32-E3-1F-1A-FB-78-06-FB-FC-2E-B0-A4-6B"},
            {"0\\Startup\\gamey.cfg", "6D-13-CB-8F-9F-82-AD-80-04-08-AF-21-BB-6F-0C-EA"},
            {"0\\Startup\\gamey.con", "6E-55-57-64-4B-15-6A-08-29-3C-6E-08-B8-D0-F7-D4"},
            {"0\\Startup\\hotkeys.con", "35-FB-66-B6-23-AF-C5-1B-35-62-AC-E5-4B-23-4C-1F"},
            {"0\\Startup\\locale.cfg", "87-B4-4D-14-60-C0-EB-60-77-43-6B-AF-11-75-32-4F"},
            {"0\\Startup\\localex.cfg", "87-B4-4D-14-60-C0-EB-60-77-43-6B-AF-11-75-32-4F"},
            {"0\\Startup\\localey.cfg", "87-B4-4D-14-60-C0-EB-60-77-43-6B-AF-11-75-32-4F"},
            {"0\\Startup\\production.cfg", "36-46-85-DB-99-6F-05-68-9C-7D-79-5D-17-ED-F1-38"},
            {"0\\Startup\\productionx.cfg", "36-46-85-DB-99-6F-05-68-9C-7D-79-5D-17-ED-F1-38"},
            {"0\\Startup\\productiony.cfg", "36-46-85-DB-99-6F-05-68-9C-7D-79-5D-17-ED-F1-38"},
            {"0\\Startup\\developer.con", "E1-41-2F-29-B0-31-4B-E8-C1-4E-BF-04-01-CA-37-A4"},
            {"0\\Startup\\editor.con", "56-24-32-1E-14-E2-4A-85-8F-51-7F-1E-18-AA-99-48"},
            {"0\\Startup\\editorx.con", "56-24-32-1E-14-E2-4A-85-8F-51-7F-1E-18-AA-99-48"},
            {"0\\Startup\\editory.con", "56-24-32-1E-14-E2-4A-85-8F-51-7F-1E-18-AA-99-48"},
            {"0\\Startup\\game.cfg", "8F-CD-32-6E-F1-91-77-DD-BF-10-04-1D-E8-26-AF-FF"},
            {"0\\Startup\\game.con", "E2-AF-EC-8C-B0-6C-74-42-7B-FA-1A-55-E4-8E-E0-E4"},
            {"0\\Startup\\gamex.cfg", "E8-B8-00-08-2B-52-24-2E-AD-69-17-9A-D8-44-D9-12"},

            // Obs UI - 1.7
            {"0\\data\\uimainnew.xml", "19-54-F6-D9-28-00-AD-4C-22-22-61-C5-8B-D8-84-79"}, // 1024 x 768
            {"0\\data\\uimainnew.xml", "6F-8E-9B-B7-72-E2-FA-C2-52-2F-6D-2A-F0-54-97-47"}, // 1280 x 800
            {"0\\data\\uimainnew.xml", "86-48-1E-EE-FF-CA-5F-84-66-BA-B9-7A-D9-AF-8F-DB"}, // 1280 x 600
            {"0\\data\\uimainnew.xml", "E8-17-E9-FC-B4-40-95-12-9C-19-F7-E1-E2-1B-DE-E7"}, // 1366 x 768
            {"0\\data\\uimainnew.xml", "67-6D-6C-5E-71-B5-C5-9E-7B-43-6F-03-95-82-D5-CD"}, // 1440 x 1080
            {"0\\data\\uimainnew.xml", "79-61-86-C0-8A-C1-34-7D-8A-10-6E-4B-4E-94-3E-42"}, // 1920 x 1080
            {"0\\data\\uimainnew.xml", "D2-91-32-A6-19-85-12-5C-22-E2-31-88-42-65-E0-2B"}, // Other Resolutions
            {"0\\data\\uimainnew.xml", "84-00-ED-C3-B7-23-E4-02-A1-A9-97-D4-25-A6-18-D1"}, // Ekanta Compatibility

            // TODO: Obs UI - 2.06
            {"0\\data\\uicommandpanelnewmin.xml", "81-FE-7D-2E-E0-21-44-93-29-5B-F6-48-65-3F-C0-15"},
            {"0\\data\\uihomecitycommandpanel.xml", "68-8F-DE-35-44-CB-70-F0-C3-52-F2-F4-9F-DF-F2-5D"},
            {"0\\data\\uihomecitytransportpanelheader.xml", "6D-27-DC-51-8F-38-65-44-13-8F-95-84-9E-97-EC-7C"},
            {"0\\data\\uimainnew.xml", "D8-F3-7A-4C-97-08-4F-D5-B7-E3-C4-19-62-FF-A1-E0"},
            {"0\\data\\uiminimappanelnewmin.xml", "1F-56-3B-08-13-C8-BE-62-5D-89-FD-67-B3-9E-47-9F"},
            {"0\\data\\uinativecommandpanel2min.xml", "0D-40-3D-C9-1F-93-B9-92-3E-A0-79-57-68-04-2E-1D"},
            {"0\\data\\uiunitselectionmin.xml", "E1-53-E8-7F-C3-7F-89-87-CE-06-C6-68-99-23-31-C8"},
            {"0\\data\\uiunitstatpanel3min.xml", "D2-A2-92-AF-D3-B6-7F-D3-1F-A7-79-E8-1D-94-F3-48"},

            // Ekanta UI
            {"0\\data\\uimainnew.xml.xmb", "7C-81-45-CE-79-4F-DC-F7-E5-83-1A-4B-81-0A-3B-81"},
            {"0\\data\\uiminimappanelnew.xml.xmb", "02-F7-73-10-72-C3-AF-E8-E4-0C-C2-FC-F1-B7-F7-C8"},
            {"0\\data\\uiminimappanelnewmin.xml.xmb", "EC-50-33-3C-28-D9-A7-72-36-6C-CC-D2-28-0F-54-03"},
            {"0\\data\\uinativecommandpanel2.xml.xmb", "E9-AA-7A-80-36-16-B1-71-4A-71-C3-A4-98-20-2C-06"},
            {"0\\data\\uinativecommandpanel2min.xml.xmb", "2A-B3-4D-05-81-92-33-E5-7F-F0-5F-0B-C0-36-04-12"},
            {"0\\data\\uiplayersummarydlg.xml.xmb", "4F-49-A1-39-14-CB-26-40-BF-97-9E-EE-EB-72-74-29"},
            {"0\\data\\uipostgamescreen.xml.xmb", "B0-88-7D-F1-10-26-62-77-32-BD-E4-E5-05-80-F4-DD"},
            {"0\\data\\uitraderoutepanel2.xml.xmb", "E6-6F-1F-86-BE-00-9F-5E-58-5E-4C-CE-BB-29-CA-D5"},
            {"0\\data\\uitraderoutepanel2min.xml.xmb", "CF-4A-E9-C2-00-40-09-81-B2-0E-58-98-42-D2-39-9F"},
            {"0\\data\\uitrainsandcontains.xml.xmb", "52-64-83-D9-1E-F9-04-D4-24-A8-2A-05-AF-0F-B4-4B"},
            {"0\\data\\uitrainsandcontainsmin.xml.xmb", "52-64-83-D9-1E-F9-04-D4-24-A8-2A-05-AF-0F-B4-4B"},
            {"0\\data\\uiunitselection.xml.xmb", "4D-BF-B5-11-F8-A9-B0-79-BB-2B-FD-B9-29-70-AD-E5"},
            {"0\\data\\uiunitselectionmin.xml.xmb", "46-41-91-85-9A-84-3D-BC-19-30-56-7C-3B-64-E1-4A"},
            {"0\\data\\uiunitstatpanel3.xml.xmb", "F1-42-4B-00-D1-7E-FB-A6-4D-E7-95-CD-08-D1-1C-ED"},
            {"0\\data\\uiunitstatpanel3min.xml.xmb", "AB-8B-7F-F1-DF-C9-27-1B-E4-AF-F5-4E-28-40-BF-AA"},
            {"0\\data\\uiunittrainingpanel.xml.xmb", "41-A7-E6-E5-F6-DC-48-B9-27-04-C6-BB-CE-34-12-7B"},
            {"0\\data\\uiunittrainingpanelmin.xml.xmb", "41-A7-E6-E5-F6-DC-48-B9-27-04-C6-BB-CE-34-12-7B"},
            {"0\\data\\uiwonderpowerpanel.xml.xmb", "E1-FA-D3-57-FE-77-32-76-0E-D6-74-2A-F5-CB-B8-E5"},
            {"0\\data\\uiwonderpowerpanelmin.xml.xmb", "38-A7-9B-2E-BC-B9-C2-4E-ED-42-E4-DE-D4-22-79-D3"},
            {"0\\data\\uiarmybanners.xml.xmb", "C4-5E-0A-6D-B8-E6-1F-98-5C-D8-A7-CF-CA-45-03-A6"},
            {"0\\data\\uicommandpanelnew.xml.xmb", "94-36-BE-30-40-96-30-B3-D0-D5-14-98-AE-24-75-7E"},
            {"0\\data\\uicommandpanelnewmin.xml.xmb", "BC-60-02-79-69-C6-AF-5D-3D-6D-C3-D0-1E-C2-AD-58"},
            {"0\\data\\uihomecitycommandpanel.xml.xmb", "C8-8D-EB-FA-60-17-39-21-63-B5-8E-65-CA-C6-C3-38"},
            {"0\\data\\uihomecitydeck.xml.xmb", "6A-01-31-2B-FD-9F-8E-AE-C3-F3-0C-99-B3-6E-4A-A7"},
            {"0\\data\\uihomecitytransportpanelheader.xml.xmb", "2D-59-8C-EF-C7-B2-E3-F9-45-40-04-F2-01-5D-30-2C"},

            // QazUI Cheese UI
            {"0\\data\\uimainnew.xml.xmb", "DC-EA-92-37-E0-17-E2-82-2C-D3-D1-F9-04-80-EB-C3"},
            {"0\\data\\uiminimappanelnew.xml.xmb", "58-5C-7F-C7-AD-31-1B-D6-68-66-90-03-3C-0C-82-5F"},
            {"0\\data\\uiminimappanelnewmin.xml.xmb", "58-5C-7F-C7-AD-31-1B-D6-68-66-90-03-3C-0C-82-5F"},
            {"0\\data\\uinativecommandpanel2.xml.xmb", "E9-AA-7A-80-36-16-B1-71-4A-71-C3-A4-98-20-2C-06"},
            {"0\\data\\uinativecommandpanel2min.xml.xmb", "E9-AA-7A-80-36-16-B1-71-4A-71-C3-A4-98-20-2C-06"},
            {"0\\data\\uiplayersummarydlg.xml.xmb", "95-00-A4-99-0F-F3-B7-B5-60-49-53-95-39-FF-D9-57"},
            {"0\\data\\uitraderoutepanel2.xml.xmb", "E6-6F-1F-86-BE-00-9F-5E-58-5E-4C-CE-BB-29-CA-D5"},
            {"0\\data\\uitraderoutepanel2min.xml.xmb", "E6-6F-1F-86-BE-00-9F-5E-58-5E-4C-CE-BB-29-CA-D5"},
            {"0\\data\\uitrainsandcontains.xml.xmb", "B7-BF-58-31-54-BF-26-A5-C6-6C-47-84-34-95-38-F0"},
            {"0\\data\\uitrainsandcontainsmin.xml.xmb", "B7-BF-58-31-54-BF-26-A5-C6-6C-47-84-34-95-38-F0"},
            {"0\\data\\uiunitselection.xml.xmb", "AF-FB-CC-7D-C9-A4-9E-0A-63-20-20-7D-08-67-23-A4"},
            {"0\\data\\uiunitselectionmin.xml.xmb", "C1-CD-E9-47-C7-28-31-23-DE-1A-85-3B-EB-A9-EA-36"},
            {"0\\data\\uiunitstatpanel3.xml.xmb", "F1-42-4B-00-D1-7E-FB-A6-4D-E7-95-CD-08-D1-1C-ED"},
            {"0\\data\\uiunitstatpanel3min.xml.xmb", "0A-F2-97-1B-23-68-3B-FB-4F-39-BE-9B-71-6F-80-08"},
            {"0\\data\\uiunittrainingpanel.xml.xmb", "BA-F0-EE-6F-E9-A6-5B-63-8E-4D-88-DD-19-87-D3-F9"},
            {"0\\data\\uiunittrainingpanelmin.xml.xmb", "BA-F0-EE-6F-E9-A6-5B-63-8E-4D-88-DD-19-87-D3-F9"},
            {"0\\data\\uiwonderpowerpanel.xml.xmb", "B1-17-5C-0D-97-C6-ED-0B-63-E1-10-B1-D6-8D-32-4E"},
            {"0\\data\\uiwonderpowerpanelmin.xml.xmb", "AB-6C-75-50-77-07-35-39-00-EF-94-44-AD-48-9A-E2"},
            {"0\\data\\uiarmybanners.xml.xmb", "F6-D2-50-EA-CB-80-38-4C-21-F2-43-B9-62-9F-86-E7"},
            {"0\\data\\uicommandpanelnew.xml.xmb", "0F-5F-BB-96-0C-01-80-D1-19-D6-DF-85-AE-70-EC-AE"},
            {"0\\data\\uicommandpanelnewmin.xml.xmb", "0F-5F-BB-96-0C-01-80-D1-19-D6-DF-85-AE-70-EC-AE"},
            {"0\\data\\uihomecitycommandpanel.xml.xmb", "A3-DA-ED-42-78-62-BA-6C-6D-DF-5B-DE-38-CC-9D-87"},
            {"0\\data\\uihomecitydeck.xml.xmb", "3B-5E-B2-47-44-B8-25-3D-3B-68-B8-31-0B-67-38-64"},

            // QazUI Standard UI
            {"0\\data\\uimainnew.xml.xmb", "75-CF-87-27-3A-B6-D3-5A-85-9D-2A-EE-F8-CB-68-3D"},
            {"0\\data\\uiminimappanelnew.xml.xmb", "58-5C-7F-C7-AD-31-1B-D6-68-66-90-03-3C-0C-82-5F"},
            {"0\\data\\uiminimappanelnewmin.xml.xmb", "58-5C-7F-C7-AD-31-1B-D6-68-66-90-03-3C-0C-82-5F"},
            {"0\\data\\uinativecommandpanel2.xml.xmb", "E9-AA-7A-80-36-16-B1-71-4A-71-C3-A4-98-20-2C-06"},
            {"0\\data\\uinativecommandpanel2min.xml.xmb", "E9-AA-7A-80-36-16-B1-71-4A-71-C3-A4-98-20-2C-06"},
            {"0\\data\\uiplayersummarydlg.xml.xmb", "95-00-A4-99-0F-F3-B7-B5-60-49-53-95-39-FF-D9-57"},
            {"0\\data\\uitraderoutepanel2.xml.xmb", "E6-6F-1F-86-BE-00-9F-5E-58-5E-4C-CE-BB-29-CA-D5"},
            {"0\\data\\uitraderoutepanel2min.xml.xmb", "E6-6F-1F-86-BE-00-9F-5E-58-5E-4C-CE-BB-29-CA-D5"},
            {"0\\data\\uitrainsandcontains.xml.xmb", "B7-BF-58-31-54-BF-26-A5-C6-6C-47-84-34-95-38-F0"},
            {"0\\data\\uitrainsandcontainsmin.xml.xmb", "B7-BF-58-31-54-BF-26-A5-C6-6C-47-84-34-95-38-F0"},
            {"0\\data\\uiunitselection.xml.xmb", "AF-FB-CC-7D-C9-A4-9E-0A-63-20-20-7D-08-67-23-A4"},
            {"0\\data\\uiunitselectionmin.xml.xmb", "C1-CD-E9-47-C7-28-31-23-DE-1A-85-3B-EB-A9-EA-36"},
            {"0\\data\\uiunitstatpanel3.xml.xmb", "F1-42-4B-00-D1-7E-FB-A6-4D-E7-95-CD-08-D1-1C-ED"},
            {"0\\data\\uiunitstatpanel3min.xml.xmb", "0A-F2-97-1B-23-68-3B-FB-4F-39-BE-9B-71-6F-80-08"},
            {"0\\data\\uiunittrainingpanel.xml.xmb", "9B-7B-5B-B8-C1-82-C7-FA-E3-49-D6-9A-C1-B3-FE-D3"},
            {"0\\data\\uiunittrainingpanelmin.xml.xmb", "9B-7B-5B-B8-C1-82-C7-FA-E3-49-D6-9A-C1-B3-FE-D3"},
            {"0\\data\\uiwonderpowerpanel.xml.xmb", "01-FE-6B-F3-FD-AB-13-A0-60-B4-B6-60-75-A6-25-B5"},
            {"0\\data\\uiwonderpowerpanelmin.xml.xmb", "23-F7-70-E5-F6-6E-A7-FF-D9-F7-D8-59-B5-79-9D-80"},
            {"0\\data\\uiarmybanners.xml.xmb", "F6-D2-50-EA-CB-80-38-4C-21-F2-43-B9-62-9F-86-E7"},
            {"0\\data\\uicommandpanelnew.xml.xmb", "0F-5F-BB-96-0C-01-80-D1-19-D6-DF-85-AE-70-EC-AE"},
            {"0\\data\\uicommandpanelnewmin.xml.xmb", "0F-5F-BB-96-0C-01-80-D1-19-D6-DF-85-AE-70-EC-AE"},
            {"0\\data\\uihomecitycommandpanel.xml.xmb", "A3-DA-ED-42-78-62-BA-6C-6D-DF-5B-DE-38-CC-9D-87"},
            {"0\\data\\uihomecitydeck.xml.xmb", "3B-5E-B2-47-44-B8-25-3D-3B-68-B8-31-0B-67-38-64"},
        };

        private AutoResetEvent _filesEvent;
        private ConcurrentQueue<Tuple<FileInfo, FileType>> _filesQueue;

        [Flags]
        private enum FileType
        {
            Cfg = 1,
            Con = Cfg << 1,
            Bar = Con << 1,
            Xml = Bar << 1,
            Xmb = Xml << 1,
            Simple = Xmb << 1,
            KeyMap = Simple << 1
        }

        private void FilesWork()
        {
            FilesBoot();

            while (true)
            {
                _filesEvent.WaitOne();

                while (_filesQueue.Count > 0)
                {
                    Tuple<FileInfo, FileType> fileData;

                    _filesQueue.TryDequeue(out fileData);

                    int resultCode = CheckFile(fileData);

                    switch (resultCode)
                    {
                        case -1:
                            SuspiciousBehavior();
                            return;
                        case 1:
                            _filesQueue.Enqueue(fileData);
                            Thread.Sleep(100);
                            break;
                    }
                }
            }
        }

        private void FilesBoot()
        {
            _filesEvent = new AutoResetEvent(true);
            _filesQueue = new ConcurrentQueue<Tuple<FileInfo, FileType>>();

            foreach (KeyValuePair<string, List<string>> fileHash in _filesHash.ToList())
            {
                string filePath = ConvertPath(fileHash.Key);

                _filesHash[filePath] = fileHash.Value;

                _filesHash.Remove(fileHash.Key);
            }

            for (int i = 0; i < _additionalFiles.Length; i++)
                _additionalFiles[i] = ConvertPath(_additionalFiles[i]);

            FileWatcher(_gamePaths[0]);
            FileWatcher(_gamePaths[1]);

            List<string> directorys = new List<string>();
            directorys.AddRange(_mainDirectorys.Select(directory => Path.Combine(_gamePaths[0], directory)));
            directorys.AddRange(_documentsDirectorys.Select(directory => Path.Combine(_gamePaths[1], directory)));

            IEnumerable<FileInfo> filesInfo = new List<FileInfo>();
            filesInfo = _additionalFiles.Select(filePath => new FileInfo(filePath)).Concat(filesInfo);
            filesInfo = directorys.Select(directory => new DirectoryInfo(directory))
                    .Where(directory => directory.Exists)
                    .Select(directoryInfo => directoryInfo.EnumerateFiles())
                    .SelectMany(filesInfo2 => filesInfo2.Where(fileInfo => _fileExtensions.Contains(fileInfo.Extension, StringComparer.OrdinalIgnoreCase)))
                    .Concat(filesInfo);

            foreach (FileInfo fileInfo in filesInfo)
                AddFileToQueue(fileInfo);
        }

        private string ConvertPath(string filePath)
        {
            int targetFolder = int.Parse(filePath.Substring(0, 1));
            filePath = filePath.Substring(2);

            return Path.Combine(_gamePaths[targetFolder], filePath);
        }

        private void AddFileToQueue(FileInfo fileInfo)
        {
            FileType fileType;

            if (new[] { ".xml", ".xmb" }.Contains(fileInfo.Extension, StringComparer.OrdinalIgnoreCase))
            {
                fileType = FileType.Xml;

                if (fileInfo.Name.StartsWith("Ui", StringComparison.OrdinalIgnoreCase) ||
                    (fileInfo.Directory?.Name.Equals("Trigger3", StringComparison.OrdinalIgnoreCase) ?? false))
                    fileType |= FileType.Simple;
                else if (fileInfo.Name.StartsWith("DefaultKeyMapY", StringComparison.OrdinalIgnoreCase))
                    fileType |= FileType.KeyMap;
                else
                    return;

                if (fileInfo.Extension.Equals(".xmb", StringComparison.OrdinalIgnoreCase))
                    fileType |= FileType.Xmb;
            }
            else if (fileInfo.Extension.Equals(".con", StringComparison.OrdinalIgnoreCase))
                fileType = FileType.Con;
            else if (fileInfo.Extension.Equals(".cfg", StringComparison.OrdinalIgnoreCase))
                fileType = FileType.Cfg;
            else if (fileInfo.Extension.Equals(".bar", StringComparison.OrdinalIgnoreCase))
                fileType = FileType.Bar;
            else
                return;

            Tuple<FileInfo, FileType> fileData = Tuple.Create(fileInfo, fileType);

            _filesQueue.Enqueue(fileData);
        }

        private int CheckFile(Tuple<FileInfo, FileType> fileData)
        {
            FileStream fileStream;

            string fileHash;

            try
            {
                fileStream = fileData.Item1.OpenRead();

                fileHash = FileHash(fileStream);

                fileStream.Position = 0;
            }
            catch (Exception e) when (e is FileNotFoundException || e is DirectoryNotFoundException)
            {
                return 0;
            }
            catch (IOException)
            {
                return 1;
            }
            catch (Exception e)
            {
                _errorType = ErrorType.File;
                _errorType |= ErrorType.CannotRead;
                _errorDetails = e.GetType().ToString();
                _errorFilePath = fileData.Item1.FullName;

                return -1;
            }

            bool suspiciousBehavior = false;

            if (!_filesHash[fileData.Item1.FullName].Contains(fileHash))
            {
                if (fileData.Item2.HasFlag(FileType.Bar))
                {
                    _errorType = ErrorType.File;
                    _errorType |= ErrorType.BarHash;
                    _errorFilePath = fileData.Item1.FullName;

                    suspiciousBehavior = true;
                }
                else
                {
                    suspiciousBehavior = fileData.Item2.HasFlag(FileType.Xml) ? CheckXml(fileData.Item1, fileStream, fileData.Item2) : CheckConf(fileStream, fileData.Item2);

                    if (suspiciousBehavior && _errorType == ErrorType.Unknown)
                    {
                        _errorType = ErrorType.File;
                        _errorType |= ErrorType.Forbidden;
                        _errorFilePath = fileData.Item1.FullName;
                    }
                }

                _filesHash.Add(fileData.Item1.FullName, fileHash);
            }

            fileStream.Close();

            return suspiciousBehavior ? -1 : 0;
        }

        private bool CheckConf(FileStream fileStream, FileType fileType)
        {
            using (StreamReader streamReader = new StreamReader(fileStream))
            {
                string line;
				
				switch (fileType)
				{
					case FileType.Cfg:
						while ((line = streamReader.ReadLine()) != null)
						{
							if (_forbiddenInCfg.Any(commandCfg => line.IndexOf(commandCfg, StringComparison.OrdinalIgnoreCase) != -1))
							{
							    _errorDetails = $"Command: {line};";
									
							    return true;
							}
						}
						break;
					case FileType.Con:
						while ((line = streamReader.ReadLine()) != null)
						{
						    if (CheckForMap(line))
                                return true;
						}
						break;
				}
			}

            return false;
        }

        private bool CheckXml(FileInfo fileInfo, FileStream fileStream, FileType fileType)
        {
            XDocument xDocument;

            try
            {
                xDocument = fileType.HasFlag(FileType.Xmb) ? Xmb.Load(fileStream) : XDocument.Load(fileStream);
            }
            catch (Exception e)
            {
                _errorType = ErrorType.File;
                _errorType |= ErrorType.Corrupted;
                _errorType |= fileType.HasFlag(FileType.Xmb) ? ErrorType.XmbToXml : ErrorType.XmlLoad;
                _errorDetails = e.GetType().ToString();
                _errorFilePath = fileInfo.FullName;

                return true;
            }

            if (fileType.HasFlag(FileType.Simple))
            {
                IEnumerable<string> commands = xDocument.Descendants(new[]
                {
                    "Command",
                    "Expression",
                    "SubMenu"
                }, StringComparison.OrdinalIgnoreCase).Select(xElement => xElement.Value);

                commands = xDocument.Descendants()
                        .Attributes()
                        .Where(xElement => xElement.Name.LocalName.IndexOf("Command", StringComparison.OrdinalIgnoreCase) != -1)
                        .Select(xElement => xElement.Value)
                        .Concat(commands);

                if (commands.Any(command => CheckForMap(command) || CheckCommand(command, "game")))
                    return true;
            }
            else if (fileType.HasFlag(FileType.KeyMap))
            {
                IEnumerable<string[]> commandsData =
                    xDocument.Descendants("KeyMapData", StringComparison.OrdinalIgnoreCase)
                        .Select(keyMapsData => keyMapsData.Elements().ToList())
                        .Select(keyMapData => new[]
                        {
                            keyMapData.FirstOrDefault("Command", StringComparison.OrdinalIgnoreCase)?.Value,
                            keyMapData.FirstOrDefault("Context", StringComparison.OrdinalIgnoreCase)?.Value
                        });

                if (commandsData.Any(commandData => CheckForMap(commandData[0]) || CheckCommand(commandData[0], commandData[1])))
                    return true;
            }

            return false;
        }

        private bool CheckCommand(string command, string context)
        {
            foreach (Tuple<Regex, string, string> commandData in _forbiddenInXml)
            {
                MatchCollection matches = commandData.Item1.Matches(command);

                if (matches.Count == 0)
                    continue;

                if (commandData.Item2 == null || commandData.Item2 != context)
                {
                    _errorDetails = $"Command: {command};";

                    return true;
                }

                if (commandData.Item3 == null)
                    continue;

                string token = commandData.Item3.Substring(0, 1);
                string allowedArgument = commandData.Item3.Substring(1);

                switch (token)
                {
                    case "=":
                        if (matches.Cast<Match>().All(match => match.Groups[1].Value.Equals(allowedArgument, StringComparison.OrdinalIgnoreCase)))
                            continue;
                        break;
                    case ".":
                        if (matches.Cast<Match>().All(match => match.Groups[1].Value.IndexOf(allowedArgument, StringComparison.OrdinalIgnoreCase) != -1))
                            continue;
                        break;
                    case "^":
                        if (matches.Cast<Match>().All(match => match.Groups[1].Value.StartsWith(allowedArgument, StringComparison.OrdinalIgnoreCase)))
                            continue;
                        break;
                }

                _errorDetails = $"Command: {command};";

                return true;
            }

            return false;
        }

        private bool CheckForMap(string line)
        {
            foreach (Match match in _mapExtractRegex.Matches(line))
            {
                string context = match.Groups[1].Value;
                string command = match.Groups[2].Value;

                if (CheckCommand(command, context))
                {
                    _errorDetails = $"Map Command: {line};";

                    return true;
                }
            }

            return false;
        }

        private string FileHash(Stream fileStream)
        {
            using (MD5 md5 = new MD5CryptoServiceProvider())
            {
                byte[] hashBytes = md5.ComputeHash(fileStream);

                return BitConverter.ToString(hashBytes);
            }
        }

        private void FileWatcher(string path)
        {
            try
            {
                if (!Directory.Exists(path))
					Directory.CreateDirectory(path);
            }
            catch { }

            FileSystemWatcher watcher = new FileSystemWatcher
            {
                Path = path,
                InternalBufferSize = 64000,
                EnableRaisingEvents = true,
                IncludeSubdirectories = true,
                NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName,
            };

            watcher.Created += OnWatcherEvent;
            watcher.Changed += OnWatcherEvent;
            watcher.Renamed += OnWatcherEvent;

            watcher.Error += OnWatcherError;

            GC.KeepAlive(watcher);
        }

        private void OnWatcherEvent(object source, FileSystemEventArgs e)
        {
            FileInfo fileInfo = new FileInfo(e.FullPath);

            bool mainFile = fileInfo.FullName.IndexOf(_gamePaths[0], StringComparison.OrdinalIgnoreCase) != -1 &&
                     _mainDirectorys.Contains(fileInfo.Directory?.Name ?? "", StringComparer.OrdinalIgnoreCase);

            bool documentsFile = fileInfo.FullName.IndexOf(_gamePaths[1], StringComparison.OrdinalIgnoreCase) != -1 &&
                     _documentsDirectorys.Contains(fileInfo.Directory?.Name ?? "", StringComparer.OrdinalIgnoreCase);

            bool validExtension = _fileExtensions.Contains(fileInfo.Extension, StringComparer.OrdinalIgnoreCase);

            bool additionalFile = _additionalFiles.Any(filePath => filePath == e.FullPath);
            
            if ((validExtension && (mainFile || documentsFile)) || additionalFile)
            {
                AddFileToQueue(fileInfo);

                _filesEvent.Set();
            }
        }

        private void OnWatcherError(object sender, ErrorEventArgs args)
        {
            try
            {
                FileSystemWatcher watcher = sender as FileSystemWatcher;

                if (watcher == null)
                    throw new InvalidOperationException();

                watcher.EnableRaisingEvents = false;
                watcher.EnableRaisingEvents = true;
            }
            catch (Exception e)
            {
                _errorType = ErrorType.File;
                _errorType |= ErrorType.Watcher;
                _errorDetails = $"{args.GetException().GetType()} | {e.GetType()}";

                SuspiciousBehavior();
            }
        }
    }
}