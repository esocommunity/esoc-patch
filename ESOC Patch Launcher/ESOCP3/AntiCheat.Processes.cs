﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using Timer = System.Timers.Timer;

namespace ESOCP3
{
    public partial class AntiCheat
    {
        private List<long> _cheaterLongs;
        private AutoResetEvent _processesEvent;
        private Dictionary<int, string> _processesClean;
        private ConcurrentQueue<FileInfo> _processesQueue;

        private void ProcessesWork()
        {
            ProcessesBoot();

            while (true)
            {
                _processesEvent.WaitOne();

                while (_processesQueue.Count > 0)
                {
                    FileInfo fileInfo;

                    _processesQueue.TryDequeue(out fileInfo);

                    int resultCode = CheckProcess(fileInfo);

                    switch (resultCode)
                    {
                        case -1:
                            SuspiciousBehavior();
                            return;
                        case 1:
                            _processesQueue.Enqueue(fileInfo);
                            Thread.Sleep(100);
                            break;
                    }
                }
            }
        }

        private void ProcessesBoot()
        {
            _cheaterLongs = new List<long>();
            _processesEvent = new AutoResetEvent(true);
            _processesClean = new Dictionary<int, string>();
            _processesQueue = new ConcurrentQueue<FileInfo>();

            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("ESOCP3.Cheater.exe"))
            {
                byte[] buffer = new byte[2048];

                for (int i = 0; i < 329; i++)
                {
                    stream.Read(buffer, 0, 2048);

                    long bufferLong = BitConverter.ToInt64(buffer, 0);

                    _cheaterLongs.Add(bufferLong);
                }
            }

            Timer timer = new Timer(1000)
            {
                Enabled = true
            };
            timer.Elapsed += OnWatcherEvent;
            timer.Start();
        }

        private void OnWatcherEvent(object sender, EventArgs eventArgs)
        {
            foreach (Process process in Process.GetProcesses())
                AddProcessToQueue(process);

            _processesEvent.Set();
        }

        private void AddProcessToQueue(Process process)
        {
            string filePath;
            int processId = process.Id;

            try
            {
                filePath = process.MainModule.FileName;
            }
            catch
            {
                return;
            }

            if (_processesClean.ContainsKey(processId) && _processesClean[processId] == filePath)
                return;

            _processesClean[processId] = filePath;

            FileInfo fileInfo = new FileInfo(filePath);

            if (!fileInfo.Exists || fileInfo.Length >= 1048576 || fileInfo.Length <= 524288)
                return;

            _processesQueue.Enqueue(fileInfo);
        }

        private int CheckProcess(FileInfo fileInfo)
        {
            Stream fileStream;

            try
            {
                fileStream = fileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            }
            catch (Exception e) when (e is FileNotFoundException || e is DirectoryNotFoundException)
            {
                return 0;
            }
            catch (IOException)
            {
                return 1;
            }
            catch (Exception e)
            {
                _errorType = ErrorType.Process;
                _errorType |= ErrorType.CannotRead;
                _errorDetails = e.GetType().ToString();
                _errorFilePath = fileInfo.FullName;

                return -1;
            }

            if (IsCheater(fileStream))
            {
                _errorType = ErrorType.Process;
                _errorType |= ErrorType.Forbidden;
                _errorFilePath = fileInfo.FullName;

                fileStream.Close();

                return -1;
            }

            fileStream.Close();

            return 0;
        }

        private bool IsCheater(Stream stream)
        {
            int matches = 0;

            byte[] buffer = new byte[2048];

            for (int i = 0; i < 329; i++)
            {
                stream.Read(buffer, 0, 2048);

                if (BitConverter.ToInt64(buffer, 0) == _cheaterLongs[i])
                    matches++;
            }

            return matches >= 10;
        }
    }
}