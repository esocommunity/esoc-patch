﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace ESOCP3
{
    public static class Extensions
    {
        public static IEnumerable<XElement> Descendants(this XDocument xDocument, string name, StringComparison comparisonType)
        {
            return xDocument.Descendants().Where(xElement => xElement.Name.LocalName.IndexOf(name, StringComparison.OrdinalIgnoreCase) != -1);
        }

        public static IEnumerable<XElement> Descendants(this XDocument xDocument, string[] names, StringComparison comparisonType)
        {
            return xDocument.Descendants().Where(xElement => names.Contains(xElement.Name.LocalName, StringComparer.OrdinalIgnoreCase));
        }

        public static XElement FirstOrDefault(this IEnumerable<XElement> iEnumerable, string name, StringComparison comparisonType)
        {
            return iEnumerable.FirstOrDefault(xElement => xElement.Name.LocalName.IndexOf(name, StringComparison.OrdinalIgnoreCase) != -1);
        }

        public static void Clear<T>(this ConcurrentQueue<T> concurrentQueue)
        {
            T item;

            while (concurrentQueue.TryDequeue(out item)) { }
        }
    }

    public class TupleList<T1, T2, T3> : List<Tuple<T1, T2, T3>>
    {
        public void Add(T1 item, T2 item2, T3 item3) { Add(new Tuple<T1, T2, T3>(item, item2, item3)); }
    }

    public class DictionaryList<T1, T2> : IEnumerable
    {
        private Dictionary<T1, List<T2>> _dictionary = new Dictionary<T1, List<T2>>();

        public void Add(T1 key, T2 value)
        {
            List<T2> list;

            if (_dictionary.ContainsKey(key))
                list = _dictionary[key];
            else
            {
                list = new List<T2>();

                _dictionary.Add(key, list);
            }

            list.Add(value);
        }

        public IEnumerator GetEnumerator() => _dictionary.GetEnumerator();

        public List<T2> this[T1 key]
        {
            get { return _dictionary.ContainsKey(key) ? _dictionary[key] : new List<T2>(); }
            set
            {
                if (_dictionary.ContainsKey(key))
                    _dictionary[key] = value;
                else
                    _dictionary.Add(key, value);
            }
        }

        public void Remove(T1 key) { _dictionary.Remove(key); }

        public List<KeyValuePair<T1, List<T2>>> ToList()
        {
            return _dictionary.ToList();
        }
    }
}