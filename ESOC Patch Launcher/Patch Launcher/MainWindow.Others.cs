﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml.Linq;
using System.Diagnostics;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;

namespace Patch_Launcher
{
    public partial class MainWindow
    {
        private void CopyRm3()
        {
            ShowInTaskbar = false;
            Visibility = Visibility.Collapsed;

            try
            {
                string rmfPath = Path.Combine(_gamePaths[1], @"RMF\");
                string rm3Path = Path.Combine(_gamePaths[1], @"RM3\");

                if (!Directory.Exists(rmfPath))
                    Directory.CreateDirectory(rmfPath);

                NativeMethods.CreateSymbolicLink(rmfPath, rm3Path, 1);
            }
            catch { }
        }

        private void ShowUnexpectedError(string[] arguments)
        {
            ShowGenericWindow();

            string serviceError = arguments[2] == "Launcher" ? "ESOC Launcher" : "ESOC Patch";

            GenericTextHeader.Text = string.Format(Texts.Global.UnexpectedErrorOccurred, serviceError);
            GenericText1.Text = Texts.Others.ErrorReportSentToTeam;
            GenericText2.Text = Texts.Global.ContinueIssueContactTeam;
            GenericText3.Text = Texts.Global.SorryForThisInconvenience;

            FirstButton.Content = Texts.Global.ForumSupport;
            SecondButton.Content = Texts.Global.MainWindow;

            ZeroButton.Visibility = Visibility.Collapsed;

            _firstButtonClick = delegate { Process.Start(SupportUrl); };
            _secondButtonClick = InitializeWindow;
        }

        private void ShowWarningDll()
        {
            ShowGenericWindow();

            GenericTextHeader.Text = Texts.Others.PatchEncounteredErrorStarting;
            GenericText1.Text = Texts.Global.InstallingLatestVersionShouldRepair;
            GenericText2.Text = Texts.Global.AutomaticInstallationOrBrowser;
            GenericText3.Text = Texts.Global.ContinueIssueContactTeam;

            ZeroButton.Content = Texts.Global.ForumSupport;
            FirstButton.Content = Texts.Global.InBrowser;
            SecondButton.Content = Texts.Global.Automatically;

            _zeroButtonClick = delegate { Process.Start(SupportUrl); };
            _firstButtonClick = delegate { Process.Start(DownloadUrl); };
            _secondButtonClick = delegate
            {
                SecondButton.IsEnabled = false;
                SecondButton.Content = $"{Texts.Global.Connecting}..";

                ThreadPool.QueueUserWorkItem(delegate
                {
                    Soap.PatchInfo(_releaseType, data =>
                    {
                        Dispatcher.BeginInvoke((Action) delegate
                        {
                            if (data == null)
                            {
                                SecondButton.IsEnabled = true;
                                SecondButton.Content = Texts.Global.TryAgain;

                                MessageBox.Show(
                                    $"{Texts.Global.ServerCannotReached}\n\n" +
                                    Texts.Global.TryAgainKeepErrorDownloadInstall,
                                    Texts.Global.LauncherTitle, MessageBoxButton.OK, MessageBoxImage.Information);

                                return;
                            }

                            DownloadUpdate(data);
                        });
                    });
                });
            };
        }

        private void ShowVisualCWarning()
        {
            ShowGenericWindow();

            GenericTextHeader.Text = Texts.Others.PatchFailedToStart;
            GenericText1.Text = Texts.Others.VisualCNotProperlyInstalled;
            GenericText2.Text = Texts.Others.VisualCManuallyInstallOpenBrowser;
            GenericText3.Text = Texts.Global.ContinueIssueContactTeam;

            FirstButton.Content = Texts.Global.ForumSupport;
            SecondButton.Content = Texts.Global.Download;

            ZeroButton.Visibility = Visibility.Collapsed;

            _zeroButtonClick = delegate { Process.Start(SupportUrl); };
            _secondButtonClick = delegate { Process.Start(VisualCUrl); };
        }

        private void SendReport(string[] arguments)
        {
            ShowInTaskbar = false;
            Visibility = Visibility.Collapsed;

            Uri addressUri = new Uri("http://eso-community.net:8080/ESOC/PatchService?WSDL");

            string soapEnvelope =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pat=\"http://patch.esoc.com/\"><soapenv:Header/><soapenv:Body>{0}</soapenv:Body></soapenv:Envelope>";

            string soapRequest = null;

            WebClient webClient;

            switch (arguments[2])
            {
                case "PatchError":
                    {
                        string exception;

                        try
                        {
                            exception = File.ReadAllText($"ESOC Patch Log/{arguments[4]}");
                            exception = WebUtility.HtmlEncode(exception);
                        }
                        catch
                        {
                            return;
                        }
                        
                        string soapArguments = "<pat:reportPatchError><patchErrorEvent>";
                        soapArguments += $"<errorMessage>{exception}</errorMessage>";
                        soapArguments += $"<errorType>{arguments[3]}</errorType>";
                        soapArguments += $"<patchVersion>{_patchVersion}</patchVersion>";
                        soapArguments += "</patchErrorEvent></pat:reportPatchError>";

                        soapRequest = string.Format(soapEnvelope, soapArguments);
                    }
                    break;
                case "SuspiciousBehavior":
                    {
                        string[] data;

                        try
                        {
                            data = File.ReadLines($"ESOC Patch Log/{arguments[3]}").ToArray();
                        }
                        catch
                        {
                            return;
                        }

                        string profilePath = Path.Combine(_gamePaths[1], "Users3\\NewProfile3.xml");

                        XDocument profile = null;

                        try
                        {
                            profile = XDocument.Load(profilePath);
                        }
                        catch { }

                        string esoName = profile?.Descendants("ESOName", StringComparison.OrdinalIgnoreCase).FirstOrDefault()?.Value ?? "Unknown";
                        string guid = profile?.Descendants("GUID", StringComparison.OrdinalIgnoreCase).FirstOrDefault()?.Value ?? "{00000000-0000-0000-0000-000000000000}";

                        string macAddress = NetworkInterface.GetAllNetworkInterfaces()
                            .Where(networkInterface => networkInterface.OperationalStatus == OperationalStatus.Up)
                            .FirstOrDefault(
                                networkInterface =>
                                    networkInterface.NetworkInterfaceType == NetworkInterfaceType.Ethernet ||
                                    networkInterface.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)?
                            .GetPhysicalAddress()
                            .ToString() ?? "000000000000";

                        webClient = new WebClient();

                        string ipAddress = null;

                        try
                        {
                            ipAddress = webClient.DownloadString("http://icanhazip.com/");
                        }
                        catch { }
                        finally
                        {
                            if (string.IsNullOrEmpty(ipAddress))
                                ipAddress = "0.0.0.0";
                        }

                        string fileRelativePath = data[1];
                        fileRelativePath = fileRelativePath.Replace(_gamePaths[0], "");
                        fileRelativePath = fileRelativePath.Replace(_gamePaths[1], "");

                        string details = string.Join("\n", data.Skip(2));
                        details = WebUtility.HtmlEncode(details);

                        string soapArguments = "<pat:reportHackingIncident><HackingReportEvent>";
                        soapArguments += $"<details>{details}</details>";
                        soapArguments += $"<error>{data[0]}</error>";
                        soapArguments += $"<esoName>{esoName}</esoName>";
                        soapArguments += $"<filePath>{fileRelativePath}</filePath>";
                        soapArguments += $"<guid>{guid}</guid>";
                        soapArguments += $"<ipAddress>{ipAddress}</ipAddress>";
                        soapArguments += $"<macAddress>{macAddress}</macAddress>";
                        soapArguments += $"<patchVersion>{_patchVersion}</patchVersion>";
                        soapArguments += "</HackingReportEvent></pat:reportHackingIncident>";

                        soapRequest = string.Format(soapEnvelope, soapArguments);
                    }
                    break;
            }

            if (string.IsNullOrEmpty(soapRequest))
                return;

            webClient = new WebClient
            {
                Headers =
                {
                    ["Content-Type"] = "text/xml;charset=utf-8",
                    ["SOAPAction"] = ""
                }
            };

            try
            {
                webClient.UploadString(addressUri, "POST", soapRequest);
            }
            catch { }
        }
    }
}