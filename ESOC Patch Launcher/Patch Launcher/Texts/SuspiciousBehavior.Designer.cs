﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Patch_Launcher.Texts {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class SuspiciousBehavior {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal SuspiciousBehavior() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Patch_Launcher.Texts.SuspiciousBehavior", typeof(SuspiciousBehavior).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The file cannot be renamed for an unknown reason..
        /// </summary>
        public static string CannotRenamedFileUnknownReason {
            get {
                return ResourceManager.GetString("CannotRenamedFileUnknownReason", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Now you can start the game and play!.
        /// </summary>
        public static string CanStartGameAndPlay {
            get {
                return ResourceManager.GetString("CanStartGameAndPlay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact Team.
        /// </summary>
        public static string ContactTeam {
            get {
                return ResourceManager.GetString("ContactTeam", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Then, copy the error report in the next page, create a new topic, and paste the error report..
        /// </summary>
        public static string CopyReportNewTopicPasteError {
            get {
                return ResourceManager.GetString("CopyReportNewTopicPasteError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Copy This.
        /// </summary>
        public static string CopyThis {
            get {
                return ResourceManager.GetString("CopyThis", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Could not read the file..
        /// </summary>
        public static string CouldNotReadFile {
            get {
                return ResourceManager.GetString("CouldNotReadFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Could not read the file, it is corrupted..
        /// </summary>
        public static string CouldNotReadFileCorrupted {
            get {
                return ResourceManager.GetString("CouldNotReadFileCorrupted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Could not read the program below..
        /// </summary>
        public static string CouldNotReadProgram {
            get {
                return ResourceManager.GetString("CouldNotReadProgram", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The ESOC Patch has detected suspicious behavior and the game has been closed..
        /// </summary>
        public static string DetectedSuspiciousBehaviorGameClosed {
            get {
                return ResourceManager.GetString("DetectedSuspiciousBehaviorGameClosed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do not run this while you are playing..
        /// </summary>
        public static string DontRunWhilePlaying {
            get {
                return ResourceManager.GetString("DontRunWhilePlaying", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error Report.
        /// </summary>
        public static string ErrorReport {
            get {
                return ResourceManager.GetString("ErrorReport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This can also be a false positive. Please contact the ESO-Community Team if you need for support..
        /// </summary>
        public static string FalsePositiveContactTeamSupport {
            get {
                return ResourceManager.GetString("FalsePositiveContactTeamSupport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This file contains forbidden commands..
        /// </summary>
        public static string FileContainsForbiddenCommands {
            get {
                return ResourceManager.GetString("FileContainsForbiddenCommands", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This file is not the original game..
        /// </summary>
        public static string FileNotOriginalOfGame {
            get {
                return ResourceManager.GetString("FileNotOriginalOfGame", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File Path.
        /// </summary>
        public static string FilePath {
            get {
                return ResourceManager.GetString("FilePath", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The file has been renamed to: .
        /// </summary>
        public static string FileRenamedTo {
            get {
                return ResourceManager.GetString("FileRenamedTo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Follow the steps below to get in contact with the ESO-Community Team..
        /// </summary>
        public static string FollowStepsBelowContactTeam {
            get {
                return ResourceManager.GetString("FollowStepsBelowContactTeam", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This is a known cheat program..
        /// </summary>
        public static string KnowCheatProgram {
            get {
                return ResourceManager.GetString("KnowCheatProgram", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Launch.
        /// </summary>
        public static string Launch {
            get {
                return ResourceManager.GetString("Launch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to If you do not have an account on the forums, you will have to create one..
        /// </summary>
        public static string NeedCreateAccounForumNotHave {
            get {
                return ResourceManager.GetString("NeedCreateAccounForumNotHave", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Account.
        /// </summary>
        public static string NewAccount {
            get {
                return ResourceManager.GetString("NewAccount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to It is not obligatory, but you can give us additional information about the error.        ///Example: Custom UI, New Hotkeys etc..
        /// </summary>
        public static string NotObligatoryAdditionalInformationAboutError {
            get {
                return ResourceManager.GetString("NotObligatoryAdditionalInformationAboutError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Operation completed successfully.
        /// </summary>
        public static string OperationCompletedSuccessfully {
            get {
                return ResourceManager.GetString("OperationCompletedSuccessfully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This is pretty suspect, may have some cheat, we do not know..
        /// </summary>
        public static string PrettySuspectHaveCheatNotKnow {
            get {
                return ResourceManager.GetString("PrettySuspectHaveCheatNotKnow", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This is pretty suspect, may a cheat program, we do not know..
        /// </summary>
        public static string PrettySuspectIsCheatNotKnow {
            get {
                return ResourceManager.GetString("PrettySuspectIsCheatNotKnow", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Some process is locking the file, it will be necessary to restart your computer..
        /// </summary>
        public static string ProcessLockingFileNeedRestartComputer {
            get {
                return ResourceManager.GetString("ProcessLockingFileNeedRestartComputer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Remove the following command yourself or try the automatic repair..
        /// </summary>
        public static string RemoveCommandOrTryRepair {
            get {
                return ResourceManager.GetString("RemoveCommandOrTryRepair", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Repair.
        /// </summary>
        public static string Repair {
            get {
                return ResourceManager.GetString("Repair", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You can try the automatic repair, this will rename the file and it will no longer be scanned by anti-cheat..
        /// </summary>
        public static string TryRepairRenameFileScannedAntiCheat {
            get {
                return ResourceManager.GetString("TryRepairRenameFileScannedAntiCheat", resourceCulture);
            }
        }
    }
}
