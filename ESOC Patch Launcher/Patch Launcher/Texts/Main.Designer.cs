﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Patch_Launcher.Texts {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Main {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Main() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Patch_Launcher.Texts.Main", typeof(Main).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to For some unknown reason, we can not start the game.        ///You will have to open the game manually..
        /// </summary>
        public static string CannotStartGame {
            get {
                return ResourceManager.GetString("CannotStartGame", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Checking updates...
        /// </summary>
        public static string CheckingUpdates {
            get {
                return ResourceManager.GetString("CheckingUpdates", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You can close the launcher, and play from GameRanger as you would normally..
        /// </summary>
        public static string CloseLauncherPlayGameRangerNormally {
            get {
                return ResourceManager.GetString("CloseLauncherPlayGameRangerNormally", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The game is already running..
        /// </summary>
        public static string GameAlreadyRunning {
            get {
                return ResourceManager.GetString("GameAlreadyRunning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The patch is currently configured to support {0}..
        /// </summary>
        public static string GameRanger_Configured {
            get {
                return ResourceManager.GetString("GameRanger_Configured", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Would you like configure the ESOC Patch for use with {0}?.
        /// </summary>
        public static string GameRanger_Question {
            get {
                return ResourceManager.GetString("GameRanger_Question", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The patch has been successfully configured for {0}..
        /// </summary>
        public static string GameRanger_Success {
            get {
                return ResourceManager.GetString("GameRanger_Success", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GameRanger Support.
        /// </summary>
        public static string GameRanger_Support {
            get {
                return ResourceManager.GetString("GameRanger_Support", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Launch Game.
        /// </summary>
        public static string LaunchGame {
            get {
                return ResourceManager.GetString("LaunchGame", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No.
        /// </summary>
        public static string No {
            get {
                return ResourceManager.GetString("No", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No new updates..
        /// </summary>
        public static string NoNewUpdates {
            get {
                return ResourceManager.GetString("NoNewUpdates", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Options.
        /// </summary>
        public static string Options {
            get {
                return ResourceManager.GetString("Options", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Building Placement Rotator.
        /// </summary>
        public static string Options_BuildRotator {
            get {
                return ResourceManager.GetString("Options_BuildRotator", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Record Games Automatically.
        /// </summary>
        public static string Options_RecordGame {
            get {
                return ResourceManager.GetString("Options_RecordGame", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show Game FPS.
        /// </summary>
        public static string Options_ShowFps {
            get {
                return ResourceManager.GetString("Options_ShowFps", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Skip Intro cinematic.
        /// </summary>
        public static string Options_SkipCinematic {
            get {
                return ResourceManager.GetString("Options_SkipCinematic", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The ESOC Patch is configured for the GameRanger..
        /// </summary>
        public static string PatchConfiguredFoGameRanger {
            get {
                return ResourceManager.GetString("PatchConfiguredFoGameRanger", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tools.
        /// </summary>
        public static string Tools {
            get {
                return ResourceManager.GetString("Tools", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Try Fix Failed to Join.
        /// </summary>
        public static string Tools_FTJ {
            get {
                return ResourceManager.GetString("Tools_FTJ", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Setup for GameRanger.
        /// </summary>
        public static string Tools_GameRanger {
            get {
                return ResourceManager.GetString("Tools_GameRanger", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mod Manager.
        /// </summary>
        public static string Tools_ModManager {
            get {
                return ResourceManager.GetString("Tools_ModManager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Recorded Games.
        /// </summary>
        public static string Tools_Replay {
            get {
                return ResourceManager.GetString("Tools_Replay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to If you want to play the original game, or to play the ESOC Patch on ESO, disable support for GameRanger..
        /// </summary>
        public static string WantPlayOnEsoDisableGameRanger {
            get {
                return ResourceManager.GetString("WantPlayOnEsoDisableGameRanger", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yes.
        /// </summary>
        public static string Yes {
            get {
                return ResourceManager.GetString("Yes", resourceCulture);
            }
        }
    }
}
