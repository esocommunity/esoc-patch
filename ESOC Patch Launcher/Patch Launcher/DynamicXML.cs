﻿using System.Dynamic;
using System.Xml.Linq;

namespace Patch_Launcher
{
    public class DynamicXml : DynamicObject
    {
        private XElement _element;

        public DynamicXml(string xml)
        {
            _element = XDocument.Parse(xml).Root;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = null;

            XAttribute attribute = _element?.Attribute(binder.Name);

            if (attribute != null)
                result = attribute.Value;
            else
            {
                XElement element = _element?.Element(binder.Name);

                if (element != null)
                    result = element.Value;

            }

            return true;
        }
    }
}