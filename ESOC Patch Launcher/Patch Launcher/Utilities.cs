﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml.Linq;
using Microsoft.Win32;
using System.Reflection;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Patch_Launcher
{
    public static class Utilities
    {
        public static class Process
        {
            public static bool Start(string fileName)
            {
                return Start(fileName, null, null, null);
            }

            public static bool Start(string fileName, string arguments)
            {
                return Start(fileName, arguments, null, null);
            }

            public static bool Start(string fileName, string arguments, Action<int> onClosed)
            {
                return Start(fileName, arguments, onClosed, null);
            }

            public static bool Start(string fileName, string arguments, string workingDirectory)
            {
                return Start(fileName, arguments, null, workingDirectory);
            }

            public static bool Start(string fileName, string arguments, Action<int> onClosed, string workingDirectory)
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process
                {
                    StartInfo =
                    {
                        WorkingDirectory = workingDirectory,
                        UseShellExecute = false,
                        Arguments = arguments,
                        FileName = fileName,
                        Verb = "runas"
                    },
                    EnableRaisingEvents = onClosed != null
                };

                try
                {
                    if (onClosed != null)
                    {
                        process.Exited += delegate
                        {
                            Application.Current.Dispatcher.BeginInvoke(onClosed, process.ExitCode);
                        };
                    }

                    process.Start();
                }
                catch
                {
                    return false;
                }

                return true;
            }

            public static bool Running(string name)
            {
                System.Diagnostics.Process process;

                return Running(name, null, out process);
            }

            public static bool Running(string name, Action callback)
            {
                System.Diagnostics.Process process;

                return Running(name, callback, out process);
            }

            public static bool Running(string name, out System.Diagnostics.Process process)
            {
                return Running(name, null, out process);
            }

            public static bool Running(string name, Action callback, out System.Diagnostics.Process process)
            {
                process = System.Diagnostics.Process.GetProcessesByName(name).FirstOrDefault();

                if (process == null) return false;
                if (callback == null) return true;

                process.EnableRaisingEvents = true;

                process.Exited += delegate
                {
                    Application.Current.Dispatcher.BeginInvoke(callback);
                };

                return true;
            }
        }

        public static class Game
        {
            public static string[] Paths()
            {
                RegistryKey localKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
                RegistryKey gameKey =
                    localKey.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft Games\\Age of Empires 3 Expansion Pack 2\\1.0");

                string setupPath = gameKey?.GetValue("SetupPath", "").ToString();

                if (string.IsNullOrEmpty(setupPath))
                    setupPath = AppDomain.CurrentDomain.BaseDirectory;

                string filePath = Path.Combine(setupPath, "data\\stringtabley.xml");

                XElement element = null;

                try
                {
                    element =
                        XDocument.Load(filePath)
                            .Descendants("String")
                            .FirstOrDefault(element2 => (string) element2.Attribute("_locID") == "42265");
                }
                catch { }

                string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

                if (element != null)
                    documentsPath = Path.Combine(documentsPath, element.Value);

                string[] gamePatchs =
                {
                    setupPath,
                    documentsPath
                };

                return gamePatchs;
            }
        }

        public static class File
        {
            public static bool Delete(string filePath)
            {
                try
                {
                    System.IO.File.Delete(filePath);
                }
                catch
                {
                    return false;
                }

                return true;
            }
        }

        public static class Patch
        {
            public static string GetVersion()
            {
                FileVersionInfo fileVersionInfo =
                    FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);

                return fileVersionInfo.FileVersion;
            }

            public static void SetVersion(string value)
            {
                RegistryKey patchKey =
                    Registry.LocalMachine.OpenSubKey(
                        "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\ESO Community Patch_is1", true);

                patchKey?.SetValue("DisplayVersion", value);
                patchKey?.Close();
            }
        }
    }

    public class OpaqueClickableImage : Image
    {
        protected override HitTestResult HitTestCore(PointHitTestParameters hitTestParameters)
        {
            BitmapSource source = (BitmapSource)Source;

            int x = (int)(hitTestParameters.HitPoint.X / ActualWidth * source.PixelWidth);
            int y = (int)(hitTestParameters.HitPoint.Y / ActualHeight * source.PixelHeight);

            if (x == source.PixelWidth)
                x--;

            if (y == source.PixelHeight)
                y--;

            byte[] pixel = new byte[4];
            source.CopyPixels(new Int32Rect(x, y, 1, 1), pixel, 4, 0);

            return pixel[3] < 10 ? null : new PointHitTestResult(this, hitTestParameters.HitPoint);
        }
    }
}