﻿using System;
using System.IO;
using System.Windows;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows.Input;

namespace Patch_Launcher
{
    public partial class MainWindow
    {
        private void ShowSuspiciousBehavior(string[] arguments)
        {
            string[] data;

            try
            {
                data = File.ReadLines($"ESOC Patch Log/{arguments[2]}").ToArray();
            }
            catch
            {
                data = new[]
                {
                    "Unknown, Unknown",
                    "Unknown",
                    "Unknown"
                };
            }

            string[] errors = data[0].Split(new[] { ", " }, StringSplitOptions.None);

            string details = string.Join("\n", data.Skip(2));

            string[] errorData =
            {
                errors[0], // Error
                errors[1], // Sub Error
                data[0], // Full Error
                details, // Details
                data[1] // File Path
            };

            int errorType = 0;

            string errorText1 = "";
            string errorText2 = "";

            switch (errorData[0])
            {
                case "File":
                    switch (errorData[1])
                    {
                        case "CannotRead":
                            errorType = 1;
                            errorText1 = $"{Texts.SuspiciousBehavior.CouldNotReadFile}\n";
                            errorText1 += Texts.SuspiciousBehavior.PrettySuspectHaveCheatNotKnow;
                            errorText2 = $"{Texts.SuspiciousBehavior.FilePath}: {errorData[4]}";
                            break;
                        case "BarHash":
                            errorType = 2;
                            break;
                        case "Forbidden":
                            errorType = 3;
                            errorText1 = $"{Texts.SuspiciousBehavior.FileContainsForbiddenCommands}\n";
                            errorText1 += Texts.SuspiciousBehavior.RemoveCommandOrTryRepair;
                            errorText2 = $"{Texts.SuspiciousBehavior.FilePath}: {errorData[4]}\n";
                            errorText2 += errorData[3];
                            GenericText2.MaxHeight = 60;
                            break;
                        case "Corrupted":
                            errorType = 3;
                            errorText1 = $"{Texts.SuspiciousBehavior.CouldNotReadFileCorrupted}\n";
                            errorText1 += Texts.SuspiciousBehavior.PrettySuspectHaveCheatNotKnow;
                            errorText2 = $"{Texts.SuspiciousBehavior.FilePath}: {errorData[4]}";
                            break;
                    }
                    break;
                case "Process":
                    switch (errorData[1])
                    {
                        case "CannotRead":
                            errorType = 1;
                            errorText1 = $"{Texts.SuspiciousBehavior.CouldNotReadFile}\n";
                            errorText1 += Texts.SuspiciousBehavior.PrettySuspectIsCheatNotKnow;
                            errorText2 = $"{Texts.SuspiciousBehavior.FilePath}: {errorData[4]}";
                            break;
                        case "Forbidden":
                            errorType = 1;
                            errorText1 = $"{Texts.SuspiciousBehavior.KnowCheatProgram}\n";
                            errorText1 += Texts.SuspiciousBehavior.DontRunWhilePlaying;
                            errorText2 = $"{Texts.SuspiciousBehavior.FilePath}: {errorData[4]}";
                            break;
                    }
                    break;
            }

            ShowGenericWindow();

            GenericTextHeader.Text = Texts.SuspiciousBehavior.DetectedSuspiciousBehaviorGameClosed;

            switch (errorType)
            {
                case 0:
                case 1:
                    if (errorType == 0)
                    {
                        GenericText1.Text = string.Format(Texts.Global.UnexpectedErrorOccurred, "ESOC Patch");
                        GenericText2.Text = Texts.Global.ContinueIssueContactTeam;
                        GenericText3.Text = Texts.Global.SorryForThisInconvenience;
                    }
                    else
                    {
                        GenericText1.Text = errorText1;
                        GenericText2.Text = errorText2;
                        GenericText3.Text = Texts.SuspiciousBehavior.FalsePositiveContactTeamSupport;
                    }

                    FirstButton.Content = Texts.Global.MainWindow;
                    SecondButton.Content = Texts.SuspiciousBehavior.ContactTeam;

                    ZeroButton.Visibility = Visibility.Collapsed;

                    _firstButtonClick = InitializeWindow;
                    _secondButtonClick = delegate { ContactTeam(errorData); };
                    break;
                case 2:
                    GenericText1.Text = Texts.Global.InstallingLatestVersionShouldRepair;
                    GenericText2.Text += Texts.Global.AutomaticInstallationOrBrowser;
                    GenericText3.Text += Texts.Global.ContinueIssueContactTeam;

                    ZeroButton.Content = Texts.SuspiciousBehavior.ContactTeam;
                    FirstButton.Content = Texts.Global.InBrowser;
                    SecondButton.Content = Texts.Global.Automatically;

                    _zeroButtonClick = delegate { ContactTeam(errorData); };
                    _firstButtonClick = delegate { Process.Start(DownloadUrl); };
                    _secondButtonClick = delegate
                    {
                        SecondButton.IsEnabled = false;
                        SecondButton.Content = $"{Texts.Global.Connecting}..";

                        ThreadPool.QueueUserWorkItem(delegate
                        {
                            Soap.PatchInfo(_releaseType, data2 =>
                            {
                                Dispatcher.BeginInvoke((Action)delegate
                                {
                                    if (data == null)
                                    {
                                        SecondButton.IsEnabled = true;
                                        SecondButton.Content = Texts.Global.TryAgain;

                                        MessageBox.Show(
                                            $"{Texts.Global.ServerCannotReached}\n\n" +
                                            Texts.Global.TryAgainKeepErrorDownloadInstall,
                                            Texts.Global.LauncherTitle, MessageBoxButton.OK, MessageBoxImage.Information);

                                        return;
                                    }

                                    DownloadUpdate(data);
                                });
                            });
                        });
                    };
                    break;
                case 3:
                    GenericText1.Text = errorText1;
                    GenericText2.Text = errorText2;
                    GenericText3.Text = $"{Texts.SuspiciousBehavior.TryRepairRenameFileScannedAntiCheat}\n\n";
                    GenericText3.Text += Texts.Global.ContinueIssueContactTeam;

                    FirstButton.Content = Texts.SuspiciousBehavior.ContactTeam;
                    SecondButton.Content = Texts.SuspiciousBehavior.Repair;

                    ZeroButton.Visibility = Visibility.Collapsed;

                    _firstButtonClick = delegate { ContactTeam(errorData); };
                    _secondButtonClick = delegate
                    {
                        Utilities.File.Delete($"{errorData[4]}.backup");

                        try
                        {
                            File.Move(errorData[4], $"{errorData[4]}.backup");
                        }
                        catch
                        {
                            ShowGenericWindow();

                            GenericTextHeader.Text = Texts.SuspiciousBehavior.CannotRenamedFileUnknownReason;
                            GenericText1.Text = Texts.SuspiciousBehavior.ProcessLockingFileNeedRestartComputer;
                            GenericText2.Text = Texts.Global.ContinueIssueContactTeam;

                            SecondButton.IsEnabled = false;

                            ZeroButton.Visibility = Visibility.Collapsed;

                            return;
                        }

                        ShowGenericWindow();

                        GenericTextHeader.Text = Texts.SuspiciousBehavior.OperationCompletedSuccessfully;

                        GenericText1.Text = $"{Texts.SuspiciousBehavior.CanStartGameAndPlay}\n";
                        GenericText1.Text += Texts.Global.ContinueIssueContactTeam;
                        GenericText2.Text = $"{Texts.SuspiciousBehavior.FileRenamedTo}:";
                        GenericText3.Text = $"{errorData[4]}.backup";

                        FirstButton.Content = Texts.Global.MainWindow;
                        SecondButton.Content = Texts.SuspiciousBehavior.Launch;

                        ZeroButton.Visibility = Visibility.Collapsed;

                        _firstButtonClick = InitializeWindow;
                        _secondButtonClick = LaunchGame;
                    };

                    break;
            }
        }

        private void ContactTeam(string[] errorData)
        {
            ShowGenericWindow();

            GenericTextHeader.Text = Texts.SuspiciousBehavior.FollowStepsBelowContactTeam;
            GenericText1.Text = Texts.SuspiciousBehavior.NeedCreateAccounForumNotHave;
            GenericText2.Text = Texts.SuspiciousBehavior.CopyReportNewTopicPasteError;
            GenericText3.Text = Texts.SuspiciousBehavior.NotObligatoryAdditionalInformationAboutError;

            ZeroButton.Content = Texts.SuspiciousBehavior.NewAccount;
            FirstButton.Content = Texts.Global.ForumSupport;
            SecondButton.Content = Texts.SuspiciousBehavior.ErrorReport;

            _zeroButtonClick = delegate { Process.Start(RegisterUrl); };
            _firstButtonClick = delegate { Process.Start(SupportUrl); };
            _secondButtonClick = delegate
            {
                string errorReport = "Error: Suspicious Behavior\n";
                errorReport += $"OS Version: {Environment.OSVersion}\n";
                errorReport += $"OS Platform: {(Environment.Is64BitOperatingSystem ? "64 Bit" : "32 Bit")}\n";
                errorReport += $"Patch Version: {_patchVersion}\n";
                errorReport += $"Error: {errorData[2]}\n";
                errorReport += $"Details: {errorData[3]}\n";
                errorReport += $"File Path: {errorData[4]}\n";

                ShowGenericWindow();

                GenericTextHeader.Text = Texts.SuspiciousBehavior.ErrorReport;

                GenericBox.Text = errorReport;
                GenericBox.Visibility = Visibility.Visible;

                ZeroButton.Content = Texts.SuspiciousBehavior.NewAccount;
                FirstButton.Content = Texts.Global.ForumSupport;
                SecondButton.Content = Texts.SuspiciousBehavior.CopyThis;

                _zeroButtonClick = delegate { Process.Start(RegisterUrl); };
                _firstButtonClick = delegate { Process.Start(SupportUrl); };
                _secondButtonClick = delegate
                {
                    Keyboard.Focus(GenericBox);

                    GenericBox.SelectAll();

                    Clipboard.SetText(GenericBox.Text);
                };
            };
        }

    }
}