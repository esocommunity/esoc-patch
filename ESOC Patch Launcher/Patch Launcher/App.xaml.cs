﻿using System;
using System.Windows;
using System.Diagnostics;
using System.IO;
using System.Windows.Controls;
using System.Windows.Media;

namespace Patch_Launcher
{
    public partial class App
    {
        public App()
        {
            RenderOptions.ClearTypeHintProperty.OverrideMetadata(typeof (FrameworkElement), new FrameworkPropertyMetadata
            {
                DefaultValue = ClearTypeHint.Enabled
            });

            TextOptions.TextFormattingModeProperty.OverrideMetadata(typeof (FrameworkElement), new FrameworkPropertyMetadata
            {
                DefaultValue = TextFormattingMode.Display
            });

            ToolTipService.ShowDurationProperty.OverrideMetadata(typeof (DependencyObject), new FrameworkPropertyMetadata
            {
                DefaultValue = int.MaxValue
            });

            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                string[] commandLineArgs = Environment.GetCommandLineArgs();

                if (commandLineArgs.Length > 1)
                {
                    switch (commandLineArgs[1])
                    {
                        case "--SendReport":
                        case "--UnexpectedError":
                            Environment.Exit(0);
                            return;
                    }
                }

                Process process = new Process
                {
                    StartInfo =
                    {
                        UseShellExecute = false,
                        Arguments = "--UnexpectedError Launcher",
                        FileName = "ESOCPatchLauncher.exe",
                        Verb = "runas"
                    }
                };

                try
                {
                    process.Start();
                }
                catch { }

                Exception exception = args.ExceptionObject as Exception;

                if (exception == null)
                    return;

                string fileName = $"Launcher Exception {DateTime.Now.ToString("dd-MM-yyyy HH.mm.ss")}.log";

                try
                {
                    Directory.CreateDirectory("ESOC Patch Log");

                    File.WriteAllText($"ESOC Patch Log/{fileName}", exception.ToString());
                }
                catch
                {
                    Environment.Exit(0);
                }

                process = new Process
                {
                    StartInfo =
                    {
                        UseShellExecute = false,
                        Arguments = $"--SendReport PatchError Launcher \"{fileName}\"",
                        FileName = "ESOCPatchLauncher.exe",
                        Verb = "runas"
                    }
                };

                try
                {
                    process.Start();
                }
                catch { }
                finally
                {
                    Environment.Exit(0);
                }
            };
        }
    }
}