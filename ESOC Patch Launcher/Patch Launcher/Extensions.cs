﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Patch_Launcher
{
    public static class Extensions
    {
        public static IEnumerable<XElement> Descendants(this XDocument xDocument, string name, StringComparison comparisonType)
        {
            return xDocument.Descendants().Where(xElement => xElement.Name.LocalName.IndexOf(name, StringComparison.OrdinalIgnoreCase) != -1);
        }
    }
}