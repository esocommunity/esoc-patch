﻿using System;
using System.IO;
using System.Net;
using System.Linq;
using System.Dynamic;
using System.Diagnostics;
using System.Collections.Generic;
using System.Windows;
using Timer = System.Timers.Timer;

namespace Patch_Launcher
{
    public static class Soap
    {
        private static Uri _addressUri = new Uri("http://eso-community.net:8080/ESOC/PatchService?WSDL");

        private static string _soapEnvelope =
            "<soapenv:Envelope xmlns:pat=\"http://patch.esoc.com/\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"><soapenv:Header/><soapenv:Body>{soapArguments}</soapenv:Body></soapenv:Envelope>";
        
        private static string SoapResponse(string xml, string node, bool cutNode)
        {
            int startPos = xml.IndexOf($"<{node}>");
            int endPos = xml.LastIndexOf($"</{node}>");

            if (startPos == -1 || endPos == -1)
                throw new FormatException("Invalid SOAP response.");

            if (cutNode)
                startPos = startPos + $"<{node}>".Length;
            else
                endPos = endPos + $"</{node}>".Length;

            return xml.Substring(startPos, endPos - startPos);
        }

        public static void PatchInfo(string releaseType, Action<dynamic> action)
        {
            string soapArguments = "<pat:getLatestPatchVersion>";
            soapArguments += $"<releaseType>{releaseType}</releaseType>";
            soapArguments += "</pat:getLatestPatchVersion>";

            string soapRequest = _soapEnvelope.Replace("{soapArguments}", soapArguments);

            Timer timer = new Timer(10000);
            timer.Elapsed += delegate
            {
                timer.Stop();

                action(null);
            };
            timer.Start();
            
            WebClient webClient = new WebClient
            {
                Headers =
                {
                    ["Content-Type"] = "text/xml;charset=utf-8",
                    ["SOAPAction"] = ""
                }
            };

            DynamicXml data = null;

            try
            {
                string result = webClient.UploadString(_addressUri, "POST", soapRequest);

                string soapResponse = SoapResponse(result, "return", false);

                data = new DynamicXml(soapResponse);
            }
            catch { }

            timer.Stop();

            action(data);
        }

        public static void Download(string fileName, double fileSize, Action<int, dynamic> completed, Action<double, string, string, string> progress)
        {
            string soapArguments = "<pat:downloadFile>";
            soapArguments += $"<fileName>{fileName}</fileName>";
            soapArguments += "</pat:downloadFile>";

            string soapRequest = _soapEnvelope.Replace("{soapArguments}", soapArguments);

            string fileSizeFormated = (fileSize/1024d/1024d).ToString("0.00");

            List<double> speedBytesAverage = new List<double>();

            Stopwatch stopWatch = new Stopwatch();

            bool updateDownloadProgress = false;

            Timer timer = new Timer(1000);
            timer.Elapsed += delegate
            {
                updateDownloadProgress = true;
            };
            timer.Start();

            WebClient webClient = new WebClient
            {
                Headers =
                {
                    ["Content-Type"] = "text/xml;charset=utf-8",
                    ["SOAPAction"] = ""
                }
            };

            webClient.UploadStringCompleted += (sender, args) =>
            {
                if (args.Cancelled || args.Error != null)
                {
                    dynamic error = new ExpandoObject();

                    error.textHeader = Texts.Soap.ErrorWhileDownloadFile;
                    error.text1 = args.Error.Message;
                    error.text2 = args.Error.InnerException?.Message;
                    error.text3 = Texts.Global.TryAgainKeepErrorDownloadInstall;

                    completed(1, error);

                    return;
                }

                fileName = Guid.NewGuid().ToString("N").Substring(0, 32) + ".rar";

                try
                {
                    string rawResponse = SoapResponse(args.Result, "fileBytes", true);

                    byte[] fileBytes = Convert.FromBase64String(rawResponse);

                    File.WriteAllBytes(fileName, fileBytes);
                }
                catch (Exception exception)
                {
                    dynamic error = new ExpandoObject();

                    if (exception is FormatException || exception is ArgumentNullException)
                    {
                        error.textHeader = Texts.Soap.DownloadCorrupted;
                        error.text1 = Texts.Soap.DownloadCorruptedExplanation;
                        error.text2 = null;
                    }
                    else if (exception is IOException || exception is UnauthorizedAccessException)
                    {
                        error.textHeader = Texts.Soap.DownloadCantWrite;
                        error.text1 = Texts.Soap.DownloadCantWriteExplanation;
                        error.text2 = null;
                    }
                    else
                    {
                        error.textHeader = Texts.Global.UnknownErrorOccurred;
                        error.text1 = exception.Message;
                        error.text2 = exception.InnerException?.Message;
                    }

                    error.text3 = Texts.Global.TryAgainKeepErrorDownloadInstall;

                    completed(1, error);

                    return;
                }

                completed(0, fileName);
            };

            webClient.UploadProgressChanged += (sender, args) =>
            {
                if (!updateDownloadProgress)
                    return;

                updateDownloadProgress = false;

                double percent = args.BytesReceived/fileSize*100;

                double speedBytes = args.BytesReceived/stopWatch.Elapsed.TotalSeconds;

                speedBytesAverage.Add(speedBytes);
                speedBytes = speedBytesAverage.Skip(speedBytesAverage.Count - 10).Average();

                string speed = speedBytes >= 1048576
                    ? $"{(speedBytes/1024d/1024d).ToString("0.00")} MB/s"
                    : $"{(speedBytes/1024d).ToString("0.00")} KB/s";

                string transferred = $"{(args.BytesReceived/1024d/1024d).ToString("0.00")} / {fileSizeFormated} MB";

                string remaining = "";
                double remainingSeconds = (fileSize - args.BytesReceived)/speedBytes;

                if (!double.IsNaN(remainingSeconds) && !double.IsInfinity(remainingSeconds))
                {
                    TimeSpan duration = TimeSpan.FromSeconds(remainingSeconds);

                    if (percent < 99)
                    {
                        if (duration.Hours > 1)
                            remaining += $"{duration.Hours.ToString("00")} {Texts.Soap.Hours}, ";
                        else if (duration.Hours == 1)
                            remaining += $"{duration.Hours.ToString("00")} {Texts.Soap.Hour}, ";

                        if (duration.Minutes > 1)
                            remaining += $"{duration.Minutes.ToString("00")} {Texts.Soap.Minutes}";
                        else if (duration.Minutes == 1)
                            remaining += $"{duration.Minutes.ToString("00")} {Texts.Soap.Minute}";

                        if (duration.Seconds != 0 && duration.Hours == 0 || duration.Minutes == 0)
                        {
                            if (duration.Minutes > 0)
                                remaining += ", ";

                            if (duration.Seconds > 1)
                                remaining += $"{duration.Seconds.ToString("00")} {Texts.Soap.Seconds}";
                            else
                                remaining += $"{duration.Seconds.ToString("00")} {Texts.Soap.Second}";
                        }
                    }
                    else
                    {
                        remaining = Texts.Soap.Finished;

                        stopWatch.Stop();
                        timer.Stop();
                    }
                }
                else
                    remaining = $"{Texts.Soap.Estimating}..";

                progress(percent, speed, transferred, remaining);
            };

            stopWatch.Start();

            webClient.UploadStringAsync(_addressUri, "POST", soapRequest);
        }
    }
}