﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml.Linq;
using System.Threading;
using System.Windows.Input;
using System.Windows.Controls;

namespace Patch_Launcher
{
    public partial class MainWindow
    {
        private string _releaseType = "Production";

        private const string SiteUrl = "http://eso-community.net/index.php";
        private const string RegisterUrl = "http://eso-community.net/ucp.php?mode=register";
        private const string DiscussionUrl = "http://eso-community.net/viewforum.php?f=80";
        private const string SupportUrl = "http://eso-community.net/viewforum.php?f=110";
        private const string DownloadUrl = "https://drive.google.com/folderview?id=0B-TThZ4HMOZCZWNhRVJCLUs2cEE";
        private const string UpdateNotesUrl = "https://docs.google.com/document/d/17RyIaOdML4GO8o0s4nMfKdVbfmd3GZodArjMRyfIwCQ";
        private const string PatchNotesUrl = "https://docs.google.com/document/d/1BU_OxBbppIg3cJ6t17_eTyMYtDf5CDyxzGlM_8d4p8k";
        private const string VisualCUrl = "https://www.microsoft.com/download/details.aspx?id=48145";

        private Grid _gridCurrent;

        private Action _zeroButtonClick;
        private Action _firstButtonClick;
        private Action _secondButtonClick;

        private string _patchVersion;

        private string[] _gamePaths;

        private string _pressedKeys;

        public MainWindow()
        {
            InitializeComponent();

            _gamePaths = Utilities.Game.Paths();

            try
            {
                _patchVersion = File.ReadAllText("beta.data");

                BetaPatch.Visibility = Visibility.Visible;

                _releaseType = "Beta";
            }
            catch
            {
                _patchVersion = Utilities.Patch.GetVersion();
            }

            EsocLogo.Click += delegate { Process.Start(SiteUrl); };

            CloseButton.Click += delegate { Close(); };

            ZeroButton.Click += delegate { _zeroButtonClick(); };
            FirstButton.Click += delegate { _firstButtonClick(); };
            SecondButton.Click += delegate { _secondButtonClick(); };

            MainLaunch.Click += delegate { LaunchGame(); };
            MainOptions.Click += delegate { ShowOptionsWindow(); };
            MainTools.Click += delegate { ShowToolsWindow(); };

            OptionsRecord.Click += delegate { Options(OptionsType.Profile, OptionsAction.Save); };
            OptionsCinematic.Click += delegate { Options(OptionsType.Cfg, OptionsAction.Save); };
            OptionsFps.Click += delegate { Options(OptionsType.Cfg, OptionsAction.Save); };
            OptionsRotator.Click += delegate { Options(OptionsType.Con, OptionsAction.Save); };
            
            ToolsGameRanger.Click += delegate { ShowGameRangerWindow(); };

            KeyDown += (sender, args) =>
            {
                _pressedKeys += args.Key.ToString();

                if (BetaPatch.Visibility == Visibility.Visible)
                {
                    if ("CURRENT".StartsWith(_pressedKeys))
                    {
                        if (_pressedKeys.Length >= 7)
                        {
                            PatchSwitch(false);
                        }
                        else
                            return;
                    }
                }
                else if ("BETA".StartsWith(_pressedKeys))
                {
                    if (_pressedKeys.Length == 4)
                    {
                        PatchSwitch(true);
                    }
                    else
                        return;
                }

                _pressedKeys = "";
            };

            MouseDown += delegate
            {
                if (Mouse.LeftButton == MouseButtonState.Pressed)
                    DragMove();
            };

            string[] commandLineArgs = Environment.GetCommandLineArgs();

            if (commandLineArgs.Length > 1)
            {
                if (CommandLine(commandLineArgs))
                    return;
            }

            InitializeWindow();
        }

        private bool CommandLine(string[] arguments)
        {
            switch (arguments[1])
            {
                case "--UnexpectedError":
                    ShowUnexpectedError(arguments);
                    return true;
                case "--WarningDll":
                    ShowWarningDll();
                    return true;
                case "--SuspiciousBehavior":
                    ShowSuspiciousBehavior(arguments);
                    return true;
                case "--VisualCWarning":
                    ShowVisualCWarning();
                    return true;
                case "--SendReport":
                    SendReport(arguments);
                    break;
                case "--CopyRM3":
                    CopyRm3();
                    break;
                case "--ReleaseChannel":
                    _releaseType = arguments[2];
                    return false;
                default:
                    return false;
            }

            Application.Current.Shutdown();

            return true;
        }

        private void InitializeWindow()
        {
            ShowMainWindow();

            ThreadPool.QueueUserWorkItem(delegate
            {
                Soap.PatchInfo(_releaseType, data =>
                {
                    Dispatcher.BeginInvoke((Action) delegate
                    {
                        MainLaunch.IsEnabled = true;

                        GameInfo.Text = Texts.Main.NoNewUpdates;
                    });

                    if (data == null)
                        return;

                    int serverVersion = int.Parse(data.versionName.Replace(".", ""));
                    int appVersion = int.Parse(_patchVersion.Replace(".", ""));

                    if (appVersion == serverVersion)
                        return;

                    int fileSize = int.Parse(data.size);
                    fileSize = fileSize*4/3;

                    string releaseDate = data.releaseDate;
                    string versionName = data.versionName;
                    string fileSizeText = (fileSize/1024d/1024d).ToString("0.00 MB");

                    Dispatcher.BeginInvoke((Action) delegate
                    {
                        ShowGrid(UpdateWarningGrid);

                        UpdateWarningDate.Text = releaseDate;
                        UpdateWarningVersion.Text = versionName;
                        UpdateWarningSize.Text = fileSizeText;

                        FirstButton.Content = Texts.Global.Back;
                        SecondButton.Content = Texts.Global.Install;

                        ZeroButton.Visibility = Visibility.Collapsed;

                        _firstButtonClick = ShowMainWindow;
                        _secondButtonClick = delegate { DownloadUpdate(data); };
                    });
                });
            });
        }

        private void PatchSwitch(bool beta)
        {
            MainLaunch.IsEnabled = false;

            BetaPatch.Visibility = beta ? Visibility.Visible : Visibility.Collapsed;

            ThreadPool.QueueUserWorkItem(delegate
            {
                Process process;

                if (Utilities.Process.Running("age3f", out process))
                {
                    process.Kill();
                }

                Directory.CreateDirectory("Patch Switcher\\Beta\\data");
                Directory.CreateDirectory("Patch Switcher\\Current\\data");

                string[] filesPath =
                {
                    "beta.data",
                    "age3f.exe",
                    "DataPF.bar",
                    "ESOCP1.dll",
                    "ESOCP2.dll",
                    "ESOCP3.dll",
                    "data\\protof.xml",
                    "data\\randommapstringsf.xml",
                    "data\\techtreef.xml",
                    "RMF\\"
                };

                foreach (string filePath in filesPath)
                {
                    string toPath = beta ? "Patch Switcher\\Current\\" : "Patch Switcher\\Beta\\";
                    string fromPath = beta ? "Patch Switcher\\Beta\\" : "Patch Switcher\\Current\\";

                    string toFilePath = Path.Combine(toPath, filePath);
                    string fromFilePath = Path.Combine(fromPath, filePath);

                    if (Directory.Exists(filePath))
                        Directory.Move(filePath, toFilePath);
                    else if (File.Exists(filePath))
                        File.Move(filePath, toFilePath);

                    while (Directory.Exists(filePath) || File.Exists(filePath))
                        Thread.Sleep(100);

                    if (Directory.Exists(fromFilePath))
                        Directory.Move(fromFilePath, filePath);
                    else if (File.Exists(fromFilePath))
                        File.Move(fromFilePath, filePath);
                }

                if (beta)
                {
                    if (File.Exists("beta.data"))
                        _patchVersion = File.ReadAllText("beta.data");
                    else
                        File.WriteAllText("beta.data", _patchVersion);
                }
                else
                    _patchVersion = Utilities.Patch.GetVersion();

                _releaseType = beta ? "Beta" : "Production";

                Dispatcher.BeginInvoke((Action) delegate
                {
                    GameInfo.Text = Texts.Main.CheckingUpdates;

                    InitializeWindow();
                });
            });
        }

        private void ShowGrid(Grid showWindow)
        {
            if (_gridCurrent != null)
                _gridCurrent.Visibility = Visibility.Collapsed;

            ZeroButton.IsEnabled = true;
            FirstButton.IsEnabled = true;
            SecondButton.IsEnabled = true;

            ZeroButton.Visibility = Visibility.Visible;
            FirstButton.Visibility = Visibility.Visible;
            SecondButton.Visibility = Visibility.Visible;

            showWindow.Visibility = Visibility.Visible;

            _gridCurrent = showWindow;
        }

        private void ShowMainWindow()
        {
            if (_gridCurrent != null)
                _gridCurrent.Visibility = Visibility.Collapsed;

            MainGrid.Visibility = Visibility.Visible;

            _gridCurrent = MainGrid;

            ZeroButton.Visibility = Visibility.Collapsed;
            FirstButton.Visibility = Visibility.Collapsed;
            SecondButton.Visibility = Visibility.Collapsed;
        }

        private void ShowOptionsWindow()
        {
            ShowGrid(OptionsGrid);

            Options(OptionsType.Patch, OptionsAction.Load);
            Options(OptionsType.Profile, OptionsAction.Load);
            Options(OptionsType.Cfg, OptionsAction.Load);
            Options(OptionsType.Con, OptionsAction.Load);

            SecondButton.Content = Texts.Global.Back;

            ZeroButton.Visibility = Visibility.Collapsed;
            FirstButton.Visibility = Visibility.Collapsed;

            _secondButtonClick = ShowMainWindow;
        }

        private void ShowToolsWindow()
        {
            ShowGrid(ToolsGrid);

            SecondButton.Content = Texts.Global.Back;

            ZeroButton.Visibility = Visibility.Collapsed;
            FirstButton.Visibility = Visibility.Collapsed;

            _secondButtonClick = ShowMainWindow;
        }

        private void ShowGameRangerWindow()
        {
            ShowGenericWindow();

            string defaultPath = Path.Combine(_gamePaths[0], "age3y.exe");
            string esocPath = Path.Combine(_gamePaths[0], "age3f.exe");
            string rePath = Path.Combine(_gamePaths[0], "age3re.exe");

            bool gameRangerEnabled = File.Exists(rePath);

            string currentName = gameRangerEnabled ? "Game Ranger" : "ESO";
            string targetName = gameRangerEnabled ? "ESO" : "Game Ranger";

            GenericTextHeader.Text = Texts.Main.GameRanger_Support;
            GenericText1.Text = string.Format(Texts.Main.GameRanger_Configured, currentName);
            GenericText2.Text = string.Format(Texts.Main.GameRanger_Question, targetName);

            FirstButton.Content = Texts.Main.No;
            SecondButton.Content = Texts.Main.Yes;

            ZeroButton.Visibility = Visibility.Collapsed;

            _firstButtonClick = ShowToolsWindow;
            _secondButtonClick = delegate
            {
                try
                {
                    if (gameRangerEnabled)
                    {
                        File.Move(defaultPath, esocPath);
                        File.Move(rePath, defaultPath);
                    }
                    else
                    {
                        File.Move(defaultPath, rePath);
                        File.Move(esocPath, defaultPath);
                    }
                }
                catch { }

                ShowGenericWindow();

                GenericTextHeader.Text = Texts.Main.GameRanger_Support;
                GenericText1.Text = string.Format(Texts.Main.GameRanger_Success, targetName);

                if (gameRangerEnabled == false)
                    GenericText2.Text = Texts.Main.CloseLauncherPlayGameRangerNormally;

                ZeroButton.Visibility = Visibility.Collapsed;
                FirstButton.Visibility = Visibility.Collapsed;

                SecondButton.Content = Texts.Global.OK;
				
                _secondButtonClick = ShowToolsWindow;
            };
        }
        
        private void ShowGenericWindow()
        {
            ShowGrid(GenericGrid);

            GenericTextHeader.Text = null;
            GenericText1.Text = null;
            GenericText2.Text = null;
            GenericText3.Text = null;
        }

        private void ShowProgressBar(bool show)
        {
            ProgressBarGrid.Visibility = show ? Visibility.Visible : Visibility.Collapsed;

            ProgressBarComplete.Width = 45;
            ProgressBarGlow.Margin = new Thickness(25, 0, 0, 0);
        }

        private void UpdateProgressBar(double percent)
        {
            double width = percent * 3.4;

            ProgressBarComplete.Width = 45 + width;
            ProgressBarGlow.Margin = new Thickness(25 + width, 0, 0, 0);
        }

        private void LaunchGame()
        {
            if (File.Exists("age3re.exe"))
            {
                MessageBox.Show(
                    $"{Texts.Main.PatchConfiguredFoGameRanger} \n" +
                    $"{Texts.Main.CloseLauncherPlayGameRangerNormally} \n" +
                    Texts.Main.WantPlayOnEsoDisableGameRanger,
                    Texts.Global.LauncherTitle, MessageBoxButton.OK, MessageBoxImage.Information);

                return;
            }

            MainLaunch.IsEnabled = false;

            ThreadPool.QueueUserWorkItem(delegate
            {
                Process process;

                if (Utilities.Process.Running("age3f", out process) || Utilities.Process.Running("age3y", out process))
                {
                    bool isVisible = false;

                    IntPtr hWnd = process.MainWindowHandle;

                    if (hWnd != IntPtr.Zero)
                    {
                        int style = NativeMethods.GetWindowLong(hWnd, -16);
                        isVisible = ((style & 0x8000000) != 0x8000000);
                    }

                    if (isVisible)
                    {
                        Dispatcher.BeginInvoke((Action)delegate
                        {
                            MainLaunch.IsEnabled = true;
                        });

                        MessageBox.Show(
                            Texts.Main.GameAlreadyRunning,
                            Texts.Global.LauncherTitle, MessageBoxButton.OK, MessageBoxImage.Information);

                        return;
                    }

                    process.Kill();
                }

                string exePath = Path.Combine(_gamePaths[0], "age3f.exe");

                Dispatcher.BeginInvoke((Action) delegate
                {
                    if (Utilities.Process.Start(exePath, null, _gamePaths[0]))
                    {
                        Application.Current.Shutdown();
                    }
                    else
                    {
                        MainLaunch.IsEnabled = true;

                        MessageBox.Show(
                            Texts.Main.CannotStartGame,
                            Texts.Global.LauncherTitle, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                });
            });
        }

        private void Options(OptionsType type, OptionsAction action)
        {
            switch (type)
            {
                case OptionsType.Profile:
                    string profilePath = Path.Combine(_gamePaths[1], "Users3\\NewProfile3.xml");

                    XDocument profile;

                    try
                    {
                        profile = XDocument.Load(profilePath);
                    }
                    catch
                    {
                        return;
                    }

                    XElement profileRecordGame =
                        profile.Descendants("GameSettings")
                            .FirstOrDefault(element => (string) element.Attribute("Name") == "MultiplayerGameSettings")?
                            .Descendants("Setting")
                            .FirstOrDefault(element => (string) element.Attribute("Name") == "gamerecordgame");

                    switch (action)
                    {
                        case OptionsAction.Load:
                            OptionsRecord.IsChecked = Convert.ToBoolean(profileRecordGame?.Value);
                            break;
                        case OptionsAction.Save:
                            if (profileRecordGame != null)
                            {
                                profileRecordGame.Value = OptionsRecord.IsChecked.ToString();

                                profile.Save(profilePath);
                            }
                            break;
                    }
                    break;
                case OptionsType.Cfg:
                    string cfgPath = Path.Combine(_gamePaths[1], "Startup\\user.cfg");

                    if (!File.Exists(cfgPath))
                    {
                        string folderPath = Path.Combine(_gamePaths[0], "Startup\\");

                        if (!Directory.Exists(folderPath))
                            Directory.CreateDirectory(folderPath);

                        cfgPath = Path.Combine(folderPath, "user.cfg");
                    }

                    List<string> cfgLines;

                    try
                    {
                        if (File.Exists(cfgPath))
                            cfgLines = File.ReadAllLines(cfgPath).ToList();
                        else
                        {
                            File.Create(cfgPath).Dispose();

                            cfgLines = new List<string>();
                        }
                    }
                    catch
                    {
                        return;
                    }

                    switch (action)
                    {
                        case OptionsAction.Load:
                            OptionsCinematic.IsChecked = cfgLines.Contains("noIntroCinematics");
                            OptionsFps.IsChecked = cfgLines.Contains("showFPS");
                            break;
                        case OptionsAction.Save:
                            if (OptionsCinematic.IsChecked.Value)
                            {
                                if (!cfgLines.Contains("noIntroCinematics"))
                                    cfgLines.Add("noIntroCinematics");
                            }
                            else
                                cfgLines.RemoveAll(line => line.Contains("noIntroCinematics"));

                            if (OptionsFps.IsChecked.Value)
                            {
                                if (!cfgLines.Contains("showFPS"))
                                    cfgLines.Add("showFPS");
                            }
                            else
                                cfgLines.RemoveAll(line => line.Contains("showFPS"));

                            Utilities.File.Delete(cfgPath);
                            File.WriteAllLines(cfgPath, cfgLines);
                            break;
                    }
                    break;
                case OptionsType.Con:
                    string conPath = Path.Combine(_gamePaths[1], "Startup\\user.con");

                    if (!File.Exists(conPath))
                    {
                        string folderPath = Path.Combine(_gamePaths[0], "Startup\\");

                        if(!Directory.Exists(folderPath))
                            Directory.CreateDirectory(folderPath);

                        conPath = Path.Combine(folderPath, "user.con");
                    }

                    List<string> conLines;

                    try
                    {
                        if (File.Exists(conPath))
                            conLines = File.ReadAllLines(conPath).ToList();
                        else
                        {
                            File.Create(conPath).Dispose();

                            conLines = new List<string>();
                        }
                    }
                    catch
                    {
                        return;
                    }

                    switch (action)
                    {
                        case OptionsAction.Load:
                            OptionsRotator.IsChecked = conLines.Any(line => line.Contains("uiWheelRotatePlacedUnit"));
                            break;
                        case OptionsAction.Save:
                            if (OptionsRotator.IsChecked.Value)
                            {
                                if (!conLines.Any(line => line.Contains("uiWheelRotatePlacedUnit")))
                                    conLines.Add("map(\"mousez\", \"building\", \"uiWheelRotatePlacedUnit\")");
                            }
                            else
                                conLines.RemoveAll(line => line.Contains("uiWheelRotatePlacedUnit"));

                            Utilities.File.Delete(conPath);
                            File.WriteAllLines(conPath, conLines);
                            break;
                    }
                    break;
            }
        }

        private enum OptionsType
        {
            Patch,
            Profile,
            Cfg,
            Con
        }

        private enum OptionsAction
        {
            Load,
            Save
        }
    }
}