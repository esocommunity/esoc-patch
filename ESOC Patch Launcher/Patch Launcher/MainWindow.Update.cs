﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Diagnostics;

namespace Patch_Launcher
{
    public partial class MainWindow
    {
        private void DownloadUpdate(dynamic data)
        {
            ShowProgressBar(true);
            ShowGrid(UpdateDownloadGrid);

            UpdateDownloadSpeed.Text = "0.00 MB/s";
            UpdateDownloadTransferred.Text =  "0.00 / 0.00 MB";
            UpdateDownloadRemaining.Text = $"{Texts.Global.Connecting}..";

            ZeroButton.Content = Texts.Update.PatchNotes;
            FirstButton.Content = Texts.Update.UpdateNotes;
            SecondButton.Content = Texts.Update.Discussion;

            _zeroButtonClick = delegate { Process.Start(PatchNotesUrl); };
            _firstButtonClick = delegate { Process.Start(UpdateNotesUrl); };
            _secondButtonClick = delegate { Process.Start(DiscussionUrl); };

            int fileSize = int.Parse(data.size);
            fileSize = fileSize * 4 / 3;

            UpdateDownloadVersion.Text = string.Format(Texts.Update.DownloadingUpdate, data.versionName);

            Soap.Download((string)data.updateFileName, fileSize, (code, downloadData) =>
            {
                switch (code)
                {
                    case 0:
                        InstallUpdate(downloadData, data.versionName, true);
                        break;
                    case 1:
                        ShowProgressBar(false);
                        ShowGenericWindow();

                        GenericTextHeader.Text = downloadData.textHeader;
                        GenericText1.Text = downloadData.text1;
                        GenericText2.Text = downloadData.text2;
                        GenericText3.Text = downloadData.text3;

                        ZeroButton.Content = Texts.Global.ForumSupport;
                        FirstButton.Content = Texts.Global.InBrowser;
                        SecondButton.Content = Texts.Global.TryAgain;

                        _zeroButtonClick = delegate { Process.Start(SupportUrl); };
                        _firstButtonClick = delegate { Process.Start(DownloadUrl); };
                        _secondButtonClick = delegate { DownloadUpdate(data); };
                        break;
                }
            }, (percent, speed, transferred, remaining) =>
            {
                UpdateProgressBar(percent);

                UpdateDownloadSpeed.Text = speed;
                UpdateDownloadTransferred.Text = transferred;
                UpdateDownloadRemaining.Text = remaining;
            });
        }

        private void InstallUpdate(string fileName, string version, bool checkForGameRunning)
        {
            if (checkForGameRunning)
            {
                bool gameRunning = Utilities.Process.Running("age3f", delegate { InstallUpdate(fileName, version, false); });

                if (gameRunning)
                {
                    ShowProgressBar(false);
                    ShowGenericWindow();

                    GenericTextHeader.Text = Texts.Update.GameRunning;
                    GenericText1.Text = Texts.Update.NeedCloseFirstInstall;
                    GenericText2.Text = Texts.Update.WeCloseOrYourself;

                    FirstButton.Content = Texts.Update.IClosed;
                    SecondButton.Content = Texts.Update.CloseIt;

                    FirstButton.IsEnabled = false;

                    ZeroButton.Visibility = Visibility.Collapsed;

                    _secondButtonClick = delegate
                    {
                        try
                        {
                            Process.GetProcessesByName("age3f").FirstOrDefault()?.Kill();
                        }
                        catch
                        {
                            SecondButton.IsEnabled = false;

                            MessageBox.Show(
                                Texts.Update.UnknownCantCloseGameNecessary,
                                Texts.Global.LauncherTitle, MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    };
                }
            }

            CloseButton.IsEnabled = false;

            ShowProgressBar(true);
            ShowGenericWindow();

            GenericTextHeader.Text = Texts.Update.InstallingUpdate;
            GenericText1.Text = Texts.Update.PleaseWaitWhileUpdateInstalled;
            GenericText2.Text = Texts.Update.InstallationCompleteRestartLauncher;
            GenericText3.Text = string.Format(Texts.Update.ProgressComplete, 0);

            ZeroButton.Content = Texts.Update.PatchNotes;
            FirstButton.Content = Texts.Update.UpdateNotes;
            SecondButton.Content = Texts.Update.Discussion;

            _zeroButtonClick = delegate { Process.Start(PatchNotesUrl); };
            _firstButtonClick = delegate { Process.Start(UpdateNotesUrl); };
            _secondButtonClick = delegate { Process.Start(DiscussionUrl); };
            
            string[] deleteFiles =
            {
                "0\\DataPF.bar",
                "0\\data\\protof.xml",
                "0\\data\\randommapstringsf.xml",
                "0\\data\\techtreef.xml",
                "0\\ESOCP1.dll",
                "0\\ESOCP2.dll",
                "0\\ESOCP3.dll",
                "0\\patch_update.exe",
                "0\\age3f.exe"
            };

            foreach (string filePath2 in deleteFiles)
            {
                int targetFolder = int.Parse(filePath2.Substring(0, 1));
                string filePath = filePath2.Substring(2);
                filePath = Path.Combine(_gamePaths[targetFolder], filePath);

                Utilities.File.Delete(filePath);
            }

            if (File.Exists("age3re.exe"))
            {
                Utilities.File.Delete("age3y.exe");

                try
                {
                    File.Move("age3re.exe", "age3y.exe");
                }
                catch { }
            }

            try
            {
                Directory.Delete("RMF", true);
            }
            catch { }

            Process process = new Process
            {
                StartInfo =
                {
                    FileName = "UnRar.exe",
                    Arguments = $"x -o+ -r -ri15 \"{fileName}\" \"{_gamePaths[0]}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    Verb = "runas"
                },
                EnableRaisingEvents = true
            };

            process.OutputDataReceived += (sender, args) =>
            {
                if (args.Data == null)
                    return;

                int startPos = args.Data.IndexOf("\b\b\b\b");

                if (startPos == -1)
                    return;

                startPos = startPos + "\b\b\b\b".Length;

                int endPos = args.Data.IndexOf("\b\b\b\b", startPos);

                if (endPos == -1)
                    return;

                string percentStr = args.Data.Substring(startPos, endPos - startPos).Trim();
                percentStr = percentStr.Remove(percentStr.Length - 1);

                double percent;

                if (double.TryParse(percentStr, out percent))
                {
                    Dispatcher.BeginInvoke((Action)delegate
                    {
                        UpdateProgressBar(percent);

                        GenericText3.Text = string.Format(Texts.Update.ProgressComplete, percent);
                    });
                }
            };

            process.Exited += delegate
            {
                Dispatcher.BeginInvoke((Action)delegate
                {
                    Utilities.File.Delete(fileName);

                    if (File.Exists("beta.version"))
                    {
                        File.WriteAllText("beta.version", version);
                    }
                    else
                    {
                        Utilities.Patch.SetVersion(version);
                    }

                    string exePath = Path.Combine(_gamePaths[0], "patch_update.exe");

                    Utilities.Process.Start(exePath);

                    Close();
                });
            };

            try
            {
                if (!File.Exists("UnRar.exe"))
                {
                    Stream stream = GetType().Assembly.GetManifestResourceStream("Patch_Launcher.UnRar.exe");

                    byte[] bytes = new byte[stream.Length];
                    stream.Read(bytes, 0, bytes.Length);

                    File.WriteAllBytes("UnRar.exe", bytes);
                }

                process.Start();
                process.BeginOutputReadLine();
            }
            catch
            {
                ShowGenericWindow();

                GenericTextHeader.Text = Texts.Global.UnknownErrorOccurred;
                GenericText1.Text = Texts.Global.TryAgainKeepErrorDownloadInstall;
                GenericText2.Text = Texts.Global.SorryForThisInconvenience;

                ZeroButton.Content = Texts.Global.ForumSupport;
                FirstButton.Content = Texts.Global.Download;
                SecondButton.Content = Texts.Global.TryAgain;

                _zeroButtonClick = delegate { Process.Start(SupportUrl); };
                _firstButtonClick = delegate { Process.Start(DownloadUrl); };
                _secondButtonClick = delegate { InstallUpdate(fileName, version, true); };
            }
        }
    }
}