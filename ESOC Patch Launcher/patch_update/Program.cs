﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace patch_update
{
    internal class Program
    {
        private static string _launcherPath;
        private static string _newLauncherPath;

        private static void Main(string[] args)
        {
            _launcherPath = "ESOCPatchLauncher.exe";
            _newLauncherPath = "NEW_ESOCPatchLauncher.exe";

            Process process = Process.GetProcessesByName("ESOCPatchLauncher").FirstOrDefault();

            if (process != null)
            {
                try
                {
                    process.Kill();
                    process.WaitForExit();
                }
                catch {}
            }

            ReplaceLauncher();
        }

        private static void ReplaceLauncher()
        {
            try
            {
                while (File.Exists(_launcherPath))
                {
                    try
                    {
                        File.Delete(_launcherPath);
                    }
                    catch
                    {
                        Thread.Sleep(100);
                    }
                }

                File.Move(_newLauncherPath, _launcherPath);

                while (File.Exists(_newLauncherPath))
                    Thread.Sleep(100);

                Process process = new Process
                {
                    StartInfo =
                    {
                        UseShellExecute = false,
                        FileName = _launcherPath,
                        Verb = "runas"
                    }
                };

                try
                {
                    process.Start();
                }
                catch {}
            }
            catch {}
        }
    }
}
