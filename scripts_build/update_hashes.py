import re, hashlib

def file_hash(path):
    blocksize = 65536
    hasher = hashlib.md5()
    try:
        with open(path, 'rb') as f:
            buf = f.read(blocksize)
            while len(buf) > 0:
                hasher.update(buf)
                buf = f.read(blocksize)
    except FileNotFoundError:
        return ""

    h = hasher.hexdigest()
    return h

def main():
	with open('..\ESOCP1\ESOCP1.cpp', 'r') as f:
		code = f.read()

	with open('..\ESOCP1\ESOCP1.cpp.bak', 'w') as f:
		f.write(code)

	esocp2 = "\"ESOCP2.dll\", \"{}\""
	esocp3 = "\"ESOCP3.dll\", \"{}\""
	code = re.sub(esocp2.format("[\w|-]*"), esocp2.format(file_hash('..\\Release\\ESOCP2.dll')), code)
	code = re.sub(esocp3.format("[\w|-]*"), esocp3.format(file_hash('..\\Release\\ESOCP3.dll')), code)

	with open('..\ESOCP1\ESOCP1.cpp', 'w') as f:
		f.write(code)

try:
	main()
except:
	import traceback
	traceback.print_exc()
	input('')