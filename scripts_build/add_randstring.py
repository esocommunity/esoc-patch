import re, string, random

def main():
	randstr = ''.join([random.choice(string.ascii_letters + string.digits) for i in range(0, 3000)])

	with open('ESOC Patch Launcher\ESOCP1\ESOCP1.cpp', 'r') as f:
		code = f.read()

	with open('ESOC Patch Launcher\ESOCP1\ESOCP1.cpp.bak', 'w') as f:
		f.write(code)

	regex = "randstr = \"{}\""
	match = re.search(regex.format("(.*?)"), code)
	if match:
		randstr = match.group(1) + randstr

	code = re.sub(regex.format('(.*?)'), regex.format(randstr), code)

	with open('ESOC Patch Launcher\ESOCP1\ESOCP1.cpp', 'w') as f:
		f.write(code)

try:
	main()
except:
	import traceback
	traceback.print_exc()
	input('')