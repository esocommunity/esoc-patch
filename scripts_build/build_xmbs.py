import os, sys
from pywinauto import application

file_converter_path = 'scripts_build/tools/FileConverter.exe'

def main():
	for (path, dirs, files) in os.walk('build\DataPF'):
		for f in files:
			if f.rsplit('.', 1)[-1].lower() in ('xml', 'particle', 'tactics'):
				appPath = '"{}" "{}"'.format(file_converter_path, os.path.abspath(os.path.join(path, f)))
				
				proc = application.Application().start(appPath)
				proc.Finished.OK.Click()

				os.remove(os.path.join(path, f))

try:
	main()
except:
	import traceback
	traceback.print_exc()
	input('')
