from pywinauto import application
import os, sys, time, win32ui

bar_editor_path = 'scripts_build/tools/BAREditor.exe'

def ToKeys(string):
    string = string.replace(' ', '{SPACE}')
    string = string.replace('(', '{(}')
    string = string.replace(')', '{)}')
    return string

def OpenArchive(app, filepath):
    app.BAREditor.MenuSelect('Datei->Offnen')

    app['Archiv offnen']['ComboBoxEx32'].TypeKeys(ToKeys(filepath))
    app['Archivoffnen']['Open'].Click()

def SaveArchive(app, destination):
    app.BAREditor.MenuSelect('Datei->Speichern')

    # The filedialog will accept a full absolute path in the filename entrybox
    app['Archiv speichern']['ComboBox1'].TypeKeys(ToKeys(os.path.abspath(destination)))
    app['Archiv speichern'].Button.Click()

def OpenFolder(app):
	app.BAREditor.MenuSelect('Erstellen->Basispfad')
	
	input("Press enter after you select the '__DataPF' folder in your Desktop.")

def ExtractArchive(source, destination):
    OpenArchive(source)

    # Ensure destination folder exists
    if not os.path.exists(destination):
        os.makedirs(destination)

    app['BAREditor'].SetFocus()
    app['BAREditor'].MenuSelect('Extrahieren->Alle Dateien')

    print(app['Browse for Folder'].UserData())

    input('')

    for c in app['Browse For Folder'].Children():

        print(c.GetProperties())

        img = c.CaptureAsImage()
        try:
            img.show()
        except AttributeError:
            print('No image')

        input('')

def main():
    app = application.Application().start(bar_editor_path)
	
    OpenFolder(app)
    SaveArchive(app, sys.argv[1])
    app['BAREditor'].Close()

try:
    main()
except:
    import traceback
    print("Automated BAREditor has encountered and error:")
    traceback.print_exc()
    input('Enter to close.')