import re, sys

replacements = ((r'build\RMF\mercenaries.xs', '"ESOC Patch {}"', "ascii",),
				(r'build\data\randommapstringsf.xml', 'ESOC Patch, version {}', "utf-16",),
				(r'ESOCP Installer.iss', 'AppVersion={}', "ascii",),
				(r'ESOC Patch Launcher\Patch Launcher\Properties\AssemblyInfo.cs', '"{}"', "utf-8"),
				(r'ESOC Patch Launcher\ESOCP3\Properties\AssemblyInfo.cs', '"{}"', "utf-8"),
				)

def main():
	version = sys.argv[1]

	# Replace the version number in all appropriate locations
	for fname, regex, enc in replacements:

		with open(fname, 'r', encoding=enc) as f:
			code = f.read()
		
		with open(fname + '.bak', 'w',  encoding=enc) as f:
			f.write(code)
		
		code = re.sub(regex.format("[\d]+.[\d]+.[\d]+.[\d]+"), regex.format(version), code)

		with open(fname, 'w', encoding=enc) as f:
			f.write(code)

try:
	main()
except:
	import traceback
	traceback.print_exc()
	input('')