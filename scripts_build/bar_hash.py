import re, hashlib

def file_hash(path):
    blocksize = 65536
    hasher = hashlib.md5()
    try:
        with open(path, 'rb') as f:
            buf = f.read(blocksize)
            while len(buf) > 0:
                hasher.update(buf)
                buf = f.read(blocksize)
    except FileNotFoundError:
        return ""

    h = hasher.hexdigest().upper()
    h = '-'.join([h[i:i+2] for i in range(0, len(h), 2)])
    return h

def main():
	with open('ESOC Patch Launcher\ESOCP3\AntiCheat.Files.cs', 'r') as f:
		code = f.read()

	with open('ESOC Patch Launcher\ESOCP3\AntiCheat.Files.cs.bak', 'w') as f:
		f.write(code)

	code = re.sub('DataPF.bar\", \"[\w|-]*\"', 'DataPF.bar", "'+file_hash('build\DataPF.bar')+'"', code)

	with open('ESOC Patch Launcher\ESOCP3\AntiCheat.Files.cs', 'w') as f:
		f.write(code)

try:
	main()
except:
	import traceback
	traceback.print_exc()
	input('')