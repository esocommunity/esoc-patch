﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace ASfilecrc
{
    class Program
    {
        static void Main(string[] args)
        {
            var m_Files = new Dictionary<string, uint>();

            if (args.Length != 2)
            {
                Console.WriteLine("Usage:\nASfilecrc.exe <folder path> <output file>\nEx ASfilecrc .\\RM3 resources.xml");
                return;
            }

            try
            {
                CreateXmlOutputfile(args, m_Files);

                foreach (var a in args)
                {
                    Console.Write("{0} ", a);
                }
                Console.WriteLine("");
                Console.WriteLine("done, hit any key to continue...");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error during execution, se error message: {0}",e);
            }
        }

        private static void CreateXmlOutputfile(string[] args, Dictionary<string, uint> m_Files)
        {
            Dictionary<string, string> EntryType = new Dictionary<string, string>();
            EntryType.Add("SET", "set");
            EntryType.Add("XS", "map");
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(args[0]);
            foreach (var file in dir.GetFiles("*.set"))
            {

                System.IO.FileStream stream = (file.OpenRead());
                byte[] byteArray = new byte[(int)stream.Length];
                stream.Read(byteArray, 0, (int)stream.Length);
                Crc32 crc32 = new Crc32();
                crc32.Polynomial = 0xEDB88320;
                crc32.Init();
                m_Files.Add(file.Name, crc32.GetHashcode(byteArray));
                stream.Close();
            }

            foreach (var file in dir.GetFiles("*.xs"))
            {
                System.IO.FileInfo xmlfile = new System.IO.FileInfo(file.FullName.Replace(file.Extension, ".xml"));
                if (xmlfile.Exists)
                {
                    System.IO.FileStream stream = (file.OpenRead());
                    byte[] byteArray = new byte[(int)stream.Length];
                    stream.Read(byteArray, 0, (int)stream.Length);
                    Crc32 crc32 = new Crc32();
                    crc32.Polynomial = 0xEDB88320;
                    crc32.Init();
                    uint xscrc = crc32.GetHashcode(byteArray);
                    stream.Close();

                    stream = (xmlfile.OpenRead());
                    byteArray = new byte[(int)stream.Length];
                    stream.Read(byteArray, 0, (int)stream.Length);
                    crc32 = new Crc32();
                    crc32.Polynomial = 0xEDB88320;
                    crc32.Init();
                    uint xmlcrc = crc32.GetHashcode(byteArray);
                    stream.Close();

                    xscrc ^= xmlcrc;

                    //file.Name.Split('.')[0]
                    m_Files.Add(file.Name, xscrc);
                }

            }

            XDocument doc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("rmfilecrc",
                    from f in m_Files
                    select new XElement(EntryType[f.Key.Split('.')[1].ToUpper()],
                              new XAttribute("name", f.Key.Split('.')[0]), f.Value)
                    ));

            doc.Save(args[1]);
        }
    }
    internal class Crc32
    {
        uint[] m_table;
        public uint Polynomial { get; set; }

        public uint GetHashcode(byte[] bytes)
        {
            uint crc = 0xffffffff;
            for (int i = 0; i < bytes.Length; i++)
            {
                byte loc = (byte)(((crc) & 0xff) ^ bytes[i]);
                crc = ((crc >> 8) ^ m_table[loc]);
            }
            return ~crc;
        }

        public Crc32()
        {
            Polynomial = 0xedb88320;
            m_table = new uint[256];
            Init();
        }
        public void Init()
        {
            uint temp = 0;
            for (uint i = 0; i < m_table.Length; i++)
            {
                temp = i;
                for (int j = 8; j > 0; j--)
                {
                    if ((temp & 1) == 1)
                    {
                        temp = (uint)((temp >> 1) ^ Polynomial);
                    }
                    else
                    {
                        temp >>= 1;
                    }
                }
                m_table[i] = temp;
            }
        }
    }
}

